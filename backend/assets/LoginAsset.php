<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //<!-- Google Fonts -->
        'https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        //<!-- Bootstrap Core Css -->
        'themes/gentelella-master/login/css/bootstrap.css',

        //<!-- Waves Effect Css -->
        'themes/gentelella-master/login/css/waves.css',

        //<!-- Animation Css -->
        'themes/gentelella-master/login/css/animate.css',
        'themes/gentelella-master/login/css/materialize.css',

        //<!-- Custom Css -->
        'themes/gentelella-master/login/css/style.css',
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        'themes/gentelella-master/login/js/jquery.min.js',
        'themes/gentelella-master/login/js/bootstrap.js',
        'themes/gentelella-master/login/js/waves.js',
        'themes/gentelella-master/login/js/jquery.validate.js',
        'themes/gentelella-master/login/js/admin.js',
        'themes/gentelella-master/login/js/sign-in.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
