<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashbordAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/gentelella-master/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css',
        //'//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        //'//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'themes/gentelella-master/css/animate.min.css',
        'themes/gentelella-master/css/custom.css',
        'themes/gentelella-master/css/maps/jquery-jvectormap-2.0.1.css',
        'themes/gentelella-master/css/icheck/flat/green.css',
        'themes/gentelella-master/css/floatexamples.css',

        'themes/gentelella-master/form_wizerd/css/gsdk-bootstrap-wizard.css'
    ];
    public $js = [
        //'themes/adminlte/bootstrap/js/bootstrap.min.js',        
       // '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        //'//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js',        
        
        'themes/gentelella-master/js/bootstrap.min.js',
        //<!-- chart js -->
        'themes/gentelella-master/js/chartjs/chart.min.js',
        
        //bootstrap progress js
        'themes/gentelella-master/js/progressbar/bootstrap-progressbar.min.js',
        'themes/gentelella-master/js/nicescroll/jquery.nicescroll.min.js',
        //icheck
        'themes/gentelella-master/js/icheck/icheck.min.js',        
        'themes/gentelella-master/js/custom.js',
        
        //moris js
        'themes/gentelella-master/js/moris/raphael-min.js',
        'themes/gentelella-master/js/moris/morris.js',
        'themes/gentelella-master/js/mycustom.js',
        //'themes/gentelella-master/js/moris/example.js',
        
        
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}