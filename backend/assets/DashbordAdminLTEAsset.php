<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashbordAdminLTEAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css',
        //'//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        
        'themes/adminLTE/bootstrap/css/bootstrap.min.css',
        //'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        '//s3-us-west-2.amazonaws.com/s.cdpn.io/102306/jquery.nestable.css',
        'themes/adminLTE/dist/css/skins/_all-skins.min.css',
        'themes/adminLTE/custom/css/jquery.fancybox.css',        
        'themes/adminLTE/custom/css/custom.css',
        'themes/adminLTE/custom/css/custom_tab.css',
        'themes/adminLTE/custom/css/smart_wizard.css',
        //'//icodefy.com/Tools/iZoom/js/Vendor/fancybox/jquery.fancybox.css',
        //'//icodefy.com/Tools/iZoom/js/Vendor/fancybox/helpers/jquery.fancybox-thumbs.css',
        //'themes/adminLTE/custom/css/zoom.css',
        'themes/adminLTE/custom/css/photoviewer.css',
        
        
        
    ];
    public $js = [
        //'//code.jquery.com/jquery-1.11.0.js',
        //'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        //'//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        //'//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js',
        
        'themes/adminLTE/bootstrap/js/bootstrap.min.js',
        '//s3-us-west-2.amazonaws.com/s.cdpn.io/102306/jquery.nestable2.js',
        'themes/adminLTE/custom/js/nestable2.js',        
        'themes/adminLTE/plugins/fastclick/fastclick.js',
        'themes/adminLTE/dist/js/app.min.js',
        
        //'themes/adminLTE/plugins/sparkline/jquery.sparkline.min.js',
        //'themes/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js',
        'themes/adminLTE/plugins/chartjs/Chart.min.js',

        '//cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js',
        'themes/adminLTE/custom/js/jquery.fancybox.js',   
        'themes/adminLTE/custom/js/custom2.js',
             
        'themes/adminLTE/custom/js/jquery.smartWizard.min.js',
        
        /*'themes/adminLTE/dist/js/pages/dashboard2.js',*/
        'themes/adminLTE/dist/js/demo.js',
        'themes/adminLTE/custom/js/custom.js',
        'themes/adminLTE/custom/js/photoviewer_2.0.js'        
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}