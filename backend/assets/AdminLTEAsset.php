<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLTEAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        
        'themes/adminLTE/bootstrap/css/bootstrap.min.css',
        'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        'themes/adminLTE/dist/css/skins/_all-skins.min.css',
        'themes/adminLTE/custom/css/custom.css',
        'themes/adminLTE/custom/css/custom_tab.css',
        'themes/adminLTE/custom/css/smart_wizard.css'
        
    ];
    public $js = [
        /*'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        'themes/adminLTE/dist/js/app.min.js',
        'themes/adminLTE/custom/js/custom_right_menu.js',*/
        
        'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        'themes/adminLTE/custom/js/customnoConflict.js',
        //'themes/adminLTE/dist/js/app.min.js',        
        //'themes/adminLTE/custom/js/custom_right_menu.js',
        

    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}