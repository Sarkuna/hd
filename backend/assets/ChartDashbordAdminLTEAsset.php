<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ChartDashbordAdminLTEAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css',
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        
        'themes/adminLTE/bootstrap/css/bootstrap.min.css',
        'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        'themes/adminLTE/dist/css/skins/_all-skins.min.css',
        'themes/adminLTE/custom/css/custom.css',
        'themes/adminLTE/custom/css/smart_wizard.css',
        '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css'
        
    ];
    public $js = [
        //'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        'themes/adminLTE/bootstrap/js/bootstrap.min.js',
        'themes/adminLTE/dist/js/app.min.js',
        //'//code.jquery.com/jquery-1.9.1.js',
        //'//code.jquery.com/ui/1.10.1/jquery-ui.js',
        //'//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js',
        
        
        //'themes/adminLTE/plugins/sparkline/jquery.sparkline.min.js',
        //'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        //'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        //'themes/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js',
        //'themes/adminLTE/plugins/chartjs/Chart.min.js',
        //'themes/adminLTE/plugins/fastclick/fastclick.js',
        
        
        //'themes/gentelella-master/js/wizard/jquery.smartWizard.js',
        //'//cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js',
        
        //'themes/adminLTE/custom/js/jquery.smartWizard.min.js',
        
        
        
        //'themes/adminLTE/custom/js/custom_right_menu.js',
        /*'themes/adminLTE/plugins/flot/jquery.flot.min.js',
        'themes/adminLTE/plugins/flot/jquery.flot.resize.min.js',
        'themes/adminLTE/plugins/jquery.flot.pie.min.js',
        'themes/adminLTE/plugins/flot/jquery.flot.categories.min.js',
        
        //'themes/adminLTE/custom/js/custom2.js',*/
        //'themes/adminLTE/custom/js/customchat.js', 
        '//cdn.jsdelivr.net/momentjs/latest/moment.min.js',
        '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js',
        //'themes/adminLTE/dist/js/pages/dashboard.js',
        //'themes/adminLTE/dist/js/demo.js',
        '//code.highcharts.com/highcharts.js'
    ];
    public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}