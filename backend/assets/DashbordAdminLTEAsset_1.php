<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashbordAdminLTEAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        
        'themes/adminLTE/bootstrap/css/bootstrap.min.css',
        'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        'themes/adminLTE/dist/css/skins/_all-skins.min.css',
        'themes/adminLTE/custom/css/custom.css',
        'themes/adminLTE/custom/css/smart_wizard.css',
        //'//icodefy.com/Tools/iZoom/js/Vendor/fancybox/jquery.fancybox.css',
        //'//icodefy.com/Tools/iZoom/js/Vendor/fancybox/helpers/jquery.fancybox-thumbs.css',
        //'themes/adminLTE/custom/css/zoom.css',
        //'themes/adminLTE/custom/css/zoomable.css',
        
    ];
    public $js = [
        //'//code.jquery.com/jquery-1.11.0.js',
        'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        
        'themes/adminLTE/bootstrap/js/bootstrap.min.js',
        //'//code.jquery.com/jquery-1.9.1.js',
        //'//code.jquery.com/ui/1.10.1/jquery-ui.js',
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js',
        'themes/adminLTE/plugins/fastclick/fastclick.js',
        'themes/adminLTE/dist/js/app.min.js',
        
        'themes/adminLTE/plugins/sparkline/jquery.sparkline.min.js',
        'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'themes/adminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'themes/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js',
        'themes/adminLTE/plugins/chartjs/Chart.min.js',
        
        //'themes/gentelella-master/js/wizard/jquery.smartWizard.js',
        '//cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js',
        //'themes/adminLTE/custom/js/e-smart-zoom-jquery.min.js',
        'themes/adminLTE/custom/js/custom2.js',
        'themes/adminLTE/custom/js/jquery.smartWizard.min.js',
        
        /*'themes/adminLTE/dist/js/pages/dashboard2.js',*/
        'themes/adminLTE/dist/js/demo.js',
        'themes/adminLTE/custom/js/custom.js',
        
        /*'//icodefy.com/Tools/iZoom/js/Vendor/jquery/jquery-ui.min.js',
        '//icodefy.com/Tools/iZoom/js/Vendor/fancybox/jquery.fancybox.js',
        '//icodefy.com/Tools/iZoom/js/Vendor/elevatezoom/jquery.elevatezoom.js',
        '//icodefy.com/Tools/iZoom/js/Vendor/panZoom/panZoom.js',
        '//icodefy.com/Tools/iZoom/js/Vendor/ui-carousel/ui-carousel.js',
        'themes/adminLTE/custom/js/zoom.js'   */     

    ];
    public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}