<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class KrajeeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
        '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        
        'themes/adminLTE/bootstrap/css/bootstrap.min.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        
        'themes/adminLTE/dist/css/skins/_all-skins.min.css',
        'themes/adminLTE/custom/css/custom.css',
        'themes/adminLTE/custom/css/custom_tab.css',
        
        
    ];
    public $js = [
        //'//code.jquery.com/jquery-1.11.0.js',
        //'themes/adminLTE/plugins/jQuery/jquery-2.2.3.min.js',
        'themes/adminLTE/bootstrap/js/bootstrap.min.js',
        'themes/adminLTE/dist/js/app.min.js', 
        //'themes/adminLTE/dist/js/demo.js',
        

    ];
    public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}