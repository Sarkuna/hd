<?php
use backend\assets\DashbordAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\TopMenuWidget;
use app\components\LeftMenuWidget;
//use app\components\AlertWidget;
//use frontend\widgets\Alert;
//use common\widgets\Alert;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;

DashbordAsset::register($this);
//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="nav-md">
<?php $this->beginBody() ?>
    <div class="container body">
        <div class="main_container">

            <div class="col-md-3 left_col">
                <?= LeftMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                
            </div>

            <!-- top navigation -->
                <?= TopMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="hidden-print">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                </div>
                <?= Alert::widget() ?>
                <div class="row">
                    <?= $content ?>
                </div>
                

                <!-- footer content -->
                <!--<div class="clearfix"></div>
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>
    </div>
    
    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
