<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <div class="login-box">
        <div class="login-logo hidden">
            <a href="/site/login"><img src="/images/icilogo.png" width="200"></a>
        </div><!-- /.login-logo -->

        <div class="login-box-body">
            <p class="login-box-msg">Please fill out your email. A link to reset password will be sent there.</p>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>            
            <?= $form->field($model, 'email', ['options' => [
                    'tag' => 'div',
                    'class' => 'form-group field-loginform-username has-feedback required',
                ],
                'template' => '{input}<span class="glyphicon glyphicon-email form-control-feedback"></span>{error}{hint}'
            ]);
            ?>
            
            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>    
                
            
            
            <?php ActiveForm::end(); ?>
            <a href="/index.php/" class="text-center">Login</a><br>            
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">How to Apply?</h4>
      </div>
      <div class="modal-body">
          <ol>
              <li type="1">  First time login:  As you have not registered before, click on 'Register' to register yourself.</li>
              <li> Once you have registered, you will receive a notification via email. You may log in with your IC No and your preferred password, given in the registration form.</li>
              <li>Complete and submit the application form.</li>
              <li>Print a copy of the Term &amp; Conditions and Application Form and bring it along to MIED together with supporting documents as per listed in page 3.</li>
          </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

