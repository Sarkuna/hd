<?php
use backend\assets\DashbordAdminLTEAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\LTE_Top_Widget;
use app\components\LTE_LeftMenuWidget;
use app\components\LTE_RightMenuWidget;
use app\components\LeftMenuWidget;
//use app\components\AlertWidget;
//use frontend\widgets\Alert;
//use common\widgets\Alert;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;
$session = Yii::$app->session;
DashbordAdminLTEAsset::register($this);
$session['currentclientID'] = 16;
//AppAsset::register($this);
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
    <div class="wrapper">

  <?= LTE_Top_Widget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        
  <!-- Left side column. contains the logo and sidebar -->
  <?= LTE_LeftMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?=
      Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ])
      ?>
    </section>
    <?= Alert::widget() ?>
    <!-- Main content -->
    <section class="content">

            <?= $content ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
      <strong>Copyright &copy; 2017-<?= date('Y')?> Powered By <a href="https://businessboosters.com.my/" target="_blank">Business Boosters</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <?= LTE_RightMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
