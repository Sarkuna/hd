<?php
use backend\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
$session = Yii::$app->session;
$baseURL = Yii::$app->request->url;
$session = Yii::$app->session;
$session['currentclientID'] = 16;
//DashbordAdminLTEAsset::register($this);
//AppAsset::register($this);
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="login-page">
    <?php $this->beginBody() ?>
    <div class="login-box">
        <div class="logo">
            <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="/upload/client_logos/'.$session['currentLogo'].'" class="img-responsive avatar" style="margin-left: auto;margin-right: auto;">';                
            }else{
                echo '<img src="/images/logo.png" class="img-responsive avatar" style="margin-left: auto;margin-right: auto;">';
            }
            ?>

        </div>
        <div class="card">
            <div class="body">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
