<?php
use backend\assets\KrajeeAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\LTE_Top_Widget;
use app\components\LTE_LeftMenuWidget;
use app\components\LTE_RightMenuWidget;
use app\components\LeftMenuWidget;
//use app\components\AlertWidget;
//use frontend\widgets\Alert;
//use common\widgets\Alert;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;
$session = Yii::$app->session;
KrajeeAsset::register($this);
//AppAsset::register($this);
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <script>
        $.noConflict();
         $.AdminLTE.tree = function (menu) {
    var _this = this;
    var animationSpeed = $.AdminLTE.options.animationSpeed;
    $(document).off('click', menu + ' li a')
      .on('click', menu + ' li a', function (e) {
        //Get the clicked link and the next element
        var $this = $(this);
        var checkElement = $this.next();

        //If the menu is not visible open it
        if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
          //Get the parent menu
          var parent = $this.parents('ul').first();
          //Get the parent li
          var parent_li = $this.parent("li");

          //Open the target menu and add the menu-open class
          checkElement.slideDown(animationSpeed, function () {
            //Add the class active to the parent li
            checkElement.addClass('menu-open');
            parent_li.addClass('active');
            //Fix the layout in case the sidebar stretches over the height of the window
            _this.layout.fix();
          });
        }
        //if this isn't a link, prevent the page from being redirected
        if (checkElement.is('.treeview-menu')) {
          e.preventDefault();
        }
      });
  };
        </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
    <div class="wrapper">

  <?= LTE_Top_Widget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        
  <!-- Left side column. contains the logo and sidebar -->
  <?= LTE_LeftMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?=
      Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ])
      ?>
    </section>
    <?= Alert::widget() ?>
    <!-- Main content -->
    <section class="content">

            <?= $content ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
      <strong>Copyright &copy; 2017-<?= date('Y')?> Powered By <a href="https://businessboosters.com.my/" target="_blank">Business Boosters</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <?= LTE_RightMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<?php $this->endBody() ?>
<script src="/themes/adminLTE/dist/js/app.min.js"></script>    
</body>
</html>
<?php $this->endPage() ?>
