<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = 'Import Excel Redemption Summery';
$this->params['breadcrumbs'][] = ['label' => 'Import Excel File', 'url' => ['/management/redemption/import-excel-redemption']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-8">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        <div class="dealer-list-create">
            <ol class="list-group">
                <?php echo $msg ?>
            </ol>
        </div>
        </div>
    </div>
</div>

