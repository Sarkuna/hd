<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\LatestOrdersWidget;
use app\components\ReceiptsListWidget;
use app\components\RecentlyAddedProductsWidget;
use app\components\TopProductsWidget;
use common\models\VIPCustomer;
use app\components\IndirectWidget;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
/*'themes/adminLTE/dist/js/pages/dashboard2.js',*/
$session = Yii::$app->session;
$clientID = 16;
$total_members = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['D']])
        ->count();
$pending = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['D'], 'email_verification' => 'Y','status' => 'P'])
        ->count();
$approve = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['D'], 'status' => 'A'])
        ->count();
$deactive = \common\models\User::find()
        ->where(['client_id' => $clientID, 'user_type' =>['D'], 'email_verification' => 'N', 'status' => 'P'])
        ->count();
?>
 

      <!-- Info boxes -->
      <div class="row">
      <?php
        /*$members = \common\models\TypeName::find()
                ->where(['clientID' => $clientID])
                ->all();
        $memberscount = 0;
        foreach($members as $member) {
            $memberscount = \common\models\User::find()->where(['client_id' => $clientID, 'type' => $member->tier_id])->count();
            echo '<div class="col-md-3 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-yellow">
                  <div class="inner">
                      <h3>'.$memberscount.'</h3>
                      <p>'.$member->name.'</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/sales/customers/distributors" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>';
        }*/
      ?>
      </div>
      <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-blue" style="padding-bottom: 25px;">
                  <div class="inner">
                      <h3><?= $total_members ?></h3>
                      <p>Total Users</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
              </div>
          </div>

          
          
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="small-box bg-yellow">
                  <div class="inner">
                      <h3><?= $pending ?></h3>
                      <p>Pending</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/sales/customers" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="small-box bg-green">
                  <div class="inner">
                      <h3><?= $approve ?></h3>

                      <p>Approve</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/sales/customers/approve" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-red">
                  <div class="inner">
                      <h3><?= $deactive ?></h3>
                      <p>Not Verified Email</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-email"></i>
                  </div>
                  <a href="/sales/customers/email-not-verified" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
      </div>
      <!-- /.row -->


      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
          <div class="col-md-7">
              
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Registration</h3>
                  <div class="pull-right box-tools">
                      <button type="button" class="reportrange btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Date range">
                          <i class="fa fa-calendar"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div id="registration"></div>
                </div>
                <!-- /.box-body -->
              </div>
          <!-- /.box -->
          <!--
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Points</h3>
              <div class="pull-right box-tools">
                  <button type="button" class="reportrange btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Date range">
                      <i class="fa fa-calendar"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="pointredeem" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
            <!-- /.box-body 
          </div>-->
          <!-- AREA CHART -->
          <!--
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Engagement</h3>
              <div class="pull-right box-tools">
                  <button type="button" class="reportrange btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Date range">
                      <i class="fa fa-calendar"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="employment"></div>
            </div>
            <!-- /.box-body
          </div> -->
          <!-- /.box -->
          
          <?= TopProductsWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
  
          
          
        </div>
          
          <div class="col-lg-5">
              <?= IndirectWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
              <?= LatestOrdersWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
              <?= RecentlyAddedProductsWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
              
              <!-- DONUT CHART -->
              <!--
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Browser market shares in January, 2018</h3>
            </div>
            <div class="box-body">
              <div id="container"></div>
            </div>
            <!-- /.box-body 
          </div>-->
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
      
 
      <script>
/*  
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chrome',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Internet Explorer',
            y: 11.84
        }, {
            name: 'Firefox',
            y: 10.85
        }, {
            name: 'Edge',
            y: 4.67
        }, {
            name: 'Safari',
            y: 4.18
        }, {
            name: 'Opera',
            y: 1.6
        }, {
            name: 'Other',
            y: 2.61
        }]
    }]
});
*/

Highcharts.chart('registration', {
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Users'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2016
        }
    },

    series: [{
        name: 'Total Users',
        data: [0 , 0, 0, <?= $total_members ?>]
    }, {
        name: 'Pending',
        data: [0 , 0, 0, <?= $pending ?>]
    }, {
        name: 'Approve',
        data: [0 , 0, 0, <?= $approve ?>]
    }, {
        name: 'Deactive',
        data: [0 , 0, 0, <?= $deactive ?>]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
/*
Highcharts.chart('employment', {
    credits: {
        enabled: false
    },
    title: {
        text: false
    },

    yAxis: {
        title: {
            text: 'Number of Users'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2015
        }
    },

    series: [{
        name: 'Desktop',
        data: [100, 135, 175, 200]
    }, {
        name: 'Mobile',
        data: [75, 125, 150, 170]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});*/
/*
Highcharts.chart('pointredeem', {
    credits: {
        enabled: false
    },
    
    chart: {
        type: 'column'
    },
    title: {
        text: false
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Values'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Awarded',
        data: [2000, 5000, 7000, 3000, 8000, 9000, 12000, 15000, 0, 0, 0, 0]

    }, {
        name: 'Redeemed',
        data: [1500, 5000, 7200, 2000, 8500, 9100, 11900, 15000, 0, 0, 0, 0]

    }]
});
*/
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('.reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});

</script>
