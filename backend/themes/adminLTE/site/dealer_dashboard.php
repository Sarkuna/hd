<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\LatestOrdersWidget;
use app\components\ReceiptsListWidget;
use app\components\RecentlyAddedProductsWidget;
use app\components\TopProductsWidget;
use common\models\VIPCustomer;
use app\components\IndirectWidget;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
/*'themes/adminLTE/dist/js/pages/dashboard2.js',*/
$total_user = \common\models\User::find()
        ->where([ 'user_type' =>'D'])
        ->count();

$pending = \common\models\Projects::find()
        ->where([ 'admin_status' => 'P'])
        ->count();
$approve = \common\models\Projects::find()
        ->where([ 'admin_status' => 'A'])
        ->count();
$deactive = \common\models\Projects::find()
        ->where([ 'admin_status' => 'D'])
        ->count();
?>

      <!--<div class="row">
          <div class="col-lg-12">
              <div class="alert alert-info alert-dismissible">
                  <h4><i class="icon fa fa-info"></i> Info!</h4>
                  Coming soon
              </div>
          </div>
       
      </div>-->

      <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-blue" >
                  <div class="inner">
                      <h3><?= $total_user ?></h3>
                      <p>Total User</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/clients/manage-my-ids" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="small-box bg-yellow">
                  <div class="inner">
                      <h3><?= $pending ?></h3>
                      <p>Pending Project</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/clients/projects/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="small-box bg-green">
                  <div class="inner">
                      <h3><?= $approve ?></h3>

                      <p>Approve Project</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-person-add"></i>
                  </div>
                  <a href="/clients/projects/approve" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-red">
                  <div class="inner">
                      <h3><?= $deactive ?></h3>
                      <p>Decline Project</p>
                  </div>
                  <div class="icon">
                      <i class="ion ion-email"></i>
                  </div>
                  <a href="/clients/projects/decline" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
      </div>
