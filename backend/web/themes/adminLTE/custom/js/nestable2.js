var updateOutput = function(e) {
  var list = e.length ? e : $(e.target),
    output = list.data('output');
  if (window.JSON) {
    output.html(window.JSON.stringify(list.nestable('serialize')));
  } else {
    output.html('JSON browser support required for this demo.');
  }
};

// activate nestable
$('#nestable').nestable({
    expandBtnHTML: '<button class="dd-hide" data-action="expand">Expand></button>',
    collapseBtnHTML: '<button class="dd-hide" data-action="collapse">Collapse</button>',
    maxDepth: 1
  })
  .on('change', updateOutput);

// output initial serialised data
updateOutput($('#nestable').data('output', $('#nestable-output')));

$('#appendnestable').click(function() {
  var res = $("input[id^='menu_id_page']:checked").map(function() {
    //return $(this).val();
        var uid = (new Date().getTime()).toString(36);
        $('ol').append('<li class="dd-item dd3-item" data-type="page" data-related-id="' + $(this).data('related-id') + '" data-title="' + $(this).data('title') + '" data-class="" data-id="0" data-custom-url="' + $(this).data('custom-url') + '" data-icon-font="" data-target="_self"><div class="dd-handle dd3-handle"></div><div class="dd3-content">' + $(this).data('title') + '</div><button class="jTrashNestable" type="button"><i class="fa fa-trash"></i></button></li>');
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        $("input[id^='menu_id_page']").removeAttr('checked');
  }).get();
});

$('#appendnestableexternal').click(function() {
  //var uid = (new Date().getTime()).toString(36);
  var vtitle = $("#menus-external_node_title").val();
  var vurl = $("#menus-external_node_url").val();
  var vtarget = $('#menus-external_node_target').val();
  $('ol').append('<li class="dd-item dd3-item" data-type="custom-link" data-related-id="0" data-title="' + vtitle + '" data-class="" data-id="0" data-custom-url="'+vurl+'" data-icon-font="" data-target="'+vtarget+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content">' + vtitle + '</div><button class="jTrashNestable" type="button"><i class="fa fa-trash"></i></button></li>');
        updateOutput($('#nestable').data('output', $('#nestable-output')));
});

$(document).on('click', '.jTrashNestable', function() {
  var li = $(this).closest('.dd-item')
  if (li.has("ol").length) {
    var children = li.find('li');
    li.replaceWith(children);
  } else {
    li.remove();
  }
  updateOutput($('#nestable').data('output', $('#nestable-output')));
});



