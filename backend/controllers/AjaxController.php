<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\VIPZone;
/**
 * Site controller
 */
class AjaxController extends Controller
{
    public function actionStates($id) {
        $states = VIPZone::find()
                ->where(['country_id' => $id])
                ->all();
        if ($states) {
            //echo $sle;
            foreach ($states as $state) {
                echo "<option value='" . $state->zone_id . "'>" . $state->name . "</option>";
            }
        } else {
            echo "<option value='0'> --- None --- </option>";
        }
    }

}
