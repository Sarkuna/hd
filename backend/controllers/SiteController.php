<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use YoHang88\LetterAvatar\LetterAvatar;
use Imagick;
use yii\imagine\Image;

use common\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\SignupForm;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','request-password-reset','reset-password','kis'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'importcustomer', 'lelong', 'copy','importproduct','importcustomer2','change-password','importdealer','info','testimage','testsms'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $session = Yii::$app->session;
        $this->layout = 'chart_layout';
        $user = \common\models\User::find()
        ->where(['id' => \Yii::$app->user->id])
        ->one();
        $session['currentusertype'] = $user->user_type;
        $session['currentclientID'] = $user->client_id;
        if (!Yii::$app->user->isGuest) {

           // if(!empty($session['currentclientID'])){
                if ($session['currentusertype'] == 'B') {
                    return $this->render('dealer_dashboard');
                }else {
                    return $this->render('client_dashboard');
                }
            //}else {
               // return $this->render('client_dashboard'); 
            //}
            /*if ($session['currentusertype'] == 'A') {
                return $this->render('administrator_dashboard');                
            } else if (!empty($session['currentclientID']) && $session['currentusertype'] == 'C') {
                return $this->render('client_dashboard');
            }*/
        } else {
            return $this->redirect(['home']);
        }
        //return $this->redirect(['home']);
    }
    
    public function actionChangePassword()
    {
	$model= new \backend\models\ChangePasswordForm();
	//$model->scenario = 'change';

	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		//$model->attributes = $_POST['User'];
		$user = User::findOne(Yii::$app->user->id);
                $user->setPassword($model->repassword);
                $user->auth_key = Yii::$app->security->generateRandomString();
		if($user->save()){                 
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Change Password', 'text' => 'Your password has been changed successfully']);
                    return $this->goHome();
                }else{
                    print_r($user->getErrors());
                }
	}

	return $this->render('password_changeform',[
		'model'=>$model,
	]);
     }
    
    public function actionCopy()
    {
        $sql = "SELECT * FROM `vip_product` WHERE `product_description` IS NULL OR `product_description` = '' AND `links` LIKE '%superbuy.my%' ";
        $products = \common\models\VIPProduct::findBySql($sql)->all();
        //$products = \common\models\VIPProduct::find()->where("product_id = '497'")->all();
        foreach($products as $product){
            $productID = $product->product_id;
            $url = $product->links;
            
            //$productup = \common\models\VIPProduct::find()->where("product_id = '" . $productID . "'")->one();
            $sql = "SELECT * FROM `vip_product` WHERE `product_id` = $productID AND `links` LIKE '%superbuy.my%' ";
            $productup = \common\models\VIPProduct::findBySql($sql)->one();
            //$productup->product_description = $this->filter_content($url);
            
            
            
            if(count($productup) > 0) {
                $url = $productup->links;
                $productup->product_description = $this->filter_content($url);

                if($productup->save(false)){
                    echo 'Producr Description Updated'.$productID.'<br>';
                }else {
                    print_r($productup->getErrors());
                }
            }
        }
    }
    
    function filter_content($url){
        // FIND ALL OF THE DESIRED DIV
    $htm = file_get_contents($url);
    $str = '<div id="productDescContent">';
    $arr = explode($str, $htm);
    $new = $arr[1];
    $len = strlen($new);

    // ACCUMULATE THE OUTPUT STRING HERE
    $out = NULL;

    // WE ARE INSIDE ONE DIV TAG
    $cnt = 1;

    // UNTIL THE END OF STRING OR UNTIL WE ARE OUT OF ALL DIV TAGS
    while ($len)
    {
        // COPY A CHARACTER
        $chr = substr($new,0,1);

        // IF THE DIV NESTING LEVEL INCREASES OR DECREASES
        if (substr($new,0,4) == '<div')  $cnt++;
        if (substr($new,0,5) == '</div') $cnt--;

        // ACTIVATE THIS TO FOLLOW THE COUNT OF NESTING LEVELS
        // echo " $cnt";

        // WHEN THE NESTING LEVEL GOES BACK TO ZERO
        if (!$cnt) break;

        // WHEN THE NESTING LEVEL IS STILL POSITIVE
        $len--;
        $out .= $chr;
        $new = substr($new,1);
    }

    // RETURN THE WORK PRODUCT
    return $str . $out . '</div>';
    exit();
    }
    
    public function actionLelong()
    {
        $sql = "SELECT * FROM `vip_product` WHERE `product_description` IS NULL OR `product_description` = '' ";
        $products = \common\models\VIPProduct::findBySql($sql)->all();
        //$products = \common\models\VIPProduct::find()->where("product_id = '497'")->all();
        foreach($products as $product){
            $productID = $product->product_id;
            $url = $product->links;
            
            $sql = "SELECT * FROM `vip_product` WHERE `product_id` = $productID AND `links` LIKE '%lelong.com.my%' ";
            $productup = \common\models\VIPProduct::findBySql($sql)->one();

            if(count($productup) > 0) {
                $url = $product->links;
                $productup->product_description = $this->filter_content_lelong($url);

                if($productup->save(false)){
                    echo 'Producr Description Updated'.$productID.'<br>';
                }else {
                    print_r($productup->getErrors());
                }
            }
        }
    }
    
    function filter_content_lelong($url){
        $htm = file_get_contents($url); 
        
        $str = '<div class="tab-pane fade in active" id="desc">';
                $arr = explode($str, $htm);
                $new = $arr[1];
                $len = strlen($new);

                // ACCUMULATE THE OUTPUT STRING HERE
                $out = NULL;

                // WE ARE INSIDE ONE DIV TAG
                $cnt = 1;

                // UNTIL THE END OF STRING OR UNTIL WE ARE OUT OF ALL DIV TAGS
                while ($len)
                {
                    // COPY A CHARACTER
                    $chr = substr($new,0,1);

                    // IF THE DIV NESTING LEVEL INCREASES OR DECREASES
                    if (substr($new,0,4) == '<div')  $cnt++;
                    if (substr($new,0,5) == '</div') $cnt--;

                    // ACTIVATE THIS TO FOLLOW THE COUNT OF NESTING LEVELS
                    // echo " $cnt";

                    // WHEN THE NESTING LEVEL GOES BACK TO ZERO
                    if (!$cnt) break;

                    // WHEN THE NESTING LEVEL IS STILL POSITIVE
                    $len--;
                    $out .= $chr;
                    $new = substr($new,1);
                }

                // RETURN THE WORK PRODUCT
            return $str . $out . '</div>';

    }
    

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        //Yii::app()->user->logout(false);

        return $this->goHome();
    }
    
    

        public function actionRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                //Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'Check your email for further instructions.']);

                return $this->goHome();
            } else {
                //Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Sorry, we are unable to reset password for the provided email address.']);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'New password saved.']);

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionKis()
    {
        //$email = 'yuenpingpaints@yahoo.com';
        $clientID = '10';
        //$newid = '125';
        $totald = \common\models\KpDealerShihan::find()
                                ->where("del = 0")
                                //->limit(10)
                                ->count();
        echo '<h2>'.$totald.'</h2>';
        $checkemails = \common\models\KpDealerShihan::find()
                                ->where("del = 0")
                                ->limit(20)
                                ->all();
        
        $n = 1;
        foreach($checkemails as $checkemail){
            $dealerID = $checkemail->dealer_CustomerCode;
            //$dealer_Email = $checkemail->dealer_Email;
            //echo $dealer_Email;
            $customer = \common\models\VipCustomerShihan::find()->where("lastname = '" . $dealerID . "'")->one();
            if(count($customer) > 0) {
                 $customer_id = $customer->customer_id;

                 $email = $checkemail->dealer_Email;
                 $generatedPassword = $checkemail->dealer_Pass;
                 $name = $checkemail->dealer_Name;
                 $dealer_Tel = $checkemail->dealer_Tel;
                 $dealer_DtCreate = $checkemail->dealer_DtCreate;
                 $dealer_CGrp = $checkemail->dealer_CGrp;
                 $dealer_Rg = $checkemail->dealer_Rg;        
                 $dealer_DtUpdate = $checkemail->dealer_DtUpdate;

                 if($checkemail->dealer_Category == 'Generic'){
                     $dealer_Category = '1';
                 }else if($checkemail->dealer_Category == 'Direct Dealer'){
                     $dealer_Category = '2';
                 }else if($checkemail->dealer_Category == 'Distributor'){
                     $dealer_Category = '3';
                 }else {
                     $dealer_Category = '';
                 }

                 $user = new \common\models\User();
                 $user->client_id = $clientID;
                 $user->username = trim($dealerID);
                 $user->email = $email;
                 $user->status = 'A';
                 $user->approved = 'Y';
                 $user->newsletter = 'E';
                 $user->user_type= 'D';

                 $user->setPassword($generatedPassword);
                 $user->generateAuthKey();
                 $user->save(false);

                 //Create Customer Profile
                 $model = new \common\models\VIPCustomer();
                 $model->userID = $user->id;
                 $model->clientID = $clientID;
                 $model->clients_ref_no = $dealerID;
                 $model->dealer_ref_no = $clientID.'-'.date('dmyHis');
                 $model->full_name = $name;
                 $model->mobile_no = $dealer_Tel;
                 $model->kis_category_id = $dealer_Category;
                 $model->dealer_CGrp = $dealer_CGrp;
                 $model->dealer_Rg = $dealer_Rg;
                 $model->created_datetime = $dealer_DtCreate;
                 $model->updated_datetime = $dealer_DtUpdate;
                 $model->save(false);

                 //Give access to shopping cart module it come from rights
                 $authassignment = new \common\models\AuthAssignment();
                 $authassignment->item_name = 'Dealer';
                 $authassignment->user_id = $user->id;;
                 $authassignment->created_at = strtotime($dealer_DtCreate);
                 $authassignment->save(false);
                 $ckcustomers = \common\models\VipCustomerRewardShihan::find()
                     ->where([
                     'customer_id' => $customer->customer_id,
                 ])->count();
                 if($ckcustomers > 0) {
                     $this->history($customer->customer_id,$user->id);
                 }
                 $model = \common\models\KpDealerShihan::findOne($checkemail->dealer_Id);
                 $model->del = '1';
                 $model->save(false);
            }else {
                echo '<p>'.$n.' Dealer Customer Code Not Maching - '.$dealerID.'<p><br>';
                $n++;
            }
        }
    }
    
    public function actionImportproduct() {
        $model = new \backend\models\ExcelForm();
        $msg = '';        

        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');
            $clientID = '14';
            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //$pre_defined = array( "code", "name", "email", "category", "telephone", "password");
                $pre_defined = array("manufacturer_id","category_id","sub_category_id","product_name","links","meta_tag_title","product_code","suppliers_product_code","variant","msrp","msrp_inc","mark","bb_price","price","points_value","disc","partner_price","ptofit","partner_price_inc","featured_status");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 20){
                    
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $product_code = $rowData[0]['product_code'];
                        
                        $product = \common\models\VIPProduct::find()->where("product_code = '" . $product_code . "'")->one();
                        if(count($product) == 0) {

                            $model = new \common\models\VIPProduct();
                            
                            
                            $model->manufacturer_id = $rowData[0]['manufacturer_id'];
                            $model->category_id = $rowData[0]['category_id'];
                            $model->sub_category_id = $rowData[0]['sub_category_id'];
                            $model->product_name = $rowData[0]['product_name'];
                            $model->product_description = 'test';
                            $model->links = $rowData[0]['links'];
                            //$model->product_description = $rowData[0]['product_description'];
                            $model->meta_tag_title = $rowData[0]['meta_tag_title'];                       
                            $model->main_image = $product_code.'_1.jpg';
                            $model->product_code = $product_code;
                            $model->suppliers_product_code = $rowData[0]['suppliers_product_code'];
                            $model->variant = $rowData[0]['variant'];                            
                            $model->msrp = $rowData[0]['msrp'];                       
                            $model->msrp_inc = $rowData[0]['msrp_inc'];
                            $model->mark = $rowData[0]['mark'];
                            $model->bb_price = $rowData[0]['bb_price'];
                            $model->points_value = $rowData[0]['points_value'];
                            $model->disc = $rowData[0]['disc'];                            
                            $model->partner_price = $rowData[0]['partner_price'];                       
                            $model->ptofit = $rowData[0]['ptofit'];
                            $model->partner_price_inc = $rowData[0]['partner_price_inc'];
                            $model->minimum = 1;
                            $model->featured_status = $rowData[0]['featured_status'];
                            $model->status = 'E';
                            $model->stock_status_id = 7;
                            $model->date_available = date('Y-m-d');
                            $model->price = $rowData[0]['price'];
                            $model->quantity = 1000;

                            if($model->save(false)){
                                echo 'Product Save - '.$product_code.'<br>';                            
                            }else {
                                echo 'Product not Save - '.$product_code.'<br>'; 
                            }
                        }else {
                            echo 'Prodcut Duplicate - '.$product_code.'<br>'; 
                        }
                    }
                }else {
                    echo 'temple not matchimng';
                }
                
                die();
                exit;
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportdealer() {
        $model = new \backend\models\ExcelForm();
        $msg = '';        
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //$pre_defined = array( "code", "name", "email", "category", "telephone", "password");
                $pre_defined = array("email","password","dealer_code","first_name","last_name","company","address1","address2","city","postcode","region","country","tel");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 13){
                    
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $email = $rowData[0]['email'];
                        $password = $rowData[0]['password'];
                        $dealer_code = $rowData[0]['dealer_code'];
                        $first_name = $rowData[0]['first_name'];
                        $last_name = $rowData[0]['last_name'];
                        $company = $rowData[0]['company'];
                        $address1 = $rowData[0]['address1'];
                        $address2 = $rowData[0]['address2'];
                        $city = $rowData[0]['city'];
                        $postcode = $rowData[0]['postcode'];
                        $region = $rowData[0]['region'];
                        $country = $rowData[0]['country'];
                        $tel = $rowData[0]['tel'];
                        
                        $usercount = User::find()->where("email = '" . $email . "'")->count();
                        if($usercount == 0) {
                            $user = new User();
                            $uname = $first_name.' '.$last_name;
                            $random_str = md5(uniqid(rand()));
                            $user->username = substr($random_str, 0, 10);
                            $user->email = $email;
                            //$random_str = md5(uniqid(rand()));
                            //$generatedPassword = substr($random_str, 0, 6);
                            $user->setPassword($password);

                            $user->generateAuthKey();
                            $user->status = 'A';
                            $user->user_type = 'B';
                            $user->client_id = $clientID;
                            $user->ip = $_SERVER['REMOTE_ADDR'];
                            $userAgent = \xj\ua\UserAgent::model();
                            $platform = $userAgent->platform;
                            $browser = $userAgent->browser;
                            $version = $userAgent->version;
                            $user->user_agent = $platform.'-'.$browser.'-'.$version;

                            $transaction = \Yii::$app->db->beginTransaction();
                                try {
                                    if ($flag = $user->save(false)) {
                                        if ($flag) {
                                            $userprofile = new \common\models\UserProfile();
                                            $userprofile->userID = $user->id;
                                            $userprofile->dealer_code = $dealer_code;                            
                                            $userprofile->first_name = $first_name;
                                            $userprofile->last_name = $last_name;
                                            $userprofile->company = $company;
                                            $userprofile->address1 = $address1;
                                            $userprofile->address2 = $address2;
                                            $userprofile->city = $city;
                                            $userprofile->postcode = $postcode;
                                            $userprofile->tel = $tel;
                                            $userprofile->region = $region;
                                            $userprofile->country = $country;
                                            $userprofile->save();

                                            if (($flag = $userprofile->save(false)) === false) {      
                                                $transaction->rollBack();
                                                        //break;
                                            }
                                            $authassignment = new \common\models\AuthAssignment();
                                            $authassignment->item_name = 'Dealers';
                                            $authassignment->user_id = $user->id;;
                                            $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                                            $authassignment->save();

                                            if (($flag = $authassignment->save(false)) === false) {      
                                                $transaction->rollBack();
                                                        //break;
                                            }
                                        }
                                    }

                                if ($flag) {
                                    $transaction->commit();
                                    echo 'User Save - '.$dealer_code.'<br>'; 
                                }
                            } catch (Exception $e) {
                                $transaction->rollBack();
                            }
                        }else {
                            echo 'user Duplicate - '.$email.'<br>'; 
                        }
                    }
                }else {
                    echo 'temple not matchimng';
                }
                
                die();
                exit;
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    
    public function actionImportcustomer() {
        $model = new \backend\models\ExcelForm();
        $msg = '';        
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //$pre_defined = array( "code", "name", "email", "category", "telephone", "password");
                $pre_defined = array( "code", "points");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));
                
                
                if($checkmatch == 6){
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $code = $rowData[0]['code'];
                        $name = $rowData[0]['name'];
                        $email = $rowData[0]['email'];
                        $category = $rowData[0]['category'];
                        $telephone = $rowData[0]['telephone'];
                        $password = $rowData[0]['password'];
                        
                        //$intake_month = date('M-y', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['intake month']));
                        
                        $numRows = \common\models\User::find()
                            ->where([
                                'username' => $code,
                            ])
                            ->count();

                        
                        if($numRows == 0) {
                            if($email == 'reserve@kansai.demo') {
                                $email = $code.'_reserve@kansai.demo';
                            }
                            
                            $user = new \common\models\User();
                            $user->client_id = $clientID;
                            $user->username = trim($code);
                            $user->email = $email;
                            $user->status = 'A';
                            $user->approved = 'Y';
                            $user->newsletter = 'E';
                            $user->user_type= 'D';
                            $user->email_verification = 'Y';
                            $user->type= $category;

                            $user->setPassword($password);
                            $user->generateAuthKey();
                            
                            if($user->save(false)){
                                $model = new \common\models\VIPCustomer();
                                $model->userID = $user->id;
                                $model->clientID = $clientID;
                                $model->clients_ref_no = $code;
                                $model->dealer_ref_no = $clientID.'-'.date('dmyHis');
                                $model->salutation_id = '999';
                                $model->full_name = $name;
                                $model->mobile_no = $telephone;
                                $model->created_datetime = date('Y-m-d H:i:s');
                                $model->updated_datetime = date('Y-m-d H:i:s');
                                $model->save(false);
                                
                                $company = new \common\models\CompanyInformation();
                                $company->user_id = $user->id;
                                $company->company_name = $name;
                                $company->save(false);

                                $namestrs = trim($model->full_name);
                                $string = str_replace(array('.',' and ', '&amp;','('),'',$namestrs);
                                $result = substr($string, 0, 3);
                                if (preg_match('/&/', $result))
                                {
                                    $string = str_replace(' & ','',$namestrs);
                                }
                                $avatar = new LetterAvatar($string, 'circle', 64);
                                $path = Yii::getAlias('@webroot').'/upload/profiles/';            
                                $filename = Yii::$app->security->generateRandomString().".png";
                                $avatar->saveAs($path . $filename);

                                $photo = new \common\models\ProfilePic();
                                $photo->userID = $user->id;
                                $photo->file_name = $filename;
                                $photo->save(false);
                                
                                $authassignment = new \common\models\AuthAssignment();
                                $authassignment->item_name = 'Dealer';
                                $authassignment->user_id = $user->id;;
                                $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                                $authassignment->save(false);
                                
                                $customer = \common\models\VipCustomerShihan::find()->where("lastname = '" . $code . "'")->one();
                                
                                $ckcustomers = \common\models\VipCustomerRewardShihan::find()
                                    ->where([
                                    'customer_id' => $customer->customer_id,
                                ])->count();
                                
                                if($ckcustomers > 0) {
                                    $this->history($customer->customer_id,$user->id);
                                }
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-ban" aria-hidden="true"></i></span>  Dealer Code# <b>('.$code.')</b> Done!</li>'; 
                                
                            }
                        }else {
                           $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Dealer Code# <b>('.$code.')</b> not found!</li>'; 
                        }
                        //$msg++;
                    }
                    //summary
                    
                    unlink($inputFile);
                    return $this->render('summary', [
                                'msg' => $msg,
                    ]);
                    exit();
                }else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template!']);
                    return $this->redirect(['importcustomer']);
                }
                return $this->redirect(['importcustomer']);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportcustomer2() {
        $model = new \backend\models\ExcelForm();
        $msg = '';        

        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');
            //$clientID = '14';
            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/customers/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //$pre_defined = array( "code", "name", "email", "category", "telephone", "password");
                $pre_defined = array( "code", "name", "email", "phone", "status");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 5){
                    for ($row = 2; $row <= $highestRow; $row++) {
                            //  Read a row of data into an array
                            $myheadings = array_map('strtolower', $headings[0]);
                            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                            $rowData[0] = array_combine($myheadings, $rowData[0]);

                            $code = $rowData[0]['code'];
                            $name = $rowData[0]['name'];
                            $email = $rowData[0]['email'];
                            $phone = $rowData[0]['phone'];
                            $status = $rowData[0]['status'];
                                    
                            $customer = \common\models\VIPCustomer::find()->where("clients_ref_no = '" . $code . "'")->one();
                            if(count($customer) > 0) {
                                $customer->full_name = '';
                                $customer->save(FALSE);
                                
                                
                                $user = \common\models\User::find()->where("id = '" . $customer->userID . "'")->one();
                                $user->status = $status;
                                $user->email = $email;
                                $user->save(false);
                                
                                $companyf = \common\models\CompanyInformation::find()->where("user_id = '" . $customer->userID . "'")->one();
                                $company = \common\models\CompanyInformation::find()->where("id = '" . $companyf->id . "'")->one();
                                
                                $company->company_name = $name;
                                $company->tel = $phone;
                                $company->save(false);
                                echo 'Yes - '.$code.'<br>'; 
                            }else {
                                echo 'No - '.$code.'<br>'; 
                            }
                    }
                }
                die();
                exit;
            }else {
                echo 'not match';
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    protected function history($oldid,$newid)  //insert 50000 records using createCommand method
    {
        $customers = \common\models\VipCustomerRewardShihan::find()
            ->where("customer_id = '" . $oldid . "'")
            //->limit(1)
            ->all();


        $sql = 'INSERT into vip_customer_reward (`clientID`, `customer_id`, `order_id`, `description`, `points`, `date_added`, `created_datetime`,`updated_datetime`,`created_by`,`updated_by`,`kis`) VALUES';
        foreach($customers as $customer){
            $sql .= '("14", "'.$newid.'", "'.$customer->order_id.'", "'.$customer->description.'", "'.$customer->points.'","'.$customer->date_added.'", "'.$customer->date_added.'", "'.$customer->date_added.'", "1", "1", "1"),';
        }
        $sql = rtrim($sql,',');
        $connection = Yii::$app->db;
        $command = $connection->createCommand($sql);
        $command -> execute();        
        //return;
        //echo $oldid.'-'.$newid.'<br>';
    }
    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionInfo() {
        phpinfo();
        exit();
    }
    
    public function actionTestsms() {
        $destination = '+601118889597';
        $username = 'vipkis';//urlencode(Yii::$app->params['sms.username']);
        $password = '1a2b3c4d';//urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $message = 'Sample Testing goes here!'.date('hmsdmY');
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);
        $type = '1';
        $agreedterm = 'YES';

        $fp = "https://www.isms.com.my/isms_send_all.php";
        $fp .= "?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&agreedterm=$agreedterm&sendid=$sender_id";
        $result = Yii::$app->VIPglobal->ismscURL($fp);
        
        print_r($result);
    }
    
    public function actionTestimage() {
        // Author: holdoffhunger@gmail.com
        // Imagick Type
        // ---------------------------------------------
        $file_to_grab = "/var/www/html/newvip/backend/web/upload/upload_invoice/10_870_6hUxNWClNqx8V2l8RU9l-TpmCIRGRz1n.jpg";
        $exif = exif_read_data($file_to_grab,0,true);
        
        echo '<pre>';
        print_r($exif);
    }
    
    function autorotate(Imagick $image) {
        echo $image->getImageOrientation();
        die;
        switch ($image->getImageOrientation()) {
            case Imagick::ORIENTATION_TOPLEFT:
                break;
            case Imagick::ORIENTATION_TOPRIGHT:
                $image->flopImage();
                break;
            case Imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateImage("#000", 180);
                break;
            case Imagick::ORIENTATION_BOTTOMLEFT:
                $image->flopImage();
                $image->rotateImage("#000", 180);
                break;
            case Imagick::ORIENTATION_LEFTTOP:
                $image->flopImage();
                $image->rotateImage("#000", -90);
                break;
            case Imagick::ORIENTATION_RIGHTTOP:
                $image->rotateImage("#000", 90);
                break;
            case Imagick::ORIENTATION_RIGHTBOTTOM:
                $image->flopImage();
                $image->rotateImage("#000", 90);
                break;
            case Imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateImage("#000", -90);
                break;
            default: // Invalid orientation
                break;
        }
        $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
        return $image;
    }
    
    

}
