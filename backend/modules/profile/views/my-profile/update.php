<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */

$this->title = 'Update Profile';
//$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->profile_ID, 'url' => ['view', 'id' => $model->profile_ID]];
$this->params['breadcrumbs'][] = 'Profile Update';
?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Profile</h2>
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content">
            <div class="user-profile-update">
                <?=
                $this->render('_form', [
                    'model' => $model,
                ])
                ?>

            </div>
        </div>
        
    </div>    
</div>

