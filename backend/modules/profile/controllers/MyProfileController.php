<?php

namespace app\modules\profile\controllers;

use Yii;
use common\models\UserProfile;
use common\models\UserProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfileController implements the CRUD actions for UserProfile model.
 */
class MyProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userID = \Yii::$app->user->id;
        $myID = \common\models\UserProfile::find()
            ->where(['userID' => $userID])
            ->one();
        $id = $myID->profile_ID;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Your profile has been updated.']);
            //return $this->redirect(['view', 'id' => $model->profile_ID]);
            //return $this->redirect(['/site/index']);
            return $this->goHome();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionChangepassword() {
        //$session = Yii::$app->session;
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $model = new \backend\models\ChangePasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $user->setPassword($model->password);
                $user->save();

                //\Yii::$app->getSession()->setFlash('success', 'Your password has been changed successfully');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Change Password', 'text' => 'Your password has been changed successfully']);
                //return $this->redirect(['/site/index']);
                return $this->goHome();
            }

            //return $this->redirect(['book-a-venue-step2']);
        }
        return $this->render("changepassword", [
                    'model' => $model,
        ]);
    }


    /**
     * Finds the UserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
