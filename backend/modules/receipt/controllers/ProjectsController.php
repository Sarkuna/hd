<?php

namespace app\modules\receipt\controllers;

use Yii;
use common\models\Projects;
use common\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\VIPCustomerReward;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApprove()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'A';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDecline()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'D';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('decline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $model = $this->findModel($id);
        $formapprove = new \common\models\ProjectActionForm();
        if ($formapprove->load(Yii::$app->request->post())) {
            $point = $formapprove->point;
            $status = $formapprove->status;
            $remark = $formapprove->remark;
            
            $model->project_approved_points = $point;
            $model->remark = $remark;
            $model->admin_status = $status;
            if($status == 'A') {
                $model->project_status = 'C';
            }else if($status == 'D') {
                $model->project_status = 'U';
            }

            $msg = '';
            if($model->save()) {
                if($model->admin_status == 'A') {
                    $pointawerd = new VIPCustomerReward();
                    $pointawerd->clientID = $clientID;
                    $pointawerd->customer_id = $model->userID;
                    $pointawerd->expiry_date = '2019-03-30';
                    $pointawerd->description = $model->project_name;
                    $pointawerd->points = round($point);
                    $pointawerd->bb_type = 'A';
                    $pointawerd->date_added = date('Y-m-d');
                    $pointawerd->save(false);
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Approve', 'text' => 'Project has been approved']);
                }else {
                    \Yii::$app->getSession()->setFlash('info', ['title' => 'Decline', 'text' => 'Project has been declined']);
                }
                
                
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Sorry Project action fail']);
            }
            
            
        }
        return $this->render('view', [
            'model' => $model,
            'formapprove' => $formapprove,
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->projects_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->projects_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
