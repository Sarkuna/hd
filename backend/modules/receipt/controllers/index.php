<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPAssignProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipassign Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipassign-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipassign Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vip_assign_products_id',
            'clientID',
            'productID',
            'date_assign',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
