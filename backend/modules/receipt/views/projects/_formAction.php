<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .field-projectactionform-point{display: none;}
</style>
 
    <!-- title row -->
        <div class="redemption-form">

            <?php $form = ActiveForm::begin(['options' => ['method' => 'post']]) ?>
            <?php
            //$formapprove->status = $model->user->status;
            echo $form->field($formapprove, 'status')->dropDownList(['A' => 'Approve', "D" => "Decline"],['prompt'=>'Select...']); 
            ?>
            <?= $form->field($formapprove, 'point')->textInput(['maxlength' => true]) ?>
            <?= $form->field($formapprove, 'remark')->textarea(['rows' => 6]) ?>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>




<?php
$script = <<< JS
    $(function () {
        $('#projectactionform-status').change(function () {
          var myval = $('#projectactionform-status').val();
          if(myval == 'A') {
              $('.field-projectactionform-point').show();
          }else {
              $('.field-projectactionform-point').hide();
          }
        });
    });
JS;
$this->registerJs($script);
?>