<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = $model->project_name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view row">
    <div class="col-lg-12"><h3 style="margin-top: 0px;" class="pull-right"><?= $model->getStatus() ?></h3></div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <?php if ($model->user->type == 5){
                    echo '<h3 class="box-title">About ID</h3>';
                }else{
                    echo '<h3 class="box-title">About Customer</h3>';
                }
                ?>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Code</strong>

                <p class="text-muted">
                    <?= $model->customer->clients_ref_no ?>
                </p>

                <hr>

                <strong><i class="fa fa-user margin-r-5"></i> Name</strong>

                <p class="text-muted"><?= $model->customer->full_name ?></p>

                <hr>

                <strong><i class="fa fa-phone margin-r-5"></i> Mobile</strong>

                <p class="text-muted"><?= $model->customer->mobile_no ?></p>

                <hr>

                <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

                <p class="text-muted"><?= $model->user->email ?></p>
            </div>
            <!-- /.box-body -->
        </div>
    </div>    
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Dealer</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Code</strong>

                <p class="text-muted">
                    <?= $model->dealerInformation->dealer_code ?>
                </p>

                <hr>

                <strong><i class="fa fa-user margin-r-5"></i> Name</strong>

                <p class="text-muted"><?= $model->dealerInformation->first_name ?> <?= $model->dealerInformation->last_name ?></p>

                <hr>

                <strong><i class="fa fa-phone margin-r-5"></i> Contact No</strong>

                <p class="text-muted"><?=  $model->dealerInformation->tel ?></p>

                <hr>

                <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

                <p class="text-muted"><?= $model->dealerInformation->user->email ?></p>
                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                <p class="text-muted">
                    <?=  $model->dealerInformation->address1.' '.$model->dealerInformation->address2 ?>
                    <br><?= $model->dealerInformation->city.' '.$model->dealerInformation->postcode ?>
                    <br><?= $model->dealerInformation->region.' '.$model->dealerInformation->countrys->name ?>
                </p>
            </div>
            <!-- /.box-body -->
        </div>

    </div>

    <div class="col-md-6">

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Nominated Points</span>
                        <span class="info-box-number"><?= Yii::$app->formatter->asInteger($model->project_nominated_points) ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <?php
            if ($model->admin_status != 'P') {
                echo '<div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Approved Points</span>
                                    <span class="info-box-number">' . Yii::$app->formatter->asInteger($model->project_approved_points) . '</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>';
            }
            ?>

        </div>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php
                if($model->admin_status == 'P') {
                    echo '<li class="active"><a href="#activity" data-toggle="tab">Action</a></li>';
                    $actv = '';
                }else {
                    $actv = 'active';
                }
                ?>
                <li class="<?= $actv ?>"><a href="#timeline" data-toggle="tab">Project Info</a></li>
                <li><a href="#gallery" data-toggle="tab">Gallery</a></li>
            </ul>
            <div class="tab-content">
                <?php if($model->admin_status == 'P') { ?>
                <div class="active tab-pane" id="activity">
                    <?= $this->render('_formAction', ['model' => $model, 'formapprove' => $formapprove,]) ?>
                </div>
                <?php } ?>
                <!-- /.tab-pane -->
                <div class="<?= $actv ?> tab-pane" id="timeline">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="width:25%">Ref:</th>
                                    <td><?= $model->projects_ref ?></td>
                                </tr>
                                <tr>
                                <th>Title:</th>
                                <td><?= Html::encode($this->title) ?></td>
                            </tr>
                                <tr>
                                    <th>Date</th>
                                    <td><?= $model->projects_date ?></td>
                                </tr>
                                <tr>
                                    <th>Client Name:</th>
                                    <td><?= $model->project_client_name ?></td>
                                </tr>
                                <tr>
                                    <th>Client Email:</th>
                                    <td><?= $model->project_client_email ?></td>
                                </tr>
                                <tr>
                                    <th>Tel:</th>
                                    <td><?= $model->project_client_tel ?></td>
                                </tr>
                                <tr>
                                    <th>Details:</th>
                                    <td><?= $model->project_details ?></td>
                                </tr>
                                <tr>
                                    <th>Admin Remark's:</th>
                                    <td><?= $model->remark ? $model->remark : 'N/A' ?></td>
                                </tr>
                            </tbody></table>
                    </div>


                </div>
                <!-- /.tab-pane -->
                
                <div class="tab-pane" id="gallery">
                    <div class="row">
                    <?php
                        $numOfCols = 3;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;
                        $uploadFiles   = \common\models\Uploads::find()->where(['ref'=>$model->ref])->all();
                        //$bigimages = $model->images;
                        foreach ($uploadFiles as $bigimage){
                            echo '<div class="col-md-'. $bootstrapColWidth.'">';
                            echo '<a class="fancybox" href="/upload/projects_gallery/'.$model->ref.'/'.$bigimage->real_filename.'" data-fancybox-group="gallery1">
                                <img src="/upload/projects_gallery/'.$model->ref.'/thumbnail/'.$bigimage->real_filename.'" class="img-responsive img-thumbnail">
                            </a>';
                            echo '</div>';
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) echo '</div><br><div class="row">';
                        }                                        
                    ?>
                    </div>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>
