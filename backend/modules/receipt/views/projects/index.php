<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects - Pending';
$this->params['breadcrumbs'][] = $this->title;

$clientID = 16;
$typename = \common\models\TypeName::find()->where([
                'clientID' => $clientID,
            ])->asArray()->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<div class="box box-primary projects-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        
    </div><!-- /.box-header -->

    <div class="box-body">
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => 'ID Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'projects_date12',
                'label' => 'Date',
                //'format' => 'html',
                'format' => ['date', 'php:d/m/Y'],
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return $model->projects_date;
                },
            ],
            [
                'attribute' => 'projects_ref',
                'label' => 'Project No.',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->projects_ref;
                },
            ],
            [
                'attribute' => 'type',
                'label' => 'User type',
                'format' => 'raw',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->user->typeName->name;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                'filter' => $typenames,
            ],
            /*[
                'attribute' => 'userID',
                'label' => 'ID Code',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->userID;
                },
            ],*/
            
            [
                'attribute' => 'project_name',
                'label' => 'Project Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->project_name;
                },
            ],
            [
                'attribute' => 'project_status12',
                'label' => 'Status',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return $model->getStatustext();
                },
            ],
            [
                'attribute' => 'project_nominated_points',
                'label' => 'Nominated Points',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->project_nominated_points;
                },
            ],           
            //'projects_date',
            //'projects_ref',
            //'projects_id',
            //'userID',
            //'clientID',
            //'dealerID',
            
            // 'project_name',
            // 'project_status',
            // 'project_client_name',
            // 'project_client_email:email',
            // 'project_client_tel',
            // 'project_details:ntext',
            // 'project_nominated_points',
            // 'project_approved_points',
            // 'ref',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [ 
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                    },
                    /*'delete' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                    },*/
                ],
            ],
        ],
    ]); ?>
        


    </div>
</div>
