<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            <p class="text-info hide"><em>Please select NRIC or Membership ID from drop-down list</em></p>
            <?php $form = ActiveForm::begin(); ?>

            
            <?= $form->field($model, 'invoice_no')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'selected_image')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'clientID')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'userID')->hiddenInput()->label(false); ?>
            



            <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>


