<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPUploadInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Invoices Item List';
$this->params['breadcrumbs'][] = ['label' => 'Upload Invoices List', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>
<div class="box box-primary vipupload-invoice-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->

    
    <div class="box-body">
        <div class="row">
        <div class="col-lg-3 offset-9">
            
            <?= $form->field($model, 'invoice_no')->textInput()->label(); ?>
            <?= $form->field($model, 'selected_image')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'clientID')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'userID')->hiddenInput()->label(false); ?>
        </div>
        </div><br>
        <?php

        $n = 1;
        foreach ($uploadimagelist as $imagelisit) {
            $img = '/upload/upload_invoice/' . $imagelisit->upload_file_new_name;
            if($n == 1){
                $in = 'in';
            }else {
                $in = '';
            }
            echo '<div class="panel box box-primary">
                 <div class="box-header with-border">
                     <h4 class="box-title" id="mydiv">
                         <a data-toggle="collapse" data-parent="#accordion" href="#collapse' . $n . '">
                             Image #' . $n . '
                         </a>
                         '.Html::checkbox('selection[]', false, ['value' => $imagelisit->upload_id]).'
                     </h4>
                 </div>
                 <div id="collapse' . $n . '" class="panel-collapse collapse ' . $in . '">
                     <div class="box-body">
                        <iframe src="' . $img . '" style="width:100%;min-height: 400px; height:auto; " frameborder="0"></iframe>
                     </div>
                 </div>
             </div>';
            $n++;
        }
        ?>
        <button class="btn btn-info btn-flat" type="submit">Submit</button>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

        function updateTextArea() {

            var allVals = [];

            $('#mydiv :checked').each(function () {

                allVals.push($(this).val());

            });

            $('#viptempreceipt-selected_image').val(allVals)

        }

        $(function () {

            $('#mydiv input').click(updateTextArea);

            updateTextArea();

        });

    </script>