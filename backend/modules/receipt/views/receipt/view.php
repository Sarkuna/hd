<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Expression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = $model->invoice_no;
$this->params['breadcrumbs'][] = ['label' => 'Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$totalpoint = \common\models\VIPCustomerAccount2::find()
        ->where(['>', 'points_in', 0])
        ->andWhere(['clientID'=>$model->clientID, 'customer_id'=>$model->reseller_name])
        ->sum('points_in');

$companyname = \common\models\CompanyInformation::find()
        ->where(['user_id'=>$model->reseller_name])
        ->count();
if($companyname > 0){
    $companyname = \common\models\CompanyInformation::find()
        ->where(['user_id'=>$model->reseller_name])
        ->one();
    
    $company_name = $companyname->company_name;
}else {
    $company_name = 'N/A';
}

?>
<style>
    .invoice{margin:0px;}
    .table>thead>tr>th {vertical-align: top;border-bottom: 2px solid #ddd;}
    .table{border-collapse: collapse;border: #dddddd;}
</style>
<section class="invoice">    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Receipt Invoice# <?= $model->invoice_no ?>
                <?php
                if(!empty($model->redemption_status_date)){
                    echo '<small class="pull-right">Approved Date: '.date('d-m-Y', strtotime($model->redemption_status_date)).'</small>&nbsp;';
                }
                ?>
                
                <small class="pull-right" style="margin-right: 15px;">Created Date: <?php echo date('d-m-Y', strtotime($model->created_datetime)) ?></small>
                
            </h2>
        </div><!-- /.col -->
    </div>
    <div class="row invoice-info">
        <div class="col-sm-5 invoice-col">
            <h4>Profile Info</h4>
            <address>
            <?php
            if(!empty($model->customer->clients_ref_no)) {
               $code = $model->customer->clients_ref_no;
            }else {
                $code = 'N/A';
            }
            
            echo 'Code : '.$code;  
            echo '<br>Date Of Receipt : '.date('d-m-Y', strtotime($model->date_of_receipt));
            echo '<br>Distributor : '.$company_name;
            if(!empty($model->customer->full_name)) {
                $name = $model->customer->full_name;
            }else {
                $name = 'N/A';
            }
            echo '<br>Full Name : '.$name;
            //echo '<br>Membership ID# : '.$model->profile->card_id;
            //echo '<br>NRIC/PP : '.$model->profile->ic_no;
            if(!empty($model->customer->mobile_no)) {
                $mobile = $model->customer->mobile_no;
            }else {
                $mobile = 'N/A';
            }
            echo '<br>Mobile # : '.$mobile;
            //echo '<br>Email ID: '.$model->user->email;
            ?>
            </address>
        </div>
        <div class="col-sm-5 invoice-col"></div><!-- /.col -->
        

        <div class="col-sm-2 invoice-col">
                <?php

                    echo '<h3 style="margin-top: 0px;" class="pull-right">'.$model->getStatustext().'</h3>';
 
                ?>
              <br/>
              
            </div><!-- /.col -->
    </div>
    
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped" border="1">
                <thead style="border: solid 1px #dddddd;">
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2" style="vertical-align: middle;" class="text-center">Product Description</th>
                        <th colspan="3" class="text-center">Base Values</th>
                        <th rowspan="2">Qty</th>
                        <th colspan="3" class="text-center">Awarded Values</th>
                        <th colspan="2" class="text-center">Product Purchased Price</th>
                  </tr>
                  <tr>
                        <td>Pack Size</td>
                        <td>Points</td>
                        <td>RM</td>
                        <td>Pack Size</td>
                        <td>Point</td>
                        <td>RM</td>
                        <td>Unit Price</td>
                        <td>Total Price</td>
                  </tr>

                </thead>
                <tbody>
                    <?php
                    $redemptionitems = common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                    $n = 1;
                    $totalpoints = 0;
                    $totalpointsrm = 0;
                    $totalpacksize = 0;
                    $tr = '';
                    if(count($redemptionitems) > 0) {
                        foreach($redemptionitems as $redemptionitem){
                            $product_list_id = $redemptionitem->product_list_id;
                            $product_list = \common\models\ProductList::find()->where(['product_list_id' => $product_list_id])->one();
                            $totalpoint = $redemptionitem->product_item_qty * $redemptionitem->product_per_value;
                            $tr .= '<tr>
                                <td>'.$n.'</td>
                                <td class="text-left" width="40%">'.$product_list->description.'</td> 
                                <td class="text-right">'.$product_list->pack_size.'</td>
                                <td class="text-right">'.$product_list->total_points_awarded.'</td> 
                                <td class="text-right">'.$redemptionitem->points_awarded_rm_value.'</td>     
                                <td class="text-right">'.$redemptionitem->product_item_qty.'</td> 
                                <td class="text-right">'.$product_list->pack_size * $redemptionitem->product_item_qty.'</td>    
                                <td class="text-right">'.$redemptionitem->qty_points_awarded.'</td>  
                                <td class="text-center">'.$redemptionitem->qty_points_awarded_rm_value.'</td>
                                <td class="text-center">'.$redemptionitem->price_per_product.'</td>     
                                <td class="text-center">'.$redemptionitem->price_total.'</td> 

                              </tr>';
                            $pck = $product_list->pack_size * $redemptionitem->product_item_qty;
                            $totalpoints += $redemptionitem->qty_points_awarded;
                            $totalpointsrm += $redemptionitem->qty_points_awarded_rm_value;
                            $totalpacksize += $pck;
                            $n++;
                        }
                    }else {
                        $tr = '<tr><td colspan="11" class="text-center">No products found</td></tr>';
                    }
                    echo $tr;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">History:</p>
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?php
                $comments = common\models\ReceiptHistory::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                if(count($comments) > 0){
                    foreach($comments as $comment){
                            echo '<b>'.$comment->getStatustext().'</b> - <em>'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                            if(!empty($comment->comment)){
                                $remark = $comment->comment;
                            }else {
                                $remark = 'N/A';
                            }
                            echo $remark.'<br><br>';
                    }
                }else{
                    echo 'No Comments';
                }
                ?>
              </p>
              
              <?php
                if($model->reasons_id != 0) {
                    echo '<p class="lead">Reason:</p>';
                    echo '<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">'.$model->reasons->name.'</p>';
                }
              ?>
            </div><!-- /.col -->
            <div class="col-xs-6">
              <p class="lead">Awarded </p>
              <div class="table-responsive">
                <table class="table align-right">
                  <tr>
                    <th>Total Points:</th>
                    <td class="text-right"><b><?= $totalpoints ?></b></td>
                  </tr>
                  <tr>
                    <th>Total RM Values:</th>
                    <td class="text-right"><b><?= Yii::$app->formatter->asDecimal($totalpointsrm) ?></b></td>
                  </tr>
                  <tr>
                    <th>Total Pack Size:</th>
                    <td class="text-right"><b><?= $totalpacksize ?></b></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
            
            <div class="col-xs-12">
                <?php
                    //$n = 1;
                    $uploadimagelist = \common\models\VIPUploadInvoice::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                    foreach ($uploadimagelist as $image) {
                        $imgthumb = '/upload/upload_invoice/thumb/' . $image->upload_file_new_name;
                        $img = '/upload/upload_invoice/' . $image->upload_file_new_name;
                       echo '<div class="col-md-3">
                            <div class="thumbnail">
                                <a data-gallery="manual" href="'.$img.'">
                                    <img src="'.$img.'">
                                </a>
                            </div>
                        </div>';
                    }
                ?>
                
            </div>
          </div><!-- /.row -->
    </div>
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="#" target="_blank" class="hide btn btn-default"><i class="fa fa-print"></i> Print</a>            
            
            <?php
            //if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
                $back = Yii::$app->request->referrer;
            //}else{
                //$back = 'index';
            //}
            if($model->status == 'N') {
                if($totalpoints >= $totalpoint) {
                    if(count($redemptionitems) > 0) {
                        echo '<button class="btn btn-success pull-right" onclick="updateGuard(' . $model->vip_receipt_id . ');return false;"><i class="fa fa-credit-card"></i> Approval Action</button>';
                    }
                }else {
                    echo "<h3><span class='label label-danger pull-right'>Insufficient balance for Distributor : ".$company_name."</span></h3>";
                }
            } else {
                echo "<h3><span class='label label-danger pull-right'>Sorry you can't change the status</span></h3>";
            }
            ?>
            <a href="javascript: history.back()" class="btn btn-default">Back</a>
            <?php
            if(!($model->status == 'A' || $model->status == 'D')) {
                echo '<a href="'.Url::to(['/receipt/receipt/update', 'id' => $model->vip_receipt_id]).'" class="btn btn-info">Edit</a>';
            }
            ?>
            <button class="hide btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
    </div>
    
    
</section>
<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Action </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/

function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/receipt/receipt/approve"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
</script>

<?php
    $script = <<<EOD
    // initialize manually with a list of links
    $('[data-gallery=manual]').click(function (e) {

      e.preventDefault();

      var items = [],
        options = {
          index: $(this).index()
        };

      $('[data-gallery=manual]').each(function () {
        items.push({
          src: $(this).attr('href'),
          title: $(this).attr('data-title')
        });
      });

      new PhotoViewer(items, options);

    }); 

EOD;
$this->registerJs($script);
    ?>