<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts List';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipreceipt-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?> <span class="label label-info">Processing</span></h3>
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vip_receipt_id',
            [
                'attribute' => 'order_num',
                'label' => 'Order Num',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->order_num;
                },
            ],
            [
                'attribute' => 'full_name',
                'label' => 'Full Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'mobile_no',
                'label' => 'Mobile No',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->mobile_no;
                },
            ],
            [
                'attribute' => 'company_name1',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ],            
            //'invoice_no',
            [
                'attribute' => 'date_of_receipt',
                'format' => 'raw',
                'headerOptions' => ['width' => '120'],
                'filter' => MaskedInput::widget([
                    'attribute' => 'date_of_receipt', 
                    'model' => $searchModel, 
                    'name' => 'input-5', 
                    'mask' => ['99-99-9999', '999-999-9999'],
                    'clientOptions' => ['alias' => 'dd-mm-yyyy']
                ]), 
                'value' => function ($model) {
                    if($model->date_of_receipt == '1970-01-01'){
                        return 'N/A';
                    }else {
                        return Yii::$app->formatter->asDatetime($model->date_of_receipt, "php:d-m-Y");
                    }
                }
            ], 
            [
                'attribute' => 'invoice_no',
                'label' => 'Invoice No',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->invoice_no;
                },
            ],            
            [
                'attribute' => 'company_name',
                'label' => 'Distributor',
                'format' => 'html',
                //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                //'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->company->company_name;
                },
            ],           
            //'userID',
            //'clientID',
            [
                'attribute' => 'total_items',
                //'label' => 'Total Transactions',
                'format' => 'html',
                'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalItems();
                },
            ],
            [
                'attribute' => 'order_total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalPoints();
                },
            ],            
                       
            [
                'attribute' => 'created_datetime',
                'format' => 'raw',
                'label' => 'Created Date',
                'headerOptions' => ['width' => '120'],
                'filter' => MaskedInput::widget([
                    'attribute' => 'created_datetime', 
                    'model' => $searchModel, 
                    'name' => 'input-5', 
                    'mask' => ['99-99-9999', '999-999-9999'],
                    'clientOptions' => ['alias' => 'dd-mm-yyyy']
                ]), 
                'value' => function ($model) {
                    if($model->created_datetime == '1970-01-01'){
                        return 'N/A';
                    }else {
                        return date('d-m-Y', strtotime($model->created_datetime));
                        //return Yii::$app->formatter->asDatetime($model->created_datetime, "php:d-m-Y");
                    }
                }
            ],
              [
                'class' => 'yii\grid\ActionColumn',
                  'headerOptions' => ['width' => '60'],
                'template' => '{update} {view} ', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'update' => function ($url, $model) {
                        if($model->status == 'P' || $model->status == 'R' || $model->status == 'N') {
                            return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));                        
                        }
                    },
                ],
            ],          
        ],
    ]); ?>
            </div>
        </div>
    </div>