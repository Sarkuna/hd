<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceipt */

$this->title = 'Edit';
$this->params['breadcrumbs'][] = ['label' => 'Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_num, 'url' => ['view', 'id' => $model->vip_receipt_id]];
//$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipreceipt-update">
        <?=
        $this->render('_form_edit_order', [
            'model' => $model,
            'pointorderitemlistsdel' => $pointorderitemlistsdel,
        ])
        ?>

    </div>
</div>
