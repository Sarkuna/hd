<?php
$session = Yii::$app->session;
$clientID = $session['currentclientID'];

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;

use common\models\State;
use common\models\ProductName;
use common\models\ProductList;
use common\models\ResellerList;
use common\models\BBPoints;
use common\models\VIPCustomer;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */

$d_product_name = ArrayHelper::map(ProductName::find()
    ->where(['clientID' => $clientID])
    ->orderBy('name ASC')->all(), 'product_name_id', 'name');

$d_area = ArrayHelper::map(State::find()->where(['status' => 'A'])->orderBy('state_name ASC')->all(), 'state_id', 'state_name');
$reseller_name = ArrayHelper::map(ResellerList::find()->where(['reseller_category_id' => $model->state])->orderBy('reseller_name ASC')->all(), 'reseller_list_id', 'reseller_name');

$distributor = \common\models\CompanyInformation::find()->joinWith(['user'])->where(['client_id' => $session['currentclientID'], 'status' => 'A', 'type' => '1'])->all();
//echo '<pre>'.print_r($distributor).die;
$distributorlist = ArrayHelper::map($distributor, 'user_id', 'company_name');
$reasons_list = ArrayHelper::map(common\models\Reasons::find()->orderBy('name ASC')->all(), 'reasons_id', 'name');

?>
<style>
    .point-order-form .dropdown-menu.open {
        z-index: 5000 !important;
    }
    
    .table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table-striped thead tr.primary:nth-child(odd) th {
    background-color: #428BCA;
    color: white;
    border-color: #357EBD;
    border-top: 1px solid #357EBD;
    text-align: center;
}
input#totalqty {
    text-align: right;
    width: 50px;
}

input#grandtotal {
    text-align: right;
    width: 100px;
}
.form-group.field-vipreceipt-remark {
    /*margin-top: 100px;
     clear: both; */
}
.field-vipreceipt-reasons_id{display: none;}
</style>

<?php $form = ActiveForm::begin(); ?>

<div class="col-lg-12 no-padding">
    <div class="col-md-3">
        <?= $form->field($model, 'invoice_no')->textInput(['autocomplete' => 'off', 'disabled' => true]) ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'date_of_receipt')->textInput(['maxlength' => true])->widget(MaskedInput::className(), ['clientOptions' => ['alias' => 'dd-mm-yyyy'],]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'reseller_name')->dropDownList($distributorlist, ['prompt' => '-- Select --',]); ?>
    </div>
    <div class="col-md-3">
        <label class="control-label" for="vipreceipt-reseller_name"></label>
        <?= $form->field($model, 'handwritten_receipt')->checkbox(); ?>
    </div>
</div>

<div class="col-lg-12">
    <?php
    $uploadimagelist = \common\models\VIPUploadInvoice::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
    $no_of_images = count($uploadimagelist);
    ?>

    <h3>No of Receipts Uploaded (<b><?= $no_of_images ?></b>)</h3>
    <?php
        //$n = 1;
        foreach ($uploadimagelist as $image) {
            $imgthumb = '/upload/upload_invoice/thumb/' . $image->upload_file_new_name;
            $img = '/upload/upload_invoice/' . $image->upload_file_new_name;
           echo '<div class="col-md-3">
                <div class="thumbnail">
                    <a data-gallery="manual" href="'.$img.'">
                        <img src="'.$img.'">
                    </a>
                </div>
            </div>';
        }
    ?>
</div>

<div class="col-md-12 no-padding">

    <div class="col-md-7">
        <?php
        $product_descriptions = ArrayHelper::map(ProductList::find()->orderBy('description ASC')->where(['clientID' => $clientID, 'del' => 'A'])->all(), 'product_list_id', function ($model) {
                    return $model['description'] . $model['metric'];
                });

        echo $form->field($model, 'product_description')->widget(Select2::classname(), [
            'data' => $product_descriptions,
            //'language' => 'de',
            'options' => [
                'placeholder' => '-- Select --',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?> 

    </div>

    <div class="col-md-2">
        <?= $form->field($model, 'vqty')->textInput() ?>
    </div>

    <div class="col-md-2">
        <?php $model->price_total = '0.00'; ?>
        <?=
        $form->field($model, 'price_total')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
            //'mask' => '9999999999999',
            //'mask' => '9',
            'clientOptions' => ['alias' => 'decimal', 'groupSeparator' => ',',],
                //'options' => ['placeholder' => '60XXXXXXXXX', 'class' => 'form-control',],
        ])
        ?> 
    </div>

    <div class="col-md-1" style="padding-left: 0px;">
        <label class="control-label" for="pointorder-vqtybtn" style="height: 20px;display: block;"></label>
        <input type="button" id="addButton" class="addButtonc btn btn-primary" value="Add" />
    </div>
</div>

<div class="col-md-12">
    <div class="direct-chat-messages no-padding">
        <table class="table fixed" id="retc"> 
            <thead>
                <tr>
                    <th class="hide">barcodename</th>
                    <th class="hide">barcodeid</th>
                    <th width="40%">Product Description</th>
                    <th width="5%">Qty</th>                                 
                    <th width="14%">Point</th>
                    <th width="15%">RM Value</th>
                    <th width="10%">Price</th>
                    <th style="width: 5%; text-align: center;"><i class="fa fa-trash" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                </tr>
            </thead>
            <tbody class="product_item_list" id="product_item_list">
                <?php
                //$pointorderitemlistsdel = common\models\PointOrderItem::find()->where(['point_order_id' => $id])->all();
                $total_items = 0;
                $iqty = 0;
                $total_point = 0;
                foreach ($pointorderitemlistsdel as $pointorderitemlist) {
                    $productlist = \common\models\ProductList::find()->where(['product_list_id' => $pointorderitemlist->product_list_id])->one();
                    if (!empty($pointorderitemlist->vip_receipt_list_id)) {
                        $delitem = '<a id="' . $pointorderitemlist->vip_receipt_list_id . '" href="javascript::;" class="deletedb"><i class="fa fa-trash" style="opacity:0.5; filter:alpha(opacity=50);"></i></a>';
                    } else {
                        $delitem = '<a href="javascript::;" class="btneditDelete"><i class="fa fa-trash" style="opacity:0.5; filter:alpha(opacity=50);"></i></a>';
                    }
                    echo '<tr id="' . $pointorderitemlist->product_list_id . '">
                                <td class="hide">' . $pointorderitemlist->product_list_id . '</td>
                                <td class="hide"></td>
                                <td>' . $productlist->description . '</td>
                                <td class="text-center qty">' . $pointorderitemlist->product_item_qty . '</td>    
                                <td class="text-center points">' . $pointorderitemlist->qty_points_awarded . '</td>     
                                
                                
                                <td class="text-center price">' . Yii::$app->formatter->asDecimal($pointorderitemlist->qty_points_awarded_rm_value) . '</td>    
                                <td class="text-center">' . $pointorderitemlist->price_total . '</td>                                
                                <td class="text-center">' . $delitem . '</td></tr>';
                    $total_items += $pointorderitemlist->product_item_qty;
                    $total_point += $pointorderitemlist->qty_points_awarded;
                    $iqty++;
                    //$total_items .= $total_items;
                }
                ?>
            </tbody>
        </table>
    </div>
    <table style="width:100%; float:right; padding:5px; color:#000; background: #5d9fd8;" id="totalTable">
        <tbody>
            <tr>
                <td colspan="2" style="padding: 5px 10px; font-weight:bold;color:#FFF;">
                    Total Items                                        </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;font-weight:bold; color:#FFF;" class="text-right">

                </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;font-weight:bold; color:#FFF;">
                    <input class="form-control" type="text" id="totalqty" name="totalqty" value="<?= $iqty ?> "/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 5px 10px; font-weight:bold; color:#FFF;">
                    Total Points                                      </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;font-weight:bold; color:#FFF;" class="text-right">

                </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;font-weight:bold; color:#FFF;">
                    <input class="form-control" type="text" id="grandtotal" name="grandtotal" value="<?= $total_point ?>" />
                    <input class="form-control" type="hidden" id="porderid" name="porderid" value="<?= $model->vip_receipt_id ?>" />
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-md-12 no-padding" style="margin-top:50px;">
    <div class="col-xs-12 col-sm-6 col-lg-6">
        <?= $form->field($model, 'remark')->textarea(['rows' => '6']) ?>
    </div>
    
    <div class="col-xs-12 col-sm-6 col-lg-6">

        <?php
        if ($model->status == 'P') {
            $list = ['R' => 'Draft', 'N' => 'Processing', 'D' => 'Decline'];
        } else if ($model->status == 'R' || $model->status == 'D') {
            $list = ['R' => 'Draft', 'N' => 'Processing', 'D' => 'Decline'];
        } else if ($model->status == 'N') {
            $list = ['N' => 'Processing', 'D' => 'Decline'];
        }
        echo $form->field($model, 'status')->dropDownList($list);
        echo $form->field($model, 'reasons_id')->dropDownList($reasons_list,['prompt'=>'Select...']);
        
        ?>

        <?php
        //if($model->status != 'D') {
        echo $form->field($model, 'assign_name')->textInput(['autocomplete' => 'off']);
        //}
        ?>
    </div>
</div>


<div class="box-footer">
        <div class="col-md-12">

            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary btgr' : 'btn btn-primary btgr', 'id' => 'btnsubmit']) ?>
            <?php
            if ($model->status != 'P') {
                echo '<a href="' . Url::to(['/receipt/receipt/view', 'id' => $model->vip_receipt_id]) . '" class="btn btn-info">View</a>';
            }
            ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
          
    <?php
    $script = <<<EOD
                var handledCount = 0;
                $('.addButtonc').unbind('click').click(function() {
                    var test = $("#vipreceipt-product_description").val();
                    var price_total = $("#vipreceipt-price_total").val();
                    price_total = parseFloat(price_total);
                    var price_per_product = $("#vipreceipt-price_total").val() / $("#vipreceipt-vqty").val();
                    //alert(test);
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                        
                    });
            
                    if (handledCount == 0){
                        var itemID = $("#vipreceipt-product_description").val(),vNode  = $(this);
                        var qty = $("#vipreceipt-vqty").val();
                        var campaignid = $("#pointorder-listing_campaign").val();
                        var sum = 0.0;
                        $.ajax({
                            url:'get-product?id='+itemID+'&qty='+qty,
                            dataType: 'json',
                            success : function(data) {
                                if (flag == 1) {
                                    alert('Item already exists');
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    $("#vipreceipt-vqty").val('');
                                    $("#vipreceipt-price_total").val('0');
                                } else {
                                    $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodeid+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid +"'/><input class='form-control' type='text' name='totqty[]' value='"+ data.qty+ "'/><input type='text' name='price_total[]' value='"+ price_total+ "'/></td><td>" + data.product + "</td><td class='text-center'>" + data.qty+ "</td><td class='text-center points'>" + data.total_points_awarded + "</td><td class='text-center'>" + (data.total_rm_awarded).toFixed(2)+ "</td><td class='text-center'>" + price_total.toFixed(2) + "</td><td class='text-center'><a href='javascript::;' class='btnDelete'><i class='fa fa-trash' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    var count = $('#retc tbody').children('tr').length;
                                    update_amounts();
                                    update_percentage();
                                    $("#totalitem").html(count);
                                    $("#totalqty").val(count);
                                    $("#vipreceipt-vqty").val('');
                                    $("#vipreceipt-price_total").val('');
                                    //$("#vipreceipt-product_description").val('');
                                    $("#vipreceipt-product_description").select2('val', 'All');
                                }
                            },
                            error : function() {
                                console.log('error');
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 1)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });

            function update_amounts()
            {
                var sum = 0.00;
                $('#retc > tbody  > tr').each(function() {
                    var point = $(this).find('.points').html();
                    //alert(point)
                    sum = sum + Number(point);
                });
                $("#grandtotal").val(sum);
            }
            function update_percentage()
            {
                var priceOne = parseFloat($('#pointorder-dealer_invoice_price').val());
                var priceTwo = parseFloat($('#grandtotal').val());
                var price3 = priceTwo / priceOne * 100;
                $('#pointorder-pay_out_percentage').val(price3.toFixed(2)); // Set the rate
            }
            
            $('#pointorder-dealer_invoice_price').on('input',function(e){
                update_percentage();
            });
            
            $('.deletedb').click(function(){
                var tableRow = $(this).closest('tr');
                var del_id = $(this).attr('id');
                $.ajax({
                    type: 'GET',
                    url: 'delete-item',
                    data: 'delete_id='+del_id,
                    cache: false,
                    success: function(){
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        update_amounts();
                        update_percentage();
                        $("#totalitem").html(count);
                        $("#totalqty").val(count);
                    }
                });
            });

            
            $("#retc").on('click','.btnDelete',function(){
                //$(this).closest('tr').remove();
                var tableRow = $(this).closest('tr');

                tableRow.find('td').fadeOut('fast', 
                    function(){
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        update_amounts();
                        update_percentage();
                        $("#totalitem").html(count);
                        $("#totalqty").val(count);
                    }
                );
            });
            
            $('#btnsubmit').on('click', function() {
                if($('#vipreceipt-status').val() != 'D') {
                    if($("#product_item_list tr").length > 0){
                        return true;
                    }else{
                        alert('Please ensure there is at least one row in product list table');
                        return false;
                    }
                }else {
                    return true;
                }
            });
            
            $(function() { 
                $('#vipreceipt-status').change(function(){
                    if($('#vipreceipt-status').val() == 'R') {
                        $('.field-vipreceipt-assign_name').show(); 
                    } else {
                        $('.field-vipreceipt-assign_name').hide(); 
                    }
                    
                    if($('#vipreceipt-status').val() == 'D') {
                        $('.field-vipreceipt-reasons_id').show(); 
                    } else {
                        $('.field-vipreceipt-reasons_id').hide(); 
                    }
            
                });
            });

           // initialize manually with a list of links
    $('[data-gallery=manual]').click(function (e) {

      e.preventDefault();

      var items = [],
        options = {
          index: $(this).index()
        };

      $('[data-gallery=manual]').each(function () {
        items.push({
          src: $(this).attr('href'),
          title: $(this).attr('data-title')
        });
      });

      new PhotoViewer(items, options);

    }); 

EOD;
$this->registerJs($script);
    ?>