<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipreceipt Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipreceipt-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipreceipt List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vip_receipt_list_id',
            'vip_receipt_id',
            'userID',
            'clientID',
            'product_list_id',
            // 'product_per_value',
            // 'product_item_qty',
            // 'user_selector',
            // 'reseller_selector',
            // 'profile_selector',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
