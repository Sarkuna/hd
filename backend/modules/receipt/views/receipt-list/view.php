<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPReceiptList */

$this->title = $model->vip_receipt_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipreceipt Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipreceipt-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->vip_receipt_list_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vip_receipt_list_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vip_receipt_list_id',
            'vip_receipt_id',
            'userID',
            'clientID',
            'product_list_id',
            'product_per_value',
            'product_item_qty',
            'user_selector',
            'reseller_selector',
            'profile_selector',
        ],
    ]) ?>

</div>
