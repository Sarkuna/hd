<?php

namespace app\modules\settings\controllers;

use Yii;
use common\models\Menus;
use common\models\MenusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use common\models\MenuItems;

/**
 * MenusController implements the CRUD actions for Menus model.
 */
class MenusController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$searchModel = new MenusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        $session = Yii::$app->session;
        $clientID = 16;
        
        $menu_list_name = Menus::find()
                ->where(['clientID' => $clientID])
                ->count();
        if($menu_list_name > 0) {
            $menu_select_id = Menus::find()
                ->where(['clientID' => $clientID])
                ->orderBy([
                    'id' => SORT_DESC,
                  ])    
                ->one();
            $id = $menu_select_id->id;
            return $this->redirect(['update', 'id' => $id]);
        }else {
            return $this->redirect(['create']);
        }
    }

    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menus();
        $session = Yii::$app->session;
        $clientID = 16;
        $pages = \common\models\Pages::find()
                ->where(['clientID' => $clientID, 'type' => 'page', 'status' => 'publish'])
                ->all();
        
        $menu_list_name = ArrayHelper::map(Menus::find()
                ->where(['clientID' => $clientID])
                ->all(),'id', 'name');

        if ($model->load(Yii::$app->request->post())) {
            $model->clientID = $clientID;
            if($model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
                //\Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Created successfully']);
            }else {
                print_r($model->getErrors());
                //\Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Created fail!']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'pages' => $pages,
                'menu_list_name' => $menu_list_name,
            ]);
        }
    }

    /**
     * Updates an existing Menus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $pages = \common\models\Pages::find()
                ->where(['clientID' => $clientID, 'type' => 'page', 'status' => 'publish'])
                ->all();
        
        $menu_list_name = ArrayHelper::map(Menus::find()
                ->where(['clientID' => $clientID])
                ->all(),'id', 'name');
       
        
        $model = $this->findModel($id);
        $menuitems = $model->menuitems;

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->display_location)) {
                $display_location = implode(",",$model->display_location);
            }else {
                $display_location = '';
            }            
            $model->display_location = $display_location;
            if($model->save()) {
                $insdata = array();
                $menu_nodes = $model->menu_nodes;
                $menu_items = json_decode($menu_nodes, true);
                if (!empty($menu_items)) {
                    MenuItems::deleteAll(['menu_id' => $id]);
                }
                foreach($menu_items as $key=>$menu_item){
                    $title = $menu_item['title'];
                    $target = $menu_item['target'];
                    $icon_font = $menu_item['iconFont'];
                    $link = $menu_item['customUrl'];
                    if(!empty($link)){
                        $vlink = $link;
                    }else {
                        $vlink = $this->slugify($title);

                    }
                    //$id = $menu_item['id'];
                    $class = $menu_item['class'];
                    $relatedId = $menu_item['relatedId'];
                    $type = $menu_item['type'];
                    $insdata[] = [$title,$vlink,$target,$key,$class,$icon_font,$model->id,$relatedId,$type];
                    //echo $key.$title.'<br>';
                }
                Yii::$app->db
                    ->createCommand()
                    ->batchInsert('menu_items', ['title','link','target','sort','class','icon_font','menu_id','related_id','type'],$insdata)
                    ->execute();
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Created successfully']);
            }else {
                \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Created fail!']);
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'pages' => $pages,
                'menuitems' => $menuitems,
                'menu_list_name' => $menu_list_name,
            ]);
        }
    }
    
    
    public function actionEdit($id)
    {
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
        $del = $this->findModel($id)->delete();
        if($del) {
            MenuItems::deleteAll(['menu_id' => $id]);
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Dleted successfully']);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public static function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
