<?php

namespace app\modules\settings\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\web\UploadedFile;

use common\models\Client;
use common\models\ClientSearch;
use common\models\ClientAddress;
use common\models\ClientBillingInformation;
use common\models\ClientOption;
use common\models\ClientPointOption;


/**
 * ClientsController implements the CRUD actions for Client model.
 */
class LogoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $model = new \backend\models\LogoForm();
        //$ClientAddress = ClientAddress::find()->where(['client_address_ID' => $logoID])->one();
        $client = $this->findModel($clientID);
        if ($model->load(Yii::$app->request->post())) {
            $newCover = UploadedFile::getInstance($model, 'file_image');
            if (!empty($newCover)) {
                if(!empty($client->company_logo)){
                    $filename = Yii::$app->basePath .'/web/upload/client_logos'.$client->company_logo;
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                }
                $newCoverName = Yii::$app->security->generateRandomString();
                $imgname = $newCoverName . '.' . $newCover->extension;
                $client->company_logo = $imgname;
                $client->save(false);
                //unlink($model->cover);
                //$model->cover = 'uploads/covers/' . $newCoverName . '.' . $newCover->extension;
                $newCover->saveAs(Yii::$app->basePath .'/web/upload/client_logos/' . $newCoverName . '.' . $newCover->extension);
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }
        }else {
            return $this->render('_form_logo', [
                        'model' => $model,
                        'client' => $client,
            ]);
        }
    }


    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
