<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipassign-products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vip_assign_products_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'productID') ?>

    <?= $form->field($model, 'date_assign') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
