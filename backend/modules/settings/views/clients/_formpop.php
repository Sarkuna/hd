<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
//$this->title = 'Send Membership Pack';
?>


<div class="col-xs-12 col-lg-12">
    <div class="membership-pack-form">
        <?php
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                //'enableAjaxValidation' => true,
            //'class' => 'form-horizontal',
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>

        <?= $form->field($model, 'company')->textInput(array('placeholder' => 'Company Name'));?>
        <?= $form->field($model, 'company_short_name')->textInput(array('placeholder' => 'Company Short Name'));?>
        <?= $form->field($model, 'cart_domain')->textInput(array('placeholder' => 'URL/Domain'));?>
        <?= $form->field($model, 'admin_domain')->textInput(array('placeholder' => 'Admin Domain'));?>
        <?= $form->field($model, 'programme_title')->textInput(array('placeholder' => 'Programme Title'));?>
        <!-- Modal Footer -->
        <div class="modal-footer">                
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    <?php ActiveForm::end(); ?>
        
    </div>    
</div>
