<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$currentclientID = $session['currentclientID'];
?>

<div class="pages-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-9">
                <?= $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>
                <?=
                $form->field($model, 'page_description')->widget(CKEditor::className(), [
                    'options' => [ 'rows' => 6],
                    'preset' => 'standard',
                    'clientOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => $currentclientID], ['allowedContent' => true]),
                        //'clientOptions' => ElFinder::ckeditorOptions('elfinder',[/* Some CKEditor Options */]), 
                ]);
                ?> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'status')->dropDownList(['draft' => 'Draft', 'publish' => 'Published']); ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
