<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Menus */

$this->title = 'Update Menus: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Menuses', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Create Menu';
?>
<div class="menus-update">
    <?= $this->render('_form', [
        'model' => $model,
        'pages' => $pages,
        'menu_list_name' => $menu_list_name,
    ]) ?>

</div>