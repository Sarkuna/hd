<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$currentclientID = $session['currentclientID'];
?>

<div class="email-template-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-2">
                <?= $form->field($model, 'code')->textInput(['readonly' => true,'maxlength' => true]) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?=
        $form->field($model, 'template')->widget(CKEditor::className(), [
                      'options' => ['rows' => 6],
                      'preset' => 'full',
                      'clientOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => $currentclientID],[]), 
                  ]);
    
        ?>
    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>