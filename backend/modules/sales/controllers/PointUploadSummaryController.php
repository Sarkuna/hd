<?php

namespace app\modules\sales\controllers;

use Yii;
use common\models\PointUploadSummaryReport;
use common\models\PointUploadSummaryReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PointUploadSummaryController implements the CRUD actions for PointUploadSummaryReport model.
 */
class PointUploadSummaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PointUploadSummaryReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new PointUploadSummaryReportSearch();        
        $searchModel->clientID = $clientID;
        $searchModel->status = 'F';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTrash()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new PointUploadSummaryReportSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'X';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('trash', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSynchronize()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $errorMessage = "";
        $remark = '';

        $points = \common\models\PointUploadSummaryReport::find()
            ->where(['clientID' => $clientID, 'status' => 'F'])
            ->all();
        
        if(count($points) > 0) {
            foreach($points as $userpoint) {
                $pointsupdate = \common\models\PointUploadSummaryReport::findOne($userpoint->id);
                
                $mainlist = \common\models\PointUploadSummary::find()      
                ->where(['clientID'=>$clientID])
                ->andWhere('point_upload_summary_id = :point_upload_summary_id', [':point_upload_summary_id' => $pointsupdate->point_upload_summary_id]) 
                ->one();
                
                $clients_ref_no = $userpoint->dealer_id;
                $countUsers = \common\models\VIPCustomer::find()    
                    ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no])               
                    ->count();
                if ($countUsers == 1) {
                    $userlisit = \common\models\VIPCustomer::find()     
                        ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no]) 
                        ->one();

                        $pointawerd = new \common\models\VIPCustomerReward();
                        $pointawerd->clientID = $clientID;
                        $pointawerd->customer_id = $userlisit->userID;
                        $pointawerd->point_upload_summary_id = $pointsupdate->point_upload_summary_id;
                        $pointawerd->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                        $pointawerd->description = $mainlist->description;
                        $pointawerd->points = $pointsupdate->points;
                        $pointawerd->bb_type = 'A';
                        $pointawerd->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                        $pointawerd->save(false);
                        
                        if($userpoint->points2 > 0){
                            $accountpoint2 = new \common\models\VIPCustomerAccount2();
                            $accountpoint2->clientID = $clientID;
                            $accountpoint2->customer_id = $pointawerd->customer_id;
                            $accountpoint2->indirect_id = 0;
                            $accountpoint2->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                            $accountpoint2->description = $mainlist->description;
                            $accountpoint2->points_in = $pointsupdate->points2;
                            $accountpoint2->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                            $accountpoint2->save(false);
                        }
                        $remark = 'Point Awarded - (Update)';
                        $status = 'S';
                        $errorMessage = $errorMessage . "Point Awarded to (" . $clients_ref_no . ")<BR>";
                }else {
                    $remark = 'Dealer ID do not exist';
                    $status = 'F';
                    $errorMessage = $errorMessage . "Dealer ID do not exist<BR>";
                }

                $pointsupdate->status_remark = $remark;
                $pointsupdate->status = $status;
                $pointsupdate->save(false);
            }
        }
        return $this->redirect(['/sales/point-upload']);
        //return $errorMessage;
    }
    
    public function actionBulk()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $vstatus= Yii::$app->request->post('status');
        $msg = '';
        if(count($selection) > 0){
            if($vstatus == 'X') {
               $model = \common\models\PointUploadSummaryReport::updateAll(['status' => 'X'], ['IN', 'id', $selection]); 
               \Yii::$app->getSession()->setFlash('success',['title' => 'Bulk Success', 'text' => 'Acction Sccessfully']);
                return $this->redirect(['/sales/point-upload-summary']);
            }else {
                foreach($selection as $valueid){
                    $pointsupdate = \common\models\PointUploadSummaryReport::findOne($valueid);
                    $mainlist = \common\models\PointUploadSummary::find()      
                    ->where(['clientID'=>$clientID])
                    ->andWhere('point_upload_summary_id = :point_upload_summary_id', [':point_upload_summary_id' => $pointsupdate->point_upload_summary_id]) 
                    ->one();

                    $clients_ref_no = $pointsupdate->dealer_id;
                    $countUsers = \common\models\VIPCustomer::find()    
                    ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no])               
                    ->count();
                    
                    if ($countUsers == 1) {
                        $userlisit = \common\models\VIPCustomer::find()     
                            ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no]) 
                            ->one();

                            $pointawerd = new \common\models\VIPCustomerReward();
                            $pointawerd->clientID = $clientID;
                            $pointawerd->customer_id = $userlisit->userID;
                            $pointawerd->point_upload_summary_id = $pointsupdate->point_upload_summary_id;
                            $pointawerd->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                            $pointawerd->description = $mainlist->description;
                            $pointawerd->points = $pointsupdate->points;
                            $pointawerd->bb_type = 'A';
                            $pointawerd->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                            $pointawerd->save(false);

                            if($userpoint->points2 > 0){
                                $accountpoint2 = new \common\models\VIPCustomerAccount2();
                                $accountpoint2->clientID = $clientID;
                                $accountpoint2->customer_id = $pointawerd->customer_id;
                                $accountpoint2->indirect_id = 0;
                                $accountpoint2->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                                $accountpoint2->description = $mainlist->description;
                                $accountpoint2->points_in = $pointsupdate->points2;
                                $accountpoint2->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                                $accountpoint2->save(false);
                            }
                            $remark = 'Point Awarded - (Update)';
                            $status = 'S';
                            //$errorMessage = $errorMessage . "Point Awarded to (" . $clients_ref_no . ")<BR>";
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Point Awarded to <b>('.$clients_ref_no.')</b></li>';
                    }else {
                        $remark = 'Dealer ID do not exist';
                        $status = 'F';
                        //$errorMessage = $errorMessage . "Dealer ID do not exist<BR>";
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Profile Not Found this Dealer ID <b>('.$clients_ref_no.')</b></li>';
                    }

                    $pointsupdate->status_remark = $remark;
                    $pointsupdate->status = $status;
                    $pointsupdate->save(false);
                } // End Foreach
                
                return $this->render('uploadexcelreport', [
                    'msg' => $msg,
                ]);
            }
        }
    }
    
    public function actionBulkTrash()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $vstatus= Yii::$app->request->post('status');

        if(count($selection) > 0){
            if($vstatus == 'F') {
               $model = \common\models\PointUploadSummaryReport::updateAll(['status' => 'F'], ['IN', 'id', $selection]); 
            }else {
                $model = \common\models\PointUploadSummaryReport::deleteAll(['IN', 'id', $selection]); 
            }
            \Yii::$app->getSession()->setFlash('success',['title' => 'Bulk Success', 'text' => 'Acction Sccessfully']);
        }        
        return $this->redirect(['/sales/point-upload-summary/trash']);
    }

    /**
     * Displays a single PointUploadSummaryReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PointUploadSummaryReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PointUploadSummaryReport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PointUploadSummaryReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PointUploadSummaryReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PointUploadSummaryReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PointUploadSummaryReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PointUploadSummaryReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
