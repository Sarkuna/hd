<?php

namespace app\modules\sales\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use common\models\PointUploadSummary;
use common\models\PointUploadSummarySearch;
use common\models\VIPCustomerReward;
use common\models\EmailQueue;
use common\models\PointUploadSummaryReport;
use common\models\PointUploadSummaryReportSearch;
use common\models\VIPCustomerAccount2;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;

/**
 * PointUploadController implements the CRUD actions for PointUploadSummary model.
 */
class PointUploadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }

    /**
     * Lists all PointUploadSummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new PointUploadSummarySearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PointUploadSummary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new PointUploadSummaryReportSearch();
        $searchModel->clientID = $clientID;
        $searchModel->point_upload_summary_id = $id;
        $dataProvider = $searchModel->searchview(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDownload($id) {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        //$columns = array('No','name', 'house_no', 'purpose', 'time_come_in_real', 'time_come_out_real', 'visit_duration', 'sign_off');
        $dataVisits = array();
        
        
        $pointsummarylists = \common\models\PointUploadSummaryReport::find()
        ->where(['clientID' => $clientID, 'point_upload_summary_id' => $id])
        ->orderBy(['id' => SORT_ASC])
        ->asArray()
        ->all();
        
        /*echo '<pre>';
        print_r($productlists);
        die;*/
        
        $numRow = 1;
        //$visit_temp1 = array('hgjhgjhgjhgjhg');
        //$dataVisits[] = $visit_temp1;
        
        foreach($pointsummarylists as $key=>$pointsummarylist){
            
            $statusid = $pointsummarylist['status'];
            if($statusid == 'N') {
                $status = 'Nothing';
            }else if($statusid == 'S') {
                $status = 'Success';
            }else if($statusid == 'F') {
                $status = 'Fail';
            }else {
                $status = 'N/A';
            }
            
            $visit_temp = array(
                    'no' => $numRow++,
                    'Dealer Code' => $pointsummarylist['dealer_id'],                    
                    'Account 1' => $pointsummarylist['points'],
                    'Account 2' => $pointsummarylist['points2'],
                    'Status Remark' => $pointsummarylist['status_remark'],
                    'Status' => $status,
                );
            $dataVisits[] = $visit_temp;
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Invoice',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                /*'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                'headerColumnCssClass' => array(
                    'id' => ExportExcelUtil::getCssClass('blue'),
                    'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                ), //define each column's cssClass for header line only.  You can set as blank.
                'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                'hightLight' => ExportExcelUtil::getCssClass("highlight"),*/
            ),/*array(
                'sheet_name' => 'Important Note',
                'sheet_title' => array("Important Note For Region Template"),
                'ceils' => array(
                    array("1.Column Platform,Part,Region must need update.")
                , array("2.Column Regional_Status only as Regional_Green,Regional_Yellow,Regional_Red,Regional_Ready.")
                , array("3.Column RTS_Date, Master_Desc, Functional_Desc, Commodity, Part_Status are only for your reference, will not be uploaded into NPI tracking system."))
            ),*/
            //sheet 2,....            
        );
        
        $excel_file = "Point_Upload_Summary";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionGuide()
    {
        return $this->render('uploadexcel');
    }

    /**
     * Creates a new PointUploadSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $point = 0;
        $remark = '';

        $model = new PointUploadSummary();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            //if ($model->excel && $model->validate()) {
                $filename = $clientID.'_'.date('dmyHis').'_'.$model->excel->baseName;
                $model->excel->saveAs(Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);

                //pre-defined value
                $pre_defined = array( "clients ref no", "account 1");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));
                
                if($checkmatch > 1){
                    $data = array();
                    $model->clientID = $clientID;
                    if($model->save()) {
                        for ($row = 2; $row <= $highestRow; ++ $row) {
                            $myheadings = array_map('strtolower', $headings[0]);
                            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                            $rowData[0] = array_combine($myheadings, $rowData[0]);
                            
                            $dealerID = $rowData[0]['clients ref no'];
                            $point1 = round($rowData[0]['account 1']);
                            $point2 = round($rowData[0]['account 2']);

                            $data[] = [$clientID,$model->point_upload_summary_id,$dealerID,$point1,$point2];
                        }
                        Yii::$app->db
                        ->createCommand()
                        ->batchInsert('point_upload_summary_report', ['clientID','point_upload_summary_id','dealer_id','points','points2'],$data)
                        ->execute(); 
                    }
                    
                    $returnedValue = Yii::$app->VIPglobal->importPoints($model->point_upload_summary_id);
                    unlink($inputFile);
                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Points Import Sccessfully']);
                        
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry points import fail!']);
                        //return $this->redirect(['uploadexcel']);
                    }
                    return $this->redirect(['view', 'id' => $model->point_upload_summary_id]);

                    
                }else {
                   $teml = 'Clients Ref No, Account 1';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']); 
                }           
        } else {
            return $this->render('create', [
                'model' => $model,
                'clientID' => $clientID,
            ]);
        }
    }
    
    public function actionCreateold()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $point = 0;
        $remark = '';

        $model = new PointUploadSummary();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $model->clientID = $clientID;
                //echo $model->type.'-'.$model->agreevalue.'-'.$model->acc_division;
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            $filename = $clientID.'_'.date('dmyHis').'_'.$model->excel->baseName;
                            $model->excel->saveAs(Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension);
                            $inputFile = Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension;
                            try{
                                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                                $objPHPExcel = $objReader->load($inputFile);
                            } catch (Exception $e) {
                                die('Error');
                            }

                            $sheet = $objPHPExcel->getSheet(0);
                            $highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                            $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                            for ($row = 1; $row <= $highestRow; ++ $row) {
                                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                                if ($row == 1) {
                                    continue;
                                }
                                $dealerID = $rowData[0][0];
                                $point = $rowData[0][1];
                                

                                //$model->agreevalue.'-'.$model->acc_division;
                                /*if($model->agreevalue == '2') {
                                    $percentage = $model->acc_division;
                                    $percentagetotal = ($percentage / 100) * $point;
                                    $point = $point - $percentagetotal; 
                                }*/

                                //Check the user for this dealerID
                                /*$countUsers = \common\models\User::find()
                                    ->where("client_id = '" . $clientID . "' AND username = '" . trim($dealerID) . "' AND type = '".trim($model->type)."'")
                                    ->count();*/
                                
                                $countUsers = \common\models\VIPCustomer::find()
                                ->join('LEFT JOIN', 'user AS u', 'u.id = userID')        
                                ->where(['clientID'=>$clientID])
                                ->andWhere('clients_ref_no = :clients_ref_no', [':clients_ref_no' => $dealerID])          
                                ->andWhere('type = :type', [':type' => $model->type])       
                                ->count();
                                if (($countUsers == 1)) {
                                    $userlisit = \common\models\VIPCustomer::find()
                                    ->join('LEFT JOIN', 'user AS u', 'u.id = userID')        
                                    ->where(['clientID'=>$clientID])
                                    ->andWhere('clients_ref_no = :clients_ref_no', [':clients_ref_no' => $dealerID])          
                                    ->andWhere('type = :type', [':type' => $model->type])
                                    ->one();

                                    $pointawerd = new VIPCustomerReward();
                                    $pointawerd->clientID = $clientID;
                                    $pointawerd->customer_id = $userlisit->userID;
                                    $pointawerd->point_upload_summary_id = $model->point_upload_summary_id;
                                    $pointawerd->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                                    $pointawerd->description = $model->description;
                                    $pointawerd->points = round($point);
                                    $pointawerd->bb_type = 'A';
                                    $pointawerd->date_added = date('Y-m-d', strtotime($model->effective_month));
                                    //$pointawerd->save();
                                    if (($flag = $pointawerd->save(false)) === false) {                                         
                                        $transaction->rollBack();
                                        break;
                                    }
                                    $remark = '';
                                    $status = 'S';
                                }else {
                                    $remark = 'Dealer ID do not exist';
                                    $status = 'F';
                                }
                                
                                if($model->agreevalue == '2') {
                                    $accountpoint2 = new VIPCustomerAccount2();
                                    $accountpoint2->clientID = $clientID;
                                    $accountpoint2->customer_id = $pointawerd->customer_id;
                                    $accountpoint2->indirect_id = 0;
                                    //$accountpoint2->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                                    $accountpoint2->description = $model->description;
                                    $accountpoint2->points_in = round($point);
                                    $accountpoint2->date_added = date('Y-m-d', strtotime($model->effective_month));
                                    $accountpoint2->save(false);
                                }
                                
                                $pointsummery = new PointUploadSummaryReport();
                                $pointsummery->clientID = $clientID;
                                $pointsummery->point_upload_summary_id = $model->point_upload_summary_id;
                                $pointsummery->dealer_id = $dealerID;
                                $pointsummery->points = $point;
                                $pointsummery->status_remark = $remark;
                                $pointsummery->status = $status;
                                $pointsummery->save(false);
                                
                                
                            }
                            //End Excel Row checking Loop
                        }                        
                    }
                    
                    if ($flag) {
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
                        /*$data = \yii\helpers\Json::encode(array(
                            'password' => $generatedPassword,
                        ));
                        $returnedValue = Yii::$app->vip->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                        return $this->redirect(['index']);*/
                        return $this->redirect(['view', 'id' => $model->point_upload_summary_id]);
                    }
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }         
            }
            \Yii::$app->getSession()->setFlash('warning', ['title' => 'Fail', 'text' => 'Oops no changes!']);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'clientID' => $clientID,
            ]);
        }
    }

    /**
     * Updates an existing PointUploadSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                $expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                $description = $model->description;
                VIPCustomerReward::updateAll(['expiry_date' => $expiry_date, 'description' => $description], 'point_upload_summary_id = '.$id.'');
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified point!']);
            }else {
                \Yii::$app->getSession()->setFlash('warning', ['title' => 'Fail', 'text' => 'Oops no changes!']);
            }
            return $this->redirect(['view', 'id' => $model->point_upload_summary_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSendsms()
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);

        
        if(!empty($client->sms_username)) {
            $username = $client->sms_username;
            $password = $client->sms_password;
        }else {
            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
        }
        
        foreach (\common\models\SMSQueue::find()->where("status = 'N' AND (mobile IS NOT NULL)")->limit(300)->all() as $smsQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $smsTemplate = \common\models\SMSTemplate::findOne($smsQueue->sms_template_id);

            $smsBody = $smsTemplate->template;
            $mobile = str_replace(' ','',$smsQueue->mobile);  

            $data2 = \yii\helpers\Json::decode($smsQueue->data);

            $name = isset($data2['name']) ? $data2['name'] : null;
            $point = isset($data2['point']) ? $data2['point'] : null;
            $invoice_no = isset($data2['invoice_no']) ? $data2['invoice_no'] : null;
            $effective_month = isset($data2['effective_month']) ? $data2['effective_month'] : null;
            $expired_point = isset($data2['expired_point']) ? $data2['expired_point'] : null;
            $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
            $expired_date = isset($data2['expired_date']) ? $data2['expired_date'] : null;
            $today_date = isset($data2['today_date']) ? $data2['today_date'] : null;
            
            if(!empty($name)){
                $name = explode(' ',trim($name));
                $name = $name[0]; 
            }

            $smsBody = str_replace('{name}', $name, $smsBody);
            $smsBody = str_replace('{point}', $point, $smsBody);
            $smsBody = str_replace('{invoice}', $invoice_no, $smsBody);
            $smsBody = str_replace('{effective_month}', $effective_month, $smsBody);
            $smsBody = str_replace('{expired_point}', $expired_point, $smsBody);
            $smsBody = str_replace('{balance_point}', $balance_point, $smsBody);
            $smsBody = str_replace('{expired_date}', $expired_date, $smsBody);
            $smsBody = str_replace('{today_date}', $today_date, $smsBody);
            //$smsBody = str_replace('{client_name}', $client_name, $smsBody);        

            $message = $smsBody;

            $destination = $mobile;        
            $sender_id = urlencode("66300");    
            $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
            $message = urlencode($message);
            $type = '1';
            $agreedterm = 'YES';


                  
            $fp = "https://www.isms.com.my/isms_send_all.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id&agreedterm=$agreedterm";
            $handle = @fopen($fp, "r");
            if ($handle) {
              while (!feof($handle)) {
                  $buffer = fgets($handle, 10000);
                  $st = $buffer;
              }
            }
            
            if($st == '2000 = SUCCESS' || $st == 'EMPTY/BLANK'){
                $status = "S";
            }else {
                $status = "N";
            }
            
            $emailQueueToUpdate = \common\models\SMSQueue::findOne($smsQueue->id);
            $emailQueueToUpdate->status = $status;
            $emailQueueToUpdate->date_to_send = date('Y-m-d');
            $emailQueueToUpdate->remark = $st;
            $emailQueueToUpdate->save(false);
        }
    }

    /**
     * Deletes an existing PointUploadSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PointUploadSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PointUploadSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PointUploadSummary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
