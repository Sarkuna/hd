<?php

namespace app\modules\sales\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use common\models\VIPCustomerReward;
use common\models\VIPCustomerRewardSearch;
use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;


/**
 * PointsController implements the CRUD actions for VIPCustomerReward model.
 */
class PointsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPCustomerReward models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $awarded_points = \common\models\VIPCustomerReward::find()
        ->where(['>', 'points', 0])
        ->andWhere(['clientID'=>$clientID])
        ->sum('points');
        
        $distributors = \common\models\VIPCustomerReward::find()
        ->joinWith(['customer'])        
        ->where(['>', 'vip_customer_reward.points', 0])
        ->andWhere(['vip_customer_reward.clientID'=>$clientID])
        ->andWhere(['vip_customer.kis_category_id'=>'3'])
        ->sum('vip_customer_reward.points');
        
        $direct_dealer= \common\models\VIPCustomerReward::find()
        ->joinWith(['customer'])        
        ->where(['>', 'vip_customer_reward.points', 0])
        ->andWhere(['vip_customer_reward.clientID'=>$clientID])
        ->andWhere(['vip_customer.kis_category_id'=>'2'])
        ->sum('vip_customer_reward.points');
        
        $indirect = \common\models\VIPCustomerReward::find()
        ->joinWith(['customer'])        
        ->where(['>', 'vip_customer_reward.points', 0])
        ->andWhere(['vip_customer_reward.clientID'=>$clientID])
        ->andWhere(['vip_customer.kis_category_id'=>'4'])
        ->sum('vip_customer_reward.points');
        
        $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['<', 'points', 0])
        ->andWhere(['clientID'=>$clientID])
        ->sum('points');
        
        $redeemed_points = str_replace('-', '', $totalminus);
        
        $expired_points = \common\models\VIPCustomerReward::find()
        ->where('expiry_date < NOW()')
        //->andWhere('publish_on_date <= NOW()');        
        ->andWhere(['clientID'=>$clientID])
        ->sum('points');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'awarded_points' => $awarded_points,
            'redeemed_points' => $redeemed_points,
            'expired_points' => $expired_points,
            'distributors' => $distributors,
            'direct_dealer' => $direct_dealer,
            'indirect' => $indirect
            
        ]);
    }
    
    public function actionIndexp()
    {
        $searchModel = new VIPCustomerRewardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPCustomerReward model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPCustomerReward model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPCustomerReward();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_reward_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPCustomerReward model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_reward_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPCustomerReward model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPCustomerReward model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPCustomerReward the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPCustomerReward::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
