<?php

namespace app\modules\sales\controllers;

use Yii;
use common\models\VIPCustomer;
use common\models\VIPCustomerAddress;
use common\models\VIPCustomerAddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AddressController implements the CRUD actions for VIPCustomerAddress model.
 */
class AddressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPCustomerAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPCustomerAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPCustomerAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPCustomerAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPCustomerAddress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionAdd($userid)
    {
        $modelcus = $this->findModelcustomer($userid);
        $model = new VIPCustomerAddress();

        if ($model->load(Yii::$app->request->post())) {
            //return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
            $model->userID = $modelcus->userID;
            $model->clientID = $modelcus->clientID;
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                return $this->redirect(['/sales/customers/view', 'id' => $modelcus->vip_customer_id, '#' => "final_decision"]);
            }else {
                print_r($model->getErrors());
            }
        } else {
            return $this->render('create_add', [
                'model' => $model,
                'modelcus' => $modelcus
            ]);
        }
    }
    
    public function actionEdit($userid)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPCustomerAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionEditaddress($id)
    {
        $model = $this->findModel($id);
        $modelcus = $this->findModelcustomer($model->userID);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
            return $this->redirect(['/sales/customers/view', 'id' => $modelcus->vip_customer_id]);
            //return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
        } else {
            return $this->render('create_add', [
                'model' => $model,
                'modelcus' => $modelcus
            ]);
        }
    }
    
    function actionRegion($id){
        $states = \common\models\VIPStates::find()->where(['country_id' => $id])->all();
        if ($states) {
            foreach ($states as $state) {
                echo "<option value='" . $state->states_id . "'>" . $state->state_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }

    /**
     * Deletes an existing VIPCustomerAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteaddress($id)
    {
        $model = $this->findModel($id);
        $modelcus = $this->findModelcustomer($model->userID);
        if(count($modelcus) > 0){
            $this->findModel($id)->delete();
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
            return $this->redirect(['/sales/customers/view', 'id' => $modelcus->vip_customer_id]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    

    /**
     * Finds the VIPCustomerAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPCustomerAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPCustomerAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelcustomer($userid)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];


        if(($model = VIPCustomer::findOne(['userID' => $userid, 'clientID' => $clientID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
