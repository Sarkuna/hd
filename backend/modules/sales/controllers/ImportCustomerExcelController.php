<?php

namespace app\modules\sales\controllers;

use Yii;
use common\models\ImportCustomerExcelSummary;
use common\models\ImportCustomerExcelSummarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ImportCustomerExcelController implements the CRUD actions for ImportCustomerExcelSummary model.
 */
class ImportCustomerExcelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImportCustomerExcelSummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new ImportCustomerExcelSummarySearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGuide()
    {
        return $this->render('uploadexcel');
    }
    
    public function actionUploadexcel() {
        
        $session = Yii::$app->session;
        $clientID = 16;
        $model = new \backend\models\ExcelFormDb();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "clients ref no", "company name", "email");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch > 2){
                    $data = array();
                    $modelimport = new ImportCustomerExcelSummary();
                    $modelimport->clientID = $clientID;
                    $modelimport->account_type = $model->account_type;
                    $modelimport->description = $model->description;  
                    if($modelimport->save()) {                        
                        for ($row = 2; $row <= $highestRow; ++ $row) {
                            $myheadings = array_map('strtolower', $headings[0]);
                            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                            $rowData[0] = array_combine($myheadings, $rowData[0]);

                            $data[] = [$modelimport->import_customer_upload_summary_id,$rowData[0]['clients ref no']/*,$rowData[0]['distributor code']*/,$rowData[0]['company name'],$rowData[0]['email'],$model->account_type,$rowData[0]['full name'],$rowData[0]['mobile number'],$rowData[0]['company address'],'P',date('Y-m-d')];

                        }
                        Yii::$app->db
                        ->createCommand()
                        ->batchInsert('import_customer_excel', ['import_customer_upload_summary_id','user_code'/*,'distributor_code'*/,'company_name', 'email','account_type','full_name','mobile','company_address','status','date_import'],$data)
                        ->execute();  
                    }
                    
                    $returnedValue = Yii::$app->VIPglobal->importCustomer($modelimport->import_customer_upload_summary_id);
                    unlink($inputFile);
                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Users Import Sccessfully']);
                        
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry users import fail!']);
                        //return $this->redirect(['uploadexcel']);
                    }
                    return $this->redirect(['view', 'id' => $modelimport->import_customer_upload_summary_id]);
                    /*return $this->render('uploadexcelreport', [
                        'msg' => $returnedValue,
                    ]);*/
                    
                }else {
                    $teml = 'Clients Ref No, Company Name, Email Address';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    return $this->redirect(['uploadexcel']);
                }
            }
        } else {
            return $this->render('uploadexcel_form', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single ImportCustomerExcelSummary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new \common\models\ImportCustomerExcelSearch();
        $searchModel->import_customer_upload_summary_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ImportCustomerExcelSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImportCustomerExcelSummary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->import_customer_upload_summary_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImportCustomerExcelSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->import_customer_upload_summary_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ImportCustomerExcelSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImportCustomerExcelSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImportCustomerExcelSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImportCustomerExcelSummary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
