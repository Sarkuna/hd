<?php

namespace app\modules\sales\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use common\models\PointUploadSummary;
use common\models\PointUploadSummarySearch;
use common\models\VIPCustomerReward;
use common\models\EmailQueue;
use common\models\PointUploadSummaryReport;
use common\models\PointUploadSummaryReportSearch;
use common\models\VIPCustomerAccount2;

/**
 * PointUploadController implements the CRUD actions for PointUploadSummary model.
 */
class PointUploadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PointUploadSummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointUploadSummarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PointUploadSummary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new PointUploadSummaryReportSearch();
        $searchModel->clientID = $clientID;
        $searchModel->point_upload_summary_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new PointUploadSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $point = 0;
        $remark = '';

        $model = new PointUploadSummary();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $model->clientID = $clientID;
                //echo $model->type.'-'.$model->agreevalue.'-'.$model->acc_division;
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            $filename = $clientID.'_'.date('dmyHis').'_'.$model->excel->baseName;
                            $model->excel->saveAs(Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension);
                            $inputFile = Yii::getAlias('@webroot').'/upload/pointuploadsummary/' . $filename . '.' . $model->excel->extension;
                            try{
                                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                                $objPHPExcel = $objReader->load($inputFile);
                            } catch (Exception $e) {
                                die('Error');
                            }

                            $sheet = $objPHPExcel->getSheet(0);
                            $highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                            $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                            for ($row = 1; $row <= $highestRow; ++ $row) {
                                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                                if ($row == 1) {
                                    continue;
                                }
                                $dealerID = $rowData[0][0];
                                $point = $rowData[0][1];
                                

                                //$model->agreevalue.'-'.$model->acc_division;
                                if($model->agreevalue == '2') {
                                    $percentage = $model->acc_division;
                                    $percentagetotal = ($percentage / 100) * $point;
                                    $point = $point - $percentagetotal; 
                                }

                                //Check the user for this dealerID
                                /*$countUsers = \common\models\User::find()
                                    ->where("client_id = '" . $clientID . "' AND username = '" . trim($dealerID) . "' AND type = '".trim($model->type)."'")
                                    ->count();*/
                                
                                $countUsers = \common\models\VIPCustomer::find()
                                ->join('LEFT JOIN', 'user AS u', 'u.id = userID')        
                                ->where(['clientID'=>$clientID])
                                ->andWhere('clients_ref_no = :clients_ref_no', [':clients_ref_no' => $dealerID])          
                                ->andWhere('type = :type', [':type' => $model->type])       
                                ->count();
                                if (($countUsers == 1)) {
                                    $userlisit = \common\models\VIPCustomer::find()
                                    ->join('LEFT JOIN', 'user AS u', 'u.id = userID')        
                                    ->where(['clientID'=>$clientID])
                                    ->andWhere('clients_ref_no = :clients_ref_no', [':clients_ref_no' => $dealerID])          
                                    ->andWhere('type = :type', [':type' => $model->type])
                                    ->one();

                                    $pointawerd = new VIPCustomerReward();
                                    $pointawerd->clientID = $clientID;
                                    $pointawerd->customer_id = $userlisit->userID;
                                    $pointawerd->point_upload_summary_id = $model->point_upload_summary_id;
                                    $pointawerd->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                                    $pointawerd->description = $model->description;
                                    $pointawerd->points = round($point);
                                    $pointawerd->bb_type = 'A';
                                    $pointawerd->date_added = date('Y-m-d', strtotime($model->effective_month));
                                    //$pointawerd->save();
                                    if (($flag = $pointawerd->save(false)) === false) {                                         
                                        $transaction->rollBack();
                                        break;
                                    }
                                    $remark = '';
                                    $status = 'S';
                                }else {
                                    $remark = 'Dealer ID do not exist';
                                    $status = 'F';
                                }
                                
                                if($model->agreevalue == '2') {
                                    $accountpoint2 = new VIPCustomerAccount2();
                                    $accountpoint2->clientID = $clientID;
                                    $accountpoint2->customer_id = $pointawerd->customer_id;
                                    $accountpoint2->indirect_id = 0;
                                    //$accountpoint2->expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                                    $accountpoint2->description = $model->description;
                                    $accountpoint2->points_in = round($percentagetotal);
                                    $accountpoint2->date_added = date('Y-m-d', strtotime($model->effective_month));
                                    $accountpoint2->save(false);
                                }
                                
                                $pointsummery = new PointUploadSummaryReport();
                                $pointsummery->clientID = $clientID;
                                $pointsummery->point_upload_summary_id = $model->point_upload_summary_id;
                                $pointsummery->dealer_id = $dealerID;
                                $pointsummery->points = $point;
                                $pointsummery->status_remark = $remark;
                                $pointsummery->status = $status;
                                $pointsummery->save(false);
                                
                                
                            }
                            //End Excel Row checking Loop
                        }                        
                    }
                    
                    if ($flag) {
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
                        /*$data = \yii\helpers\Json::encode(array(
                            'password' => $generatedPassword,
                        ));
                        $returnedValue = Yii::$app->vip->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                        return $this->redirect(['index']);*/
                        return $this->redirect(['view', 'id' => $model->point_upload_summary_id]);
                    }
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }         
            }
            \Yii::$app->getSession()->setFlash('warning', ['title' => 'Fail', 'text' => 'Oops no changes!']);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'clientID' => $clientID,
            ]);
        }
    }

    /**
     * Updates an existing PointUploadSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                $expiry_date = date('Y-m-d', strtotime($model->expiry_date));
                $description = $model->description;
                VIPCustomerReward::updateAll(['expiry_date' => $expiry_date, 'description' => $description], 'point_upload_summary_id = '.$id.'');
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified point!']);
            }else {
                \Yii::$app->getSession()->setFlash('warning', ['title' => 'Fail', 'text' => 'Oops no changes!']);
            }
            return $this->redirect(['view', 'id' => $model->point_upload_summary_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PointUploadSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PointUploadSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PointUploadSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PointUploadSummary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
