<?php

namespace app\modules\sales\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use common\models\VIPCustomerReward;
use common\models\VIPCustomerRewardSearch;
use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;


/**
 * PointsController implements the CRUD actions for VIPCustomerReward model.
 */
class PointsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPCustomerReward models.
     * @return mixed
     */
    public function actionIndex($type)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->type = $type;
        $dataProvider = $searchModel->searchpoints(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
        
        $awarded_points = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'A'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type])
        //->andWhere(['=', 'user.status', 'A']); 
        ->andWhere(['u.status'=>'A'])
        //->andWhere($type.' = :'.$action, [':'.$action => 1])        
        ->sum('points');

        
        $totalminus = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'V'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type]) 
        ->andWhere(['u.status'=>'A'])        
        ->sum('points');
        
        $redeemed_points = str_replace('-', '', $totalminus);
        
        $expired_points = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'E'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type])
        ->andWhere(['u.status'=>'A'])
        ->sum('points');
        $expired_points = str_replace('-', '', $expired_points);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'awarded_points' => $awarded_points,
            'redeemed_points' => $redeemed_points,
            'expired_points' => $expired_points,
            
        ]);
    }
    
    
    
    public function actionDetailedPoints()
    {
        $this->layout = '/main_krajee';
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        //$searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        

        return $this->render('detailed_points', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,            
        ]);
    }
    
    public function actionTest($type)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        
        $awarded_points = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'A'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type])
        ->andWhere(['u.status'=>'A'])      
        ->sum('points');

        
        $totalminus = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'V'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type])
        ->andWhere(['u.status'=>'A'])        
        ->sum('points');
        
        $redeemed_points = str_replace('-', '', $totalminus);
        
        $expired_points = \common\models\VIPCustomerReward::find()
        ->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['bb_type' => 'E'])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere('type = :type', [':type' => $type])
        ->andWhere(['u.status'=>'A'])        
        ->sum('points');
        $expired_points = str_replace('-', '', $expired_points);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'awarded_points' => $awarded_points,
            'redeemed_points' => $redeemed_points,
            'expired_points' => $expired_points,
            
        ]);
    }
    
    public function actionSubAccount()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new \common\models\VIPCustomerAccount2Search();
        $searchModel->clientID = $clientID;
        $searchModel->indirect_id = 0;
        //$searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $awarded_points = \common\models\VIPCustomerAccount2::find()
        //->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['>', 'points_in', 0])
        ->andWhere(['clientID'=>$clientID, 'indirect_id' => 0])
        //->andWhere('type = :type', [':type' => $type])
        //->andWhere($type.' = :'.$action, [':'.$action => 1])        
        ->sum('points_in');

        
        $totalminus = \common\models\VIPCustomerAccount2::find()
        //->join('LEFT JOIN', 'user AS u', 'u.id = customer_id')        
        ->where(['<', 'points_in', 0])
        ->andWhere(['clientID'=>$clientID])
        ->andWhere(['!=', 'indirect_id', 0])        
        ->sum('points_in');
        
        $redeemed_points = str_replace('-', '', $totalminus);
        
        $expired_points = \common\models\VIPCustomerReward::find()
        ->where('expiry_date < NOW()')
        //->andWhere('publish_on_date <= NOW()');        
        ->andWhere(['clientID'=>$clientID])
        ->sum('points');

        return $this->render('sub-account', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'awarded_points' => $awarded_points,
            'redeemed_points' => $redeemed_points,
            
        ]);
    }
    
    public function actionIndexp()
    {
        $searchModel = new VIPCustomerRewardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionUploadexcel() {
        
        $session = Yii::$app->session;
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "clients ref no", "company name", "email");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                
                if($checkmatch > 2){
                    $data = array();

                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        
                        $data[] = [$rowData[0]['product name'],$rowData[0]['links'],$rowData[0]['product code'],$rowData[0]['suppliers product code (isku)'],$rowData[0]['variant'],$rowData[0]['msrp'],$rowData[0]['partner price'],$deliverydata,$rowData[0]['manufacturer id'],$rowData[0]['category id'],$rowData[0]['sub category id'],date('Y-m-d')];

                    }
                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('product_excel_upload', ['product_name','links', 'product_code','sp_code','variant','msrp','partner_price','delivery','manufacturer','category','sub_category','create_date'],$data)
                    ->execute();

                    $returnedValue = Yii::$app->VIPglobal->importProductUpload();

                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Product Import Sccessfully']);
                        //return $this->redirect(['uploadexcel']);
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry product import fail!']);
                    }
                    unlink($inputFile);
                }else {
                    $teml = 'Clients Ref No, Company Name, Email Address';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }

                return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single VIPCustomerReward model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPCustomerReward model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPCustomerReward();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_reward_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPCustomerReward model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_reward_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPCustomerReward model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPCustomerReward model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPCustomerReward the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPCustomerReward::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
