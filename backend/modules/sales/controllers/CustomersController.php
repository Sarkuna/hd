<?php

namespace app\modules\sales\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use YoHang88\LetterAvatar\LetterAvatar;

use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;
use common\models\VIPCustomerAddress;
use common\models\VIPCustomerAddressSearch;
use common\models\VIPCustomerBank;
use common\models\VIPCustomerBankSearch;
use common\models\User;
use common\models\VIPZone;
use common\models\VIPCustomerReward;
use common\models\VIPCustomerRewardSearch;
use common\models\EmailQueue;
use common\models\AuthAssignment;
use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\CompanyInformation;

/**
 * CustomersController implements the CRUD actions for VIPCustomer model.
 */
class CustomersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $session = Yii::$app->session;

        if (parent::beforeAction($action)) {
            
            if (in_array($action->id, array('view','approvel','view-avatar'))) {
                if (!empty($_GET['id'])) {
                    $numRows = VIPCustomer::find()
                            ->where([
                                'clientID' => $session['currentclientID'],
                                'vip_customer_id' => $_GET['id']
                            ])
                            ->count();
                    if ($numRows == 0)
                        throw new ForbiddenHttpException('You are trying to view or modify restricted record.', 403);
                }
            }
            
            if (in_array($action->id, array('resend-email-verify','email-verify'))) {
                if (!empty($_GET['id'])) {
                    $numRows = User::find()
                            ->where([
                                'client_id' => $session['currentclientID'],
                                'id' => $_GET['id']
                            ])
                            ->count();
                    if ($numRows == 0)
                        throw new ForbiddenHttpException('You are trying to view or modify restricted record.', 403);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all VIPCustomer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'P';
        $searchModel->email_verification = 'Y';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $this->userType();
        
        /*$active_members = User::find()->where(['client_id' => $clientID, 'status' => 'A', 'user_type' => 'D', 'cart_user' => 0])->count();
        $deactive_members = User::find()->where(['client_id' => $clientID, 'status' => 'D', 'user_type' => 'D', 'cart_user' => 0])->count();
        $no_emails = User::find()->where(['client_id' => $clientID, 'status' => 'A', 'user_type' => 'D', 'cart_user' => 0, 'email' => null])->count();*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
        ]);
    }
    
    public function actionSearch()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = 0;
        $searchModel->cartuser = '999';
        if ($searchModel->load(Yii::$app->request->post())) {
            $searchModel->clientID = $clientID;
        }else {
            $searchModel->clientID = 0;
        }
        $dataProvider = $searchModel->searchall(Yii::$app->request->queryParams);
        return $this->render('all_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'model' => $model,
        ]);
    }
    
    
    
    public function actionEmailNotVerified()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'P';
        $searchModel->email_verification = 'N';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $this->userType();
  
        return $this->render('emailnotverified', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
        ]);
    }
    
    public function actionEmailNotVerifiedDelete()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'P';
        $searchModel->email_verification = 'N';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $this->userType();
        
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        if(!empty($selection)) {
            foreach($selection as $valueid){
                $model = $this->findModel($valueid);
                //echo $valueid.'-'.$model->userID.'<br>';
                AuthAssignment::find()->where(['user_id' =>$model->userID])->one()->delete();
                //VIPCustomerAddress::find()->where(['userID' =>$model->userID])->one()->delete();
                User::find()->where(['id' =>$model->userID])->one()->delete();
                $this->findModel($valueid)->delete();
            }
            return $this->redirect(['email-not-verified-delete']);
        }else {  
            return $this->render('emailnotverifieddel', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'users' => $users,
            ]);
        }
    }
    
    public function actionApprove()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'A';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $this->userType();
        
        return $this->render('approve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
        ]);
    }
    
    public function actionDeactive()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'D';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $users = $this->userType();
        
        return $this->render('deactive', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
        ]);
    }
    
    public function actionTrash()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->status = 'X';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $this->userType();
        
        return $this->render('trash', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
        ]);
    }
    
    

    /**
     * Displays a single VIPCustomer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $address = VIPCustomerAddress::find()->where(['userID' => $model->userID, 'clientID' => $model->clientID])->all();
        $bankslisit = VIPCustomerBank::find()->where(['userID' => $model->userID, 'clientID' => $model->clientID])->all();

        $searchrewardModel = new VIPCustomerRewardSearch();
        $searchrewardModel->clientID = $model->clientID;
        $searchrewardModel->customer_id = $model->userID;
        $datarewardProvider = $searchrewardModel->search(Yii::$app->request->queryParams);
        
        $searchOrder = new VIPOrderSearch();
        $searchOrder->clientID = $model->clientID;
        $searchOrder->customer_id = $model->userID;
        $dataProviderOrder = $searchOrder->search(Yii::$app->request->queryParams);
        
        $companyid = CompanyInformation::find()->where(['user_id' => $model->userID])->one();
        if(count($companyid) > 0){
            $companyInformation = CompanyInformation::findOne($companyid->id);
        }else {
            $companyInformation = new CompanyInformation();
        }
        
        $formapprove = new \common\models\Approve();
        return $this->render('view', [
            'model' => $model,
            'address' => $address,
            'bankslisit' => $bankslisit,
            'searchrewardModel' => $searchrewardModel,
            'datarewardProvider' => $datarewardProvider,
            'searchOrder' => $searchOrder,
            'dataProviderOrder' => $dataProviderOrder,
            'companyInformation' => $companyInformation,
            'formapprove' => $formapprove,
        ]);
    }
    
    public function actionPointSummary()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $allcustomer = VIPCustomer::find()
                ->where(['clientID' => $clientID])
                ->andWhere(['not', ['mobile_no' => null]])
                ->all();
        $data = array();
        foreach($allcustomer as $customer) {
            //$model = $this->findModel($id);
            $smsTemplatecount = \common\models\SMSQueue::find()
                            ->where(['user_id' => $customer->userID])
                            ->andWhere(['like', 'created_datetime', date('Y-m-d')])
                            ->count();
            if($smsTemplatecount == 0) {
            $exppoint = Yii::$app->VIPglobal->PointsExpire($customer->userID);
            if($exppoint > 0) {
            $customer->getBalance();
            $smsTemplate = \common\models\SMSTemplate::find()
                            ->where(['code' => 'MPS03'])
                            ->one();
            
            if(!empty($customer->full_name)) {
                $arr = explode(' ',trim($customer->full_name));
                if(strlen($arr[0]) > 10) {                   
                    $newname = substr($arr[0],10);
                    $name = $newname;
                } else {
                    $name = $arr[0];
                }
            }else {
                $name = $customer->clients_ref_no;
            }

            $mobile_no = str_replace("-","",$customer->mobile_no);
            $mobile_no = str_replace(" ","",$mobile_no);
                            
            
                $jsonData = \yii\helpers\Json::encode(array(
                    'name' => $name,
                    'expired_point' => $exppoint,
                    'balance_point' => $customer->getBalance(),
                    'expired_date' => '31/12/19',
                    'today_date' => date('d/m/y'),
                ));
 
                    /*$data[] = [$smsTemplate->id,$clientID,$customer->userID,$mobile_no,$jsonData,'N',date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'1','1'];
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('sms_queue', ['sms_template_id','clientID','user_id','mobile','data','status','created_datetime','updated_datetime','created_by','updated_by'],$data)
                        ->execute();*/
                        $smsQueue = new \common\models\SMSQueue();
                        $smsQueue->sms_template_id = $smsTemplate->id;
                        $smsQueue->sms_type = 'pts_monthly_expiry';
                        $smsQueue->clientID = $clientID;
                        $smsQueue->user_id = $customer->userID;
                        $smsQueue->mobile = $mobile_no;
                        $smsQueue->data = $jsonData;
                        $smsQueue->status = 'N';
                        $smsQueue->save(false);
                }
            }
        }
        die;
    }
    
    public function actionPointSummaryEmail()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $allcustomer = VIPCustomer::find()
                ->joinWith(['user','company'])
                ->where(['clientID' => $clientID])
                ->andWhere(['not', ['user.email' => null]])
                ->andWhere(['not like', 'user.email', '@kansai.demo'])
                ->all();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                            ->where(['clientID' => $clientID, 'code' => 'MPS03'])
                            ->one();
        foreach($allcustomer as $customer) {
            $exppoint = Yii::$app->VIPglobal->PointsExpire($customer->userID);
            if($exppoint > 0) {
                $month_date = date('d/m/y');
                $expired_date = '31/12/19';
                $expired_point = $expired_date.' '.$exppoint;
                $balance_point = $customer->getBalance();

                if(!empty($customer->full_name)) {
                    $arr = explode(' ',trim($customer->full_name));
                    if(strlen($arr[0]) > 10) {                   
                        $newname = substr($arr[0],10);
                        $name = $newname;
                    } else {
                        $name = $arr[0];
                    }
                }else {
                    $name = $customer->clients_ref_no;
                }
                
                $company_name = $customer['company']['company_name'];
                if(!empty($company_name)) {
                    $company_name = $company_name;
                }else {
                    $company_name = 'N/A';
                }
                
                $jsonData = \yii\helpers\Json::encode(array(
                    'name' => $name,
                    'company_name' => $company_name,
                    'today_date' => date('d/m/y'),
                    'balance_point' => $customer->getBalance(),
                    'expired_point' => $exppoint,                    
                    'expired_date' => '31/12/19',                    
                ));
                
   
                $emailQueue = new \common\models\EmailQueue();
                $emailQueue->email_template_id = $emailTemplate->id;
                $emailQueue->clientID = $clientID;
                $emailQueue->user_id = $customer->userID;
                $emailQueue->name = $name;
                $emailQueue->email = $customer['user']['email'];
                $emailQueue->data = $jsonData;
                $emailQueue->status = 'P';
                $emailQueue->save(false);
                
                if($emailQueue->save(false)) {
                    echo 'Success - '.$customer['user']['email'];
                }else {
                    echo 'Fail - '.$customer['user']['email'];
                }
            }  
        }
        exit();
    }
    
    public function actionSendReminderEmail() {

        $siteUrl = 'http://kis.my';
        
        foreach (\common\models\EmailQueue::find()->where("status = 'P' AND (email IS NOT NULL) AND (date_to_send IS NULL OR date_to_send <= CURDATE())")->limit(100)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');
            
            $client = \common\models\Client::findOne($emailQueue->clientID);
            //$siteUrl = Yii::$app->request->hostInfo;
            $siteUrl = $client->cart_domain;
            $admin_url = $client->admin_domain;
            $client_name = $client->company;
            $client_email = $client->clientAddress->email_address;
            $client_programme_title = $client->programme_title;

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_id);

            $emailBody = $emailTemplate->template;

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);

            $name = isset($data2['name']) ? $data2['name'] : null;
            $company_name = isset($data2['company_name']) ? $data2['company_name'] : null;
            $today_date = isset($data2['today_date']) ? $data2['today_date'] : null;
            $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
            $expired_point = isset($data2['expired_point']) ? $data2['expired_point'] : null;
            $expired_date = isset($data2['expired_date']) ? $data2['expired_date'] : null;
            
            $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
            
            $emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{name}', $name, $emailSubject);
                
            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{company_name}', $company_name, $emailBody);
            $emailBody = str_replace('{today_date}', $today_date, $emailBody);
            $emailBody = str_replace('{balance_point}', $balance_point, $emailBody);
            $emailBody = str_replace('{expired_point}', $expired_point, $emailBody);
            $emailBody = str_replace('{expired_date}', $expired_date, $emailBody);

            if (!empty($emailQueue->email)) {
                Yii::$app->mailer->compose()
                    //->setTo('shihanagni@gmail.com')    
                    ->setTo($emailQueue->email)
                    ->setFrom([$client_email =>  $client_name])
                    ->setSubject($emailSubject)
                    ->setHtmlBody($emailBody)
                    ->send();

                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->status = "C";
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
                
            }

            
        }
    }
    
    
    
    public function actionAccount2($id)
    {
        //$searchOrder = new ();
        $searchOrder->clientID = $model->clientID;
        $searchOrder->customer_id = $model->userID;
        $dataProviderOrder = $searchOrder->search(Yii::$app->request->queryParams);
    }
    
    public function actionAccountTwoData() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $book_id = "";
	$output = "";
        $n= 1;
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $n = $data['rowCount'] + 1;
            $account2lists = \common\models\VIPCustomerAccount2::find()
           ->select('account2_id, description, points_in, date_added')
           ->where(['clientID'=>$clientID, 'customer_id' => $data['userid']])
           ->andWhere(['<', 'account2_id', $data['last_row']])
           ->orderBy(['date_added'=>SORT_DESC])         
           ->limit(20)
           ->asArray()
           ->all();
           if(count($account2lists) > 0) {
            foreach($account2lists as $account2list) {
                $book_id = $account2list['account2_id'];
                $output .='<tr>
                    <td>' . $n . '</td>
                    <td>' . date('d-m-y', strtotime($account2list['date_added'])) . '</td>                            
                    <td>' . $account2list['description'] . '</td>
                    <td class="text-right">' . $account2list['points_in'] . '</td>   
                    </tr>';
                $n++;
            }

            $output .= '<tr id="remove_row">
                    <td colspan="4"><center><button id="load" class="btn btn-info btn-block" data-id="' . $book_id . '">Load More</button></center></td>
            </tr>';
           } else {
               $output .= '<tr><td class="text-center" colspan="4">You reached the end</td></tr>';
           }
            echo $output;
        }
    }
    
    /**
     * Display avatar image
     * @param type $id
     * @return type
     */
    
    public function actionViewAvatar() {
        $id = Yii::$app->request->get('id');
        $size = Yii::$app->request->get('size');
        $model = $this->findModel($id);

        if(!empty($model->full_name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $model->full_name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    
    public function actionLog($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $totallog = \common\models\ActionLog::find()
        ->where(['client_id'=>$clientID, 'user_id'=>$id, 'action' => 'login'])
        ->orderBy([
            'time' => SORT_DESC,
        ])        
        ->limit(20)      
        ->all();
        return $this->render('log', [
            'totallog' => $totallog,
        ]);
    }

    /**
     * Creates a new VIPCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $model = new VIPCustomer();
        $companyInformation = new CompanyInformation();
        $model->scenario = 'create';
        
        if ($model->load(Yii::$app->request->post()) && $companyInformation->load(Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->client_id = $clientID;
            $user->username = trim($model->clients_ref_no);
            $user->email = $model->email;
            $user->status = 'A';
            $user->approved = 'N';
            $user->newsletter = $model->newsletter;
            $user->user_type= 'D';
            $user->type = $model->type;
            
            
            $random_str = md5(uniqid(rand()));
            $generatedPassword = substr($random_str, 0, 6);

            $user->setPassword($generatedPassword);
            $user->generateAuthKey();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $user->save(false)) {
                    if ($flag) {
                        //$userprofile = new UserProfile();
                        $model->userID = $user->id;
                        $model->clientID=$clientID;
                        $model->dealer_ref_no = $clientID.'-'.date('dmyHis');

                        if ($flag = $model->save(false)) {                            
                            $companyInformation->user_id = $model->userID;
                            if (($flag = $companyInformation->save(false)) === false) {
                                $transaction->rollBack();
                                //break;
                            }
                        }
                        /*if (($flag = $model->save(false)) === false) {      
                            $transaction->rollBack();
                                    //break;
                        }*/
                        $authassignment = new AuthAssignment();
                        $authassignment->item_name = 'Dealer';
                        $authassignment->user_id = $user->id;
                        $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                        $authassignment->save();
                        //AuthAssignment
                        
                    }
                }

            if ($flag) {
                $transaction->commit();
                //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
                $data = \yii\helpers\Json::encode(array(
                    'password' => $generatedPassword,
                ));
                $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                return $this->redirect(['index']);
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

            
            $model->save();
            return $this->redirect(['view', 'id' => $model->vip_customer_id]);
        } else {
            //print_r($model->getErrors());
            return $this->render('create', [
                'model' => $model,
                'companyInformation' => $companyInformation,
                'clientID' => $clientID,
            ]);
        }
    }
    
    public function actionApprovel($id)
    {
        //$model = new PainterProfile();
        $formapprove = new \common\models\Approve();
        if ($formapprove->load(Yii::$app->request->post())){
            $model = $this->findModel($id);
            if ($formapprove->validate()) {
                    $user = User::find()
                    ->where(['id' => $model->userID])
                    ->one();
                    $user->status = $formapprove->status;
                    
                    if($user->status == 'A')
                    {
                        $user->email_verification = 'Y';
                        $subject = "";
                        if(empty($user->date_last_login)) {
                            $random_str = md5(uniqid(rand()));
                            $generatedPassword = substr($random_str, 0, 6);
                            $user->setPassword($generatedPassword);
                            
                            $data = \yii\helpers\Json::encode(array(
                                'password' => $generatedPassword,
                            ));
                            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                        }else {
                            $data = "";
                            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['approval'], $data,$subject);
                        }
                    }
                    $user->save(FALSE);
                    //\Yii::$app->getSession()->setFlash('success', 'Action Successful');
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Approve', 'text' => 'Action Successful.']);
                    return $this->redirect(['view', 'id' => $id]);
            }else{
                print_r($formapprove->getErrors());
            }
        }
        //return $this->redirect(['view', 'id' => $model->vip_customer_id]);
    }
    
    public function actionResendEmailVerify($id)
    {
        if(!empty($id)) {
            $user = User::find()
                    ->where(['id' => $id])
                    ->one();
            
            $data = \yii\helpers\Json::encode(array(
                'verification_email_key' => $user->activation_key,
            ));
            $subject = "";
            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.email.verify'], $data,$subject);
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Email verifycation resnd', 'text' => 'An email has been sent to the registrant for verification']);   
            return $this->redirect('email-not-verified');
            
        }
        
    }
    
    public function actionEmailVerify($id)
    {
        
        if(!empty($id)) {
            $user = User::find()
                ->where(['id' => $id])
                ->one();
            $user->status = 'A';
            $user->email_verification = 'Y';
            $user->save(FALSE);
            $subject = "";
            $data = "";
            
            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['approval'], $data,$subject);
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Email verifycation & Account Approve', 'text' => 'Email has been successfully verified']);   
            return $this->redirect('email-not-verified');
            
        }
    }
    
    public function actionImportExcel()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $msg = '';
        $model = new \common\models\ExcelImportForm();
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $filename = date('dmyHis').'_'.$model->excel->baseName;
                $model->excel->saveAs(Yii::getAlias('@webroot').'/upload/customers/' . $filename . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot').'/upload/customers/' . $filename . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $dealerID = $rowData[0][0];
                    $email = $rowData[0][1];
                    
                    /*$name = $rowData[0][2];
                    $email = $rowData[0][3];
                    $mobile = $rowData[0][4];*/
                        $countUsers = \common\models\User::find()
                        ->where("client_id = '" . $clientID . "' AND username = '" . trim($dealerID) . "'")
                        ->count();
                    
                        if (($countUsers == 0)) {
                            if(!empty($email)){
                                $checkemail = \common\models\User::find()
                                ->where("client_id = '" . $clientID . "' AND email = '" . trim($email) . "'")
                                ->count();
                            }else {
                                $checkemail = 0;
                            }
                            
                            if($checkemail == 0){
                                $user = new User();
                                $user->client_id = $clientID;
                                $user->username = trim($dealerID);
                                $user->email = $email;
                                $user->status = 'A';
                                $user->approved = 'Y';
                                $user->newsletter = 'E';
                                $user->user_type= 'D';

                                $random_str = md5(uniqid(rand()));
                                $generatedPassword = substr($random_str, 0, 6);
                                $user->setPassword($generatedPassword);
                                $user->generateAuthKey();
                                $data = \yii\helpers\Json::encode(array(
                                    'password' => $generatedPassword,
                                ));
                                //$returnedValue = Yii::$app->vip->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                                $user->save(false);

                                //Create Customer Profile
                                $model = new VIPCustomer();
                                $model->userID = $user->id;
                                $model->clientID = $clientID;
                                $model->clients_ref_no = trim($dealerID);
                                $model->dealer_ref_no = $clientID.'-'.date('dmyHis');
                                $model->full_name = $dealerID;
                                $model->save(false);

                                //Give access to shopping cart module it come from rights
                                $authassignment = new AuthAssignment();
                                $authassignment->item_name = 'Dealer';
                                $authassignment->user_id = $user->id;;
                                $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                                $authassignment->save(false);

                                $registrationEmailTemplate = \common\models\EmailTemplate::find()
                                ->where(['code' => Yii::$app->params['email.template.code.registration']])
                                ->one();

                                //Save the data to cron
                                $emailQueue = new \common\models\EmailQueue();
                                $emailQueue->email_template_id = $registrationEmailTemplate->id;
                                $emailQueue->clientID = $clientID;
                                $emailQueue->user_id = $user->id;
                                $emailQueue->name = $user->username;
                                $emailQueue->email = $user->email;
                                $emailQueue->data = $data;
                                $emailQueue->status = 'P';
                                $emailQueue->email_queue_type = 'C';
                                $emailQueue->save(false);
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  New Profile has been created <b>('.$dealerID.')</b></li>';
                            }else {
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-times" aria-hidden="true"></i></span>  Profile not created <b>('.$dealerID.') email already taken!</b></li>';
                            }                                
                                
                        }else {
                            /*$user = \common\models\User::find()
                                ->where("client_id = '" . $clientID . "' AND username = '" . trim($dealerID) . "' AND email IS NULL ")
                                ->count();*/
                             if (!empty($email)) {
                                $user = \common\models\User::find()
                                        ->where("client_id = '" . $clientID . "' AND username = '" . trim($dealerID) . "'")
                                        ->one();
                                $user->email = $email;
                                $user->save(false);

                                $emailQueue = \common\models\EmailQueue::find()
                                        ->where("clientID = '" . $clientID . "' AND user_id = '" . $user->id . "'")
                                        ->one();
                                $emailQueue->email = $email;
                                $emailQueue->save(false);
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-check" aria-hidden="true"></i></span> Email has been updated <b>(' . $dealerID . ')</b></li>';
                            }
                        }   
                    
                }               

                //unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                ]);
            }
        } else {
            $session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionTest() {
       $sender_name = "Shihan";
       $emailTo = "shmshihan@yahoo.com";
       $message = Yii::$app->mailer->compose()
                    ->setTo($emailTo)
                    //->setFrom([\Yii::$app->params['adminIresidenzMail'] => \Yii::$app->name])
                    //->setReplyTo(\Yii::$app->params['replyTo'])
                    ->setFrom([\Yii::$app->params['adminEmail'] => $sender_name]) 
                    //->setReplyTo(\Yii::$app->params['adminIresidenzMail'])
                    ->setSubject("sddsfsdfsdfsdfsdf")
                    ->setHtmlBody("sgjfshg fjsh fjsdj sdjgfjsd s sd")
                    ->send();

      //$mail = \Yii::$app->mailer->send($message);
       
       print_r($message);


    }

    /**
     * Updates an existing VIPCustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $model = $this->findModel($id);

        $userID = $model->userID;
        $user = $this->findModeluser($userID);
        $model->email = $user->email;
        $model->status = $user->status;
        $model->type = $user->type;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save(false);
                //$companyInformation->save(false);
                $user->email = $model->email;
                $user->save(false);
                return $this->redirect(['view', 'id' => $model->vip_customer_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                //'companyInformation' => $companyInformation
            ]);
        }
    }
    
    public function actionUpdatecompany($id)
    {
        $model = $this->findModel($id);
        $userid = $model->userID;
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        if ($companyInformation->load(Yii::$app->request->post())){
            if($companyInformation->validate()){
                $companyInformation->save(false);                
                //Yii::$app->session->setFlash('success', ' Company Information has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Company Information', 'text' => 'Company Information has been updated!']);
                return $this->redirect(['view', 'id' => $model->vip_customer_id, 'tab' => 'company']);                
            }else{
                return $this->render('_form_company', [
                    'model' => $model,
                    'companyInformation' => $companyInformation,
                ]);
            }
        }else{
            return $this->render('_form_company', [
                'model' => $model,
                'companyInformation' => $companyInformation,
            ]);
        }
    }
    
    public function actionUpdateProfile($id)
    {
        $model = $this->findModel($id);
        
        $userID = $model->userID;
        $user = $this->findModeluser($userID);
        
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            $user->status = $model->status;
            $user->save(false);
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->vip_customer_id]);
        } else {
            $model->status = $user->status;
            return $this->render('update_custom', [
                'model' => $model,
            ]);
        }
    }
    
    
    
    public function actionAddRewardPoint($id)
    {
        $modelcustomer = $this->findModel($id);
        $model = new VIPCustomerReward();
        if ($model->load(Yii::$app->request->post())) {
            $model->clientID = $modelcustomer->clientID;
            $model->customer_id = $modelcustomer->userID;
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
            }else {
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'You have modified customer!']);
            }
            return $this->redirect(['view', 'id' => $modelcustomer->vip_customer_id]);
        } else {
            return $this->render('add_reward_point', [
                'model' => $model,
            ]);
        }
        
    }
    
    public function actionSendpassword($id) {
        $model = $this->findModelsendpassword($id);

        if ($model->email != null) {
            $random_str = md5(uniqid(rand()));
            $generatedPassword = substr($random_str, 0, 6);
            $model->setPassword($generatedPassword);
            //$model->password_generated = $generatedPassword;
            
            
            //$model->password_sent_status = 'Y';

            $data = \yii\helpers\Json::encode(array(
                'password' => $generatedPassword,
            ));
            if($model->save()){
                $returnedValue = Yii::$app->VIPglobal->sendEmail($id, Yii::$app->params['password.reset.request'], $data);                
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Password has been sent']);
            }else{
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Action unsuccessful']);
            }            
        }else{
            \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Email is not provided']);
        }
        return $this->redirect(['index']);
    }
    
    public function actionTrashAccount($id) {
        $model = $this->findModeluser($id);
        if ($model->email != null) {
           $email = $model->email.'_trash';
        }else {
           $email = 'N/A' ;
        }
        
        $model->status = 'X';
        $model->email = $email;

        if($model->save()){
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Account moved to Trash']);
        }else{
            \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Action unsuccessful']);
        }
        return $this->redirect(['index']);
    }
    
    public function actionSendPasswordManual($id) {
        $password = new \common\models\ChangePasswordForm();
        $model = $this->findModelsendpassword($id);
        if ($password->load(Yii::$app->request->post()) && $password->validate()) {
            if ($model->email != null) {
                //$random_str = md5(uniqid(rand()));
                //$generatedPassword = substr($random_str, 0, 6);
                $generatedPassword = $password->confirm_password;
                $model->setPassword($generatedPassword);
                $model->email_verification = 'Y';
                //$model->password_generated = $generatedPassword;


                //$model->password_sent_status = 'Y';

                $data = \yii\helpers\Json::encode(array(
                    'password' => $generatedPassword,
                ));
                if($model->save()){
                    $returnedValue = Yii::$app->VIPglobal->sendEmail($id, Yii::$app->params['password.reset.request'], $data);                
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Password has been sent']);
                }else{
                    \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Action unsuccessful']);
                }            
            }else{
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Email is not provided']);
            }
            
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        return $this->render('resetPassword', [
            'model' => $model,
            'password' => $password,
        ]);
    }
    
    public function actionLogintoStore($id) {
        $session = Yii::$app->session;
        $clientID = 16;
        $storeurl = \common\models\Client::find()
                    ->where(['clientID' => $clientID])
                    ->one();

        $url=$storeurl->cart_domain;
        $model = $this->findModeluser($id);
        $model->token = Yii::$app->security->generateRandomString();
        
        if($model->save()){
            $urllink= $url.'/site/store-login?'.http_build_query(['token' => $model->token]);
            return $this->redirect($urllink);
            //Yii::$app->response->redirect(array($url.'/site/store-login','token'=>$model->token));
            exit(0);
            //return $this->redirect([$url.'/site/store-login', 'token' => $model->token, ['target'=>'_blank']]);
        }
        
    }
    
    public function actionActive($id) {
        $model = $this->findModeluser($id);
        $model->status = 'A';
        $model->approved = 'Y';
        //$model->email_verification = 'Y';
        
        if($model->save()){            
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Profile has been activated']);
        }else{
            \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'Action unsuccessful']);
        } 
        return $this->redirect(['index']);
    }
    
    public function actionImportExcelNoEmail()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $msg = '';
        $model = new \common\models\ExcelImportForm();

        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $filename = date('dmyHis').'_'.$model->excel->baseName;
                $model->excel->saveAs(Yii::getAlias('@webroot').'/upload/customers/' . $filename . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot').'/upload/customers/' . $filename . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $salutation = $rowData[0][0];
                    $name = $rowData[0][1];                    
                    $email = $rowData[0][2];
                    $mobile = $rowData[0][3];
                    $company = $rowData[0][4];
                    $dealerid = $rowData[0][5];
                    
                    if(!empty($salutation)) {
                       $salutation = $rowData[0][0]; 
                    }else {
                       $salutation = '999' ;
                    }
                    
                    $mobile1 = preg_replace("/[\s_-]/", "", $mobile);
                    $newnum = ltrim($mobile1, '0');
                    $newmobile = '+60'.$newnum;

                    if(!empty($email)){
                        $parts = explode("@", $email);
                        $username = $parts[0];
                        
                        $checkemail = \common\models\User::find()
                        ->where("client_id = '" . $clientID . "' AND email = '" . trim($email) . "'")
                        ->count();
                        
                        if($checkemail == 0){
                            $email = strtolower($email);
                            $checkcode = \common\models\VipCustomerShihan::find()
                        ->where("email = '" . trim($email) . "'")
                        ->one();
                            
                            $user = new User();
                            $user->client_id = $clientID;
                            $user->username = trim($username);
                            $user->email = trim($email);
                            $user->email_verification = 'Y';
                            $user->status = 'A';
                            $user->approved = 'Y';
                            $user->newsletter = 'E';
                            $user->user_type= 'D';
                            $user->type = '5';
                            $generatedPassword = 'Demo@123';
                            $user->setPassword($generatedPassword);
                            $user->generateAuthKey();
                            $user->save(false);

                            //Create Customer Profile
                            $model = new VIPCustomer();
                            $model->userID = $user->id;
                            $model->clientID = $clientID;
                            $model->clients_ref_no = trim($checkcode->lastname);
                            $model->dealer_ref_no = $clientID.'-'.date('dmyHis');
                            $model->salutation_id = $salutation;
                            $model->full_name = ucwords($name);
                            $model->mobile_no = $newmobile;
                            $model->assign_dealer_id = $dealerid;
                            $model->created_by = '1';
                            $model->save(false);
                            
                            $companyInformation = new CompanyInformation();
                            $companyInformation->user_id = $user->id;
                            $companyInformation->company_name = ucwords($company);
                            $companyInformation->save(false);

                            //Give access to shopping cart module it come from rights
                            $authassignment = new AuthAssignment();
                            $authassignment->item_name = 'Dealer';
                            $authassignment->user_id = $user->id;;
                            $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                            $authassignment->save(false);
                            
                            $parts = explode(' ', $name);
                            if(count($parts) == 1){
                                $firstname = $parts[0].' '.$parts[0];
                            }else {
                                $firstname = $parts[0].' '.$parts[1];
                            }

                            $avatar = new LetterAvatar($firstname);
                            $path = Yii::getAlias('@webroot').'/upload/profiles/';           
                            $filename = Yii::$app->security->generateRandomString().".png";
                            $avatar->saveAs($path . $filename);

                            $photo = new \common\models\ProfilePic();
                            $photo->userID = $user->id;
                            $photo->file_name = $filename;
                            $photo->save(false);

                            /*$registrationEmailTemplate = \common\models\EmailTemplate::find()
                            ->where(['code' => Yii::$app->params['email.template.code.registration']])
                            ->one();

                            //Save the data to cron
                            $emailQueue = new \common\models\EmailQueue();
                            $emailQueue->email_template_id = $registrationEmailTemplate->id;
                            $emailQueue->clientID = $clientID;
                            $emailQueue->user_id = $user->id;
                            $emailQueue->name = $user->username;
                            $emailQueue->email = $user->email;
                            $emailQueue->data = $data;
                            $emailQueue->status = 'P';
                            $emailQueue->email_queue_type = 'C';
                            $emailQueue->save(false);*/
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  New Profile has been created <b>('.$email.')</b></li>';
                        }else {
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-times" aria-hidden="true"></i></span>  Profile not created <b>('.$email.') email already taken!</b></li>';
                        }
                    }else {
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-times" aria-hidden="true"></i></span>  Sorry no email exits!</b></li>';
                    }
                            
                      
                    
                }               

                //unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                ]);
            }
        } else {
            $session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionUploadexcel() {
        
        $session = Yii::$app->session;
        $model = new \backend\models\ExcelFormDb();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "clients ref no", "company name", "email");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                
                if($checkmatch > 2){
                    $data = array();

                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        
                        $data[] = [$rowData[0]['product name'],$rowData[0]['links'],$rowData[0]['product code'],$rowData[0]['suppliers product code (isku)'],$rowData[0]['variant'],$rowData[0]['msrp'],$rowData[0]['partner price'],$deliverydata,$rowData[0]['manufacturer id'],$rowData[0]['category id'],$rowData[0]['sub category id'],date('Y-m-d')];

                    }
                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('product_excel_upload', ['product_name','links', 'product_code','sp_code','variant','msrp','partner_price','delivery','manufacturer','category','sub_category','create_date'],$data)
                    ->execute();

                    $returnedValue = Yii::$app->VIPglobal->importProductUpload();

                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Product Import Sccessfully']);
                        //return $this->redirect(['uploadexcel']);
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry product import fail!']);
                    }
                    unlink($inputFile);
                }else {
                    $teml = 'Clients Ref No, Company Name, Email Address';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }

                return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }
    

    /**
     * Deletes an existing VIPCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionExpiry()
    {
        $expired_timedate=date('Y')."12312359";
        echo $expired_timedate.'<br>';
        
        $year=date('Y',strtotime("-1 year"));
        echo $year.'<br>';
        
        //$year=date('Y',strtotime("-1 year"));
        //echo date("2020-12-31",strtotime("-2 year"));
        $date = strtotime('2020-12-31 -23 month');
        echo date('Y-m-d', $date).'<br>';
        
        $expyear='2020';//date("Y");
        $rewardstring = "Points Expired on 31-".date("m")."-".date("Y");
        echo $rewardstring.'<br>';
        
        $expyearminus=$expyear+1;
        $expdatestart = $expyear.'-01-01 00:00:00';
        $expdateend = $expyearminus.'-12-31 23:59:59'; //adjust to march later
        
        echo $expdatestart.' to '.$expdateend;
        
    }
    
    
    protected function userType(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $users = \common\models\TypeName::find()
            ->select('name, tier_id')        
            ->where(['clientID' => $clientID])
            ->asArray()->all();
        
        return $users;
    }
    

    /**
     * Finds the VIPCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPCustomer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModeluser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelsendpassword($id) {
        if (($model = \common\models\User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
