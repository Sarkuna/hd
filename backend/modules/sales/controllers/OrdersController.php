<?php

namespace app\modules\sales\controllers;

use Yii;
use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\VIPCustomerReward;
use common\models\VIPCustomerRewardSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\ExportBehavior;
use app\components\ExportExcelUtil;

/**
 * OrdersController implements the CRUD actions for VIPOrder model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }
    
    public function beforeAction($action)
    {
        return true;
    }

    /**
     * Lists all VIPOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 10;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProcessing()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 20;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionShipped()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 30;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('shipped', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionComplete()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 40;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('complete', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRefunded()
    {
        $session = Yii::$app->session;
        $clientID =16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 50;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('refunded', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCancelled()
    {
        $session = Yii::$app->session;
        $clientID =16;
        
        $searchModel = new VIPOrderSearch();
        $searchModel->clientID = $clientID;
        $searchModel->order_status_id = 60;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cancelled', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        return $this->render('view_print', [
            'model' => $model,
        ]);
    }
    
    public function actionOrderapprove($id=null)
    {
        $model = $this->findModel($id);
        $actionform = new \backend\models\OrderActionForm();
        $totalpoint = $model->totalPoints + $model->shippingPointTotal;

        if (Yii::$app->request->isAjax && $actionform->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($actionform);
        }
        if ($actionform->load(Yii::$app->request->post())) {
            $model->order_status_id = $actionform->order_status_id;
            if(!empty($actionform->bb_invoice_no)) {
                $model->bb_invoice_no = $actionform->bb_invoice_no;
            }
            if($model->save()){
                if($model->order_status_id == '50' || $model->order_status_id == '60'){
                    $reward = new VIPCustomerReward();
                    $reward->clientID = $model->clientID;
                    $reward->customer_id = $model->customer_id;
                    $reward->order_id = $model->order_id;
                    $reward->description = 'Product '.$model->orderStatus->name.' : Order # '.$model->invoice_prefix.'';
                    $reward->points = $totalpoint;
                    $reward->date_added = date('Y-m-d H:i:s');
                    $reward->bb_type = 'V';
                    $reward->save(false);
                }
                
                $history = new \common\models\VIPOrderHistory();
                $history->order_id = $model->order_id;
                $history->order_status_id = $actionform->order_status_id;
                $history->comment = $actionform->comment;
                $history->date_added = date('Y-m-d');
                $history->save(false);
                
                if($actionform->notify == 1) {
                    $data = \yii\helpers\Json::encode(array(
                        'email_comment' => $actionform->comment,
                    ));
                    $code = 'order.status_'.$model->order_status_id;
                    $returnedValue = Yii::$app->VIPglobal->sendEmailOrder($model->order_id, Yii::$app->params[$code], $data);
                }
                
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified order!']);
                               
            }else {
                //print_r($model->getErrors());die;
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'You have modified order!']);
            }
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->renderAjax('_formaction', [
                'model' => $model,
                'actionform' => $actionform,
            ]);
        }
    }
    
    public function actionBulkOrder(){
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $vstatus= Yii::$app->request->post('redemption_status');
        $vcomment= Yii::$app->request->post('comment');
        $invoice_num = Yii::$app->request->post('invoice_num');
        $notify = Yii::$app->request->post('notify');

        if(count($selection) > 0){
            $data = array();
            foreach($selection as $valueid){
                $id = $valueid;
                $model = $this->findModel($id);
                //$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);
                $balancepoint = $model->last_point_balance;
                $totalpoint = $model->totalPoints + $model->shippingPointTotal;

                if($balancepoint < 0) {
                    $vstatus = 10;
                    $vcomment = 'Point balance is insufficient';
                }

                $model->order_status_id = $vstatus;
                
                if(!empty($invoice_num)) {
                    $model->bb_invoice_no = $invoice_num;
                }
                
                if($model->save()){
                    if($model->order_status_id == '50' || $model->order_status_id == '60'){
                        $reward = new VIPCustomerReward();
                        $reward->clientID = $model->clientID;
                        $reward->customer_id = $model->customer_id;
                        $reward->order_id = $model->order_id;
                        $reward->description = 'Product '.$model->orderStatus->name.' : Order # '.$model->invoice_prefix.'';
                        $reward->points = $totalpoint;
                        $reward->date_added = date('Y-m-d H:i:s');
                        $reward->bb_type = 'V';
                        $reward->save(false);
                    }
                    $data[] = [$model->order_id,$vstatus,$vcomment,date('Y-m-d'),date("Y-m-d H:i:s"),date("Y-m-d H:i:s"),Yii::$app->user->id,Yii::$app->user->id];
                }
                
                if($notify == 1) {
                    $dataemail = \yii\helpers\Json::encode(array(
                        'email_comment' => $vcomment,
                    ));
                    $code = 'order.status_'.$model->order_status_id;
                    $returnedValue = Yii::$app->VIPglobal->sendEmailOrder($model->order_id, Yii::$app->params[$code], $dataemail);
                }
            }
            
            Yii::$app->db
                ->createCommand()
                ->batchInsert('vip_order_history', ['order_id','order_status_id', 'comment','date_added','created_datetime','updated_datetime','created_by','updated_by'],$data)
                ->execute();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Bulk Success', 'text' => 'Acction Sccessfully']);
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new VIPOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        die;
        $model = new VIPOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                /*if($model->order_status_id == '50' || $model->order_status_id == '60'){
                    $reward = new VIPCustomerReward();
                    $reward->clientID = $model->clientID;
                    $reward->customer_id = $model->customer_id;
                    $reward->order_id = $model->order_id;
                    $reward->description = 'Product '.$model->orderStatus->name.' : Order # '.$model->invoice_prefix.'';
                    $reward->points = $model->totalPoints;
                    $reward->date_added = date('Y-m-d H:i:s');
                    $reward->save(false);
                }
                
                $data = '';
                $code = 'order.status_'.$model->order_status_id;
                $returnedValue = Yii::$app->vip->sendEmailOrder($model->order_id, Yii::$app->params[$code], $data);*/
            }else {
                \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail', 'text' => 'You have modified order!']);
            }
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        die;
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDownload($file_name, $file_type = 'excel', $deleteAfterDownload = false) {

        if (empty($file_name)) {
            return 0;
        }
        $baseRoot = Yii::getAlias('@webroot') . "/upload/";
        $file_name = $baseRoot . $file_name;
        if (!file_exists($file_name)) {
            return 0;
        }
        $fp = fopen($file_name, "r");
        $file_size = filesize($file_name);

        if ($file_type == 'excel') {
            header('Pragma: public');
            header('Expires: 0');
            header('Content-Encoding: none');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: public');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Description: File Transfer');
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            header('Content-Transfer-Encoding: binary');
            Header("Content-Length:" . $file_size);
        } else if ($file_type == 'picture') { //pictures
            Header("Content-Type:image/jpeg");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        } else { //other files
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        }
 
        $buffer = 1024;
        $file_count = 0;

        while (!feof($fp) && $file_count < $file_size) {
            $file_con = fread($fp, $buffer);
            $file_count+=$buffer;
            echo $file_con;
        }

        fclose($fp);
        if ($deleteAfterDownload) {
            unlink($file_name);
        }
        return 1;
    }
    
    public function actionInvoice() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $columns = array('No','name', 'house_no', 'purpose', 'time_come_in_real', 'time_come_out_real', 'visit_duration', 'sign_off');
        $dataVisits = array();
        
        
        $productlists = \common\models\VIPOrderProduct::find()->joinWith([
            'order',
            //'order.customer','order.company',
            'order.customer' => function($que) {
                $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
            },
            'order.company' => function($que) {
                $que->select(['user_id','company_name']);
            },
                //'user',
                //'condoUnit'
        ])
        ->where(['=', 'vip_order.clientID', $clientID])
        //->andWhere(['=', 'vip_order.order_status_id', '10'])
        /*->andWhere(['sign_off' => 'O'])
        ->andWhere(['>=', 'vms_visits.updated_datetime', date($date_from)])
        ->andWhere(['<=', 'vms_visits.updated_datetime', date($date_to)])*/
        ->orderBy(['vip_order.created_datetime' => SORT_DESC])
        ->asArray()
        ->all();
        
        /*echo '<pre>';
        print_r($productlists);
        die;*/
        
        $numRow = 1;
        
        foreach($productlists as $key=>$productlist){
            if($productlist['shipping_point'] == 0) {
                $shipping = 0;
            }else {
                $shipping = $productlist['shipping_price'];
            }
            $uprice = $productlist['point'] + $shipping;
            $amount_RM = $productlist['quantity'] * $uprice;
            $totpoint = $productlist['quantity'] * $productlist['point'];
            $totpointship = $productlist['quantity'] * $productlist['shipping_point'];
            
            $total_pro = $productlist['point'] + $productlist['shipping_point'];
            $totl_f = $productlist['quantity'] * $total_pro;
            
            $statusid = $productlist['order']['order_status_id'];
            if($statusid == '10') {
                $status = 'Pending';
            }else if($statusid == '20') {
                $status = 'Processing';
            }else if($statusid == '30') {
                $status = 'Shipped';
            }else if($statusid == '40') {
                $status = 'Complete';
            }else if($statusid == '50') {
                $status = 'Refunded';
            }else if($statusid == '60') {
                $status = 'Cancelled';
            }else {
                $status = 'N/A';
            }
            
            $visit_temp = array(
                    'no' => $numRow++,
                    'Company Name' => $productlist['order']['company']['company_name'],
                    'Dealer Code' => $productlist['order']['customer']['clients_ref_no'],                    
                    'order_no.' => $productlist['order']['invoice_prefix'],                             
                    'order_date' => date("d-m-Y", strtotime($productlist['order']['created_datetime'])),
                    'Invoice number' => $productlist['order']['bb_invoice_no'],
                    'product_code' => $productlist['model'],
                    'product_description' => $productlist['name'],
                    'qty' => $productlist['quantity'],
                    'unit/pts' => $productlist['point'],
                    'shipping/pts' => $productlist['shipping_point'],
                    'total product/unit' => $totl_f,
                    'Total RM' => $totl_f / 2,
                    'Status' => $status,
                    //'Point' => $totpoint,
                    //'Shipping Point' => $totpointship,
                    //'Total(Point + Shipping)' => $totpoint + $totpointship,
                    //'amount_RM' => $amount_RM,
                    //'SST_0%' => '',
                    //'Total Amount (RM)' => $amount_RM,
                    
                    
                );
            $dataVisits[] = $visit_temp;
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Invoice',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                /*'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                'headerColumnCssClass' => array(
                    'id' => ExportExcelUtil::getCssClass('blue'),
                    'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                ), //define each column's cssClass for header line only.  You can set as blank.
                'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                'hightLight' => ExportExcelUtil::getCssClass("highlight"),*/
            ),array(
                'sheet_name' => 'Sheet 2',
                'sheet_title' => array(),
                'ceils' => array()
            ),
            //sheet 2,....            
        );
        
        $excel_file = "Invoice";
        $this->export2excel($excel_content,$excel_file);
    }

    /**
     * Finds the VIPOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
