<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummaryReport */

$this->title = 'Create Point Upload Summary Report';
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-upload-summary-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
