<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummaryReport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-upload-summary-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'point_upload_summary_id',
            'dealer_id',
            'points',
            'status_remark',
            'status',
        ],
    ]) ?>

</div>
