<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointUploadSummaryReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fail Points';
//$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summaries', 'url' => ['/sales/point-upload']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-upload-summary-report-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right hide">
                <?= Html::a('<i class="fa fa-sync-alt"></i>', ['synchronize'], ['class' => 'btn btn-success', 'title' => 'Synchronize']) ?>
                
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <?=Html::beginForm(['/sales/point-upload-summary/bulk'],'post');?>
            <?php if ($dataProvider->totalCount > 0) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-2 no-padding">
                        <?= Html::dropDownList('status', '', ['S' => 'Synchronize', 'X' => 'Trash'], ['class' => 'form-control',]) ?>
                    </div>
                    <div class="col-lg-4 no-padding">
                        <div class="form-group">
                            <?=
                            Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'btnsubmit',])
                            ?>
                            <?= Html::a('Refresh', ['/sales/point-upload-summary'], ['class' => 'btn btn-default']) ?>

                        </div>
                    </div> 
                </div>    
            </div>
            <?php } ?>
            <div class="vipcustomer-index table-responsive">
                
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                        'dealer_id',
                        [
                            'attribute' => 'effective_month',
                            'label' => 'Effective Month',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return date('d/m/Y', strtotime($model->pointupload->effective_month));
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Description',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->pointupload->description;
                            },
                        ],            
                        'points',
                        'status_remark',
                        [
                            'attribute' => 'remark',
                            'label' => 'Remarks',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->getRemark();
                            },
                        ],             
                                    
                    // 'status',
                    //['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
            </div>
            <?= Html::endForm();?>
        </div>
    </div>
</div>

<?php
    $clientScript = '
        $("#btnsubmit").click(function()
        {
            var keys = $("#w0").yiiGridView("getSelectedRows");
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }            
        });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>