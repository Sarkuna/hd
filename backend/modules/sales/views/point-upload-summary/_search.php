<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummaryReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-upload-summary-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'point_upload_summary_id') ?>

    <?= $form->field($model, 'dealer_id') ?>

    <?= $form->field($model, 'points') ?>

    <?= $form->field($model, 'status_remark') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
