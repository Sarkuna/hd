<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\Banks;

$banks = Banks::find()
        //->where(['option_id' => '2'])
        ->orderBy([
    'bank_name' => SORT_ASC,
])->all();
$bankslist = ArrayHelper::map($banks, 'id', 'bank_name');

?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'vipcustomer-address-add-form','class' => 'form-horizontal form-label-left']]); ?>
<div class="box-body">
    <?=
    $form->field($model, 'bank_name_id', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->widget(Select2::classname(), [
        'data' => $bankslist,
        //'language' => 'de',
        'options' => [
            'placeholder' => '-- Select --',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?=
    $form->field($model, 'account_name', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'account_number', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'nric_passport', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>
</div>
<div class="box-footer">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>