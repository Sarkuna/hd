<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\components\Customers_Status_Widget;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$clientID = $session['currentclientID'];
$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$typename = \common\models\TypeName::find()->where([
                'clientID' => 16,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<div class="row">
    <?php
        foreach($users as $user) {
            $usercount = \common\models\User::find()
            //->select('name, tier_id')        
            ->where(['client_id' => $clientID, 'status' => 'P', 'type' => $user['tier_id'], 'email_verification' => 'Y'])
            ->asArray()->count();
            echo '<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
        <span class="info-box-text">'.$user['name'].'</span>
              <span class="info-box-number">'.$usercount.'</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>';
        }
    ?>
      </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?> <span class="label label-warning">Pending</span></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?php

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],             
                        'full_name',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],
                        'mobile_no',                      
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,       
                        ],
                        /*[
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["P" => "Pending", "A" => "Approve", "D" => "Deactive"],        
                        ],*/           
                        //'gender',
                        // 'telephone_no',
                        //'mobile_no',
                        // 'date_of_Birth',
                        // 'nric_passport_no',
                        // 'Race',
                        // 'Region',
                        //'country_id',
                        // 'address_id',
                        // 'nationality_id',
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->created_datetime));
                            },
                        ],            
                        // 'updated_datetime',
                        // 'created_by',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ],            
                    ],
                ]);
               ?>
            </div>
        </div>
    </div>


