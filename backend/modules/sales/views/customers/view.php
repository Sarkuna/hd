<style>
    .nav>li>a {
        padding: 10px 10px;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a {
    background-color: #00C0EF;
    color: #ffffff;
}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;


$picPro = '<img alt="User profile picture" src="view-avatar?id='.$model->vip_customer_id.'" class="profile-user-img img-responsive img-circle">';
?>
<div class="vipcustomer-view">
    <div class="row">
        <div class="col-lg-12"><h3 style="margin-top: 0px;" class="pull-right"><?= $model->user->getStatustext() ?></h3></div>
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <?php echo $picPro ?>

              <h3 class="profile-username text-center"><?= Html::encode($model->full_name) ?></h3>
              <h4 class="text-muted text-center"><label class="label label-primary"><?= $model->user->typeName->name ?></label></h4>
              <p class="text-muted text-center">Clients Ref No: <?= Html::encode($model->clients_ref_no) ?></p>
              <p class="text-muted text-center">Company Name: <?= ($companyInformation->company_name) ? $companyInformation->company_name : Yii::t("app", "N/A") ?></p>

<?php
if($model->user->status != 'X') { ?>
<?= $this->render('_formAction', ['model' => $model, 'formapprove' => $formapprove,]) ?>
<?php } ?>
            </div>
            <!-- /.box-body -->
          </div>
          
          <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Account 1</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Awarded Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalAdd()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Redeemed Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalMinus()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Expired Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getPointExpiry()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Balance Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getBalance()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <?php
                            $exppoint = Yii::$app->VIPglobal->PointsExpire($model->userID);
                            ?>
                            <b>Points Expire on 31-12-19</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($exppoint) ?></a>
                        </li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
          <!-- /.box -->
          
          <!-- Account 2 Summery Box -->
            <?php if($model->user->type == '1') {  ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Account 2</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Awarded Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalAddAcc2()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Redeemed Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalMinusAcc2()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Expired Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalExpiryAcc2()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Balance Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getBalanceAcc2()) ?></a>
                        </li>
                        <li class="list-group-item">
                            <?php
                            $exppoint = Yii::$app->VIPglobal->PointsExpire($model->userID);
                            ?>
                            <b>Points Expire on 31-12-20</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getBalanceAcc2()) ?></a>
                        </li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <?php } ?>
          <!-- End Account 2 Summery -->
        </div>
        <!-- /.col -->
        <div class="col-md-9  profile-data">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs responsive" id = "profileTab">
                <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Info'); ?></a></li>
                <li id = "company-tab"><a href="#company" data-toggle="tab"><i class="fa fa-building"></i> <?php echo Yii::t('app', 'Company Info'); ?></a></li>
                <li id = "address-tab"><a href="#address" data-toggle="tab"><i class="fa fa-credit-card"></i> <?php echo Yii::t('app', 'Shipping Address'); ?></a></li>
                <li id = "bank_info-tab"><a href="#bank_info" data-toggle="tab"><i class="fa fa-bank"></i> <?php echo Yii::t('app', 'Bank Info'); ?></a></li>
                <li id = "reward_points-tab"><a href="#reward_points" data-toggle="tab"><i class="fa fa-star"></i> Reward Points</a></li>
                <li id = "orders-tab"><a href="#orders" data-toggle="tab"><i class="fa fa-newspaper-o"></i> Orders</a></li>
                <?php
                if($model->user->type == '1') {
                    echo '<li id = "account2-tab"><a href="#account2" data-toggle="tab"><i class="fa fa-exchange-alt"></i> Account2</a></li>';
                }
                ?>
            </ul>
            <div id='content' class="tab-content responsive">
                <div class="tab-pane active" id="personal">
                    <?= $this->render('_tab_personal', ['model' => $model]) ?>	
                </div>
                <div class="tab-pane" id="company">
                    <?= $this->render('_tab_company', ['model' => $model, 'companyInformation' => $companyInformation]) ?>
                </div>
                <div class="tab-pane" id="address">
                    <?= $this->render('_tab_shpping_view', ['model' => $model, 'address' => $address,])?>
                </div>
                <div class="tab-pane" id="bank_info">
                    <?= $this->render('_tab_bank_view', ['model' => $model, 'bankslisit' => $bankslisit,])?>
                </div>
                <div class="tab-pane" id="reward_points">
                    <?=
                  $this->render('_tab_reward_points', [
                      'model' => $model,
                      'searchrewardModel' => $searchrewardModel,
                      'datarewardProvider' => $datarewardProvider,
                  ])
                  ?>
                </div>
                <div class="tab-pane" id="orders">
                    <?=
                  $this->render('_tab_orders', [
                      'model' => $model,
                      'searchOrder' => $searchOrder,
                      'dataProviderOrder' => $dataProviderOrder,
                  ])
                  ?>
                </div>
                
                <div class="tab-pane" id="account2">
                    <?= $this->render('_tab_account2_view', ['model' => $model])?>
                </div>
                
                
            </div> 
          
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
        </div>
      </div>
</div>


<?php
$script = <<< JS
    $(function() {
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTab', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop() || $('html').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);
        });
    });    
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>