<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<hr>

<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Dealer Information'); ?>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Dealer Code</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->dealer_code ?></div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Dealer Company</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->company ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label">First Name</div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->first_name ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label">Last Name</div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->last_name ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label">Email</div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->user->email ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label">Tel</div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?=  $model->dealerInformation->tel ?></div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label" style="min-height: 85px;">Address</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text" style="min-height: 85px;"><?=  $model->dealerInformation->address1.' '.$model->dealerInformation->address2 ?>
            <br><?= $model->dealerInformation->city.' '.$model->dealerInformation->postcode ?>
            <br><?= $model->dealerInformation->region.' '.$model->dealerInformation->countrys->name ?>
        </div>
    </div>
    
    
</div>