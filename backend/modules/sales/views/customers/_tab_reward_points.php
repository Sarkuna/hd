<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
$amount = 0;
    if (!empty($datarewardProvider->getModels())) {
        foreach ($datarewardProvider->getModels() as $key => $val) {
            $amount += $val->points;
        }
    }
?>
<p class="pull-right">
    <?php if($model->user->status != 'X') { ?>
        <?= Html::a('Add Reward Points', ['add-reward-point', 'id' => $model->vip_customer_id], ['class' => 'btn btn-primary btn-sm']) ?>
    <?php } ?>
</p>
<?= GridView::widget([
        'dataProvider' => $datarewardProvider,
        //'filterModel' => $searchrewardModel,
        //'summary'=>'',
	'showFooter'=>true,
        'footerRowOptions'=>['style'=>'font-weight:bold;text-align: right;'],
	'showHeader' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'customer_reward_id',
            //'clientID',
            //'customer_id',
            //'order_id',
            //'date_added',
            [
                'attribute' => 'date_added',
                'label' => 'Date Added',
                'format' => 'html',
                'value' => function ($model) {
                    $datea = date('d/m/Y', strtotime($model->date_added));
                    return $datea;
                },
                'options' => ['width' => '100']
            ],
            [
                'attribute' => 'description',
                'label' => 'Description',
                'format' => 'html',
                'value' => function ($model) {
                    if($model->bb_type == 'V') {
                        //return $model->description;
                        return Html::a($model->description, ['/sales/orders/view', 'id' => $model->order_id]);
                    }else {
                        return $model->description;
                    }
                },
                //'options' => ['width' => '100']
            ],            
            //'description:ntext',
            /*[
                'attribute' => 'points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'label' => 'Points',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->points;
                },
                'options' => ['width' => '100']
            ],*/
            //'bb_type',    
            [
                'attribute' => 'points',
                'label' => 'Points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'format' => 'html',
                'options' => ['width' => '100'],
                'value' => function ($model, $key, $index, $widget) {
                    return $model->points;
                },
                'footer' => $amount,
            ],
                        
            // 'date_added',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

        ],
    ]); ?>