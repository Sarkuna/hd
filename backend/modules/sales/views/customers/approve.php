<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => 16,
            ])->one();

$typename = \common\models\TypeName::find()->where([
                'clientID' => 16,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<div class="row">
    <?php
    foreach ($users as $user) {
        $usercount = \common\models\User::find()
                        //->select('name, tier_id')        
                        ->where([ 'status' => 'A', 'type' => $user['tier_id']])
                        ->asArray()->count();
        echo '<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
        <span class="info-box-text">' . $user['name'] . '</span>
              <span class="info-box-number">' . $usercount . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>';
    }
    ?>
</div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?> <span class="label label-success">Approved</span></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?php

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],           
                        'full_name',
                        /*[
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],*/
                        //'mobile_no',                    
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,       
                        ],
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->created_datetime));
                            },
                        ],
                        [
                            'attribute' => 'date_last_login',
                            'label' => 'Date Last Login',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                if(!empty($model->user->date_last_login)) {
                                    return date('d-m-Y H:i:s', strtotime($model->user->date_last_login));
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],            
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->getActionsapprove();
                            },
                        ],            
                    ],
                ]);
               ?>
            </div>
        </div>
    </div>


