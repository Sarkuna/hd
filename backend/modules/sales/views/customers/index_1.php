<?php

use yii\helpers\Html;
use yii\grid\GridView;
$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Customers';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();
?>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">
                <?php
                    if($myclient->upload_receipt_module == '0') {
                ?>
                
                <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Add New']) ?>
                <?= Html::a('<i class="fa fa-download"></i>', ['import-excel'], ['class' => 'btn btn-info', 'title' => 'Import Excel']) ?>
                <?php
                }
                ?>
        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?php
                if($myclient->upload_receipt_module == '0') {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        'clients_ref_no',
                        // 'dealer_ref_no',
                        'full_name',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],
                        /*[
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["P" => "Pending", "N" => "Not Approved", "A" => "Active", "D" => "Deactive", "X" => "Deleted"],        
                        ], */           
                        //'gender',
                        // 'telephone_no',
                        //'mobile_no',
                        // 'date_of_Birth',
                        // 'nric_passport_no',
                        // 'Race',
                        // 'Region',
                        //'country_id',
                        // 'address_id',
                        // 'nationality_id',
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["P" => "Pending", "A" => "Active", "D" => "Deactive"],
                        ],
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->created_datetime;
                            },
                        ],            
                        // 'updated_datetime',
                        // 'created_by',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ],            
                    ],
                ]);
                
                            
                }else {
                    echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        // 'dealer_ref_no',
                        /*'full_name',
                        [
                            'attribute' => 'mobile_no',
                            'label' => 'Mobile No',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->mobile_no;
                            },
                        ],*/
                       [
                            'attribute' => 'end_user_level_id',
                            'label' => 'Yearly Purchase Volume',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->end_user_level_id)) {
                                   return $model->yearly->name; 
                                }else {
                                    return 'N/A';
                                }
                                
                            },
                        ],                                   
                        'company_name',
                        [
                            'attribute' => 'CIDB_registration_number',
                            'label' => 'CIDB No',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->CIDB_registration_number;
                            },
                        ],
                        [
                            'attribute' => 'CIDB_Grade',
                            'label' => 'CIDB Grade',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->CIDB_Grade;
                            },
                        ],            
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["P" => "Pending", "N" => "Not Approved", "A" => "Active", "D" => "Deactive", "X" => "Deleted"],          
                        ],            
                        //'gender',
                        // 'telephone_no',
                        //'mobile_no',
                        // 'date_of_Birth',
                        // 'nric_passport_no',
                        // 'Race',
                        // 'Region',
                        //'country_id',
                        // 'address_id',
                        // 'nationality_id',
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->created_datetime;
                            },
                        ],            
                        // 'updated_datetime',
                        // 'created_by',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ],            
                    ],
                ]);
               }
               ?>
            </div>
        </div>
    </div>


