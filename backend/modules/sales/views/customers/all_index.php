<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\components\Customers_Status_Widget;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$clientID = $session['currentclientID'];
$this->title = 'Customers Search';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$typename = \common\models\TypeName::find()->where([
                'clientID' => $session['currentclientID'],
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>



    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?= $this->render('_formSearch', ['searchModel' => $searchModel]); ?>
                <?php

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                if(!empty($model->companyInfo->company_name)) {
                                    return $model->companyInfo->company_name;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],             
                        'full_name',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->user->email)) {
                                    return $model->user->email;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        'mobile_no',                      
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->user->typeName->name)) {
                                    return $model->user->typeName->name;
                                }else {
                                    return 'N/A';
                                }
                                
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,       
                        ],          
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->created_datetime));
                            },
                        ],            
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getAllaction();
                            },
                        ],            
                    ],
                ]);
               ?>
            </div>
        </div>
    </div>


