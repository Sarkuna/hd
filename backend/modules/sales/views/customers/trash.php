<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$clientID = 16;
$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => $clientID,
            ])->one();

$typename = \common\models\TypeName::find()->where([
                'clientID' => $clientID,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?> <span class="label label-danger">Trash</span></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?php

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],           
                        'full_name',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],
                        'mobile_no',                    
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,       
                        ],
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Date Added',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->created_datetime));
                            },
                        ],
                        [
                            'attribute' => 'date_last_login',
                            'label' => 'Date Last Login',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                if(!empty($model->user->date_last_login)) {
                                    return date('d-m-Y H:i:s', strtotime($model->user->date_last_login));
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '100'],
                            'template' => '{view} {log}', //{view} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<i class="fa fa-address-card"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'btn btn-info btn-sm']));
                                },
                                'log' => function ($url, $model) {
                                    $url = '/sales/customers/log?id='.$model->userID;
                                    return (Html::a('<i class="fa fa-eye"></i>', $url, ['title' => Yii::t('app', 'Log'), 'class' => 'btn btn-default btn-sm']));
                                } 
                            ],
                        ],                         
                    ],
                ]);
               ?>
            </div>
        </div>
    </div>


