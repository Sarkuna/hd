<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
?>

<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Shipping Address'); ?>
	<div class="pull-right">
	<?php if($model->user->status != 'X') { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Add'), ['/sales/address/add', 'userid' => $model->userID, 'tab' => 'address'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="box-body">
    <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Postcode</th>
                        <th>Region / State</th>
                        <th>Country</th>                        
                        <th style="width: 90px"></th>
                    </tr>               
              
            <?php
            if(count($address) > 0){
                $n = 1;
                foreach($address as $myaddress){
                    $edit = '<a href="' . Url::to(['/sales/address/editaddress', 'id' => $myaddress->vip_customer_address_id]) . '" title="Edit" class = "btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i></a> ';
                    
                    $del = Html::a('<i class="fa fa-trash"></i>', ['/sales/address/deleteaddress', 'id' => $myaddress->vip_customer_address_id], [
                        'class' => 'btn btn-danger btn-sm',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this?',
                            'method' => 'post',
                        ],
                    ]);
                    echo '<tr>
                        <td>'.$n.'</td>
                        <td>'.$myaddress->firstname.' '.$myaddress->lastname.'</td>
                        <td>'.$myaddress->address_1.' '.$myaddress->address_2.'</td>
                        <td>'.$myaddress->city.'</td>
                        <td>'.$myaddress->postcode.'</td>
                        <td>'.$myaddress->states->state_name.'</td>
                        <td>'.$myaddress->country->name.'</td>
                        <td>'.$edit.$del.'</td>
                    </tr>';
                    $n++;
                }
            }else {
                echo '<tr><td colspan="8" class="text-center">No results found.</td></tr>';
            }
            ?>
            </tbody>
            </table> 
    </div>
</div>