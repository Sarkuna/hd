<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];

$total = 0;
$totalpoint = \common\models\VIPCustomerAccount2::find()
->where(['>', 'points_in', 0])
->andWhere(['clientID'=>$clientID, 'customer_id'=>$model->userID])
->sum('points_in');

$redeemed_point = 0;
$redeemed_point = \common\models\VIPCustomerAccount2::find()
->where(['<', 'points_in', 0])
->andWhere(['clientID'=>$clientID, 'customer_id'=>$model->userID])
->sum('points_in');

$account2lists = \common\models\VIPCustomerAccount2::find()
           ->select('account2_id, description, points_in, date_added')
           ->where(['clientID'=>$clientID, 'customer_id' => $model->userID])
           ->orderBy(['date_added'=>SORT_DESC])
           ->limit(20)
           ->asArray()
           ->all();
$account2countall = \common\models\VIPCustomerAccount2::find()
           ->where(['clientID'=>$clientID, 'customer_id' => $model->userID])
           ->count();

$redeemed_point = str_replace('-', '', $redeemed_point);
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Account 2'); ?>
	<div class="pull-right">

	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row hide">
    <div class="col-lg-4 col-md-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $totalpoint ? $totalpoint : '0' ?></h3>
                <p>Total Available Point</p>                            
            </div>

        </div>
    </div>

    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $redeemed_point ? $redeemed_point : '0' ?></h3>
                <p>Redeemed Point</p>
            </div>

        </div>
    </div>
    <!-- ./col -->

    <!-- ./col -->
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $totalpoint - $redeemed_point ?></h3>

                <p>Balance Point</p>
            </div>                          
        </div>
    </div>

    <!--Welcome KANSAI DEALER<br>You have 3,697 points<br>Expiring on 31 March 2018: 0points-->
</div>

<div class="box-body">
    <table class="table table-bordered" id="myTable">
                <tbody id= "data_table">
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">Added Date</th>
                        <th width="65%">Description</th>
                        <th width="10%">Points</th>
                    </tr>               
                    <?php
                    $n = 1;
                    $book_id = '';
                    
                    if(count($account2lists) > 0) {
                        foreach($account2lists as $account2list){
                            $book_id = $account2list['account2_id'];
                            echo '<tr>
                                <td>'.$n.'</td>
                                <td>'.date('d-m-y', strtotime($account2list['date_added'])).'</td>                            
                                <td>'.$account2list['description'].'</td>
                                <td class="text-right">'.$account2list['points_in'].'</td>   
                                </tr>';
                            $n++;
                        }
                        
                        if($account2countall > 20) {
                            echo '<tr id="remove_row">
				<td colspan="4"><center><button id="load" class="btn btn-info btn-block" data-id="'.$book_id.'">Load More</button></center></td>
                            </tr>';
                        }
                        
                    }else {
                        echo '<tr><td class="text-center" colspan="4">No record found!</td></tr>';
                    }
                    ?>
            
            </tbody>
            </table>
</div>

<?php
    $tst = Url::to(['account-two-data']);
    $userID = $model->userID;
    $script = <<<EOD
            $(document).ready(function(){
	$(document).on('click', '#load', function(){
		var last_row = $(this).data('id');
                var rowCount = $('#myTable >tbody >tr').length - 2;
		$.ajax({
			url: '$tst',
			type: 'POST',
			data: {'last_row': last_row, 'userid': $userID, 'rowCount': rowCount},
                        cache: false,
			success: function(data){
				if(data != ""){
					$('#remove_row').html('<td class="text-center" colspan="4">Loading...</td>');
					setTimeout(function(){
						$('#remove_row').remove();
						$('#data_table').append(data);
					}, 3000);
				}else{
					$('#load').html('No Data');
				}
			}
		});
		
	});
});
EOD;
$this->registerJs($script);
    ?>   