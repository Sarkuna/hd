<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
/* @var $form yii\widgets\ActiveForm */
use common\models\Salutation;
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\EndUserLevel;
use common\models\ProfileSelection;
use borales\extensions\phoneInput\PhoneInput;


$session = Yii::$app->session;

$countrylists = VipCountry::find()->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');


$regionlists = VIPZone::find()->orderBy([
    'name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'zone_id', 'name');

$yearly_purchases = EndUserLevel::find()
            ->where([
                'clientID' => $session['currentclientID'],
        ])->all();
$yearly_purchase = ArrayHelper::map($yearly_purchases, 'end_user_level_id', 'name');


$profile_selections = ProfileSelection::find()
            ->where([
                'clientID' => $session['currentclientID'],
        ])->all();
$profile_selection = ArrayHelper::map($profile_selections, 'profile_selection_id', 'name');
?>


<div class="vipcustomer-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipcustomer-form','class' => 'form-horizontal form-label-left']]); ?>
<div class="box-body">
    <?php
    $salutations = Salutation::find()->all();
    $salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');
    //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
    echo $form->field($model, 'salutation_id', [
                'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])
            ->dropDownList(
                    $salutationsData, ['prompt' => '--', 'id' => 'salutation']
    );
    ?>
    
    
    
    <?= $form->field($model, 'full_name', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
    <?= $form->field($model, 'nric_passport_no', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    

    <?= $form->field($model, 'mobile_no', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(PhoneInput::className(), [
        'jsOptions' => [
            'onlyCountries' => ['MY'],
            'nationalMode' => false,
        ]
    ]);  ?>

    
    

<?= $form->field($model, 'address_1', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
<?= $form->field($model, 'city', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>    

<?= $form->field($model, 'postcode', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>

<?= $form->field($model, 'country_id', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(Select2::classname(), [
                'data' => $country,
                //'language' => 'de',
                'options' => [
                    'placeholder' => 'Select ...',
                    'onchange' => '$.get( "' . Url::toRoute('/ajax/states') . '", {id: $(this).val() } )
                        .done(function( data ) {
                           $("#vipcustomer-region").html(data);
                        }
                    );'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],               
                                
            ]);  ?>
    
    <?= $form->field($model, 'Region', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(Select2::classname(), [
                    'data' => $regionlist,
                    //'language' => 'de',
                    'options' => [
                        'placeholder' => '-- Select --',
                     ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);  ?>

    


    <?= $form->field($model, 'nationality_id', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(Select2::classname(), [
                'data' => $country,
                //'language' => 'de',
                'options' => [
                    'placeholder' => 'Select ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],               
                                
            ]);  ?>
    
    <?= $form->field($model, 'company_name', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    
    <?= $form->field($model, 'CIDB_registration_number', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    <?= $form->field($model, 'CIDB_Grade', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>
    

    <?=
    $form->field($model, 'end_user_level_id', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->dropDownList($yearly_purchase);
    ?>
    
    <?=
    $form->field($model, 'profile_selection_id', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->dropDownList($profile_selection);
    ?>
    
    <?=
    $form->field($model, 'status', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->dropDownList(['P' => 'Pending', 'N' => 'Not Approved', 'A' => 'Activate', 'D' => 'Deactivate', 'X' => 'Deleted']);
    ?>

</div>
<div class="box-footer">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
