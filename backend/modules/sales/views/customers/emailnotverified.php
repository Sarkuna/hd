<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use app\components\Customers_Status_Widget;

$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$clientID = 16;
$this->title = 'Email not Verified';
$this->params['breadcrumbs'][] = $this->title;
$myclient = \common\models\Client::find()->where([
                'clientID' => 16,
            ])->one();

$typename = \common\models\TypeName::find()->where([
                'clientID' => 16,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<style>
    .sp{margin-left: 2px;}
    input#vipcustomersearch-created_datetime {display:none;}
</style>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">

        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?php

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                         [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],            
                        'full_name',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],
                        'mobile_no',           
                        /*[
                            'attribute' => 'companyInfo',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],*/            
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,       
                        ],
                        /*[
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->getStatustext();
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["P" => "Pending", "A" => "Approve", "D" => "Deactive"],        
                        ],*/           
                        //'gender',
                        // 'telephone_no',
                        //'mobile_no',
                        // 'date_of_Birth',
                        // 'nric_passport_no',
                        // 'Race',
                        // 'Region',
                        //'country_id',
                        // 'address_id',
                        // 'nationality_id',
                        [
                            'attribute' => 'created_datetime',
                            'value' => 'created_datetime',
                            'label' => 'Date',
                            //'format' => ['php:D, d-M-Y H:i:s A'],
                            //'format' => 'datetime',
                            'format' =>  ['date', 'php:d-m-Y'],

                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_datetime',
                                'convertFormat' => true,
                                'readonly' => true,
                                //'type' => DatePicker::TYPE_BUTTON,
                                //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                                'pluginOptions' => [
                                    'minViewMode'=>'months',
                                    'format' => 'yyyy-MM'
                                    //'format' => 'dd-M-yyyy'
                                ],
                            ]),
                            'options' => ['width' => '100']
                        ],             
                        // 'updated_datetime',
                        // 'created_by',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ],            
                    ],
                ]);
               ?>
            </div>
        </div>
    </div>


