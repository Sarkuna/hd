<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="invoice no-margin">    
    <!-- title row -->
    <div class="row">
        <div class="redemption-form">

            <?php $form = ActiveForm::begin(['action' => ['/sales/customers/approvel', 'id' => Yii::$app->request->get('id')],'options' => ['method' => 'post']]) ?>
            <?php
            $formapprove->status = $model->user->status;
            echo $form->field($formapprove, 'status')->dropDownList(['P' => 'Pending', 'A' => 'Approve', "D" => "Deactive", 'X' => 'Delete']); 
            ?>


            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    
</section>


