<?php
error_reporting(0);
include ('inc/header.php');
(date("m") > 3) ? $expyear = date("Y") + 1 : $expyear = date("Y");
?>
<body>
    <!--top navi-->
    <!--side navi-->
    <div id="container">
        <?php include('main_menu.php'); ?>		
        <div id="content">
            <div class="page-header">
                <div class="container-fluid">
                    <h1>Dashboard</h1>
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Point</a></li>
                    </ul>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <?php include('topcard.php'); ?>
                        <div class="form-box col-lg-8">
                            <form role="form-control" action="pointview.php" method="post">
                                <div class="input-group input-group-sm space col-lg-4">
                                    <span class="input-group-addon" id="sizing-addon1">Name</span>
                                    <input class="form-control" type="text" name="string" placeholder="Search by name" id="string" value="<?php echo stripcslashes($_REQUEST["string"]); ?>" aria-describedby="sizing-addon1" />
                                </div>
                                <div class="input-group input-group-sm space col-lg-3">	
                                    <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-globe fa-sm"></i> Region</span>
                                    <select class="form-control" name="dealer_Rg" id="Dealer_Region sizing-addon2">
                                        <option value="">  -- Option --  </option>
                                        <?php
                                        $sql = "SELECT dealer_Rg FROM kp_point inner join kp_dealer on kp_point.dealer_CustomerCode = kp_dealer.dealer_CustomerCode GROUP BY dealer_Rg ORDER BY dealer_Rg";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows > 0) {
                                            while ($row = $result->fetch_array()) {
                                                echo "<option value='" . $row["dealer_Rg"] . "'" . ($row["dealer_Rg"] == $_REQUEST["dealer_Rg"] ? " selected" : "") . ">" . $row["dealer_Rg"] . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="input-group input-group-sm space col-lg-3">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">Search <i class="fa fa-check-square-o fa-sm"></i></button>
                                        <a href="pointview.php" class="btn btn-warning">Reset <i class="fa  fa-history fa-sm"></i></a>
                                    </span>	
                                </div>
                            </form>
                        </div>					
                        <div class="col-lg-10">
                            <table class="table table-hover col-l-12 col-md-12 col-sm-12 col-xs-12">
                                <tr>
                                    <th>Dealer Code</th>
                                    <th>Dealer Name</th>
                                    <th>Awarded Points</th>
                                    <th>Redeemed Points</th>
                                    <th>Expired Points</th>
                                    <th>Balance Points</th>
                                    <th>Points Expiring<br>31 March <?= $expyear ?></th>
                                </tr>
                                <?php
                                $exppointstot = 0;
                                //create connection for OC
                                $connoc = new mysqli($servername, $username, $password, $dbocname);
                                //check connection
                                if ($connoc->connect_error) {
                                    die("Connection failed:" . $connoc->connect_error);
                                }

                                //by name or email
                                if ($_REQUEST["string"] <> '') {
                                    $search_string = " AND Dealer_Name LIKE '%" . $conn->real_escape_string($_REQUEST["string"]) . "%'";
                                }
                                //by region
                                if ($_REQUEST["dealer_Rg"] <> '') {
                                    $search_region = " AND kp_dealer.dealer_Rg ='" . $conn->real_escape_string($_REQUEST["dealer_Rg"]) . "'";
                                }
                                $sql = "SELECT * FROM kp_dealer inner join kp_point on kp_dealer.dealer_CustomerCode = kp_point.dealer_CustomerCode" . $search_string . $search_region;

                                $result = $conn->query($sql);
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        if ($shuff) {
                                            $shuffcode = str_shuffle($row['dealer_CustomerCode']);
                                            $shuffname = str_shuffle($row['dealer_Name']);
                                        } else {
                                            $shuffcode = $row['dealer_CustomerCode'];
                                            $shuffname = $row['dealer_Name'];
                                        }

                                        $expyearminus = $expyear - 1;
                                        $expdatestart = ($expyearminus - 1) . '-04-01 00:00:00';
                                        $expdateend = $expyearminus . '-03-31 23:59:59'; //adjust to march later


                                        $sqlexp = "SELECT customer_id FROM vip_customer WHERE lastname = '" . $row['dealer_CustomerCode'] . "'";
                                        $resultexp = $connoc->query($sqlexp);
                                        $rowexp = $resultexp->fetch_assoc();
                                        $custID = $rowexp['customer_id'];

                                        $sqlexp = "SELECT SUM(points) AS totpoints FROM vip_customer_reward WHERE customer_id = '$custID' AND description NOT LIKE '%Order ID%'
								AND description NOT LIKE '%Expired%' AND date_added BETWEEN '$expdatestart' AND '$expdateend'"; //
                                        $resultexp = $connoc->query($sqlexp);
                                        $rowexp = $resultexp->fetch_assoc();
                                        $totpoints = $rowexp['totpoints']; //balance points: total awarded - total redeemed - total expired

                                        /*
                                          $sqlexp = "SELECT SUM(points) AS awardpoints FROM vip_customer_reward WHERE description LIKE '%$expyearminus%' AND customer_id = '$custID'";
                                          $resultexp = $connoc->query($sqlexp);
                                          $rowexp = $resultexp->fetch_assoc();
                                          $awardpoints = $rowexp['awardpoints'];
                                         */


                                        $startdate = ($expyearminus - 1) . '-04-01 00:00:00';
                                        $enddate = $expyear . '-03-31 23:59:59';
                                        $sqlexp = "SELECT SUM(points) AS redpoints FROM vip_customer_reward WHERE customer_id = '$custID' AND description LIKE '%Order ID%' AND date_added BETWEEN '$startdate' AND '$enddate'";
                                        $resultexp = $connoc->query($sqlexp);
                                        $rowexp = $resultexp->fetch_assoc();
                                        $redpoints = $rowexp['redpoints'];

                                        $exppoints = $totpoints + $redpoints; //plus because red points are negative in value

                                        $exppoints = round($exppoints * 0.36 / 0.5); //remove this line after expiry in 2018 - this is for point value ajustment in 3 Jul 2017

                                        if ($exppoints < 0) {
                                            $exppoints = 0;
                                        }
                                        $exppointstot+=$exppoints;
                                        echo'
								<tr>
									<td>' . $shuffcode . '</td>
									<td>' . $shuffname . '</td>
									<td>' . $row['points_Awarded'] . '</td>
									<td>' . $row['points_Redeemed'] . '</td>
									<td>' . $row['points_Expired'] . '</td>
									<td>' . $row['points_Balance'] . '</td>
									<td>' . $exppoints . '</td>
								</tr>				
									';
                                    }
                                } else {
                                    echo'
								<tr>
									<td colspan="7">Result not found.</td>
								</tr>
									';
                                }
                                $connoc->close();
                                $sql = "SELECT sum(points_Awarded), sum(points_Redeemed), sum(points_Expired), sum(points_Balance) 
						FROM kp_point inner join kp_dealer on kp_point.dealer_CustomerCode = kp_dealer.dealer_CustomerCode";

                                $result1 = $conn->query($sql);
                                while ($row = $result1->fetch_assoc()) {
                                    $award = $row['sum(points_Awarded)'];
                                    $redeem = $row['sum(points_Redeemed)'];
                                    $expired = $row['sum(points_Expired)'];
                                    $flexi = $row['sum(points_Balance)'];
                                    echo'
								<tr class="double_line">
									<td colspan="2"><h3>TOTAL</h3></td>
									<td>' . $award . '</td>
									<td>' . $redeem . '</td>
									<td>' . $expired . '</td>
									<td>' . $flexi . '</td>
									<td>' . $exppointstot . '</td>
								</tr>';
                                }
                                $conn->close();
                                ?>								
                            </table>
                        </div>
                    </div>
                </div>		
            </div>		
        </div>
        <footer id="footer">
            <a href="#">Kansai Admin Portal</a> <i class="fa fa-copyright fa-sm"></i> 2015 All Rights Reserved.
            <br>
            Version 1.0
        </footer>
    </div>
</body>
</html>
