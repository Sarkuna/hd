<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="vipcustomer-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipcustomer-form','class' => 'form-horizontal form-label-left']]); ?>
<div class="box-body">
    
    <?= $form->field($model, 'date_added', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->widget(DatePicker::classname(), [
    'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
    'removeButton' => false,
    'pluginOptions' => [
        'autoclose' => true,
        'todayHighlight' => false,
        'todayBtn' => false,
        'format' => 'yyyy-mm-dd',
    //'startDate' => $startDate,
    ]]) ?>
    

<?= $form->field($model, 'description', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>

<?= $form->field($model, 'points', [
  'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->textInput(array('placeholder' => ''));  ?>  
    
 <?php
 //A-Awarded, E-Expired, C-(Redeemed or Refunded) 
echo $form->field($model, 'bb_type',[
    'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
])->dropDownList(
            ['A' => 'Awarded', 'E' => 'Expired', 'V' => 'Redeemed or Refunded']
    ); ?>   
    
</div>
<div class="box-footer">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
