<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Bank Info'); ?>
	<div class="pull-right">
	<?php if($model->user->status != 'X') { ?>
		<?= Html::a('Add ', ['/sales/banks/add', 'userid' => $model->userID], ['class' => 'btn btn-primary btn-sm']) ?>
	<?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Bank Name</th>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>NRIC/Passport</th>
                        <th>Verify</th>                     
                        <th style="width: 90px"></th>
                    </tr>               
              
            <?php
            if(count($bankslisit) > 0){
                $n = 1;
                foreach($bankslisit as $bank){
                    if($bank->bank_status == 'P'){
                        $edit = '<a href="' . Url::to(['/sales/banks/editbank', 'id' => $bank->vip_customer_bank_id]) . '" title="Edit" class = "btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i></a> ';
                    }else {
                        $edit = '';
                    }
                    
                    $del = Html::a('<i class="fa fa-trash"></i>', ['/sales/banks/deletebank', 'id' => $bank->vip_customer_bank_id], [
                        'class' => 'btn btn-danger btn-sm',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this?',
                            'method' => 'post',
                        ],
                    ]);
                    echo '<tr>
                        <td>'.$n.'</td>
                        <td>'.$bank->bank->bank_name.'</td>
                        <td>'.$bank->account_name.'</td>
                        <td>'.$bank->account_number.'</td>
                        <td>'.$bank->nric_passport.'</td>
                        <td>'.$bank->getStatus().'</td>
                        <td>'.$edit.$del.'</td>
                    </tr>';
                    $n++;
                }
            }else {
                echo '<tr><td colspan="8" class="text-center">No results found.</td></tr>';
            }
            ?>
            </tbody>
            </table>
        </div>