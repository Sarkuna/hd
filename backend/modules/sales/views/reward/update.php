<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerReward */

$this->title = 'Update Vipcustomer Reward: ' . $model->customer_reward_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Rewards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->customer_reward_id, 'url' => ['view', 'id' => $model->customer_reward_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipcustomer-reward-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
