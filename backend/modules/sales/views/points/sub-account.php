<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Account Points';
$this->params['breadcrumbs'][] = ['label' => 'Manage Points', 'url' => ['?type=1']];
$this->params['breadcrumbs'][] = $this->title;
$expired_points = 0;
?>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Awarded Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($awarded_points ? : $awarded_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Redeemed Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($redeemed_points  ? : $redeemed_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-star-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Expired Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($expired_points ? : $expired_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-star-half"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Balance Points</span>
                <span class="info-box-number">
                    <?php
                    $balance_points = 0;
                    $balance_points = $awarded_points - $redeemed_points - $expired_points;
                    echo Yii::$app->formatter->asInteger($balance_points);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    
</div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        // 'dealer_ref_no',
                        
                        /*[
                            'attribute' => 'clients_ref_no',
                            'label' => 'Dealer Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],*/
                        //'customer_id',
                        [
                            'attribute' => 'code',
                            'label' => 'Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->customer->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],            
                        [
                            'attribute' => 'customer_id',
                            'label' => 'Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->customer->full_name;
                            },
                        ],
                        //'description',
                        //'points_in',          
                        //'full_name',
                        /*[
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],*/
                        [
                            'attribute' => 'awarded_points',
                            'label' => 'Awarded Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalAdd());
                            },
                        ],            
                        [
                            'attribute' => 'redeemed_points',
                            'label' => 'Redeemed Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalMinus());
                            },
                        ],                        
                        [
                            'attribute' => 'expired_points',
                            'label' => 'Expired Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalExpiry());
                            },
                        ],
                        [
                            'attribute' => 'balance_points',
                            'label' => 'Balance Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '150'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                $balance_points = 0;
                                $balance_points = $model->getTotalAdd() - $model->getTotalMinus();
                                return Yii::$app->formatter->asInteger($balance_points);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '50'],
                            'template' => '{view}', //{view} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    $url = '/sales/customers/view?id='.$model->customer->vip_customer_id.'#account2';
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                }
                            ],
                        ],        
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>


