<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Awarded Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($awarded_points ? : $awarded_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Redeemed Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($redeemed_points  ? : $redeemed_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-star-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Expired Points</span>
                <span class="info-box-number"><?= Yii::$app->formatter->asInteger($expired_points ? : $expired_points = '0') ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-star-half"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Balance Points</span>
                <span class="info-box-number">
                    <?php
                    $balance_points = 0;
                    $balance_points = $awarded_points - $redeemed_points - $expired_points;
                    echo Yii::$app->formatter->asInteger($balance_points);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    
</div>



    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                       
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        'full_name',
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Dealer Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'userID',
                            'label' => 'userID',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->userID;
                            },
                        ],            
                        [
                            'attribute' => 'company_name',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],                     
                        //'full_name',
                        [
                            'attribute' => 'awarded_points',
                            'label' => 'Awarded Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalAdd());
                            },
                        ],           
                        [
                            'attribute' => 'redeemed_points',
                            'label' => 'Redeemed Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalMinus());
                            },
                        ],
                        
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Y1 Balance b/f',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getY1bf());
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Y2 Balance b/f',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getY2bf());
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Y3 Balance b/f',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getY3bf());
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Y4 Balance b/f',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getY4bf());
                            },
                        ],
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Y5 Balance b/f',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getY5bf());
                            },
                        ],
                                    
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Going to Expiry',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getExpiryY4());
                            },
                        ],
                                    
                        [
                            'attribute' => 'expired_points1',
                            'label' => 'Balance',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                $tot = $model->getTotalAdd() - $model->getTotalMinus() - $model->getEy2() - $model->getEy3() - $model->getEy4();
                                return Yii::$app->formatter->asInteger($tot);
                                //return Yii::$app->formatter->asInteger($model->getY5());
                            },
                        ],
                                 
                    ]           
                
                ?>
                <?php
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]) . "<hr>\n".
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    
                ]); 
                ?>
            </div>
        </div>
    </div>


