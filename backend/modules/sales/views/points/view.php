<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerReward */

$this->title = $model->customer_reward_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Rewards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-reward-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->customer_reward_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->customer_reward_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'customer_reward_id',
            'clientID',
            'customer_id',
            'order_id',
            'description:ntext',
            'points',
            'date_added',
            'created_datetime',
            'updated_datetime',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
