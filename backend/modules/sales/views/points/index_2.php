<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <span class="info-box-text"><strong>Awarded Points</strong></span>
                <h3><?= Yii::$app->formatter->asInteger($awarded_points) ?></h3>
                <p>Distributors : <?= Yii::$app->formatter->asInteger($distributors ? : $distributors = '0') ?></p>
                <p>Direct Dealer : <?= Yii::$app->formatter->asInteger($direct_dealer ? : $direct_dealer = '0') ?></p>
                <p>Indirect : <?= Yii::$app->formatter->asInteger($indirect ? : $indirect = '0') ?></p>
            </div>
            <div class="icon">
                <i class="fa fa-star"></i>
            </div>          
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <span class="info-box-text"><strong>Redeemed Points</strong></span>
                <h3><?= Yii::$app->formatter->asInteger($redeemed_points  ? : $redeemed_points = '0') ?></h3>
                <p>Distributors : 0pts</p>
                <p>Direct Dealer : 0pts</p>
                <p>Indirect : 0pts</p>
            </div>
            <div class="icon">
                <i class="fa fa-star-o"></i>
            </div>          
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <span class="info-box-text"><strong>Expired Points</strong></span>
                <h3><?= Yii::$app->formatter->asInteger($expired_points ? : $expired_points = '0') ?></h3>
                <p>Distributors : 0pts</p>
                <p>Direct Dealer : 0pts</p>
                <p>Indirect : 0pts</p>
            </div>
            <div class="icon">
                <i class="fa fa-star-o"></i>
            </div>          
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <span class="info-box-text"><strong>Balance Points</strong></span>
                <h3>
                    <?php
                    $balance_points = 0;
                    $balance_points = $awarded_points - $redeemed_points - $expired_points;
                    echo Yii::$app->formatter->asInteger($balance_points);
                    ?>
                </h3>
                <p>Distributors : 0pts</p>
                <p>Direct Dealer : 0pts</p>
                <p>Indirect : 0pts</p>
            </div>
            <div class="icon">
                <i class="fa fa-star-o"></i>
            </div>          
        </div>
    </div>
</div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">
                <?= Html::a('<i class="fa fa-download"></i>', [''], ['class' => 'btn btn-info', 'title' => 'Export Excel']) ?>
            
        
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index table-responsive">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'vip_customer_id',
                        //'userID',
                        //'clientID',
                        //'salutation_id',
                        //'clients_ref_no',
                        // 'dealer_ref_no',
                        
                        [
                            'attribute' => 'clients_ref_no',
                            'label' => 'Dealer Code',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                        ],
                        'full_name',
                        /*[
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                        ],*/
                        [
                            'attribute' => 'awarded_points',
                            'label' => 'Awarded Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalAdd());
                            },
                        ],            
                        [
                            'attribute' => 'redeemed_points',
                            'label' => 'Redeemed Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalMinus());
                            },
                        ],
                        
                        [
                            'attribute' => 'expired_points',
                            'label' => 'Expired Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asInteger($model->getTotalExpiry());
                            },
                        ],
                        [
                            'attribute' => 'balance_points',
                            'label' => 'Balance Points',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '150'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                $balance_points = 0;
                                $balance_points = $model->getTotalAdd() - $model->getTotalMinus() - $model->getTotalExpiry();
                                return Yii::$app->formatter->asInteger($balance_points);
                            },
                        ],
                        // 'updated_datetime',
                        // 'created_by',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        /*[
                            'attribute' => 'action',
                            'label' => 'Action',
                            'format' => 'raw',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ],*/          
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>


