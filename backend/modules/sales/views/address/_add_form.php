<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        //->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => '129'])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');

?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'vipcustomer-address-add-form','class' => 'form-horizontal form-label-left']]); ?>
<div class="box-body">
        
    <?=
    $form->field($model, 'firstname', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'lastname', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'company', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'address_1', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'address_2', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'postcode', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>
    
    <?=
    $form->field($model, 'city', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>
    
    <?=
        $form->field($model, 'country_id', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->dropDownList($country, [
            'prompt' => 'Select...',
            'onchange' => '$.get( "' . Url::toRoute('/sales/address/region') . '", {id: $(this).val() } )
                .done(function( data ) {
                   $("#vipcustomeraddress-state").html(data);
                }
            );'
          ]);
    ?>
    
    <?=
    $form->field($model, 'state', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->dropDownList($regionlist, ['prompt' => 'Select...']);
    ?>

</div>
<div class="box-footer">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>