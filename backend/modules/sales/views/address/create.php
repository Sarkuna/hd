<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

$this->title = 'Create Vipcustomer Address';
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipcustomer-address-create">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>