<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

$this->title = 'Shipping Address';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['/sales/customers/view', 'id'=>$modelcus->vip_customer_id]];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipcustomer-address-create">
        <?=
        $this->render('_add_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
