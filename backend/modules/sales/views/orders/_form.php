<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */
/* @var $form yii\widgets\ActiveForm */
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = VIPZone::find()
        ->where(['country_id' => '129'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'zone_id', 'name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');

$OrderStatus = common\models\VIPOrderStatus::find()
        //->where(['option_id' => '2'])
        ->orderBy([
    'order_status_id' => SORT_ASC,
])->all();
$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
$this->title = 'Shipping Details';
?>

<div class="viporder-form row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['options' => ['id' => 'viporder-form','class' => 'form-horizontal form-label-left']]); ?>

                    <?=
                    $form->field($model, 'shipping_firstname', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_lastname', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_company', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_address_1', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_address_2', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>

                    <?=
                    $form->field($model, 'shipping_city', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_postcode', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_country_id', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($country, ['prompt' => 'Select...']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_zone', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($regionlist, ['prompt' => 'Select...']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_zone_id', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($optionslist, ['prompt' => 'Select...']);
                    ?>

                

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>

        
        

    <?php ActiveForm::end(); ?>
    </div>

    

    

</div>
