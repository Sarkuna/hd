<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);
?>
<div class="row">
    <div class="col-lg-12">
        <section class="invoice no-margin">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> <?= $model->invoice_prefix ?>
                        <small class="pull-right">Order date: <?= date('d/m/Y', strtotime($model->created_datetime)) ?></small>
                        
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <b>Dealer info</b>
                    <address>
                        Code: <?= $model->customer->clients_ref_no ?><br>
                        Company Name: <?= $model->customer->company->company_name ?><br><br>
                        
                        Name: <strong><a href="<?php echo Url::to(['/sales/customers/view', 'id' => $model->customer->vip_customer_id]); ?>" target="_blank"><?= $model->customer->full_name ?></a></strong><br>
                        Email: <a href="mailto:<?= $model->customeremail->email ?>"><?= $model->customeremail->email ?></a><br>
                        Mobile: <?= $model->customer->mobile_no ?><br>                      
                        Telephone: <?= $model->customer->telephone_no ?><br><br>
                        
                        

                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>Shipping Details</b>
                    <address>
                        <strong><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></strong><br>
                        <?= $model->shipping_company ?><br>
                        <?= $model->shipping_address_1 ?>
                        <?= $model->shipping_address_2 ?><br>
                        <?= $model->shipping_city ?> 
                        <?= $model->shipping_postcode ?><br>
                        <?= $model->shipping_zone ?> - <?= $model->shipping_zone_id ?><br>
                        <?= $model->shipping_country_id ?>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <?php
                    $invoice_num = $model->bb_invoice_no ? $model->bb_invoice_no : 'N/A';
                    echo '<h3 style="margin-top: 0px;" class="pull-right"><span class="label label-' . $model->orderStatus->labelbg . '">' . $model->orderStatus->name . '</span></h3>';
                    echo '<div class="clear"></div><b>Invoice #'.$invoice_num.'</b><br>';
                    echo '<b>Last Balance Points:</b> '.Yii::$app->formatter->asInteger($model->last_point_balance).'pts';
                    ?>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-left">Product</td>
                                <td class="text-left">Model</td>
                                <td class="text-right">Quantity</td>
                                <td class="text-right">Unit Points</td>
                                <td class="text-right">Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //print_r($model->getOrderProducts())
                            $subtotal = 0; $total = 0; $shipping_point_total = 0;
                            foreach ($model->orderProducts as $subproduct) {
                                //echo $subproduct->name.'<br>';
                                $optlisit = '';
                                $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                if ($OrderOption > 0) {
                                    $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                    foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                        $optlisit .= '<br>
                                  &nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                    }
                                }
                                echo '<tr>
                              <td class="text-left"><a href="' . Url::to(['/catalog/products/view', 'id' => $subproduct->product_id]) . '" target="_blank">' . $subproduct->name . '</a>
                                  ' . $optlisit . '
                              </td>
                              <td class="text-left">' . $subproduct->model . '</td>
                              <td class="text-right">' . $subproduct->quantity . '</td>
                              <td class="text-right">' . $subproduct->point . 'pts</td>
                              <td class="text-right">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</td>
                          </tr>';
                                $subtotal += $subproduct->point_total;
                                $shipping_point_total += $subproduct->shipping_point_total;
                                
                            }
                            $total = $subtotal + $shipping_point_total;
                            ?>
                        </tbody>
                    </table>
                    <p>Comment: <?= $model->comment ? $model->comment : 'N/A' ?></p>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-8">
                    <p class="lead">History:</p>


                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        <?php
                            $comments = common\models\VIPOrderHistory::find()->where(['order_id' => $model->order_id])->all();
                            if(count($comments) > 0){
                                foreach($comments as $comment){
                                    if(!empty($comment->comment)){
                                        //$redemptionstatus = common\models\VIPOrderStatus::find()->where(['order_status_id' => $comment->order_status_id])->one();
                                        $actionlabel = '<span class="label label-'.$comment->orderstatus->labelbg.'">'.$comment->orderstatus->name.'</span>';
                                        echo $actionlabel.' <em class="pull-right">'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                                        if($comment->type == 1) {
                                            echo '<em>by: System</em><br>';
                                        }else {
                                            echo '<em>by: '.$comment->by->first_name.' '.$comment->by->last_name.'</em><br>';
                                        }
                                        echo $comment->comment.'<br><br>';
                                    }
                                }
                            }else{
                                echo 'No Comments';
                            }
                        ?>
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Order Summary</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody><tr>
                                    <th style="width:50%">Subtotal:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($subtotal) ?>pts</td>
                                </tr>
                                <tr>
                                    <th>Shipping:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($shipping_point_total) ?>pts</td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asInteger($total) ?>pts</td>
                                </tr>
                            </tbody></table>
                    </div>
                    
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <?php
                    if($model->order_status_id != '40' && $model->order_status_id != '50' && $model->order_status_id != '60') {
                        echo '<button class="btn btn-success pull-right" onclick="updateGuard('.$model->order_id.');return false;"><i class="fa fa-credit-card"></i> Approval Action</button>';
                    }
                    ?>
                    <?= Html::a('<i class="fa fa-print"></i> Print</a>', ['print', 'id' => $model->order_id], ['class' => 'btn btn-default','target'=>'_blank',]) ?>


                    <button type="button" class="btn btn-primary pull-right hide" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </button>
                </div>
            </div>
        </section>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Action </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/sales/orders/orderapprove"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
</script>