<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Orders';
$this->params['breadcrumbs'][] = $this->title;
$clientID = 16;
$typename = \common\models\TypeName::find()->where([
                'clientID' => $clientID,
            ])->asArray()->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
$OrderStatus = common\models\VIPOrderStatus::find()
        //->where(['option_id' => '2'])
        ->orderBy([
    'order_status_id' => SORT_ASC,
])->all();
$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
?>
<style>
    .actionbulk{display: none;}
</style>
<div class="box box-primary viporder-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?> - Accepted</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-lg-12 bulkact"> 
                <button class="btn btn-primary" id="btntest">Bulk Action</button>
            </div>
            <?=Html::beginForm(['/sales/orders/bulk-order'],'post');?>
                <div class="col-lg-12 actionbulk">
                    <div class="col-lg-2 no-padding">
                        <label> Status</label>
                        <?=Html::dropDownList('redemption_status','',['10' => 'Pending', '20' => 'Processing', '40' => 'Complete'],['class'=>'form-control',])?>
                        <label> Invoice Number </label>
                    <?= Html::textInput('invoice_num',"",['id'=>'downloadSourceCode','class' => 'form-control']); ?>
                    </div>
                    <div class="col-lg-6">
                        <label> Comment</label>
                        <?= Html::textarea('comment',"",['id'=>'downloadSourceCode','rows' => 6, 'class' => 'form-control']); ?>
                    </div>
                    <div class="col-lg-4 no-padding">
                        <?= Html::checkBox('notify',false,['label' => 'Notify to customer']); ?>
                        <label class="control-label" for="redemptionbulk-comment"></label>
                        <div class="form-group">
                            <?=
                            Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'btnsubmit',])
                            ?>
                            <?= Html::a('Cancel', ['/sales/orders'], ['class'=>'btn btn-default']) ?>
                            
                        </div>
                    </div>                      
                </div>
            </div>
            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            ['class' => '\yii\grid\CheckboxColumn',
                'headerOptions' => ['width' => '10'],
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                     if ($model->last_point_balance >= 0) {
                         return ['value' => $key];
                     }
                     return ['disabled' => true];
                 },
            ],
            [
                'attribute' => 'created_datetime1',
                'label' => 'Order Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            [
                'attribute' => 'clients_ref_no',
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->clients_ref_no;
                },
            ],            
            [
                'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ],       
            [
                'attribute' => 'type',
                'label' => 'Type',
                'format' => 'raw',
                'headerOptions' => ['width' => '90'],
                'value' => function ($model) {
                    return $model->user->typeName->name;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                'filter' => $typenames,
            ],
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                    return Yii::$app->formatter->asInteger($totalpoint);
                },
            ], 
             
            [
                'attribute' => 'last_point_balance',
                'label' => 'Last Point Balance',
                'format' => 'html',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asInteger($model->last_point_balance);
                },
            ],            
            [
                'attribute' => 'updated_datetime1',
                'label' => 'Updated Date',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],
            [
                'attribute' => 'bb_invoice_no',
                'label' => 'Invoice Number',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->bb_invoice_no ? $model->bb_invoice_no : 'TBA';
                },
            ],                     
            //'clientID',
            //'customer_id',
            // 'customer_group_id',
            // 'shipping_firstname',
            // 'shipping_lastname',
            // 'shipping_company',
            // 'shipping_address_1',
            // 'shipping_address_2',
            // 'shipping_city',
            // 'shipping_postcode',
            // 'shipping_country_id',
            // 'shipping_zone',
            // 'shipping_zone_id',
            // 'shipping_method',
            // 'shipping_code',
            // 'comment:ntext',
            //'order_status_id',
            // 'language_id',
            // 'ip',
            // 'user_agent',
            // 'accept_language',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'update' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
            </div>
            <?= Html::endForm();?> 
        </div>
    </div>

<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });
        $("#btntest").click(function()
        {
            $(".actionbulk").slideToggle( "slow" );
            $(".bulkact").hide();
            
            
        });
        $("#btnsubmit").click(function()
        {
            var keys = $("#w1").yiiGridView("getSelectedRows");
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>