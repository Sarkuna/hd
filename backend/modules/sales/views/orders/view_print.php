<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <section class="invoice no-margin">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> <?= $model->invoice_prefix ?>
                        <small class="pull-right">Date Added: <?= date('d/m/Y', strtotime($model->created_datetime)) ?></small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-5 invoice-col">
                    <b>Customer Info</b>
                    <address>
                        <strong><?= $model->customer->full_name ?></strong><br>
                        Company Name: <?= $model->storename->company ?><br>
                        Email: <a href="mailto:<?= $model->customeremail->email ?>"><?= $model->customeremail->email ?></a><br>
                        Telephone: <?= $model->customer->telephone_no ?><br>
                        Mobile: <?= $model->customer->mobile_no ?>

                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-5 invoice-col">
                    <b>Shipping Details</b>
                    <address>
                        <strong><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></strong><br>
                        <?= $model->shipping_company ?><br>
                        <?= $model->shipping_address_1 ?>
                        <?= $model->shipping_address_2 ?><br>
                        <?= $model->shipping_city ?> 
                        <?= $model->shipping_postcode ?><br>
                        <?= $model->customerAddress->states->name ?> <?= $model->customerAddress->delivery->name ?><br>
                        <?= $model->customerAddress->country->name ?>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-2 invoice-col">
                    <?php
                    echo '<h3 style="margin-top: 0px;" class="pull-right"><span class="label label-' . $model->orderStatus->labelbg . '">' . $model->orderStatus->name . '</span></h3>';
                    ?>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-left">Product</td>
                                <td class="text-left">Model</td>
                                <td class="text-right">Quantity</td>
                                <td class="text-right">Unit Points</td>
                                <td class="text-right">Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //print_r($model->getOrderProducts())
                            $total = '';
                            foreach ($model->orderProducts as $subproduct) {
                                //echo $subproduct->name.'<br>';
                                $optlisit = '';
                                $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                if ($OrderOption > 0) {
                                    $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                    foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                        $optlisit .= '<br>
                                  &nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                    }
                                }
                                echo '<tr>
                              <td class="text-left">' . $subproduct->name . '
                                  ' . $optlisit . '
                              </td>
                              <td class="text-left">' . $subproduct->model . '</td>
                              <td class="text-right">' . $subproduct->quantity . '</td>
                              <td class="text-right">' . $subproduct->point . 'pts</td>
                              <td class="text-right">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</td>
                          </tr>';
                                $total += $subproduct->point_total;
                            }
                            ?>
                        </tbody>
                    </table>
                    <p>Commend: <?= $model->comment ? $model->comment : 'N/A' ?></p>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-7">
                    <p class="lead">History:</p>


                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        <?php
                            $comments = common\models\VIPOrderHistory::find()->where(['order_id' => $model->order_id])->all();
                            if(count($comments) > 0){
                                foreach($comments as $comment){
                                    if(!empty($comment->comment)){
                                        //$redemptionstatus = common\models\VIPOrderStatus::find()->where(['order_status_id' => $comment->order_status_id])->one();
                                        $actionlabel = '<span class="label label-'.$comment->orderstatus->labelbg.'">'.$comment->orderstatus->name.'</span>';
                                        echo $actionlabel.' <em class="pull-right">'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                                        echo '<em>by: '.$comment->by->first_name.' '.$comment->by->last_name.'</em><br>';
                                        echo $comment->comment.'<br><br>';
                                    }
                                }
                            }else{
                                echo 'No Comments';
                            }
                        ?>
                    </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-5">
                    <p class="lead"></p>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th><h3>Total:</h3></th>
                                    <td class="text-right"><h3><?= Yii::$app->formatter->asInteger($total) ?>pts</h3></td>
                                </tr>
                            </tbody></table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
        </section>
    </div>
</div>


<script>
/***
  * Start Update Gardian Jquery
***/
window.onload = function () {
    window.print();
}
</script>