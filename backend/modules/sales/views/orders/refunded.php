<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Orders';
$this->params['breadcrumbs'][] = $this->title;
$clientID = 16;
$typename = \common\models\TypeName::find()->where([
                'clientID' => $clientID,
            ])->asArray()->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<div class="box box-primary viporder-index">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?> - Refunded</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'created_datetime1',
                'label' => 'Order Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            [
                'attribute' => 'clients_ref_no',
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->clients_ref_no;
                },
            ],            
            [
                'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->companyInfo->company_name;
                },
            ],
            [
                'attribute' => 'type',
                'label' => 'Type',
                'format' => 'raw',
                'headerOptions' => ['width' => '90'],
                'value' => function ($model) {
                    return $model->user->typeName->name;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                'filter' => $typenames,       
            ],
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                    return Yii::$app->formatter->asInteger($totalpoint);
                },
            ],            
            [
                'attribute' => 'updated_datetime1',
                'label' => 'Updated Date',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],
            [
                'attribute' => 'bb_invoice_no',
                'label' => 'Invoice Number',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->bb_invoice_no ? $model->bb_invoice_no : 'TBA';
                },
            ],
            //'clientID',
            //'customer_id',
            // 'customer_group_id',
            // 'shipping_firstname',
            // 'shipping_lastname',
            // 'shipping_company',
            // 'shipping_address_1',
            // 'shipping_address_2',
            // 'shipping_city',
            // 'shipping_postcode',
            // 'shipping_country_id',
            // 'shipping_zone',
            // 'shipping_zone_id',
            // 'shipping_method',
            // 'shipping_code',
            // 'comment:ntext',
            //'order_status_id',
            // 'language_id',
            // 'ip',
            // 'user_agent',
            // 'accept_language',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'update' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
            </div>
        </div>
    </div>

