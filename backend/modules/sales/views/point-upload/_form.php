<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummary */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$typename = \common\models\TypeName::find()
            ->where([
                'clientID' => $clientID,
        ])->all();
$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
$model->agreevalue = 1;
?>
<style>
    .field-pointuploadsummary-acc_division{display: none;}
</style>
<div class="point-upload-summary-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'point-upload-summary-form', 'class' => 'form-horizontal form-label-left']]); ?>
    <div class="box-body">
        <?php
        if($model->isNewRecord){
            echo $form->field($model, 'excel', [
                'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->fileInput();
        }
        ?>
        
        
        <?=
        $form->field($model, 'acc_division', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput();
        ?>
        
        <?=
        $form->field($model, 'effective_month', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
            //'startDate' => $startDate,
    ]])
        ?>

        <?=
        $form->field($model, 'description', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textarea(['rows' => 6]);
        ?>

        <?=
        $form->field($model, 'expiry_date', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
            //'startDate' => $startDate,
    ]])
        ?>

    </div>

    <div class="box-footer">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <?= Html::submitButton($model->isNewRecord ? 'Upload File' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $("input[type='radio']").change(function(){
   
if($(this).val()=="2")
{
    $(".field-pointuploadsummary-acc_division").show();
}
else
{
       $(".field-pointuploadsummary-acc_division").hide(); 
}
    
});
JS;
$this->registerJs($script);
?>