<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummary */

$this->title = 'Update Point Upload Summary';
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'View', 'url' => ['view', 'id' => $model->point_upload_summary_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="point-upload-summary-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
