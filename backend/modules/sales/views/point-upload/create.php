<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummary */

$this->title = 'Point File to upload';
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="point-upload-summary-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'clientID' => $clientID,
        ])
        ?>

    </div>
</div>
