<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\PointUploadSummary */

$this->title = 'View';
$this->params['breadcrumbs'][] = ['label' => 'Point Upload Summary', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    /*table.detail-view th {
        width: 25%;
    }

    table.detail-view td {
        width: 75%;
    }*/
</style>

<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-lg-10 text-left" style="padding-left: 0px;">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-lg-2 text-right hide">
            <?= Html::a('Update', ['update', 'id' => $model->point_upload_summary_id], ['class' => 'btn btn-primary']) ?>
        </div>


    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="point-upload-summary-view">
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <tr>
                        <th>Upload Date</th>
                        <td colspan="3"><?= date('d/m/Y', strtotime($model->created_datetime)) ?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td colspan="3"><?= $model->description ?></td>
                    </tr>
                    <tr>
                        <th>Effective Month</th>
                        <td colspan="3"><?= date('d/m/Y', strtotime($model->effective_month)) ?></td>
                    </tr>
                    <tr>
                        <th>Expiry Date</th>
                        <td colspan="3"><?= date('d/m/Y', strtotime($model->expiry_date)); ?></td>
                    </tr>
                    
                    
                </tbody>
            </table>
            <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Account - 1</h3>
                    </div>
                    <div class="box-body box-profile">
                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <tbody>

                                <tr>
                                    <th>Total Accounts</th>
                                    <td><?= $model->getNoofAccounts(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPts()); ?></td>
                                </tr>

                                <tr>
                                    <th>Valid Accounts</th>
                                    <td><?= $model->getNoofAccountsSuccess(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPtsSuccess()); ?></td>
                                </tr>
                                <tr>
                                    <th>Invalid Accounts</th>
                                    <td><?= $model->getInvalidAccounts(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPtsInvalid()); ?></td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Account - 2</h3>
                    </div>
                    <div class="box-body box-profile">
                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <tbody>

                                <tr>
                                    <th>Total Accounts</th>
                                    <td><?= $model->getNoofAccounts2(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPts2()); ?></td>
                                </tr>

                                <tr>
                                    <th>Valid Accounts</th>
                                    <td><?= $model->getNoofAccountsSuccess2(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPtsSuccess2()); ?></td>
                                </tr>
                                <tr>
                                    <th>Invalid Accounts</th>
                                    <td><?= $model->getInvalidAccounts2(); ?></td>
                                    <th>Total Pts</th>
                                    <td><?= Yii::$app->formatter->asInteger($model->getTotalPtsInvalid2()); ?></td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            </div>
            
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'point_upload_summary_id',
            'dealer_id',
            [
                'attribute' => 'points',
                'label' => 'Account 1',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->points;
                },
            ],
            [
                'attribute' => 'points',
                'label' => 'Account 2',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->points2;
                },
            ],            
            'status_remark',
            //'status',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getStatus();
                },
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

        </div>
    </div>
</div>