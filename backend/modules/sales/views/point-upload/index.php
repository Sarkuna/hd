<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointUploadSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Point Upload Summary';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">
                <?= Html::a('<i class="fa fa-plus"></i>', ['guide'], ['class' => 'btn btn-success', 'title' => 'Guide']) ?>
                
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="point-upload-summary-index table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'point_upload_summary_id',
            //'clientID',
            [
                'attribute' => 'created_datetime',
                'label' => 'Upload Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            'description:ntext',
            [
                'attribute' => 'effective_month',
                'label' => 'Effective Month',
                'format' => 'html',
                'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->effective_month));
                },
            ],
            [
                'attribute' => 'no_of_accounts',
                'label' => 'Total Account',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getNoofAccounts();
                },
            ],
            [
                'attribute' => 'total_pts',
                'label' => 'Total Points',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asInteger($model->getTotalPts());
                },
            ],
            [
                'attribute' => 'Total_succesful_acc_upload',
                'label' => 'Valid Accounts',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getNoofAccountsSuccess();
                },
            ],
            [
                'attribute' => 'invalid_accounts',
                'label' => 'Invalid Accounts',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getInvalidAccounts();
                },
            ],
            /*[
                'attribute' => 'Total_succesful_pts_upload',
                'label' => 'Total succesful pts upload',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asInteger($model->getTotalPtsSuccess());
                },
            ],*/            
            [
                'attribute' => 'expiry_date',
                'label' => 'Expiry Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->expiry_date));
                },
            ],             
            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '50'],
                'template' => '{view} {download}', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                    'download' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, ['title' => Yii::t('app', 'Download'),]));
                    },        
                    'update' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
            </div>
        </div>
    </div>

