<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ImportCustomerExcelSummary */

$this->title = 'Group - '.$model->import_customer_upload_summary_id.date('dmY', strtotime($model->created_datetime));
$this->params['breadcrumbs'][] = ['label' => 'Import Customer Excel Summaries', 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;
?>


<style>
    table.detail-view th {
        width: 25%;
    }

    table.detail-view td {
        width: 75%;
    }
</style>

<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-lg-10 text-left" style="padding-left: 0px;">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-lg-2 text-right hide">
            
        </div>


    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="point-upload-summary-view">

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'point_upload_summary_id',
                    //'clientID',
                    [
                        'attribute' => 'created_datetime',
                        'label' => 'Upload Date',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return date('d/m/Y', strtotime($model->created_datetime));
                        },
                    ],
                    [
                        'attribute' => 'created_datetime',
                        'label' => 'Upload By',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->usercreate->first_name.' '.$model->usercreate->last_name;
                        },
                    ],
                    [
                        'attribute' => 'account_type',
                        'label' => 'Account Type',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->accountType->name;
                        },
                    ],            
                    'description:ntext',            

                [
                    'attribute' => 'no_of_accounts',
                    'label' => 'No of Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->getNoofAccounts();
                    },
                ],

                [
                    'attribute' => 'Total_succesful_acc_upload',
                    'label' => 'Total succesful New Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->getNoofAccountsSuccess();
                    },
                ],
                [
                    'attribute' => 'Total_succesful_pts_upload',
                    'label' => 'Total succesful Update Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        //Yii::$app->formatter->asInteger();
                        return $model->getNoofAccountsUpdate();
                    },
                ],            
                ],
            ])
            ?>
            
            
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'point_upload_summary_id',
            'user_code',
            'company_name',            
            'email',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->getStatus();
                },
            ],
            [
                'attribute' => 'remark',
                'label' => 'Remark',
                'format' => 'html',
                'value' => function ($model) {
                    if(!empty($model->remark)) {
                        $remark = $model->remark;
                    }else{
                        $remark = 'TBA';
                    }
                    return $remark;
                },
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
