<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Import Database With Excel Files';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Excel Summaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$typename = \common\models\TypeName::find()
            ->all();
$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>
<div class="row">
    
    <div class="col-lg-7">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="box-body">
                
                <div class="col-lg-6">
                    <h3 class="text-center">
                        <?= Html::a('<i class="fas fa-file-upload fa-3x"></i>', ['uploadexcel'], ['title' => 'Add New']) ?>
                    </h3>
                    <h4 class="text-center">Import File</h4>
                    <p class="text-center">Please upload a Excel file with a proper format
                        (User code, Company Name and email are mandatory)</p>

                </div>
                <div class="col-lg-6">
                    <h3 class="text-center"><a href="https://drive.google.com/uc?export=download&id=1xkmBukrUvgVg4v_yTt3zqwyXtO25VbHA"><i class="fas fa-file-download fa-3x"></i></a></h3>
                    <h4 class="text-center">Download Sample File</h4>
                    <p class="text-center">Download a proper Excel file for import</p>

                </div>
                <hr>
                <div class="col-lg-12">
                    <h3 class="box-title">Guide</h3>
                    <h4>Step 1 : Download the Excel format from the icon above.</h4>
                    <h4>Step 2 : Insert the database based on the excel file fields.</h4>
                    <iframe src="https://drive.google.com/file/d/10za3bKzU3Q0uzocUXyxbELtwFbSu4sUW/preview" width="550" height="480"></iframe>
                    <p>Parameters in Excel File</p>
                    <table class="table table-striped">
                        <tr>
                            <td>Column A<b class="text-red">*</b></td>
                            <td>Clients Ref No : Your user ID No.</td>
                        </tr>
                        <tr>
                            <td>Column B<b class="text-red">*</b></td>
                            <td>Company Name</td>
                        </tr>
                        <tr>
                            <td>Column C<b class="text-red">*</b></td>
                            <td>Email Address</td>
                        </tr>
                        <tr>
                            <td>Column D</td>
                            <td>Full Name</td>
                        </tr>
                        <tr>
                            <td>Column E</td>
                            <td>Mobile Number e.g +60121234567</td>
                        </tr>
                        <tr>
                            <td>Column F</td>
                            <td>Company Address</td>
                        </tr>
                    </table>

                    <p>1. Column A to C are mandatory.</p>
                    <p>2. Column D to F are optional and can be left empty.</p>

                    <h4>Step 3 : Upload the Excel file</h4>
                </div>
            </div>

        </div>

    </div>
</div>