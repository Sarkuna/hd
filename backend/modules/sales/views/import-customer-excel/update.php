<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ImportCustomerExcelSummary */

$this->title = 'Update Import Customer Excel Summary: ' . $model->import_customer_upload_summary_id;
$this->params['breadcrumbs'][] = ['label' => 'Import Customer Excel Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->import_customer_upload_summary_id, 'url' => ['view', 'id' => $model->import_customer_upload_summary_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="import-customer-excel-summary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
