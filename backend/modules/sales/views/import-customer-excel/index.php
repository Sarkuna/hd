<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ImportCustomerExcelSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Database Upload Summary';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary import-customer-excel-summary-index">
        <div class="box-header with-border">
            <div class="col-lg-10 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="col-lg-2 text-right">
                <?= Html::a('<i class="fa fa-plus"></i>', ['guide'], ['class' => 'btn btn-success', 'title' => 'Guide']) ?>
            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="point-upload-summary-index table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'import_customer_upload_summary_id',
            //'clientID',
            [
                'attribute' => 'created_datetime',
                'label' => 'Upload Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Uploaded By',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->usercreate->first_name.' '.$model->usercreate->last_name;
                },
            ],
            [
                'attribute' => 'account_type',
                'label' => 'Account Type',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->accountType->name;
                },
            ],            
            'description:ntext',
            [
                    'attribute' => 'no_of_accounts',
                    'label' => 'No of Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->getNoofAccounts();
                    },
                ],

                [
                    'attribute' => 'Total_succesful_acc_upload',
                    'label' => 'New Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->getNoofAccountsSuccess();
                    },
                ],
                [
                    'attribute' => 'Total_succesful_pts_upload',
                    'label' => 'Update Accounts',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        //Yii::$app->formatter->asInteger();
                        return $model->getNoofAccountsUpdate();
                    },
                ],            
            //'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
             [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    }
                ],
            ],           
        ],
    ]); ?>
            </div>
        </div>
    </div>

