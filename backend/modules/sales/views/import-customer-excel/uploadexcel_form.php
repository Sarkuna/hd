<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Import Database With Excel Files';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Excel Summaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide'), 'url' => ['guide']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$clientID = 16;
$typename = \common\models\TypeName::find()
            ->where([
                'clientID' => $clientID,
        ])->all();
$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>
<div class="row">
    <div class="col-lg-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="box-body">
                <?= $form->field($model, 'excel')->fileInput() ?>
                <?= $form->field($model, 'account_type')->dropDownList(
                    $typenames, ['prompt' => 'Select', 'id' => 'type']
            ); ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>