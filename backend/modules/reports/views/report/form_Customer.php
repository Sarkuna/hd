<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

$session = Yii::$app->session;
//$this->title = 'Report';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
$typename = \common\models\TypeName::find()->where([
                'clientID' => 16,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');

?>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Report Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'report-form']); ?>
        <div class="box-body">
            
            <?php
            echo $form->field($modelForm, 'type')->widget(Select2::classname(), [
                'data' => $actypslist,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a type ...','multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,                    
                ],
            ]);
            ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Search...', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset', ['customer-account-summary'], ['class'=>'btn btn-info']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>