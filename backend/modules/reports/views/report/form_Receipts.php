<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$session = Yii::$app->session;
//$this->title = 'Report';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;

$distributor = \common\models\CompanyInformation::find()->joinWith(['user'])->where(['client_id' => $session['currentclientID'], 'status' => 'A', 'type' => '1'])->all();
$distributorlist = ArrayHelper::map($distributor, 'user_id', 'company_name');

$indirect = \common\models\VIPReceipt::find()
        //->select(['*'])
        ->joinWith([
            'customer' => function($que) {
                $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
            },
        ])
        ->where(['vip_receipt.clientID' => $session['currentclientID']])
        ->groupBy('vip_receipt.userID')
        ->all();

$indirectlist = ArrayHelper::map($indirect, 'userID', 'customer.full_name');
?>
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Report Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'report-form']); ?>
        <div class="box-body">
            
            <div class="col-md-4">
                
                <?=
                $form->field($modelForm, 'date_range')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy',
                    'autoclose' => true,
                    'minViewMode' => 2,
                    'startDate' => $startyear,
                    'endDate' => date('Y-m-d')
                ]
            ]);
            
            ?>
            </div>
            <div class="col-md-5">
            <?=
            $form->field($modelForm, 'reseller_name')->widget(Select2::classname(), [
                'data' => $distributorlist,
                //'language' => 'de',
                'options' => [
                    'placeholder' => '-- Select --',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]);
            ?>
            </div>
            <div class="col-md-3">
            <?=
            $form->field($modelForm, 'indirect')->widget(Select2::classname(), [
                'data' => $indirectlist,
                //'language' => 'de',
                'options' => [
                    'placeholder' => '-- Select --',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]);
            ?>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Search...', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset', ['receipts-summary'], ['class'=>'btn btn-info']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>