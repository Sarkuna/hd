<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
$typename = \common\models\TypeName::find()->where([
                'clientID' => 16,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>

<div class="row">
    <?=
    $this->render('form_Orders', ['modelForm' => $modelForm]);
    ?>
</div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'clients_ref_no',
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'User Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                return $model->order->company->company_name;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->customer->clients_ref_no;
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'label' => 'Type',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => $typenames,
                        ],
                        [
                            'attribute' => 'order_no',
                            'label' => 'Order No.',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->invoice_prefix;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Order Date',
                            'format' => 'html',
                            'value' => function ($model) {
                                return date("d-m-Y", strtotime($model->order->created_datetime));
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Invoice Number',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->bb_invoice_no;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Product Code',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->model;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Product Description',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->name;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Qty',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->quantity;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Unit/pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->point;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Shipping/pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->shipping_point;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Total Pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                $total_pro = $model->point + $model->shipping_point;
                                $totl_f = $model->quantity * $total_pro;
                                return $totl_f;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Total RM',
                            'format' => 'html',
                            'value' => function ($model) {
                                $total_pro = $model->point + $model->shipping_point;
                                $totl_f = $model->quantity * $total_pro;
                                return $totl_f / 2;
                            },
                        ],            
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->orderStatus->name;
                            },
                        ],            
                        /*[
                            'attribute' => 'company_name1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->companyInfo->company_name;
                            },
                        ],            
                        [
                            'attribute' => 'mobile1',
                            'label' => 'Full Name',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->full_name;
                            },
                        ],
           
                        [
                            'attribute' => 'type1',
                            'label' => 'Type',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->user->typeName->name;
                            },
                        ],
                        [
                            'attribute' => 'awarded_points',
                            'label' => 'Awarded Points',
                            'format' => 'html',
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalAdd();
                            },
                        ],
                                    
                        [
                            'attribute' => 'redeemed_points',
                            'label' => 'Redeemed Points',
                            'format' => 'html',
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalMinus();
                            },
                        ],
                        [
                            'attribute' => 'expired_points',
                            'label' => 'Expired Points',
                            'format' => 'html',
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getExpired();
                            },
                        ],            
                        [
                            'attribute' => 'balance',
                            'label' => 'Balance',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                $tot = $model->getTotalAdd() - $model->getTotalMinus() - $model->getExpired() - $model->getEy3() - $model->getEy4();
                                return $tot;
                            },
                        ],*/            
                                
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>



<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>

