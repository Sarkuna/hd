<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    tr:last-child {font-weight: bold;}
</style>
<div class="row">
    <?=
    $this->render('form_sms', ['modelForm' => $modelForm, 'startyear' => $startyear,]);
    ?>
</div>

<div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-lg-12 text-left" style="padding-left: 0px;">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

            </div>
            
            
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="vipcustomer-index">
                <?php
                if (! empty($dataProvider)) {      
                 $gridColumns = [
                        //['class' => 'kartik\grid\SerialColumn'],
                        'Month',
                        'Dealer & Distributor Pts Upload',
                        'Receipts Approved',
                        'Monthly Notifications',
                        //'Total'          
                    ]           
                
                ?>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'showColumnSelector' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                
                }
                ?>
            </div>
        </div>
    </div>
