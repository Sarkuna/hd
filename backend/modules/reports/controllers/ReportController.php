<?php

namespace app\modules\reports\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use DateTime;


use common\models\VIPCustomer;
use common\models\ReportForm;
use common\models\ReportFormCustomer;
use common\models\ReportOrderForm;
use common\models\ReceiptsForm;
use common\models\VIPReceipt;
use common\models\TypeName;
use common\models\SMSQueue;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;

class ReportController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }
    public function actionIndex()
    {
        
        $model = new \common\models\ReportBasicForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->history($model->type);
        }
        return $this->render('reportform', [
            'model' => $model
        ]);
    }
    
    protected function history($type)  //insert 50000 records using createCommand method
    {
        $session = Yii::$app->session;
        $customers = VIPCustomer::find()->joinWith([
            'user',
            'companyInfo'
        ])->where(['user.type' => $type, 'clientID' => $session['currentclientID']])
          ->orderBy(['user.created_datetime' => SORT_DESC])->each();
        
        $filename = Date('YmdGis').'Summary_Report.xls';
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Pragma: no-cache"); 
            header("Expires: 0");

            echo '<table border="1" width="100%">';
            echo '<thead><tr>
                    <th>Code</th>
                    <th>Company Name</th>
                    <th>Company Address</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Email Verified</th>
                    <th>Mobile Number</th>                    
                    <th>Status</th>
                    <th>Date Added</th>
            </tr></thead>';
            echo '<tbody>';
            $tr = '';
            foreach($customers as $customer) {
                $client_status = $customer['user']['status'];
                if ($client_status == "X") {
                    $returnValue = 'Deleted';
                } else if ($client_status == "P") {
                    $returnValue = 'Pending';
                } else if ($client_status == "C") {
                    $returnValue = 'Cancel';
                } else if ($client_status == "A") {
                    $returnValue = 'Approved';
                } else if ($client_status == "D") {
                    $returnValue = 'Deactive';
                } else if ($client_status == "N") {
                    $returnValue = 'Not Approved';
                }
                
                $email_status = $customer['user']['email_verification'];
                if($email_status == 'Y' || $email_status == 'y') {
                    $verify = 'Yes';
                }else {
                    $verify = 'No';
                }
                
                $address = $customer['companyInfo']['mailing_address'];

                $tr .= '<tr>
                <td>'.$customer['clients_ref_no'].'&nbsp;</td>
                <td>'.$customer['companyInfo']['company_name'].'</td>
                <td>'.$address.'</td>
                <td>'.$customer['full_name'].'</td>
                <td>'.$customer['user']['email'].'</td>
                <td>'.$verify.'</td>    
                <td>'.$customer['mobile_no'].'&nbsp;</td> 
                <td>'.$returnValue.'</td> 
                <td>'.date('d-m-Y', strtotime($customer['user']['created_datetime'])).'</td>
 
                </tr>';
            }
            echo $tr;
            echo '</tbody>';
            echo '</table>';
            die;
    }
    
    public function actionCustomerAccountSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportFormCustomer();
        
        $actyps = TypeName::find()
            ->where([
                'clientID' => 16,              
        ])->all();
        $actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
        
        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = explode('-', $type);
            $modelForm->type = $type;
        }else {
            $type = $actypslist;
        }
        
        if($modelForm->load(Yii::$app->request->post())){
            $type = implode('-', $modelForm->type);
            $filter_string = '/reports/report/customer-account-summary?type='.$type;
            return $this->redirect([$filter_string]);            
        }
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => 16])
                        ->andwhere(['IN', 'user.type', $type])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('customers', [
                'actypslist' => $actypslist,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionPointsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportFormCustomer();
        
        $actyps = TypeName::find()
            ->where([
                'clientID' => 16,              
        ])->all();
        $actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');
        
        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = explode('-', $type);
            $modelForm->type = $type;
        }else {
            $type = $actypslist;
        }
        
        if($modelForm->load(Yii::$app->request->post())){
            $type = implode('-', $modelForm->type);
            $filter_string = '/reports/report/points-summary?type='.$type;
            return $this->redirect([$filter_string]);            
        }
        
        $data = VIPCustomer::find()->select(['vip_customer_id', 'userID', 'clientID', 'clients_ref_no', 'full_name', 'mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id', 'email', 'email_verification', 'status', 'type', 'created_datetime']);
                    },
                            'companyInfo' => function($que) {
                        $que->select(['user_id', 'company_name', 'mailing_address']);
                    },
                        ])
                        ->where(['clientID' => 16])
                        ->andwhere(['IN', 'user.type', $type])
                        ->orderBy(['user.created_datetime' => SORT_DESC]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                        //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);

            return $this->render('points_summary', [
                'actypslist' => $actypslist,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionOrdersSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/orders-summary?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        
        $data = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'order.customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'order.company' => function($que) {
                    $que->select(['user_id','company_name']);
                },
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['between', 'vip_order.created_datetime', $date_from, $date_to ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('orders_summary', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReceipts()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportOrderForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }
        
        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }

            $filter_string = '/reports/report/receipts?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d');
            return $this->redirect([$filter_string]);          
        }
        /*$query->joinWith(['customer', 'reseller',
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }
            ]);*/

        $data = \common\models\VIPReceipt::find()->joinWith([
                //'order',
                'customer' => function($que) {
                    $que->select(['userID','clients_ref_no','dealer_ref_no', 'full_name']);
                },
                'reseller' => function($que) {
                    $que->select(['user_id','company_name']);
                },
                'indirectCompany' => function ($q) {
                        $q->from('company_information uc');
                }        
            ])->where(['vip_receipt.clientID' => $session['currentclientID']])
            ->andwhere(['between', 'vip_receipt.created_datetime', $date_from.' 00:00:00', $date_to.' 23:59:59' ])            
            //->andwhere(['IN', 'user.type', $type])
            ->orderBy(['vip_receipt.created_datetime' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('receipts_list', [
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReceiptsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReceiptsForm();
        
        $receiptstartdate = VIPReceipt::find()->where(['clientID' => $session['currentclientID']])->count();
        if($receiptstartdate > 0){
            $receiptstartdate = VIPReceipt::find()->where(['clientID' => $session['currentclientID']])
            ->orderBy(['created_datetime' => SORT_ASC])        
            ->one();
            $startyear = date('Y', strtotime($receiptstartdate->created_datetime));
        }else {
            $startyear = '2019';
        }
        

        $year = Yii::$app->getRequest()->getQueryParam('year');
        if(empty($date_to)){
            $year = date('Y');
        }
        
        $reseller_name = Yii::$app->getRequest()->getQueryParam('name');
        $indirect = Yii::$app->getRequest()->getQueryParam('indirect');
        
        if(!empty($reseller_name) && !empty($indirect)){
           $rname = '&name='.$reseller_name;
           $gindirect = '&indirect='.$indirect;
           $datawhere = ['reseller_name' => $reseller_name, 'userID' => $indirect];
           $datawhere_pts = ['vip_receipt.reseller_name' => $reseller_name, 'vip_receipt.userID' => $indirect];
        }elseif(!empty($reseller_name)){
           $rname = '&name='.$reseller_name;
           $datawhere = ['reseller_name' => $reseller_name];
           $datawhere_pts = ['vip_receipt.reseller_name' => $reseller_name];
        }elseif(!empty($indirect)){
           $gindirect = '&indirect='.$indirect;
           $datawhere = ['userID' => $indirect];
           $datawhere_pts = ['vip_receipt.userID' => $indirect];
        }else {
            $rname = '';
            $datawhere = '';
            $datawhere_pts = '';
        }

        $modelForm->date_range = date('Y');
        $modelForm->reseller_name = $reseller_name;
        $modelForm->indirect = $indirect;
        //$modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $year = date('Y', strtotime($modelForm->date_range));
            if(!empty($modelForm->reseller_name)) {
                $rname = '&name='.$modelForm->reseller_name;
            }else {
                $rname = '';
            }
            
            if(!empty($modelForm->indirect)) {
                $gindirect = '&indirect='.$modelForm->indirect;
            }else {
                $gindirect = '';
            }
            
            $filter_string = '/reports/report/receipts-summary?year='.$year.$rname.$gindirect;
            return $this->redirect([$filter_string]);          
        }

            $i = 1;
            $data = array();
            
            
            $mydaate = $year.'-01-01';
            $month = strtotime($mydaate);
            $pendingtotal = 0; $draftstotal = 0; $processingtotal = 0; $approvedtotal = 0; $approvedtotalsum = 0; $declinetotal = 0; $totalsum = 0;
            while($i <= 12)
            {
                
                $month_name_no = date('m', $month);
                $start_month = $year.'-'.$month_name_no.'-01 00:00:00';
                $a_date = $year.'-'.$month_name_no.'-01';                
                $a_date = date("Y-m-t", strtotime($a_date));
                $end_month = $a_date.' 23:59:59';

                $month_name = date('F', $month);                
                $pending = VIPReceipt::find()
                        //->where($datawhere)
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'P'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $drafts = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'R'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $processing = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'N'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                $approved = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'A'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                
                $approvedpts = VIPReceipt::find()
                        ->joinWith('receiptList')
                        ->where(['vip_receipt.clientID' => $session['currentclientID'], 'vip_receipt.status' => 'A'])
                        ->andwhere($datawhere_pts)
                        ->andwhere(['between', 'vip_receipt.created_datetime', $start_month, $end_month ])
                        ->sum('vip_receipt_list.qty_points_awarded');
                
                if($approvedpts > 0) {
                    $approvedpts = $approvedpts;
                }else {
                    $approvedpts = 0;
                }
                
                $decline = VIPReceipt::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'D'])
                        ->andwhere($datawhere)
                        ->andwhere(['between', 'created_datetime', $start_month, $end_month ])
                        ->count();
                
                $total = $pending + $drafts + $processing + $approved + $decline;
                $pendingtotal += $pending;
                $draftstotal += $drafts;
                $processingtotal += $processing;
                $approvedtotal += $approved;
                $declinetotal += $decline;
                $totalsum += $total;
                $approvedtotalsum += $approvedpts;
                
                $data[] = array(
                    'Month' => $month_name, 
                    'Pending' => $pending, 
                    'Drafts' => $drafts, 
                    'Processing' => $processing, 
                    'Approved' => $approved,
                    'Approved Pts' => $approvedpts,
                    'Decline' => $decline, 
                    'Total' => $total
                );
                $month = strtotime('+1 month', $month);
                $i++;
            }
            $data2 = [
                ['Month' => 'Total', 'Pending' => $pendingtotal, 'Drafts' => $draftstotal, 'Processing' => $processingtotal, 'Approved' => $approvedtotal, 'Approved Pts' => $approvedtotalsum, 'Decline' => $declinetotal, 'Total' => $totalsum],
            ];
            
            $data3 = array_merge($data, $data2);
            /*$data = [
                ['id' => 1, 'name' => 'name 1'],
                ['id' => 2, 'name' => 'name 2'],
                ['id' => 100, 'name' => 'name 100'],
            ];*/

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data3,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('receipts_summary', [
                'startyear' => $startyear,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionSmsSummary()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReceiptsForm();
        
        $startyear = '2019';


        $year = Yii::$app->getRequest()->getQueryParam('year');
        if(empty($date_to)){
            $year = date('Y');
        }

        $modelForm->date_range = date('Y');
        //$modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));

        if($modelForm->load(Yii::$app->request->post())){
            $year = date('Y', strtotime($modelForm->date_range));

            $filter_string = '/reports/report/sms-summary?year='.$year;
            return $this->redirect([$filter_string]);          
        }

            $i = 1;
            $data = array();
            
            
            $mydaate = $year.'-01-01';
            $month = strtotime($mydaate);
            $pts_upload_total = 0; $receipts_total = 0; $pts_monthly_expiry_total = 0; $totalsum = 0;
            while($i <= 12)
            {
                
                $month_name_no = date('m', $month);
                $start_month = $year.'-'.$month_name_no.'-01';
                $a_date = $year.'-'.$month_name_no.'-01';                
                $a_date = date("Y-m-t", strtotime($a_date));
                $end_month = $a_date;

                $month_name = date('F', $month);                
                $pts_upload = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'pts_upload'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                $receipts = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'receipts'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                $pts_monthly_expiry = SMSQueue::find()
                        ->where(['clientID' => $session['currentclientID'], 'status' => 'S', 'sms_type' => 'pts_monthly_expiry'])
                        ->andwhere(['between', 'date_to_send', $start_month, $end_month ])
                        ->count();
                
                
                $total = $pts_upload + $receipts + $pts_monthly_expiry;
                $pts_upload_total += $pts_upload;
                $receipts_total += $receipts;
                $pts_monthly_expiry_total += $pts_monthly_expiry;
                $totalsum += $total;
                
                $data[] = array(
                    'Month' => $month_name, 
                    'Dealer & Distributor Pts Upload' => $pts_upload, 
                    'Receipts Approved' => $receipts, 
                    'Monthly Notifications' => $pts_monthly_expiry,
                    'Total' => $total
                );
                $month = strtotime('+1 month', $month);
                $i++;
            }
            $data2 = [
                ['Month' => 'Total', 'Dealer & Distributor Pts Upload' => $pts_upload_total, 'Receipts Approved' => $receipts_total, 'Monthly Notifications' => $pts_monthly_expiry_total],
            ];
            
            $data3 = array_merge($data, $data2);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data3,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
            ]);

            return $this->render('sms_summary', [
                'startyear' => $startyear,
                'modelForm' => $modelForm,
                'dataProvider' => $dataProvider,
            ]);
    }
    public function actionDetailedPointSummary()
    {
        $this->layout = '/main_krajee';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new \common\models\VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        //$searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;


        return $this->render('detailed_point_summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,            
        ]);
    }
    
    //Not use
    public function actionCustomerAccount()
    {
        $session = Yii::$app->session;
        $this->layout = '/main_krajee';
        $modelForm = new ReportForm();
        
        $date_from = Yii::$app->getRequest()->getQueryParam('date_from');
        if(empty($date_from)){
            $date_from = date('Y-m-d');
        }
        $date_to = Yii::$app->getRequest()->getQueryParam('date_to');
        if(empty($date_to)){
            $date_to = date('Y-m-d');
        }

        $type = Yii::$app->getRequest()->getQueryParam('type');
        if(!empty($type)){
            $type = json_decode($type);
        }

        $modelForm->date_range = date('d M, Y', strtotime($date_from)) . ' to ' . date('d M, Y', strtotime($date_to));
        
        if($modelForm->load(Yii::$app->request->post())){
            //$type = implode(',', $modelForm->type);
            $type=json_encode($modelForm->type);
            $date_range = str_replace(',', '', $modelForm->date_range);
            $daterange = explode( 'to', $date_range);
            $date_from_arr = explode(' ', ltrim($daterange[0]));
            $date_to_arr = explode(' ', ltrim($daterange[1]));
            
            if(!empty($date_from_arr)){
                $dateParse = date_parse($date_from_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_from = new DateTime();
                $date_from->setDate((int)$date_from_arr[2], $monthNum, (int)$date_from_arr[0]);
            }

            if(!empty($date_to_arr)){
                $dateParse = date_parse($date_to_arr[1]);
                $monthNum = isset($dateParse['month']) ? $dateParse['month'] : 1;
                $date_to = new DateTime();
                $date_to->setDate((int)$date_to_arr[2], $monthNum, (int)$date_to_arr[0]);
            }
            
            $filter_string = '/reports/report/customer-account?date_from='.$date_from->format('Y-m-d').'&date_to='.$date_to->format('Y-m-d').'&type='.$type;
            return $this->redirect([$filter_string]);
        }


            
            $data = VIPCustomer::find()->select(['vip_customer_id','userID','clientID','clients_ref_no','full_name','mobile_no'])->joinWith([
                    'user' => function($que) {
                        $que->select(['id','email','email_verification','status','type','created_datetime']);
                    },
                    'companyInfo' => function($que) {
                        $que->select(['user_id','company_name','mailing_address']);
                    },
                ])
                ->where(['clientID' => $session['currentclientID']])
                ->andwhere(['IN', 'user.type', $type])
                ->orderBy(['user.created_datetime' => SORT_DESC]);
                    
                 $dataProvider = new ActiveDataProvider([
                    'query' => $data,
                    //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
                ]);
                 
            return $this->render('customers', [
                //'model' => $model,
                'dataProvider' => $dataProvider,
            ]);     


        
        
    }
    

}
