<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAccount2 */

$this->title = 'Create Vipcustomer Account2';
$this->params['breadcrumbs'][] = ['label' => 'Vipcustomer Account2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-account2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
