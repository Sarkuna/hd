<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerAccount2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vipcustomer Account2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcustomer-account2-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vipcustomer Account2', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'account2_id',
            'clientID',
            'customer_id',
            'indirect_id',
            'description:ntext',
            // 'points_in',
            // 'points_out',
            // 'date_added',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
