<?php

//namespace app\modules\support\controllers;
namespace app\modules\clients\controllers;

use Yii;
use yii\web\Controller;
use backend\models\SignupForm;
use common\models\UserSearch;
use common\models\User;
use common\models\UserProfile;
use common\models\AuthAssignment;

use yii\web\NotFoundHttpException;
use YoHang88\LetterAvatar\LetterAvatar;

/**
 * Default controller for the `support` module
 */
class UsersController extends Controller
{
    const USER_TYPE = 'C';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $searchModel = new UserSearch();
        $searchModel->client_id = $clientID;
        $searchModel->user_type = 'C';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        //return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new SignupForm();
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //echo 'test'.die;
                $user = new User();
                $uname = $model->fname.' '.$model->lname;
                $user->username = $this->generateUniqueusername($uname, 10);
                $user->email = $model->email;
                $user->setPassword($model->password);
                $user->generateAuthKey();
                $user->status = $model->status;
                $user->user_type = 'C';
                $user->client_id = $clientID;
                $user->created_at = '0';
                $user->updated_at = '0';
                
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $user->save(false)) {
                        if ($flag) {
                            $userprofile = new UserProfile();
                            $userprofile->userID = $user->id;
                            $userprofile->first_name = $model->fname;
                            $userprofile->last_name = $model->lname;
                            $userprofile->save();

                            if (($flag = $userprofile->save(false)) === false) {      
                                $transaction->rollBack();
                                        //break;
                            }
                            /*
                            $authassignment = new AuthAssignment();
                            $authassignment->item_name = 'Client';
                            $authassignment->user_id = $user->id;;
                            $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                            $authassignment->save();
                            
                            if (($flag = $authassignment->save(false)) === false) {      
                                $transaction->rollBack();
                                        //break;
                            }*/
                        }
                    }

                if ($flag) {
                    $transaction->commit();
                    //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified clients!']);
                    $data = \yii\helpers\Json::encode(array(
                        'password' => $model->password,
                    ));
                    $returnedValue = Yii::$app->vip->sendEmailAdmin($user->id, Yii::$app->params['admin.register'], $data);
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
            }else{
                return $this->render('create', [
                'model' => $model,
            ]);
            }
            
            //return $this->redirect(['view', 'id' => $model->manufacturer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = $this->findModel($id);
        $countuser = User::find()
                ->where(['id' => $model->userID, 'client_id' => $clientID])
                ->count();
        if($countuser){
            //$model = new SignupForm();
            $countuser = User::find()
                ->where(['id' => $model->userID, 'client_id' => $clientID])
                ->one();
            $profile = UserProfile::find()
                ->where(['userID' => $model->userID])
                ->one();
            $model->email = $countuser->email;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                
                $avatar = new LetterAvatar($profile->first_name.$profile->last_name);
                $path = Yii::getAlias('@webroot').'/upload/profiles/';            
                $filename = Yii::$app->security->generateRandomString().".png";
                $avatar->saveAs($path . $filename);

                $photo = new \common\models\ProfilePic();
                $photo->userID = $model->userID;
                $photo->file_name = $filename;
                $photo->save(false);
                            
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified sub categories!']);
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->vip_sub_categories_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function generateUniqueusername($string_name = "Mike Tyson", $rand_no = 200) {
        while (true) {
            $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
            $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

            $part1 = (!empty($username_parts[0])) ? substr($username_parts[0], 0, 8) : ""; //cut first name to 8 letters
            $part2 = (!empty($username_parts[1])) ? substr($username_parts[1], 0, 5) : ""; //cut second name to 5 letters
            $part3 = ($rand_no) ? rand(0, $rand_no) : "";

            $username = $part1 . str_shuffle($part2) . $part3; //str_shuffle to randomly shuffle all characters 

            $username_exist_in_db = $this->usernameExist($username); //check username in database
            if (!$username_exist_in_db) {
                return $username;
            }
        }
    }

    protected function usernameExist($username) {
        $countuser = User::find()
                ->where(['username' => $username])
                ->count();

        return $countuser;
    }

}
