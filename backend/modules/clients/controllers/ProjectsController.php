<?php

namespace app\modules\clients\controllers;

use Yii;
use common\models\Projects;
use common\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $session = Yii::$app->session;
        
        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('view'))) {
                if (!empty($_GET['id'])) {
                    $numRows = Projects::find()
                            ->where([
                                'projects_id' => $_GET['id'],
                                'dealerID' => Yii::$app->user->id
                                
                            ])
                            ->count();
                    if ($numRows == 0)
                        throw new ForbiddenHttpException('You are trying to view or modify restricted record.', 403);
                }
            }
            return true;
        } else {
            return false;
        }

    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'P';
        $searchModel->dealerID = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApprove()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'A';
        $searchModel->dealerID = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDecline()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $searchModel = new ProjectsSearch();
        $searchModel->admin_status = 'D';
        $searchModel->dealerID = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('decline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionTypename($id) {
        $dealers = \common\models\VIPCustomer::find()
               ->joinWith(['user'])
               ->select('userID, full_name')
               ->where(['assign_dealer_id' => Yii::$app->user->id])
               ->andWhere(['user.type' => $id])
               ->asArray()->all();
        $dealerslist = ArrayHelper::map($dealers, 'userID', 'full_name');
        
     return $dealerslist;
    
    }
    public function getSubCatList($id){
        $dealers = \common\models\VIPCustomer::find()
               ->joinWith(['user'])
               ->select('userID, full_name')
               ->where(['assign_dealer_id' => Yii::$app->user->id])
               ->andWhere(['user.type' => $id])
               ->asArray()->all();
        $dealerslist = ArrayHelper::map($dealers, 'userID', 'full_name');
        
     return $dealerslist;
    }
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $model = new Projects();

        if ($model->load(Yii::$app->request->post())) {
            $model->clientID = $clientID;
            $model->dealerID = Yii::$app->user->id;
            $model->admin_status = 'P';
            $model->projects_date = date('Y-m-d');
            $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
            
            if($model->save()){
               \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Project have been created']); 
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Sorry project have not created']); 
            }
            return $this->redirect(['view', 'id' => $model->projects_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->projects_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
