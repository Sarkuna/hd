<?php

//namespace app\modules\support\controllers;
namespace app\modules\clients\controllers;

use Yii;
use yii\web\Controller;
use backend\models\SignupFormDealers;
use common\models\UserSearch;
use common\models\User;
use common\models\UserProfile;
use common\models\AuthAssignment;

use yii\web\NotFoundHttpException;
use YoHang88\LetterAvatar\LetterAvatar;

/**
 * Default controller for the `support` module
 */
class DealersController extends Controller
{
    const USER_TYPE = 'B';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $searchModel = new UserSearch();
        $searchModel->client_id = $clientID;
        $searchModel->user_type = 'B';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        //return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new SignupFormDealers();
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //echo 'test'.die;
                $user = new User();
                $uname = $model->first_name.' '.$model->last_name;
                $user->username = $this->generateUniqueusername($uname, 10);
                $user->email = $model->email;
                
                $random_str = md5(uniqid(rand()));
                $generatedPassword = substr($random_str, 0, 6);
                $user->setPassword($generatedPassword);

                $user->generateAuthKey();
                $user->status = 'A';
                $user->user_type = 'B';
                $user->client_id = $clientID;
                //$user->ip = Yii::$app->getRequest()->getUserIP();
                $user->ip = $_SERVER['REMOTE_ADDR'];
                $userAgent = \xj\ua\UserAgent::model();
                $platform = $userAgent->platform;
                $browser = $userAgent->browser;
                $version = $userAgent->version;
                $user->user_agent = $platform.'-'.$browser.'-'.$version;
                //$user->created_at = '0';
                //$user->updated_at = '0';
                
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $user->save(false)) {
                        if ($flag) {
                            $userprofile = new UserProfile();
                            $userprofile->userID = $user->id;
                            $userprofile->dealer_code = $model->dealer_code;                            
                            $userprofile->first_name = $model->first_name;
                            $userprofile->last_name = $model->last_name;
                            $userprofile->company = $model->company;
                            $userprofile->address1 = $model->address1;
                            $userprofile->address2 = $model->address2;
                            $userprofile->city = $model->city;
                            $userprofile->postcode = $model->postcode;
                            $userprofile->tel = $model->tel;
                            $userprofile->region = $model->region;
                            $userprofile->states = $model->states;
                            $userprofile->country = $model->country;
                            $userprofile->save();

                            if (($flag = $userprofile->save(false)) === false) {      
                                $transaction->rollBack();
                                        //break;
                            }
                            /*
                            $authassignment = new AuthAssignment();
                            $authassignment->item_name = 'Dealers';
                            $authassignment->user_id = $user->id;;
                            $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                            $authassignment->save();
                            
                            if (($flag = $authassignment->save(false)) === false) {      
                                $transaction->rollBack();
                                        //break;
                            }*/
                        }
                    }

                if ($flag) {
                    $transaction->commit();
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'New dealer account has been created']);
                    $data = \yii\helpers\Json::encode(array(
                        'password' => $generatedPassword,
                    ));
                    $returnedValue = Yii::$app->VIPglobal->sendEmailAdmin($user->id, Yii::$app->params['dealer.register'], $data);
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
            }else{
                return $this->render('create', [
                'model' => $model,
            ]);
            }
            
            //return $this->redirect(['view', 'id' => $model->manufacturer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = $this->findModel($id);
        $modelupdate = new \backend\models\UpdateFormDealers();
        
        
        if ($model->load(Yii::$app->request->post()) && $modelupdate->load(Yii::$app->request->post())) {
                $model->dealer_code = $modelupdate->dealer_code;

                if($model->save(false)) {
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified profile!']);
                }else {
                    print_r($model->getErrors());
                    print_r($modelupdate->getErrors());
                    die;
                }
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->vip_sub_categories_id]);
            
        } else {
            $modelupdate->email = $model->user->email;
            $modelupdate->dealer_code = $model->dealer_code;
            return $this->render('update', [
                'model' => $model,
                'modelupdate' => $modelupdate
            ]);
        }
    }
    
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }
    
    public function actionDealercode() {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        $data = Yii::$app->request->post();
        $dcode =  $data['dcode'];
        $code = \common\models\UserProfile::find()->where(['dealer_code'=>$dcode])->count();
        echo $code;
        //return \yii\helpers\Json::encode($msg);
    }
    
    public function actionCheckemail() {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        $data = Yii::$app->request->post();
        $email =  $data['email'];
        $user = User::find()->where(['email'=>$email, 'client_id' => $client_id, 'user_type' => 'B'])->count();
        /*$mstates = Guarantor::find()
                    ->where(['intakeID' => $currentIntakeID, 'NEW_IC_NO' => $mySaveId, ])
                    ->count();*/
        echo $user;
        //return \yii\helpers\Json::encode($msg);
    }
    
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function generateUniqueusername($string_name = "Mike Tyson", $rand_no = 200) {
        while (true) {
            $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
            $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

            $part1 = (!empty($username_parts[0])) ? substr($username_parts[0], 0, 8) : ""; //cut first name to 8 letters
            $part2 = (!empty($username_parts[1])) ? substr($username_parts[1], 0, 5) : ""; //cut second name to 5 letters
            $part3 = ($rand_no) ? rand(0, $rand_no) : "";

            $username = $part1 . str_shuffle($part2) . $part3; //str_shuffle to randomly shuffle all characters 

            $username_exist_in_db = $this->usernameExist($username); //check username in database
            if (!$username_exist_in_db) {
                return $username;
            }
        }
    }

    protected function usernameExist($username) {
        $countuser = User::find()
                ->where(['username' => $username])
                ->count();

        return $countuser;
    }

}
