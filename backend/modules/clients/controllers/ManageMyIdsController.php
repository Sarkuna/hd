<?php

//namespace app\modules\support\controllers;
namespace app\modules\clients\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;
use common\models\VIPCustomerAddress;
use common\models\VIPCustomerAddressSearch;
use common\models\VIPCustomerBank;
use common\models\VIPCustomerBankSearch;
use common\models\User;
use common\models\VIPZone;
use common\models\VIPCustomerReward;
use common\models\VIPCustomerRewardSearch;
use common\models\EmailQueue;
use common\models\AuthAssignment;
use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\CompanyInformation;
use common\models\UserProfile;

/**
 * Default controller for the `support` module
 */
class ManageMyIdsController extends Controller
{
    const USER_TYPE = 'B';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $searchModel = new VIPCustomerSearch();
        $searchModel->clientID = $clientID;
        $searchModel->assign_dealer_id = Yii::$app->user->id;
        //$searchModel->status = 'P';
        //$searchModel->email_verification = 'Y';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        //return $this->render('index');
    }
    
    public function actionView($id)
    {
        $model = $this->findModelcustomer($id);
        $companyid = CompanyInformation::find()->where(['user_id' => $model->userID])->one();
        if(count($companyid) > 0){
            $companyInformation = CompanyInformation::findOne($companyid->id);
        }else {
            $companyInformation = new CompanyInformation();
        }
        
        return $this->render('view', [
            'model' => $model,
            /*'address' => $address,
            'bankslisit' => $bankslisit,
            'searchrewardModel' => $searchrewardModel,
            'datarewardProvider' => $datarewardProvider,
            'searchOrder' => $searchOrder,
            'dataProviderOrder' => $dataProviderOrder,*/
            'companyInformation' => $companyInformation,
        ]);
    }

   
    
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelcustomer($id)
    {
        if (($model = VIPCustomer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
