<?php

//namespace app\modules\support\controllers;
namespace app\modules\clients\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use common\models\VIPAssignCategories;
use common\models\VIPAssignSubCategories;
use common\models\AssignCategoriesForm;
use common\models\AssignSubCategoriesForm;
use common\models\VIPAssignCategoriesSearch;
use common\models\VIPProduct;
use common\models\VIPProductSearch;
use common\models\VIPAssignProducts;
use common\models\VIPAssignProductsSearch;
use common\models\Uploads;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;
/**
 * Default controller for the `support` module
 */
class  AssignController extends Controller
{
    const USER_TYPE = 'C';
    public function behaviors()
    {
        return [
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $session['currentusertype'] = 'A';
        

        if($session['currentusertype'] == 'A') {
            $page = 'assign_products_admin';
        }else {
            $page = 'assign_products';
        }
        
        $searchModel = new VIPAssignProductsSearch();
        $searchModel->clientID = $clientID;
        //$searchModel->painter_login_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        
        $formfilter = new \common\models\ProductFillterForm();
        return $this->render($page, [
                'formfilter' => $formfilter,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionCategories()
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $model = new AssignCategoriesForm();
        
        $mylist = "";
        $clist = VIPAssignCategories::find()->where(['clientID' => $clientID])->count();
        if($clist > 0){
            $list2 = VIPAssignCategories::find()->where(['clientID' => $clientID])->all();
            foreach ($list2 as $list){
                $mylist[] = $list->categories_id;
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $selection = $model->categories_id;
            if(!empty($selection)){
                if($clist > 0){
                    $del = VIPAssignCategories::deleteAll(['clientID' => $clientID]);
                }
                foreach ($selection as $id) {
                    $assignCategories = new VIPAssignCategories();
                    $assignCategories->categories_id = $id;
                    $assignCategories->clientID = $clientID;
                    $assignCategories->save();
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified categories!']);
                return $this->redirect(['categories']);
            } 

            //$model->save();
            //return $this->redirect(['view', 'id' => $model->vip_assign_categories_id]);
        } else {
            $model->categories_id = $mylist;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSubCategories($catid=null)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $model = new AssignSubCategoriesForm();

        $mylist = "";
        $clist = VIPAssignSubCategories::find()->where(['clientID' => $clientID, 'categories_id' => $catid])->count();
        if($clist > 0){
            $list2 = VIPAssignSubCategories::find()->where(['clientID' => $clientID, 'categories_id' => $catid])->all();
            foreach ($list2 as $list){
                $mylist[] = $list->sub_categories_id;
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $selection = $model->sub_categories_id;
            if(!empty($selection)){
                if($clist > 0){
                    $del = VIPAssignSubCategories::deleteAll(['clientID' => $clientID, 'categories_id' => $catid]);
                }
                foreach ($selection as $id) {
                    $assignCategories = new VIPAssignSubCategories();
                    $assignCategories->clientID = $clientID;
                    $assignCategories->categories_id = $catid;                    
                    $assignCategories->sub_categories_id = $id;
                    $assignCategories->save();
                }   
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified categories!']);
                return $this->redirect(['sub-categories']);
            } 

            //$model->save();
            //return $this->redirect(['view', 'id' => $model->vip_assign_categories_id]);
        } else {
            $model->sub_categories_id = $mylist;
            return $this->render('sub-categorie', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModelAssign($id);
        if ($model->load(Yii::$app->request->post())) {
            
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Update', 'text' => 'Action successful!']);
            }else {
                \Yii::$app->getSession()->setFlash('warning',['title' => 'Update', 'text' => 'Action Fail!']);
            }
            return $this->redirect(['products']);
        } else {
            return $this->render('_form_value', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionProductfilter()
    {
        //$post = Yii::$app->request->post();
        $postData = Yii::$app->request->post();
        $categories = $postData['ProductFillterForm']['categories'];
        $subcategories = $postData['ProductFillterForm']['subcategories'];
        $manufacturer = $postData['ProductFillterForm']['manufacturer'];
        $price= $postData['ProductFillterForm']['price'];
        $quantity = $postData['ProductFillterForm']['quantity'];
        
        $searchModel = new VIPProductSearch();
        //vip_product.product_id
        //$searchModel->product_id = '177';        
        $searchModel->category_id = $categories;
        if($subcategories > 0) {
            $searchModel->sub_category_id = $subcategories;
        }
        //$searchModel->painter_login_id = $id;
        $dataProvider = $searchModel->searchassign(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        //$modelprofile = $this->findModel($id);
        //$modelprofile = \common\models\PainterProfile::find()->where(['user_id' => $id])->one();
        
        return $this->render('bulk_assign_products', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'modelprofile' => $modelprofile,
        ]);
    }
    
    public function actionUploadExcel() {
        
        $session = Yii::$app->session;
        $clientID = 16;
        
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "product name", "product code", "msrp", "partner price", "peninsular", "sabah", "sarawak", "singapore");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                
                if($checkmatch == 8){
                    $data = array();
                    $peninsular = 0;
                    $sabah = 0;
                    $sarawak = 0;
                    $singapore = 0;
                    $deliverydata = '';
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        
                        $peninsular = $rowData[0]['peninsular'];
                        $sabah = $rowData[0]['sabah'];
                        $sarawak = $rowData[0]['sarawak'];
                        $singapore = $rowData[0]['singapore'];
                    
                        $deliverydata = \yii\helpers\Json::encode(array(
                                    '1' => $peninsular,
                                    '2' => $sabah,
                                    '3' => $sarawak,
                                    '4' => $singapore,
                        ));
                        
                        $data[] = [$rowData[0]['product code'],$clientID,$rowData[0]['msrp'],$rowData[0]['partner price'],$deliverydata,'N','P'];

                    }
                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('assign_product_excel_upload', ['product_code','clientID','msrp','partner_price','delivery','featured_status','status'],$data)
                    ->execute();

                    $returnedValue = Yii::$app->VIPglobal->importAssignProduct($clientID);

                    \common\models\AssignProductExcelUpload::deleteAll('clientID = :clientID', [':clientID' => $clientID]);
                    unlink($inputFile);
                }else {
                    $teml = 'product name, product code, featured, msrp, partner price, peninsular, sabah, sarawak, singapore';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }

                return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }
    
    public function actionDownloadExcel(){
        $products = VIPProduct::find()->select(['product_name','product_code'])->where(['status' => 'E'])->all();
        $dataProducts = array();
        if(!empty($products)){
            foreach($products as $key=>$product){
                $product_temp = array(
                    'Product Name' => $product['product_name'],
                    'Product Code' => $product['product_code'],
                    'MSRP' => '',
                    'Partner Price' => '',
                    'Peninsular' => '',
                    'Sabah' => '',
                    'Sarawak' => '',
                    'Singapore' => '',
                );
                $dataProducts[] = $product_temp;
            }
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataProducts); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Products',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                //'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                //'headerColumnCssClass' => array(
                    //'id' => ExportExcelUtil::getCssClass('blue'),
                    //'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                //), //define each column's cssClass for header line only.  You can set as blank.
                //'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                //'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                //'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            )
            //sheet 2,....            
        );
        $excel_file = "VIP Products";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionBulkassign(){
        $session = Yii::$app->session;
        $clientID = 16;
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');//typecasting

        if(count($selection) > 0){
            $msg = 1;
            foreach ($selection as $valueid) {
                $exists = VIPAssignProducts::find()->where([ 'clientID' => $clientID, 'productID' => $valueid])->exists();
                if (!$exists) {
                    //it exists'clientID', 'productID', 'date_assign'
                    $VIPAsspro = new VIPAssignProducts();
                    $VIPAsspro->clientID = $clientID;
                    $VIPAsspro->productID = $valueid;
                    $VIPAsspro->date_assign = date('Y-m-d');
                    $VIPAsspro->save(false);
                }
                $msg++;
            }
            if($msg > 1){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Assign Product List', 'text' => 'Action successful!']);
                return $this->redirect(['products']);
            }
        }else{
            \Yii::$app->getSession()->setFlash('warning',['title' => 'Assign Product List', 'text' => 'No items selected!']);
            return $this->redirect(['products']);
        }
    }
    
    public function actionProductView($id)
    {
        $session = Yii::$app->session;
        $clientID = 16;
        $productcheck = VIPAssignProducts::find()->where(['vip_assign_products_id' => $id, 'clientID' => $clientID])->one();

        if(count($productcheck) == 1){
            //$post = Yii::$app->request->post();
            $model = $this->findModel($productcheck->productID);
            list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($model->ref);

            return $this->render('product_view', [
                'model' => $model,
                'initialPreview'=>$initialPreview,
                'initialPreviewConfig'=>$initialPreviewConfig,
                //'modelprofile' => $modelprofile,
            ]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionProductLisitView($id)
    {
        $model = $this->findModel($id);
        return $this->render('product_view', [
            'model' => $model,
        ]);
    }
    
    public function actionRemove($id)
    {
        $this->findModelAssign($id)->delete();        
        \Yii::$app->getSession()->setFlash('success',['title' => 'Unassign', 'text' => 'Product successful unassign!']);
        return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = VIPProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelAssign($id)
    {
        if (($model = VIPAssignProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function getInitialPreview($ref) {
            $datas = Uploads::find()->where(['ref'=>$ref])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/catalog/products/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    
    private function getTemplatePreview(Uploads $model){     
            $filePath = VIPProduct::getUploadUrl().$model->ref.'/thumbnail/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);
            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    
    public function actionDownload($file_name, $file_type = 'excel', $deleteAfterDownload = false) {
        if (empty($file_name)) {
            return 0;
        }
        $baseRoot = Yii::getAlias('@webroot') . "/upload/";
        $file_name = $baseRoot . $file_name;
        if (!file_exists($file_name)) {
            return 0;
        }
        $fp = fopen($file_name, "r");
        $file_size = filesize($file_name);

        if ($file_type == 'excel') {
            header('Pragma: public');
            header('Expires: 0');
            header('Content-Encoding: none');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: public');
            //header('Content-Type: application/vnd.ms-excel');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Description: File Transfer');
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            header('Content-Transfer-Encoding: binary');
            Header("Content-Length:" . $file_size);
            
            /*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');*/
        } else if ($file_type == 'picture') { //pictures
            Header("Content-Type:image/jpeg");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        } else { //other files
            die;
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        }
 
        $buffer = 1024;
        $file_count = 0;

        while (!feof($fp) && $file_count < $file_size) {
            $file_con = fread($fp, $buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        //echo fread($fp, $file_size);
        fclose($fp);
        if ($deleteAfterDownload) {
            unlink($file_name);
        }
        return 1;
    }

    

}
