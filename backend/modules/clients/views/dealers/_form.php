<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
use common\models\VipCountry;
$countrylists = VipCountry::find()->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');
?>

<div class="vipmanufacturer-form">
    <?php
    $form = ActiveForm::begin(['options' => [
                    'class' => 'form-label-left'
    ]]);
    ?>
    <div class="box-body">
        <div class="col-md-6">

            <?= $form->field($model, 'dealer_code')->textInput(array('placeholder' => '')); ?>

            <?= $form->field($model, 'company')->textInput(array('placeholder' => '')); ?>
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'first_name', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(array('placeholder' => ''));
                    ?>
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'last_name', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(array('placeholder' => ''));
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'email', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(array('placeholder' => ''));
                    ?>
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'tel', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(array('placeholder' => ''));
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'address1')->textInput(array('placeholder' => '')); ?>
            <?= $form->field($model, 'address2')->textInput(array('placeholder' => '')); ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'city')->textInput(array('placeholder' => '')); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'postcode')->textInput(array('placeholder' => '')); ?>
                </div>
                <div class="col-md-4">
                    <?php
                        $states_list = ['Kuala Lumpur' => 'Kuala Lumpur', 'Selangor' => 'Selangor', 'Kelantan' => 'Kelantan', 'Pahang' => 'Pahang', 'Terengganu' => 'Terengganu', 'Kedah' => 'Kedah', 'Penang' => 'Penang', 'Perak' => 'Perak', 'Perlis' => 'Perlis', 'Johor' => 'Johor', 'Melaka' => 'Melaka', 'Negeri Sembilan' => 'Negeri Sembilan', 'Sabah' => 'Sabah', 'Sarawak' => 'Sarawak'];
                        echo $form->field($model, 'states')->dropDownList($states_list,['prompt'=>'Select Option']);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php
                        $region_list = ['Central' => 'Central', 'East Coast' => 'East Coast', 'Northern' => 'Northern', 'Southern' => 'Southern', 'East Malaysia' => 'East Malaysia'];
                        echo $form->field($model, 'region')->dropDownList($region_list,['prompt'=>'Select Option']);
                    ?>
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'country', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->widget(Select2::classname(), [
                        'data' => $country,
                        //'language' => 'de',
                        'options' => [
                            'placeholder' => 'Select ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

            </div>
        </div>
        <div class="row">
        <div class="box-footer col-lg-12">    
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>