<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */

$this->title = 'Update';
$this->params['breadcrumbs'][] = ['label' => 'Manage Dealers / Distributor', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->manufacturer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?=
    $this->render('_form_update', [
        'model' => $model,
        'modelupdate' => $modelupdate
    ])
    ?>


</div>