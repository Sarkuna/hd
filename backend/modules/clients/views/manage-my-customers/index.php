<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View My Customers';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->

    <div class="box-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'clients_ref_no',
                    'label' => 'Code',
                    'format' => 'html',
                    'headerOptions' => ['width' => '100'],
                    'value' => function ($model) {
                        return $model->clients_ref_no;
                    },
                ],          
                'full_name',
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->user->email;
                    },
                ],
                'mobile_no', 
                           

               [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [
                        'view' => function ($url, $model) {
                          return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                        },
                    ],
                //'visible' => $visible,
                ],     
            ],
        ]);
        ?>


    </div>
</div>

