<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Company Information'); ?>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('company_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($companyInformation->company_name) ? $companyInformation->company_name : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('company_reg_type') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $companyInformation->company_reg_type ?  $companyInformation->registration->name : Yii::t("app", "N/A")?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('company_registration_number') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $companyInformation->company_registration_number ?></div>
        </div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('mailing_address') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($companyInformation->mailing_address) ? $companyInformation->mailing_address : Yii::t("app", "N/A") ?></div>
    </div>
    
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('tel') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $companyInformation->tel ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('fax') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $companyInformation->fax ?></div>
        </div>
    </div>
    

    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-xs-12 edusec-profile-text"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('contact_person') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($companyInformation->contact_person) ? $companyInformation->contact_person : Yii::t("app", "N/A") ?></div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('contact_no') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($companyInformation->contact_no) ? $companyInformation->contact_no : Yii::t("app", "N/A") ?></div>
    </div>
</div>

