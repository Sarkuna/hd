<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
/* @var $form yii\widgets\ActiveForm */
use common\models\VIPCustomer;

$dealers = VIPCustomer::find()
           ->joinWith(['user'])
           ->select('userID, full_name')
           ->where(['assign_dealer_id' => Yii::$app->user->id])
           ->orWhere(['user.type' => $model->type])
           ->asArray()->all();

$dealerslist = ArrayHelper::map($dealers, 'userID', 'full_name');
$typename = \common\models\TypeName::find()->all();
$typenames = ArrayHelper::map($typename,'tier_id','name');
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="box-body">
    <div class="col-lg-6">
        <?= $form->field($model, 'project_name')->textInput(['maxlength' => true]) ?>
        <?=
            $form->field($model, 'type', [
                    //'template' => "<div class='form-label-group'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(Select2::classname(), [
                'data' => $typenames,
                //'language' => 'de',
                'options' => [
                    'placeholder' => 'Select Type of User',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                
         ]);
        ?>
           
        <?=
    $form->field($model, 'userID', [
            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->widget(Select2::classname(), [
        'data' => $dealerslist,
        //'language' => 'de',
        'options' => [
            'placeholder' => 'Select ...',           
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
      
        
        <?= $form->field($model, 'project_client_name')->textInput(['maxlength' => true]) ?>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'project_client_email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'project_client_tel')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <?=
    $form->field($model, 'project_status', [
            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->widget(Select2::classname(), [
        'data' => ['P'=>'Potential', 'I'=>'In Progress', 'C'=>'Close Sales', 'U'=>'Unsuccessful'],
        //'language' => 'de',
        'options' => [
            'placeholder' => 'Select ...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'project_nominated_points')->textInput() ?>
    

    <?= $form->field($model, 'project_details')->textarea(['rows' => 6]) ?>
    </div>

    

    
</div>
<div class="box-footer">  
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
