<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TotalProductList */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body">
    <div class="total-product-list-form">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'distributor_code')->textInput() ?>

        <?= $form->field($model, 'brand')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'product_name_distirbutor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'iws')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mz')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
