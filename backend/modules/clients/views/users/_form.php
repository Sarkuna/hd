<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipmanufacturer-form">

    <?php
    $form = ActiveForm::begin(['options' => [
                    'class' => 'form-horizontal form-label-left'
    ]]);
    ?>
<div class="box-body">

    <?=
    $form->field($model, 'email', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'fname', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'lname', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'password', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

    <?=
    $form->field($model, 'confirmpassword', [
        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
    ])->textInput(array('placeholder' => ''));
    ?>

<?php
//$themes = common\models\VipCountry::find()->all();
$listData = ['A' => 'Active', 'P' => 'Pending', 'D' => 'Deactive'];
//echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
echo $form->field($model, 'status', [
            'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-6'>{input}</div>\n{hint}\n{error}"
        ])
        ->dropDownList(
                $listData
                //['prompt'=>'--', 'id' => 'relation1']    
);
?>

</div>
<div class="box-footer">    
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

</div>
