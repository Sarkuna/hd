<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin User List';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
    </div><!-- /.box-header -->

    <div class="box-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'profile_ID',
                //'userID',
                //'client_ID',
                //'user.email',
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->email;
                    },
                ],
                    
                [
                    'attribute' => 'fname',
                    'label' => 'Name',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->adminUserProfile->first_name.' '.$model->adminUserProfile->last_name;
                    },
                ],    
                [
                    'attribute' => 'status',
                    'label' => 'Status',
                    'format' => 'html',
                    'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                return $model->getStatustext();
            },
                ],
                [
                    'attribute' => 'created_datetime',
                    'label' => 'Date Added',
                    //'format' => 'html',
                    'format' => ['date', 'php:d/m/Y'],
                    'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                return $model->created_datetime;
            },
                ],
                //'user.created_datetime',
                // 'address1',
                // 'address2',
                // 'city',
                // 'postcode',
                // 'region',
                // 'country',
                // 'image',
                //['class' => 'yii\grid\ActionColumn'],
               [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}', //{view} {delete}
                'buttons' => [
                    'update' => function ($url, $model) {
                        $url = 'users/update?id='.$model->adminUserProfile->profile_ID;
                        return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                    },
                    'delete' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                    }        
                            /* ,
                          'view' => function ($url, $model) {
                          return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                          },
                           */
                    ],
                //'visible' => $visible,
                ],     
            ],
        ]);
        ?>


    </div>
</div>

