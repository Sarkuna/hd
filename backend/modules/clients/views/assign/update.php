<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignProducts */

$this->title = 'Update Vipassign Products: ' . $model->vip_assign_products_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_assign_products_id, 'url' => ['view', 'id' => $model->vip_assign_products_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipassign-products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
