<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\VIPCategories;
use common\models\VIPAssignCategories;


/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignCategories */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$clientID = $session['currentclientID']
?>

<div class="vipassign-categories-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
    
    <div class="row">
            <div class="col-lg-6">
                <?php
                $query = VIPAssignCategories::find()
                ->joinWith(['categories'])
                ->where(['is_active' => '0'])->orderBy('name')->all();
                $list = ArrayHelper::map($query, 'categories_id', 'categories.name');
                $catID =  Yii::$app->request->get('catid');
                if(!empty($catID)){
                   $model->categories_id = $catID; 
                }
                ?>
                
                <?= $form->field($model, 'categories_id')->dropDownList($list, 
			//['prompt'=>'Select...']);
                        ['prompt'=>'-Choose a Product-',
                            'onchange' => 'document.location.href = "/clients/assign/sub-categories?catid=" + this.value',
                        ]);
                ?>
                
                <?php
                $listsub = ArrayHelper::map(\common\models\VIPSubCategories::find()->where(['parent_id' => $catID])->orderBy('name')->all(), 'vip_sub_categories_id', 'name');
                ?>
                <label class="control-label" for="reportform-groupname">Sub Categorie[s]</label><br>    
                <!--<input type="checkbox" id="select_all"/> Select All -->
                <?=  $form->field($model, 'sub_categories_id')->checkboxList($listsub, 
                      [
                         'item'=>function ($index, $label, $name, $checked, $value){
                            $checked = $checked ? 'checked' : '';
                            return "<label class='ckbox ckbox-primary col-md-6'> <input class='rcheckbox' type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>{$label}</label>";
                                    }
                      ])->label(false); ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
    <div class="form-group">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <?= Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn btn-primary']) ?>
            
        </div>
    </div>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
    $script = <<<EOD
            
$("input[name='AssignCategoriesForm[categories_id][]']").change(function () {
    var maxAllowed = 2;
    var cnt = $("input[name='AssignCategoriesForm[categories_id][]']:checked").length;
    if (cnt > maxAllowed){
        $(this).prop("checked", "");
        alert('Select maximum ' + maxAllowed + ' Categorie[s]');
    }
});

EOD;
$this->registerJs($script);
?>            