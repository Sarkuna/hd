<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */

$this->title = 'Assign Category';
$this->params['breadcrumbs'][] = ['label' => 'Assign Category', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Assign Category</h3>
        </div>
        <div class="vipcategories-create">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>

