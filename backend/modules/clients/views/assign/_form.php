<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignCategories */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$clientID = 16;
?>

<div class="vipassign-categories-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <div class="col-md-12 col-sm-6 col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn btn-primary']) ?>
            
        </div>
    </div>
    <div class="row">
            <div class="col-lg-6">
                <label class="control-label" for="reportform-groupname">Categorie[s]</label><br>    
                <!--<input type="checkbox" id="select_all"/> Select All -->

                <?php
                $list = ArrayHelper::map(\common\models\VIPCategories::find()->where(['is_active' => '0'])->orderBy('name')->all(), 'vip_categories_id', 'name');
                ?>
                
                <?=  $form->field($model, 'categories_id')->checkboxList($list, 
                      [
                         'item'=>function ($index, $label, $name, $checked, $value){
                            $checked = $checked ? 'checked' : '';
                            return "<label class='ckbox ckbox-primary col-md-6'> <input class='rcheckbox' type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>{$label}</label>";
                                    }
                      ])->label(false); ?>
            </div>
        </div>


    

    <?php ActiveForm::end(); ?>

</div>

<?php
    $script = <<<EOD
            
$("input[name='AssignCategoriesForm[categories_id][]']").change(function () {
    var maxAllowed = 10;
    var cnt = $("input[name='AssignCategoriesForm[categories_id][]']:checked").length;
    if (cnt > maxAllowed){
        $(this).prop("checked", "");
        alert('Select maximum ' + maxAllowed + ' Categorie[s]');
    }
});

EOD;
$this->registerJs($script);
?>            