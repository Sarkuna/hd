<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assign Product List';
$this->params['breadcrumbs'][] = ['label' => 'Assign Products', 'url' => ['products']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-12 col-sm-12 col-xs-12 vipproduct-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'hide btn btn-success pull-right', 'title' => 'Add New']) ?>
        </div><!-- /.box-header -->

        <div class="box-body">
            <?php
                
                if ($dataProvider->totalCount > 0) {
                   $disable = false;
                }else{
                   $disable = true; 
                }
                ?>
            <?=Html::beginForm(['bulkassign'],'post');?>
                <div class="row">
                <div class="col-lg-2">
                <input type="hidden" name="painterid" value="">   
                <?=
                Html::submitButton('Go', ['class' => 'btn btn-info', 'disabled' => $disable, 'data' => [
                        'confirm' => 'Are you sure you want to redeem selected items?',
                        'method' => 'post',                        
                    ],]);
                ?>
                <?= Html::a('Cancel', ['products'], ['class' => 'btn btn-default']) ?>     
                </div>
                <div class="col-lg-10 text-right">

                </div>
            </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],

            //'vip_categories_id',
            [
                'attribute' => 'image',
                'label' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    $url = $model->main_image ? Html::img('/upload/product_cover/'.$model->main_image, ['alt' => 'myImage', 'width' => '70'], ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : '<i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
                    //$url = \Yii::getAlias('@backend/web/upload/product_cover/') . $model->main_image;
                    //return Html::img($url, ['alt' => 'myImage', 'width' => '70', 'height' => '50']);
                    return $url;
                }
            ],
            'product_name',
            'product_code',
            'points_value',
            'quantity',
            'status',
            //'description:ntext',
            //'image',
            //['class' => 'yii\grid\ActionColumn'],
            [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{product-lisit-view}', //{view} {delete}
                            'buttons' => [
                                'product-lisit-view' => function ($url, $model) {
                                    return (Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Edit'),]));
                                }
                                    ],
                                //'visible' => $visible,
                                ],
        ],
    ]); ?>
            <?= Html::endForm();?> 
        </div>
    </div>
</div>
<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });
        $("input[type=checkbox]").change(function () {
            //recalculate();
        });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>


<script language="javascript">  
    function recalculate() {
        
        var sum = 0;
        var sumrm = 0.00;
        var sumtot = 0.00;
        var a = "";
        $("input[name='selection[]']:checked").each(function () {
            $this=$(this);
            var amount = $(this).closest("tr").find("td:nth-child(6)").text();
            var amountrm = $(this).closest("tr").find("td:nth-child(7)").text();
            //alert(amount);
            
            if (this.checked) sum = sum + parseFloat(amount);
            if (this.checked) sumrm = sumrm + parseFloat(amountrm);
            if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if (this.checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });

        $("#redemption-orderid").val(a); 
        $("#output").text(sum);
        $("#outputrm").text(sumrm.toFixed(2));
        $("#redemption-req_points").val(sum);
        $("#redemption-req_amount").val(sumtot.toFixed(2));
        $("#multiamt").val(sum);
        
    }
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 