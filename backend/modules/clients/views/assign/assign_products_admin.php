<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */

$this->title = 'Assign Products';
$this->params['breadcrumbs'][] = ['label' => 'Assign Products', 'url' => ['products']];
//$this->params['breadcrumbs'][] = $this->title;
$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];
$mark_up = $clientsetup['pervalue'];
?>
<style>
    .rg {margin-right: 5px;}
</style>


    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <?= Html::a('<i class="fa fa-upload"></i>', ['upload-excel'], ['class' => 'btn btn-primary pull-right', 'title' => 'Upload Excel']) ?> 
            <?= Html::a('<i class="fa fa-download"></i>', ['download-excel'], ['class' => 'btn btn-success pull-right rg', 'title' => 'Download List of Products']) ?>
        </div><!-- /.box-header -->

        <div class="box-body">
            
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vip_categories_id',
            [
                'attribute' => 'image',
                'label' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    $url = $model->product->main_image ? Html::img('/upload/product_cover/thumbnail/'.$model->product->main_image, ['alt' => 'myImage', 'width' => '70'], ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : '<i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
                    //$url = \Yii::getAlias('@backend/web/upload/product_cover/') . $model->main_image;
                    //return Html::img($url, ['alt' => 'myImage', 'width' => '70', 'height' => '50']);
                    return $url;
                }
            ],
            //'product.product_name',
            //'product.product_code',
            //'product.price',
            [
                'attribute' => 'product_name',
                'label' => 'Product Name',
                'format' => 'raw',
                //'headerOptions' => ['width' => '120'],                
                'value' => function ($model) {
                    return $model->product->product_name;
                }
            ],        
            [
                'attribute' => 'product_code',
                'label' => 'Product Code',
                'format' => 'raw',
                'headerOptions' => ['width' => '120'],                
                'value' => function ($model) {
                    return $model->product->product_code;
                }
            ], 
            [
                'attribute' => 'cost_price',
                'label' => 'Retail Price RM',
                'format' => 'raw',
                'headerOptions' => ['width' => '120'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    //$price = Yii::$app->VIPglobal->myPrice($model->product->product_id);
                    //$price = $model->product->price;
                    $price = $model->msrp;
                    return Yii::$app->formatter->asDecimal($price);
                }
            ],
            [
                'attribute' => 'mark_up',
                'label' => 'Mark Up',
                'format' => 'raw',
                'headerOptions' => ['width' => '90'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    $mark_up = Yii::$app->VIPglobal->markUp($model->product->product_id);
                    return Yii::$app->formatter->asDecimal($mark_up);
                }
            ],
            [
                'attribute' => 'mark_up_type',
                'label' => 'Type',
                'format' => 'raw',
                'headerOptions' => ['width' => '50'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    $price = Yii::$app->VIPglobal->markUpType($model->product->product_id);
                    return $price;
                }
            ],        
            [
                'attribute' => 'display_price',
                'label' => 'Display Price RM',
                'format' => 'raw',
                'headerOptions' => ['width' => '120'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    $display_price = Yii::$app->VIPglobal->myPrice($model->product->product_id);
                    return Yii::$app->formatter->asDecimal($display_price);
                }
            ],        
            [
                'attribute' => 'point',
                'headerOptions' => ['width' => '90'],
                'label' => 'Point ('.$pointpersent.')',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    $point = Yii::$app->VIPglobal->myPoint($model->product->product_id);
                    return Yii::$app->formatter->asInteger($point);
                }
            ],        
            //'product.quantity',
            'product.status',
            //'description:ntext',
            //'image',
            //['class' => 'yii\grid\ActionColumn'],
            [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{product-view} {update} {remove}', //{view} {delete}
                            'buttons' => [
                                'product-view' => function ($url, $model) {
                                    return (Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-sm', 'title' => Yii::t('app', 'View'),]));
                                },
                                'update' => function ($url, $model) {
                                    return (Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit'),]));
                                },
                                'remove' => function ($url, $model) {
                                    return (Html::a('<i class="fas fa-unlink"></i>', $url, 
                                    [
                                        'class' => 'btn btn-danger btn-sm', 
                                        'title' => Yii::t('app', 'Unassign'),
                                        'data' => [
                                            'confirm' => 'Are you sure you want to unassign this?',
                                            'method' => 'post',
                                        ],
                                    ]));
                                },
                                /* ,
                                      'view' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                      },
                                      'delete' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                                      } */
                                    ],
                                //'visible' => $visible,
                                ],
        ],
    ]); ?>
        </div>
        
    </div>    

