<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View My IDs';
$this->params['breadcrumbs'][] = $this->title;
$clientID = 16;
$typename = \common\models\TypeName::find()->where([
                'clientID' => $clientID,
            ])->all();

$typenames = ArrayHelper::map($typename, 'tier_id', 'name');
?>



<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->

    <div class="box-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'clients_ref_no',
                    'label' => 'Code',
                    'format' => 'html',
                    'headerOptions' => ['width' => '100'],
                    'value' => function ($model) {
                        return $model->clients_ref_no;
                    },
                ],          
                'full_name',
                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->user->email;
                    },
                ],
                'mobile_no', 
                           
                [
                    'attribute' => 'type',
                    'label' => 'User Category',
                    'format' => 'raw',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->user->typeName->name;
                    },
                    'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                    'filter' => $typenames,
                ],
               [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [
                        'view' => function ($url, $model) {
                          return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                        },
                    ],
                //'visible' => $visible,
                ],     
            ],
        ]);
        ?>


    </div>
</div>

