<style>
    .nav>li>a {
        padding: 10px 10px;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a {
    background-color: #00C0EF;
    color: #ffffff;
}
</style>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'View My IDs', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;


$profile_pic = \common\models\ProfilePic::find()->where([
    'userID' => $model->userID,
])->count();

if($profile_pic == 1) {
    $profile_pic = \common\models\ProfilePic::find()->where([
        'userID' => $model->userID,
    ])->one();
    $img = '/upload/profiles/'.$profile_pic->file_name;
}else {
    $img = '../../images/avatar_image.jpg';
}
///upload/profiles/
?>
<div class="vipcustomer-view">
    <div class="row">
        <div class="col-lg-12"><h3 style="margin-top: 0px;" class="pull-right"><?= $model->user->getStatustext() ?></h3></div>
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?= $img ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?= Html::encode($model->full_name) ?></h3>
              <p class="text-muted text-center">Clients Ref No: <?= Html::encode($model->clients_ref_no) ?></p>

              <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                  <b>Awarded Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalAdd()) ?></a>
                </li>
                <li class="list-group-item">
                  <b>Redeemed Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getTotalMinus()) ?></a>
                </li>
                <li class="list-group-item">
                  <b>Expired Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getPointExpiry()) ?></a>
                </li>
                <li class="list-group-item">
                  <b>Balance Points</b> <a class="pull-right"><?= Yii::$app->formatter->asInteger($model->getBalance()) ?></a>
                </li>
                
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9  profile-data">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs responsive" id = "profileTab">
                <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Info'); ?></a></li>
                <li id = "company-tab"><a href="#company" data-toggle="tab"><i class="fa fa-building"></i> <?php echo Yii::t('app', 'Company Info'); ?></a></li>
            </ul>
            <div id='content' class="tab-content responsive">
                <div class="tab-pane active" id="personal">
                    <?= $this->render('_tab_personal', ['model' => $model]) ?>	
                </div>
                <div class="tab-pane" id="company">
                    <?= $this->render('_tab_company', ['model' => $model, 'companyInformation' => $companyInformation]) ?>
                </div>
            </div> 
          
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
        </div>
      </div>
</div>

<?php
$script = <<< JS
    $(function() {
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTab', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>