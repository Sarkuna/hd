<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductName */

$this->title = 'Create Product Name';
$this->params['breadcrumbs'][] = ['label' => 'Product Names', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary product-name-creat">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>


</div>