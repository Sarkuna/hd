<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_list_id') ?>

    <?= $form->field($model, 'clientID') ?>

    <?= $form->field($model, 'product_name_id') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'pack_size') ?>

    <?php // echo $form->field($model, 'total_points_awarded') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
