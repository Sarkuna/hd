<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductList */

$this->title = $model->product_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Product Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_list_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_list_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_list_id',
            'clientID',
            'product_name_id',
            'description:ntext',
            'pack_size',
            'total_points_awarded',
        ],
    ]) ?>

</div>
