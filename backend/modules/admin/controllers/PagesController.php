<?php

namespace app\modules\admin\controllers;

use Yii;
use common\models\Pages;
use common\models\PagesSearch;
use common\models\UploadsSlider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $searchModel = new PagesSearch();
        $searchModel->clientID = 16;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $model = new Pages();

        if ($model->load(Yii::$app->request->post())) {
            $model->clientID = 16;
            $model->type = 'page';
            //$model->save();
            if($model->save()){
               \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Page have been created']); 
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Sorry page have not create']); 
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionBanners()
    {
        $session = Yii::$app->session;
        $ref = 16;
        $model = new \backend\models\BannerForm();
        
        list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($ref);
        return $this->render('_form_logo', [
            'model' => $model,
            'initialPreview'=>$initialPreview,
            'initialPreviewConfig'=>$initialPreviewConfig,
        ]);

    }
    
    public function actionUploadajax(){
           $this->Uploads(true);
     }
     
     private function CreateDir($folderName){
        if($folderName != NULL){
            $basePath = Yii::$app->basePath .'/web/upload/slider/';
            FileHelper::createDirectory($basePath.$folderName,0777);
        }
        return;
    }
     
     private function Uploads($isAjax=false) {
        $session = Yii::$app->session; 
             if (Yii::$app->request->isPost) {
                $images = UploadedFile::getInstancesByName('upload_ajax');
                if ($images) {
                    $ref = 16;
                    $this->CreateDir($ref);
                    foreach ($images as $file){
                        $fileName       = $file->baseName . '.' . $file->extension;
                        $realFileName   = md5($file->baseName.time()) . '.' . $file->extension;
                        $savePath       = Yii::$app->basePath .'/web/upload/slider/'.$ref.'/'. $realFileName;
    
                    
                        if($file->saveAs($savePath)){
                            /*if($this->isImage(Url::base(true).'/'.$savePath)){
                                $this->createThumbnail($ref,$realFileName);
                            } */
                          
                            $model                  = new UploadsSlider;
                            $model->clientID        = $ref;
                            $model->file_name       = $fileName;
                            $model->real_filename   = $realFileName;
                            $model->save();
                            if($isAjax===true){
                                echo json_encode(['success' => 'true']);
                            }
                            
                        }else{
                            if($isAjax===true){
                                echo json_encode(['success'=>'false','eror'=>$file->error]);
                            }
                        }
                        
                        
                        
                    }
                }
             }
     }
     
     private function getInitialPreview($ref) {
            $datas = UploadsSlider::find()->where(['clientID'=>$ref])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/admin/pages/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    
    private function getTemplatePreview(UploadsSlider $model){     
            $filePath = Url::base(true).'/upload/slider/'.$model->clientID.'/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);
            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image img-responsive', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    
    public function actionDeletefileAjax(){
        $this->enableCsrfValidation = false;
        $model = UploadsSlider::findOne(Yii::$app->request->post('key'));
        if($model!==NULL){
            $filename = Yii::$app->basePath .'/web/upload/slider/'.$model->clientID.'/'. $model->real_filename;
            if($model->delete()){
                @unlink($filename);
                echo json_encode(['success'=>true]);
            }else{
                print_r($model->getErrors());
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->page_id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $fanclub = \common\models\MenuItems::find()
        ->where(['related_id'=>$id])
        ->one()
        ->delete();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
