<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SMSTemplate */

$this->title = 'Update SMS Template: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'SMS Templates', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary smstemplate-update">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="email-template-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
