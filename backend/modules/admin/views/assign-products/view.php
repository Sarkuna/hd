<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignProducts */

$this->title = $model->vip_assign_products_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipassign-products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->vip_assign_products_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vip_assign_products_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vip_assign_products_id',
            'clientID',
            'productID',
            'date_assign',
        ],
    ]) ?>

</div>
