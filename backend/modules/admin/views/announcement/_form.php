<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipannouncement-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-9">

   <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
            </div>
            
            <div class="col-lg-3">

    <?= $form->field($model, 'type')->dropDownList(['A' => 'Approve User', 'N' => 'Not Approve User'],['prompt'=>'Select...']); ?>
    <?= $form->field($model, 'lang')->dropDownList(['en' => 'English','ch' => 'Mandarin','bm' => 'Malay']); ?>
    
    <?= $form->field($model, 'status')->dropDownList(['D' => 'Draft','E' => 'Published']); ?>
            </div>
        </div>
        

    
    </div>
    <div class="box-footer">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Publish' : 'Publish', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
