<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\web\UploadedFile;

use common\models\Client;
use common\models\ClientSearch;
use common\models\ClientAddress;
use common\models\ClientBillingInformation;
use common\models\ClientOption;
use common\models\ClientPointOption;


/**
 * ClientsController implements the CRUD actions for Client model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
       
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTest() {
        return \Yii::$app->mailer->compose()
     ->setFrom('shihanagni@gmail.com')
     ->setTo('shihanagni@gmail.com')
     ->setSubject('Email sent from Yii2-Swiftmailer')
     ->setHtmlBody('TEsat Email')
     ->send();
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $address = ClientAddress::find()->where(['clientID' => $id])->one();
        $billing_address = ClientBillingInformation::find()->where(['clientID' => $id])->one();
        $clientoption = ClientOption::find()->where(['clientID' => $id])->one();
        $formapprove = new \common\models\Approve();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'address' => $address,
            'billing_address' => $billing_address,
            'clientoption' => $clientoption,
            'formapprove' => $formapprove
        ]);
    }
    
    public function actionApprovel($id)
    {
        //$model = new PainterProfile();
        $formapprove = new \common\models\Approve();
        if ($formapprove->load(Yii::$app->request->post())){
            $model = $this->findModel($id);
            $model->client_status = $formapprove->status;
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Approve', 'text' => 'Action Successful.']);
                return $this->redirect(['view', 'id' => $id]);
            }else{
                print_r($formapprove->getErrors());
            }
        }
        //return $this->redirect(['view', 'id' => $model->vip_customer_id]);
    }
    
    public function actionEditSite($id=null)
    {
        $model = $this->findModel($id);       
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Site', 'text' => 'Action has been updated!']);
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->renderAjax('_formpop', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionEditSetting($id=null)
    {
        $model = $this->findModelSetting($id);       
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Setting', 'text' => 'Action has been updated!']);
            return $this->redirect(['view', 'id' => $model->clientID, 'tab' => 'setting']);
        } else {
            return $this->renderAjax('_formpop_setting', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionEditAddress($id=null)
    {
        $model = $this->findModeladdress($id);       
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Company Address', 'text' => 'Action has been updated!']);
            return $this->redirect(['view', 'id' => $model->clientID, 'tab' => 'company']);
        } else {
            return $this->renderAjax('_formpop_address', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Client();
        $addressmodel = new ClientAddress();
        $clientoption = new ClientOption();
    }
    public function actionCreatedelete()
    {
        //$this->layout = '/mainwizerdform';
        $model = new Client();
        $addressmodel = new ClientAddress();
        $billinginformation = new ClientBillingInformation();
        $clientoption = new ClientOption();
        $clientpointoption = new ClientPointOption();

        if ($addressmodel->load(Yii::$app->request->post()) && $billinginformation->load(Yii::$app->request->post()) && $clientoption->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            if(Model::validateMultiple([$addressmodel, $billinginformation, $clientoption, $model])){ 
                $newCover = UploadedFile::getInstance($model, 'fimage');
                if (!empty($newCover)) {
                    $newCoverName = Yii::$app->security->generateRandomString();
                    $imgname = $newCoverName . '.' . $newCover->extension;
                    $model->favicon = $imgname;
                    //$ClientAddress->save(false);
                    //unlink($model->cover);
                    //$model->cover = 'uploads/covers/' . $newCoverName . '.' . $newCover->extension;
                    $newCover->saveAs(Yii::$app->basePath .'/web/upload/favicon/' . $newCoverName . '.' . $newCover->extension);
                }
                if($model->save()){               
                   $addressmodel->clientID = $model->clientID;
                   $addressmodel->save(false);
                   $billinginformation->clientID = $model->clientID;
                   $billinginformation->save(false);
                   
                   $clientoption->clientID = $model->clientID;
                   $clientoption->save(false);
                   
                   $clientpointoption->clientID = $model->clientID;
                   $clientpointoption->save(false);
                   
                   \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                    return $this->redirect(['index']);
                }
            }else {
                return $this->render('create', [
                    'model' => $model,
                    'addressmodel' => $addressmodel,
                    'billinginformation' => $billinginformation,
                    'clientoption' => $clientoption,
                    'clientpointoption' => $clientpointoption
                ]);
            }            
            //return $this->redirect(['view', 'id' => $model->clientID]);
        } else {
            $model->client_SMS = 'N';
            $model->client_email = 'N';
            $model->membership = 'N';
            return $this->render('create', [
                'model' => $model,
                'addressmodel' => $addressmodel,
                'billinginformation' => $billinginformation,
                'clientoption' => $clientoption,
                'clientpointoption' => $clientpointoption
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //$this->layout = '/mainwizerdform';
        $model = $this->findModel($id);
        
        $client_address = \common\models\ClientAddress::find()->where(['clientID' => $id])->one();
        $addid = $client_address->client_address_ID;
        $addressmodel = $this->findModeladdress($addid);
        
        $b_information = \common\models\ClientBillingInformation::find()->where(['clientID' => $id])->one();
        $biid = $b_information->billing_information_ID;
        $billinginformation = $this->findBillinginformation($biid);
        
        $c_option = \common\models\ClientOption::find()->where(['clientID' => $id])->one();
        $cpid = $c_option->option_ID;
        $clientoption = $this->findClientoption($cpid);
        
        $c_point_option = ClientPointOption::find()->where(['clientID' => $id])->one();
        $cpointid = $c_point_option->client_point_option_id;
        $clientpointoption = $this->findClientpointoption($cpointid);

        if ($addressmodel->load(Yii::$app->request->post()) && $billinginformation->load(Yii::$app->request->post()) && $clientoption->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
        //if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Model::validateMultiple([$addressmodel, $billinginformation, $clientoption, $model])){
                $newCover = UploadedFile::getInstance($model, 'fimage');
                if (!empty($newCover)) {
                    $newCoverName = Yii::$app->security->generateRandomString();
                    $imgname = $newCoverName . '.' . $newCover->extension;
                    $model->favicon = $imgname;
                    //$ClientAddress->save(false);
                    //unlink($model->cover);
                    //$model->cover = 'uploads/covers/' . $newCoverName . '.' . $newCover->extension;
                    $newCover->saveAs(Yii::$app->basePath .'/web/upload/favicon/' . $newCoverName . '.' . $newCover->extension);
                }
                $model->save();
                $addressmodel->save(false);
                $billinginformation->save(false);
                $clientoption->save(false);
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->clientID]);
            }else {
                return $this->render('update', [
                    'model' => $model,
                    'addressmodel' => $addressmodel,
                    'billinginformation' => $billinginformation,
                    'clientoption' => $clientoption,
                    'clientpointoption' => $clientpointoption
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'addressmodel' => $addressmodel,
                'billinginformation' => $billinginformation,
                'clientoption' => $clientoption,
                'clientpointoption' => $clientpointoption
            ]);
        }
    }
    
    public function actionLogo($logoID)
    {
        $model = new \backend\models\LogoForm();
        $ClientAddress = Client::find()->where(['client_address_ID' => $logoID])->one();
        if ($model->load(Yii::$app->request->post())) {
            $newCover = UploadedFile::getInstance($model, 'file_image');
            if (!empty($newCover)) {
                if(!empty($ClientAddress->company_logo)){
                    $filename = Yii::$app->basePath .'/web/upload/client_logos'.$ClientAddress->company_logo;
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                }
                $newCoverName = Yii::$app->security->generateRandomString();
                $imgname = $newCoverName . '.' . $newCover->extension;
                $ClientAddress->company_logo = $imgname;
                $ClientAddress->save(false);
                //unlink($model->cover);
                //$model->cover = 'uploads/covers/' . $newCoverName . '.' . $newCover->extension;
                $newCover->saveAs(Yii::$app->basePath .'/web/upload/client_logos/' . $newCoverName . '.' . $newCover->extension);
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }
        }else {
            return $this->render('_form_logo', [
                        'model' => $model,
                        'ClientAddress' => $ClientAddress,
            ]);
        }
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelSetting($id)
    {
        if (($model = ClientOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModeladdress($id)
    {
        if (($modeladdress = \common\models\ClientAddress::findOne($id)) !== null) {
            return $modeladdress;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findBillinginformation($id)
    {
        if (($billinginformation = \common\models\ClientBillingInformation::findOne($id)) !== null) {
            return $billinginformation;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findClientoption($id)
    {
        if (($clientoption = \common\models\ClientOption::findOne($id)) !== null) {
            return $clientoption;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findClientpointoption($id)
    {
        if (($clientpointoption = ClientPointOption::findOne($id)) !== null) {
            return $clientpointoption;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
