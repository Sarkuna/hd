<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->company;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-view">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    
                    <h2> <?= $model->getStatus() ?></h2>
                    <?php
                        /*if(!empty($model->clientAddress->company_logo)){
                            $img = '/upload/client_logos/'.$model->clientAddress->company_logo;
                            echo '<img alt="User profile picture" src="'.$img.'" class="profile-user-img img-responsive">';
                        }*/
                        //$picPro = '<img alt="User profile picture" src="view-avatar?id='.$model->vip_customer_id.'" class="profile-user-img img-responsive img-circle">';
                    ?>

                    <?= $this->render('_formAction', ['model' => $model, 'formapprove' => $formapprove,]) ?>


                </div>
            </div>
        </div>
        <div class="col-md-9 profile-data">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs responsive" id = "profileTab">
                    <li class="active" id = "info-tab"><a href="#info" data-toggle="tab"><i class="fa fa-globe"></i> <?php echo Yii::t('app', 'Site Info'); ?></a></li>
                    <li id = "company-tab"><a href="#company" data-toggle="tab"><i class="fa fa-building"></i> <?php echo Yii::t('app', 'Company Address'); ?></a></li>
                    <li id = "setting-tab"><a href="#setting" data-toggle="tab"><i class="fas fa-cog"></i> <?php echo Yii::t('app', 'Setting'); ?></a></li>
                </ul>
                
                <div id='content' class="tab-content responsive">
                    <div class="tab-pane active" id="info">
                        <?= $this->render('_tab_site', ['model' => $model]) ?>
                    </div>
                    <div class="tab-pane" id="company">
                        <?= $this->render('_tab_client_address', ['model' => $model, 'address' => $address, 'billing_address' => $billing_address])?>
                    </div>
                    <div class="tab-pane" id="setting">
                        <?= $this->render('_tab_setting', ['model' => $model, 'clientoption' => $clientoption])?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
