<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Logo';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
?>

<div class="col-md-12 col-sm-12 col-xs-12 client-logo-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>            
        </div><!-- /.box-header -->
        <div class="box-body">  
            <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['options' => ['id' => 'demo-form2', 'class' => 'form-horizontal form-label-left']]); ?>

            <?php
            echo $form->field($model, 'file_image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                    'initialPreview' => [
                        //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                        $ClientAddress->company_logo ? Html::img('/upload/client_logos/'.$ClientAddress->company_logo, ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : null, // checks the models to display the preview
                    ],
                ]
            ]);
            ?>
            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
