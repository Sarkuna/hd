
    <?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntake */

$this->title = $model->bonus_intake_year;
$this->params['breadcrumbs'][] = ['label' => 'Bonus Intakes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bonus-intake-view">
    <div class="row">
        <!-- left column -->
        <div class="col-md-7">
            <link href="/themes/adminLTE/custom/css/jquery.stepProgressBar.css" rel="stylesheet" type="text/css">
                    <script src="https://www.jqueryscript.net/demo/Animated-Step-Progress-Indicator-With-jQuery-StepProgressBar/src/jquery.stepProgressBar.js"></script>

            <?php
                //foreach($bonusintakelist as $listitem){
                echo '<div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">'.date('M-Y', strtotime($listitem->month_start)).' '.date('M-Y', strtotime($listitem->month_end)).'</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <p class="text-left">
                            <strong></strong>
                        </p>';
                        /*echo '<table class="table no-margin">
                            <thead>
                                <tr>
                                    <th width="30%">Name</th>
                                    <th width="20%">Target</th>
                                    <th width="50%"></th>
                                </tr>
                            </thead>
                            <tbody>';*/
                $mystring = 0;
                $jid = '';
                                $n = 1;
                                foreach ($bonus_product as $bonus_product_one){
                                    //echo '<div class="row">';
                                    echo '<div class="col-lg-4 barname">'.$bonus_product_one['customer']['full_name'];
                                    echo '</div><div class="col-lg-8"><div id="myGoal'.$n.'"></div></div>';
                                    $totalpacksize = $bonus_product_one['totalpacksize'];
                                    $target = $listitem->list_target;
                                    $jid = $n;
                                    $this->registerJs("$('#myGoal$jid').stepProgressBar({
                                        currentValue: $totalpacksize,
                                        steps: [
                                          { value: '' },
                                          {
     
                                            value: $totalpacksize,
                                            bottomLabel: '<i class=\"fa fa-thumbs-down fa-fw\"></i>'
                                          },
                                          {
                                            value: $target,
                                            bottomLabel: '<i class=\'fa fa-gift fa-fw\'></i>'
                                          },
                                          {  
                                            value: 1000, 
                                            bottomLabel: '<i class=\'fa fa-thumbs-up fa-fw\'></i>'
                                          }
                                        ],
                                        unit: ''
                                      });", yii\web\View::POS_READY);
                                    $n++;
                                }
                    echo '</div>
                </div>

            </div>';
                //}
                ?>
            
           <!-- End column -->
        </div>

        <!-- right column -->
        <div class="col-md-5">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Target Completion</h3>
                </div>
                
                <div class="box-body">
                    
                    <?php
                        foreach($bonusintakelist as $bonusintakelistitem){
                            echo '<div class="progress-group">
                                <span class="progress-text"><a href="?id='.$model->bonus_intake_id.'&itemid='.$bonusintakelistitem->bonus_intake_list_id.'">'.date('M-Y', strtotime($bonusintakelistitem->month_start)).' '.date('M-Y', strtotime($bonusintakelistitem->month_end)).'</a></span>
                                <span class="progress-number"><b>130</b>/'.$bonusintakelistitem->list_target.'</span>

                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-aqua" style="width: 0%"></div>
                                </div>
                              </div>';
                        }
                    ?>
            </div>

        </div>
    </div>
</div>
<?php

?>

