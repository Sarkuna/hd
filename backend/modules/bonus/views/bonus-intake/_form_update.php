<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BonusIntake */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-intake-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="col-md-7">
            <div class="row">
                <div class="col-md-4">
                    <?php
                    echo $form->field($model, 'bonus_intake_year')->dropDownList(
                            ['2018' => '2018', '2019' => '2019', '3' => '2020'],
                            ['disabled'=>true],
                            ['prompt' => 'Select...']
                    );
                    ?>
                </div>

                <div class="col-md-4">
                    <?php
                    $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
                    echo $form->field($model, 'bonus_intake_month')->dropDownList(
                            $months, ['disabled'=>true], ['prompt' => 'Select...']
                    );
                    ?>
                </div>
                
                <div class="col-md-4">   
                    <?php
                    echo $form->field($model, 'bonus_intake_type')->dropDownList(
                            ['1' => '12 Months', '2' => '6 Months', '3' => '3 Months'], ['disabled'=>true], ['prompt' => 'Select...']
                    );
                    ?>

                </div>

            </div>
            
            <div class="row">    
                

                <div class="col-md-6">
                    <?= $form->field($model, 'bonus_intake_target')->textInput(['readonly' => true]) ?>

                </div>
            </div>
            
            <div class="row">    
                <div class="col-md-12">
                    <table class="table fixed" id="retc"> 
                           <thead>
                               <tr>
                                   <th class="hide">barcodename</th>
                                   <th class="hide">barcodeid</th>
                                   <th width="60%">Interval</th>
                                   <th width="40%">Target</th>                                 
                               </tr>
                           </thead>
                           <tbody class="product_item_list" id="product_item_list">
                               <?php
                                    if(count($bonusintakelist) > 0) {
                                        foreach($bonusintakelist as $bonusintakelistitem){
                                            //<tr><td>'+data[key]['startdate']+' '+data[key]['enddate']+'<input type="hidden" id="startdate" name="startdate[]" value="'+data[key]['startdate']+'" /><input type="hidden" id="enddate" name="enddate[]" value="'+data[key]['enddate']+'" /></td><td><input class="form-control price" type="text" id="enddate" name="bonus_intake_target[]" value="" /></td></tr>');
                                            echo '<tr id="'.$bonusintakelistitem->bonus_intake_list_id.'">
                                                <td>'.date('M-Y', strtotime($bonusintakelistitem->month_start)).' '.date('M-Y', strtotime($bonusintakelistitem->month_end)).'</td>
                                                <td class="text-center points"><input type="hidden" name="bonus_intake_list_id[]" value="'.$bonusintakelistitem->bonus_intake_list_id.'" /><input class="form-control price" type="text" id="enddate" name="bonus_intake_target[]" value="'.$bonusintakelistitem->list_target.'" /></td></tr>';
                                                //$total_items += $pointorderitemlist->product_item_qty;
                                                //$iqty++;
                                        }
                                    }else {
                                        echo '<tr><td colspan="2" class="text-center">Oops!</td></tr>';
                                    }
                               ?>
                               
                           </tbody>
                    </table>

                </div>
            </div>
            
            <div class="box-footer">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?php echo \yii\helpers\Html::a( 'Back', Yii::$app->request->referrer, ['class' => 'btn btn-info']); ?>
                </div>
            </div>
        </div>    
    </div>
    <?php ActiveForm::end(); ?>

</div>

 <?php
    $script = <<<EOD
                var handledCount = 0;
                $('body').on('keyup','.price',function(){
                    update_amounts();
                }); 

                $('#bonusintake-bonus_intake_type').change(function(){
                    //alert('test');
                    $('#product_item_list').html('');
                    var syear = $("#bonusintake-bonus_intake_year").val();
                    var smonth = $("#bonusintake-bonus_intake_month").val();
                    var stype = $("#bonusintake-bonus_intake_type").val();
                    $.ajax({
                        url:'type?syear='+syear+'&smonth='+smonth+'&stype='+stype,
                        dataType: 'json',
                        success : function(data) {
                            for(var key in data) {
                                $('#product_item_list').append('<tr><td>'+data[key]['startdate']+' '+data[key]['enddate']+'<input type="hidden" id="startdate" name="startdate[]" value="'+data[key]['startdate']+'" /><input type="hidden" id="enddate" name="enddate[]" value="'+data[key]['enddate']+'" /></td><td><input class="form-control price" type="text" id="enddate" name="bonus_intake_target[]" value="" /></td></tr>');
                                //console.log(data[key]);
                            }
                        },
                        error : function() {
                            console.log('error');
                        }
                    });
                });

            function update_amounts()
            {
                var sum = 0.00;
                $('#retc > tbody  > tr').each(function() {
                    var price = $(this).find('.price').val();
                    sum = sum + Number(price);
                });
                $("#bonusintake-bonus_intake_target").val(sum);
            }
            

EOD;
$this->registerJs($script);
    ?>
