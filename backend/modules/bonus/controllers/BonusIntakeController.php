<?php

namespace app\modules\bonus\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use backend\models\Modelm;

use common\models\BonusIntake;
use common\models\BonusIntakeSearch;
use common\models\BonusIntakeList;
use common\models\BonusProduct;

/**
 * BonusIntakeController implements the CRUD actions for BonusIntake model.
 */
class BonusIntakeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BonusIntake models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new BonusIntakeSearch();
        $searchModel->clientID = $clientID;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BonusIntake model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$itemid =null)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        if(empty($itemid)) {
            $listitem = BonusIntakeList::find()->where(['clientID' => $clientID,'bonus_intake_id' => $id])->one();
        }else {
            $listitem = BonusIntakeList::find()->where(['clientID' => $clientID,'bonus_intake_id' => $id, 'bonus_intake_list_id' => $itemid])->one();
        }
        
        //$bonus_product = BonusIntakeList::find()->where(['clientID' => $clientID,'bonus_intake_id' => $id, 'bonus_intake_list_id' => $itemid])->one();
        $bonus_product = BonusProduct::find()
            ->select('bonus_product.userID, sum(bonus_product.total_pack_size) as totalpacksize')
            ->joinWith('customer')    
            ->asArray()
            ->where(['bonus_product.clientID' => $clientID])
            ->andWhere(['>=','bonus_product.date_of_receipt',$listitem->month_start])
            ->andWhere(['<=','bonus_product.date_of_receipt',$listitem->month_end])    
            ->groupBy('bonus_product.userID')    
            ->all();

        $bonusintakelist = BonusIntakeList::find()->where(['clientID' => $clientID,'bonus_intake_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'bonusintakelist' => $bonusintakelist,
            'listitem' => $listitem,
            'bonus_product' => $bonus_product,
            
        ]);
    }

    /**
     * Creates a new BonusIntake model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new BonusIntake();
        $bonusintakelist = new BonusIntakeList();
        if ($model->load(Yii::$app->request->post())) {
            $model->clientID = $clientID;
            
            $data = Yii::$app->request->post();
            $startdate = $data['startdate'];
            $enddate = $data['enddate'];
            $bonus_intake_target = $data['bonus_intake_target'];

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if ($flag) {
                        foreach ($startdate as $key => $n) {
                            $bonusintakelist1 = new BonusIntakeList();
                            $monthstart = date('Y-m', strtotime($startdate[$key])) . '-01';
                            $lastday = date('Y-m', strtotime($enddate[$key]));
                            $last_day_this_month = date('t', strtotime($lastday));
                            $monthend = $lastday . '-' . $last_day_this_month;

                            $bonusintakelist1->bonus_intake_id = $model->bonus_intake_id;
                            $bonusintakelist1->clientID = $clientID;
                            $bonusintakelist1->month_start = $monthstart;
                            $bonusintakelist1->month_end = $monthend;
                            $bonusintakelist1->list_target = $bonus_intake_target[$key];

                            if (($flag = $bonusintakelist1->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                }

                if ($flag) {
                    $transaction->commit();
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success!', 'text' => 'Your action has been submitted.']);
                    return $this->redirect(['index']);
                }else {
                    $transaction->rollBack();
                    \Yii::$app->getSession()->setFlash('danger', ['title' => 'Fail!', 'text' => 'Your action has not been submitted.']);
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'bonusintakelist' => $bonusintakelist
            ]);
        }
    }

    /**
     * Updates an existing BonusIntake model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $model = $this->findModel($id);

        $bonusintakelist = BonusIntakeList::find()->where(['clientID' => $clientID,'bonus_intake_id' => $id])->all();

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            $bonus_intake_list_id = $data['bonus_intake_list_id'];
            $bonus_intake_target = $data['bonus_intake_target'];

            foreach ($bonus_intake_list_id as $key => $n) {
                $id = $bonus_intake_list_id[$key];
                $intakelist = $this->findModellist($id);
                $intakelist->list_target = $bonus_intake_target[$key];
                $intakelist->save(false);
                print_r($intakelist->getErrors());
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->bonus_intake_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'bonusintakelist' => $bonusintakelist
            ]);
        }
    }

    /**
     * Deletes an existing BonusIntake model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionType($syear,$smonth,$stype)
    {
        $res = '';
        $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
        $start = '';
        // SOME TEST DATES FOR THE FISCAL YEARS
        $fys = ['FEDERAL'  => $months[$smonth]];

        $y = $syear;
        foreach ($fys as $key => $start)
            {
                $start = $y.' '.$start;
                // THE END OF THE YEAR
                $nextyear           = date('Y-M', strtotime($start . ' + 12 Months'));

                // THE FIRST DAY OF THE QUARTER
                if($stype == '3') {
                    $res["q1"]["startdate"] = date('Y-M', strtotime($start));
                    $res["q2"]["startdate"] = date('Y-M', strtotime($start . ' + 3 Months'));
                    $res["q3"]["startdate"] = date('Y-M', strtotime($start . ' + 6 Months'));
                    $res["q4"]["startdate"] = date('Y-M', strtotime($start . ' + 9 Months'));

                    // THE LAST DAY OF THE QUARTER
                    $res["q1"]["enddate"] = date('Y-M', strtotime($res["q2"]["startdate"] . ' - 1 Day'));
                    $res["q2"]["enddate"] = date('Y-M', strtotime($res["q3"]["startdate"] . ' - 1 Day'));
                    $res["q3"]["enddate"] = date('Y-M', strtotime($res["q4"]["startdate"] . ' - 1 Day'));
                    $res["q4"]["enddate"] = date('Y-M', strtotime($nextyear           . ' - 1 Day'));
                }else if($stype == '2') {
                    $res["q1"]["startdate"] = date('Y-M', strtotime($start));
                    $res["q2"]["startdate"] = date('Y-M', strtotime($start . ' + 6 Months'));
                    // THE LAST DAY OF THE QUARTER
                    $res["q1"]["enddate"] = date('Y-M', strtotime($res["q2"]["startdate"] . ' - 1 Day'));
                    $res["q2"]["enddate"] = date('Y-M', strtotime($nextyear           . ' - 1 Day'));
                }else {
                    $res["q1"]["startdate"] = date('Y-M', strtotime($start));
                    $res["q1"]["enddate"] = date('Y-M', strtotime($nextyear           . ' - 1 Day'));
                }
                // SAVE THESE ELEMENTS
                $new[$key] = $res;
            }

        $output = json_encode($res);
        echo $output;
        //echo json_encode($response);
    }

    /**
     * Finds the BonusIntake model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BonusIntake the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BonusIntake::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModellist($id)
    {
        if (($model = BonusIntakeList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
