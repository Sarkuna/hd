<?php

namespace app\modules\catalog\controllers;

use Yii;
use common\models\VIPSubCategories;
use common\models\VIPSubCategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubCategoriesController implements the CRUD actions for VIPSubCategories model.
 */
class SubCategoriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPSubCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPSubCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPSubCategories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPSubCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPSubCategories();
        $model->position = '0';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified sub categories!']);
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->vip_sub_categories_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPSubCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified sub categories!']);
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->vip_sub_categories_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VIPSubCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionLists($id)
    {
        $countPosts = VIPSubCategories::find()
                ->where(['parent_id' => $id])
                ->count();
 
        $posts = VIPSubCategories::find()
                ->where(['parent_id' => $id])
                ->orderBy('name ASC')
                ->all();
 
        if($countPosts>0){
            //$lisit = "<option value='0'>All</option>";
            foreach($posts as $post){
                //$lisit .= "<option value='".$post->vip_sub_categories_id."'>".$post->name."</option>";
                $count = \common\models\VIPProduct::find()
                ->where(['category_id' => $post->parent_id,'sub_category_id' => $post->vip_sub_categories_id])
                ->count();
                echo "<option value='".$post->vip_sub_categories_id."'>".$post->name." ($count)</option>";
            }
            //echo $lisit;
        }
        else{
            echo "<option>-</option>";
        }
 
    }

    /**
     * Finds the VIPSubCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPSubCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPSubCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
