<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */

$this->title = 'Update Categor: ' . $model->name;
//$this->title = 'Categories';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->vip_categories_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="vipcategories-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>

