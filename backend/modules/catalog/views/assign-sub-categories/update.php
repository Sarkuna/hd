<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignSubCategories */

$this->title = 'Update Vipassign Sub Categories: ' . $model->vip_assign_sub_categories_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Sub Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_assign_sub_categories_id, 'url' => ['view', 'id' => $model->vip_assign_sub_categories_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipassign-sub-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
