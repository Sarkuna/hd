<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignCategories */

$this->title = 'Update Vipassign Categories: ' . $model->vip_assign_categories_id;
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vip_assign_categories_id, 'url' => ['view', 'id' => $model->vip_assign_categories_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vipassign-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
