<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPAssignCategories */

$this->title = 'Create Vipassign Categories';
$this->params['breadcrumbs'][] = ['label' => 'Vipassign Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipassign-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
