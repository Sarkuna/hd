<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOptionDescriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Options';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
    </div><!-- /.box-header -->

    <div class="box-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'option_id',
                //'language_id',
                'name',
                //['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}', //{view} {delete}
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return (Html::a('<i class="far fa-edit"></i>', $url, ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Edit'),]));
                        }
                            /* ,
                              'view' => function ($url, $model) {
                              return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                              },
                              'delete' => function ($url, $model) {
                              return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                              } */
                            ],
                        //'visible' => $visible,
                        ],
                    ],
                ]);
                ?>
    </div>
</div>
