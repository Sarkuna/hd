<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPOptionDescription */

$this->title = 'Add Options';
$this->params['breadcrumbs'][] = ['label' => 'Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>

    <?=
    $this->render('_form', [
        'model' => $model,
        //'modelsOptionvalue' => $modelsOptionvalue
        //'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new common\models\VIPOptionValueDescription()] : $modelsOptionvalue            
        'modelsOptionvalue' => $modelsOptionvalue
    ])
    ?>

</div>

