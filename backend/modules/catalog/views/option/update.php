<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOptionDescription */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Options', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->option_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="option-update">

        <?=
        $this->render('_form', [
            'model' => $model,
            //'modelsOptionvalue' => $modelsOptionvalue
            'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new common\models\VIPOptionValueDescription()] : $modelsOptionvalue
        ])
        ?>

    </div>
</div>
