<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = 'Add Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary vipproduct-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>            
    </div><!-- /.box-header -->


    <div class="vipproduct-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'initialPreview' => [],
            'initialPreviewConfig' => [],
            'deliveryzone' => (empty($deliveryzone)) ? [new VIPProductDeliveryZone] : $deliveryzone,
            'modelsOptionvalue' => (empty($modelsOptionvalue)) ? [new VIPProductOptionValue] : $modelsOptionvalue,
        ])
        ?>

    </div>
</div>

