<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    div#general img {max-width: 100%;}
    
</style>
<div class="box box-primary vipproduct-view">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <h3 class="pull-right"><?= $model->getStatus() ?></h3>
        <p style="margin-top:20px;">
            <?= Html::a('Update', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->product_id], [
                'class' => 'btn btn-danger hide',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
            
        </p>

    </div>
</div>


<div class="row">
    <div class="col-md-3">
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['class' => 'thumbnail img-responsive', 'alt' => 'Logo', 'title' => 'Logo']) : null ?>
                <strong><?= $model->getAttributeLabel('manufacturer_id') ?></strong>

                <p class="text-muted">
                    <?= $model->manufacturer_id === null ? 'N\A' : $model->manufacturer->name ?>
                </p>

                <hr>

                <strong><?= $model->getAttributeLabel('category_id') ?></strong>

                <p class="text-muted">
                    <?= $model->categories->name ?>
                </p>

                <hr>

                <strong><?= $model->getAttributeLabel('sub_category_id') ?></strong>

                <p class="text-muted">
                    <?= $model->subCategories->name ?>
                </p>

                <hr>
                <strong><?= $model->getAttributeLabel('product_code') ?></strong>
                <p class="text-muted">
                    <?= $model->product_code ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('price') ?></strong>
                <p class="text-muted">
                    <?= Yii::$app->formatter->asDecimal($model->price) ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('msrp') ?></strong>
                <p class="text-muted">
                    <?= Yii::$app->formatter->asDecimal($model->msrp) ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('quantity') ?></strong>
                <p class="text-muted">
                    <?= $model->quantity ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('minimum') ?></strong>
                <p class="text-muted">
                    <?= $model->minimum ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('stock_status_id') ?></strong>
                <p class="text-muted">
                    <?= $model->stockStatus->name ?>
                </p>
                <hr>
                <strong><?= $model->getAttributeLabel('date_available') ?></strong>
                <p class="text-muted">
                    <?= date('d-m-Y', strtotime($model->date_available)) ?>
                </p>
                
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


    <div class="vipproduct-form col-md-9">
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#general" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General</a></li>
                    <li role="presentation" class=""><a href="#tab_images" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Images</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="general">
                        <strong><?= $model->getAttributeLabel('product_name') ?></strong>
                        <p class="text-muted">
                            <?= Html::encode($model->product_name) ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('links') ?></strong>
                        <p class="text-muted">
                            <?= $model->links == '' ? 'N\A' : $model->links ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('meta_tag_title') ?></strong>
                        <p class="text-muted">
                            <?= $model->meta_tag_title ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('variant') ?></strong>
                        <p class="text-muted">
                            <?= $model->variant == '' ? 'N\A' : $model->variant ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('remarks') ?></strong>
                        <p class="text-muted">
                            <?= $model->remarks == '' ? 'N\A' : $model->remarks ?>
                        </p>
                        <hr>
                        <strong><?= $model->getAttributeLabel('product_description') ?></strong><br>
                        <?= $model->product_description == '' ? 'N\A' : $model->product_description ?>
                    </div>


                    <div class="tab-pane" id="tab_images">
                        <div class="row">
                            <div class="col-lg-12 gallery">
                                <?= dosamigos\gallery\Gallery::widget(['items' => $model->getThumbnails($model->ref, Html::encode($model->product_code))]); ?>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

    </div>
</div>

