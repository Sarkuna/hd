<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\DatePicker;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;
use kartik\money\MaskMoney;

use common\models\VIPManufacturer;
use common\models\VIPCategories;
use common\models\VIPSubCategories;
use common\models\VIPStockStatus;



/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .kv-file-content img {width: 200px !important;}
</style>

<div class="vipproduct-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipproduct-form','class' => 'form-horizontal form-label-left','enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>
    
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#general" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General</a></li>
        <li role="presentation" class=""><a href="#tab_data" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Data</a></li>
        <li role="presentation" class=""><a href="#tab_links" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Links</a></li>
        <li role="presentation" class=""><a href="#tab_delivery" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Delivery</a></li>
        <li role="presentation" class="hide"><a href="#tab_option" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Option</a></li>
        <li role="presentation" class=""><a href="#tab_images" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Images</a></li>
        <li role="presentation" class=""><a href="#tab_cover" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Cover</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="general">    
              <?= $form->field($model, 'product_name', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->textInput(array('placeholder' => ''));  ?>

              <?= $form->field($model, 'product_description', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-10 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->widget(CKEditor::className(), [
                      'options' => ['rows' => 6],
                      'preset' => 'full'
                  ]);  ?>


              <?= $form->field($model, 'meta_tag_title', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->textInput(array('placeholder' => ''));  ?>
            
            <?= $form->field($model, 'featured_status', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->radioList(array('N'=>'No', 'Y'=>'Yes')); ?>
            
            
            
        </div>

        <div class="tab-pane" id="tab_data">     
            <?=
            $form->field($model, 'product_code', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            <?=
            $form->field($model, 'suppliers_product_code', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            
            <?=
            $form->field($model, 'price', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(MaskMoney::className(), [
                //'mask' => '9',
                //'clientOptions' => ['repeat' => 10, 'greedy' => false],
                'pluginOptions' => [
                    'prefix' => 'RM ',
                    'thousands' => ',',
                    'decimal' => '.',
                    'precision' => 2
                ],
            ]);
            ?>
            
            <?=
            $form->field($model, 'msrp', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(MaskMoney::className(), [
                //'mask' => '9',
                //'clientOptions' => ['repeat' => 10, 'greedy' => false],
                'pluginOptions' => [
                    'prefix' => 'RM ',
                    'thousands' => ',',
                    'decimal' => '.',
                    'precision' => 2
                ],
            ]);
            ?>

            <?=
            $form->field($model, 'quantity', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 3, 'greedy' => false],
            ]);
            ?>
            	
            <?=
            $form->field($model, 'variant', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>    
            <?=
            $form->field($model, 'minimum', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 3, 'greedy' => false],
            ]);
            ?>
            
            <?php
                $stockstatus = VIPStockStatus::find()->all();
                $stockstatusData = ArrayHelper::map($stockstatus, 'stock_status_id', 'name');
                //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                echo $form->field($model, 'stock_status_id', [
                            'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                                $stockstatusData, ['prompt' => '--', 'id' => 'relation1']
                );
            ?>
            <?=
                        $form->field($model, 'date_available',[
                            'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])->widget(DatePicker::classname(), [                            
                            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => false,
                                'todayBtn' => false,
                                'format' => 'yyyy-mm-dd',
                            //'startDate' => $startDate,
                            ],
                        ]);
                        ?>  
            <?=
            $form->field($model, 'remarks', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textarea(['rows' => '6']);
            ?> 
            <?php
            $statusData = ['E' => 'Enabled', 'D' => 'Disabled'];
//echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'status', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $statusData, ['prompt' => '--', 'id' => 'status']
            );
            ?>  
            
        </div>
        
        <div class="tab-pane" id="tab_links">     
            <?php
            $manufacturer = VIPManufacturer::find()->all();
            $manufacturerData = ArrayHelper::map($manufacturer, 'manufacturer_id', 'name');
            ?>
            <?=
            $form->field($model, 'manufacturer_id', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
            ])->widget(Select2::classname(), [
                'data' => $manufacturerData,
                'options' => ['placeholder' => 'Select...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?> 

            <?php
            $categories = VIPCategories::find()->all();
            $categoriesData = ArrayHelper::map($categories, 'vip_categories_id', 'name');
            //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'category_id', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $categoriesData, [
                        'prompt' => '--',
                        'id' => 'categories_id',
                        'onchange' => '
                                    $.get( "' . Url::toRoute('/catalog/sub-categories/lists') . '", { id: $(this).val() } )
                                        .done(function( data ) {
                                            $( "select#vip_sub_categories_id" ).html( data );
                                        }
                                    );
                                '
                            ]
            );
            ?>
            

            <?php
            $subcategories = VIPSubCategories::find()->all();
            $subcategoriesData = ArrayHelper::map($subcategories, 'vip_sub_categories_id', 'name');
            //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'sub_category_id', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $subcategoriesData, ['prompt' => '--', 'id' => 'vip_sub_categories_id']
            );
            ?>
            <?=
            $form->field($model, 'links', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
        </div>
        
        <div class="tab-pane" id="tab_delivery">
            <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6 table-responsive">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-zone', // required: css class selector
                    'widgetItem' => '.zone-item', // required: css class
                    'limit' => 20,
                    'min' => 0,
                    'insertButton' => '.add-zone', // css class
                    'deleteButton' => '.del-zone', // css class
                    'model' => $deliveryzone[0],
                    'formId' => 'vipproduct-form',
                    'formFields' => [
                        'product_option_value_id',
                        'zone_id',                        
                        'price'
                    ],
                ]); ?>
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Zone</th>
                            <th>Price</th>
                            <th>
                                <button type="button" class="add-zone btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody class="container-zone">
                    <?php 

                    foreach ($deliveryzone as $z => $modelzone): ?>
                        <tr class="zone-item filters">
                            <td class="vcenter">
                                <?php
                                    // necessary for update action.
                                    if (! $modelzone->isNewRecord) {
                                        echo Html::activeHiddenInput($modelzone, "[{$z}]product_option_value_id");
                                    }
                                    //->where(['option_id' => $optionID])
                                   $zones = common\models\VIPZone::find()->all();
                                   $zonesData = ArrayHelper::map($zones, 'zone_id', 'name');
                                   echo $form->field($modelzone, "[{$z}]zone_id")->widget(Select2::classname(), [
                                    'data' => $zonesData,
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label(false);
                                ?>
                            </td>
                            <td class="vcenter">
                                    <?=
                                    $form->field($modelzone, "[{$z}]price")->widget(MaskMoney::className(), [
                                        'pluginOptions' => [
                                            'prefix' => 'RM ',
                                            'thousands' => ',',
                                            'decimal' => '.',
                                            'precision' => 2
                                        ],
                                    ])->label(false);
                                    ?>
                            </td>
                            
                            <td class="text-center vcenter" style="width: 90px;">
                                <button type="button" class="del-zone btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
                            </td>
                        </tr>
                     <?php endforeach; ?>
                    </tbody>
                    
                </table>

                <?php DynamicFormWidget::end(); // end of payment widget ?>
                    </div>
            </div>
            </div>
        </div>

        <div class="tab-pane hide" id="tab_option">
            <div class="row">
            <div class="col-xs-12 table-responsive">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-options', // required: css class selector
                    'widgetItem' => '.option-item', // required: css class
                    'limit' => 20,
                    'min' => 0,
                    'insertButton' => '.add-option', // css class
                    'deleteButton' => '.del-option', // css class
                    'model' => $modelsOptionvalue[0],
                    'formId' => 'vipproduct-form',
                    'formFields' => [
                        'product_option_value_id',
                        'option_id',                        
                        'option_value_id',
                        'quantity','subtract','price_prefix','price','points_prefix','points'
                    ],
                ]); ?>
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="18%">Option Value</th>
                            <th width="15%">Quantity</th>
                            <th width="12%">Subtract Stock</th>
                            <th width="10%">Price</th>
                            <th width="10%">Points</th>
                            <th class="text-center" width="5%">
                                <button type="button" class="add-option btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody class="container-options">
                    <?php 

                    foreach ($modelsOptionvalue as $ix => $modelLoads): ?>
                        <tr class="option-item filters">
                            <td class="vcenter">
                                <?php
                                    // necessary for update action.
                                    if (! $modelLoads->isNewRecord) {
                                        echo Html::activeHiddenInput($modelLoads, "[{$ix}]product_option_value_id");
                                    }
                                    //->where(['option_id' => $optionID])
                                   $optcolour = common\models\VIPOptionValueDescription::find()->all();
                                   $optcolourData = ArrayHelper::map($optcolour, 'option_value_id', 'name');
                                   //echo $form->field($modelLoads, "[{$ix}]option_value_id")->label(false)->dropDownList(common\models\VIPProduct::getHierarchy(), ['prompt' => 'Select...']); 
                                   echo $form->field($modelLoads, "[{$ix}]option_value_id")->widget(Select2::classname(), [
                                    'data' => common\models\VIPProduct::getHierarchy(),
                                    //'language' => 'de',
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label(false);
                                ?>
                            </td>
                            <td class="vcenter">
                                <?= $form->field($modelLoads, "[{$ix}]quantity")->label(false)->textInput(['maxlength' => true]) ?>
                            </td>
                            <td class="vcenter">
                                <?= $form->field($modelLoads, "[{$ix}]subtract")->label(false)->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Select...']) ?>
                            </td>
                            <td class="vcenter">
                                <?= $form->field($modelLoads, "[{$ix}]price_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-'], ['prompt' => 'Select...']) ?>
                                <?= $form->field($modelLoads, "[{$ix}]price")->label(false)->textInput(['maxlength' => true]) ?>
                            </td>
                            <td class="vcenter">
                                <?= $form->field($modelLoads, "[{$ix}]points_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-'], ['prompt' => 'Select...']) ?>
                                <?= $form->field($modelLoads, "[{$ix}]points")->label(false)->textInput(['maxlength' => true]) ?>
                            </td>
                            <td class="text-center vcenter" style="width: 90px;">
                                <button type="button" class="del-option btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
                            </td>
                        </tr>
                     <?php endforeach; ?>
                    </tbody>
                    
                </table>

                <?php DynamicFormWidget::end(); // end of payment widget ?>
            </div>
            </div>
        </div>
        
        <div class="tab-pane" id="tab_images">    
            <div class="form-group field-upload_files">
                <div class="col-md-2 col-sm-3 col-xs-12 text-right"><label class="control-label" for="upload_files[]"> Photos </label></div>
                <div class="col-md-10 col-sm-6 col-xs-12">
                    <?=
                    FileInput::widget([
                        'name' => 'upload_ajax[]',
                        'options' => ['multiple' => true, 'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                        'pluginOptions' => [
                            'showPreview' => true,
                            'overwriteInitial' => false,
                            'initialPreviewShowDelete' => true,
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => $initialPreview,
                            //'initialPreview'=> $initialPreview[$model->ref],
                            //'initialPreviewConfig'=>$model->initialPreview($model->docs,'docs','config'),
                            'initialPreviewConfig' => $initialPreviewConfig,
                            'uploadUrl' => Url::to(['/catalog/products/uploadajax']),
                            'uploadExtraData' => [
                                'ref' => $model->ref,
                            ],
                            'maxFileCount' => 10
                        ]
                    ]);
                    ?>
                </div>    
            </div>
        </div>
        
        <div class="tab-pane" id="tab_cover">
            <div class="row">
                <div class="col-lg-offset-2 col-lg-10 col-sm-6 col-xs-12">
                    <?php
                    echo $form->field($model, 'main_image1')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => [
                                //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                                $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : null, // checks the models to display the preview
                            ],
                        ]
                    ])->label(false);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
    <div class="box-footer">
        <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10 col-md-12 col-sm-6 col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-save"></i>' : '<i class="fa fa-save"></i>', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
            <?= \yii\helpers\Html::a( '<i class="fa fa-reply"></i>', Yii::$app->request->referrer, ['class' => 'btn btn-default']); ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    $.ajaxSetup({
        data: <?= \yii\helpers\Json::encode([
            \Yii::$app->request->csrfParam => \Yii::$app->request->csrfToken,
        ]) ?>
    });
</script>
<?php
    $script = <<<EOD
            $('#vipproduct-price123').change(function() {
                var point = $(this).val();
                var newpoint = point / 0.01;
                $("#vipproduct-points_value").val(newpoint);
            });
            $('#vipproduct-product_name').change(function() {
                var a = this.value.toLowerCase();
                var currVal = a.replace(/ /g, "_");
                $("#vipproduct-meta_tag_title").val(currVal);
             });
            

EOD;
$this->registerJs($script);