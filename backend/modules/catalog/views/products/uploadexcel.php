<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Bulk Product Upload Excel';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel">
    <header class="panel-heading sm" data-color="theme-inverse">
        <h2><?= Html::encode($this->title) ?></h2>

    </header>
    <div class="panel-body">
        <div class="user-upload-csv gradient-widget">
            <p>Please upload Excel file:</p>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                    <?= $form->field($model, 'excel')->fileInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

