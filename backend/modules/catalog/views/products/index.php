<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success pull-right', 'title' => 'Add New']) ?>
    </div><!-- /.box-header -->

    <div class="box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'vip_categories_id',
                [
                    'attribute' => 'image',
                    'label' => 'Image',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $url = $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['alt' => 'myImage', 'width' => '70'], ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : '<i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
                        //$url = \Yii::getAlias('@backend/web/upload/product_cover/') . $model->main_image;
                        //return Html::img($url, ['alt' => 'myImage', 'width' => '70', 'height' => '50']);
                        return $url;
                    }
                        ],
                        'product_name',
                        'product_code',
                        //'price',
                        [
                            'attribute' => 'price',
                            'label' => 'Price RM',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->price);
                            },
                        ],        
                        'quantity',
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->getStatus();
                            },
                            'options' => ['width' => '100'],
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                            'filter' => ["E" => "Enabled", "D" => "Disabled "],
                        ],        
                        //'description:ntext',
                        //'image',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'options' => ['width' => '110'],
                            'template' => '{update} {view}', // {delete}
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return (Html::a('<i class="fa fa-edit"></i>', $url, ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Edit'),]));
                                },
                                'view' => function ($url, $model) {
                                    return (Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info', 'title' => Yii::t('app', 'View'),]));
                                }        
                                    /* ,
                                      'view' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                      },
                                      'delete' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                                      } */
                                    ],
                                //'visible' => $visible,
                                ],
                            ],
                        ]);
                        ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->

