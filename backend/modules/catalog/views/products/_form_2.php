<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;

use common\models\VIPManufacturer;
use common\models\VIPCategories;
use common\models\VIPSubCategories;
use common\models\VIPStockStatus;

use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="vipproduct-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipproduct-form','class' => 'form-horizontal form-label-left','enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>
    <div class="form-group">
        <div class="col-md-12 col-sm-6 col-xs-12 text-right">
            <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-save"></i>' : '<i class="fa fa-save"></i>', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
            <?= \yii\helpers\Html::a( '<i class="fa fa-reply"></i>', Yii::$app->request->referrer, ['class' => 'btn btn-default']); ?>
        </div>
    </div>
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General</a></li>
        <li role="presentation" class=""><a href="#tab_data" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Data</a></li>
        <li role="presentation" class=""><a href="#tab_links" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Links</a></li>
        <li role="presentation" class=""><a href="#tab_option" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Option</a></li>
        <li role="presentation" class=""><a href="#tab_images" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Images</a></li>
        <li role="presentation" class=""><a href="#tab_cover" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Cover</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="general" aria-labelledby="home-tab">
              <?= $form->field($model, 'product_name', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->textInput(array('placeholder' => ''));  ?>

              <?= $form->field($model, 'product_description', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->widget(CKEditor::className(), [
                      'options' => ['rows' => 6],
                      'preset' => 'full'
                  ]);  ?>


              <?= $form->field($model, 'meta_tag_title', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->textInput(array('placeholder' => ''));  ?>
        </div>
        
        <div role="tabpanel" class="tab-pane fade" id="tab_data" aria-labelledby="profile-tab">
            <?=
            $form->field($model, 'product_code', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            <?=
            $form->field($model, 'suppliers_product_code', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            
            <?=
            $form->field($model, 'price', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            <?=
            $form->field($model, 'points_value', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            <?=
            $form->field($model, 'quantity', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            
            <?=
            $form->field($model, 'minimum', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            
            <?php
                $stockstatus = VIPStockStatus::find()->all();
                $stockstatusData = ArrayHelper::map($stockstatus, 'stock_status_id', 'name');
                //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
                echo $form->field($model, 'stock_status_id', [
                            'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                                $stockstatusData, ['prompt' => '--', 'id' => 'relation1']
                );
            ?>
            <div class="form-group field-vipproduct-date_available">
                <div class="col-md-2 col-sm-3 col-xs-12 text-right"><label class="control-label" for="vipproduct-date_available">Date Available</label></div>
                <div class="col-md-7 col-sm-6 col-xs-12">
                        <?=
                        $form->field($model, 'date_available')->widget(DatePicker::classname(), [
                            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => false,
                                'todayBtn' => false,
                                'format' => 'yyyy-mm-dd',
                            //'startDate' => $startDate,
                            ],
                        ])->label(false);
                        ?>  
                </div>
            </div>
            <?php
            $statusData = ['E' => 'Enabled', 'D' => 'Disabled'];
//echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'status', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $statusData, ['prompt' => '--', 'id' => 'status']
            );
            ?>  
            
        </div>
        
        <div role="tabpanel" class="tab-pane fade" id="tab_links" aria-labelledby="profile-tab">
            <?php
            $manufacturer = VIPManufacturer::find()->all();
            $manufacturerData = ArrayHelper::map($manufacturer, 'manufacturer_id', 'name');
            //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'manufacturer_id', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $manufacturerData, ['prompt' => '--', 'id' => 'relation1']
            );
            ?>
    
        
    <?php
    $categories = VIPCategories::find()->all();
    $categoriesData = ArrayHelper::map($categories, 'vip_categories_id', 'name');
    //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
    echo $form->field($model, 'category_id', [
                            'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                            $categoriesData,          
                            [
                                'prompt'=>'--',
                                'id' => 'categories_id',
                                'onchange'=>'
                                    $.get( "'.Url::toRoute('/catalog/sub-categories/lists').'", { id: $(this).val() } )
                                        .done(function( data ) {
                                            $( "select#vip_sub_categories_id" ).html( data );
                                        }
                                    );
                                '
                            ]    
                        );
    ?>
    
    <?php
    $subcategories = VIPSubCategories::find()->all();
    $subcategoriesData = ArrayHelper::map($subcategories, 'vip_sub_categories_id', 'name');
    //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
    echo $form->field($model, 'sub_category_id', [
                            'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                        ])
                        ->dropDownList(
                            $subcategoriesData,          
                            ['prompt'=>'--', 'id' => 'vip_sub_categories_id']    
                        );
    ?>
        </div>
        
        <div role="tabpanel" class="tab-pane fade tab_option" id="tab_option" aria-labelledby="profile-tab">
            <div class="col-xs-3">
                <ul class="nav nav-tabs tabs-left tbopt">
                    <?php
                    //$arraySize = count($taboptions);
                    //for($i=0;$i<$arraySize;$i++){
                    foreach($taboptions as $i => $taboption){
                        if($i == 0){
                            $active = 'active';
                        }else{
                            $active = '';
                        }
                        echo '<li class="'.$active.'"><a href="#option'.$i.'" data-toggle="tab">'.$taboption['name'].'</a></li>';
                    }
                    ?>
                    <!--<li class="active"><a href="#colour" data-toggle="tab">Colour</a></li>
                    <li><a href="#delivery" data-toggle="tab">Delivery</a></li>-->
                </ul>
            </div>
            <div class="col-xs-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    <?php
                    $a = 0;
                    foreach($taboptions as $i => $taboption){
                        if($i == 0){
                            $active = 'active';
                        }else{
                            $active = '';
                        }
                        echo '<div class="tab-pane '.$active.'" id="option'.$i.'">';
                        $optionID = $taboption['option_id'];
                        DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_inner', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-loads', // required: css class selector
                            'widgetItem' => '.load-item', // required: css class
                            'insertButton' => '.add-load', // css class
                            'deleteButton' => '.del-load', // css class
                            'model' => $modelsOptionvalue[$i][0],
                            'formId' => 'vipproduct-form',
                            'formFields' => [
                                'product_option_value_id',
                                'option_value_id',
                                'quantity',
                                'subtract',
                                'price_prefix',
                                'price',
                                'points_prefix',
                                'points',
                                'option_id'
                                
                            ],
                        ]);
                        echo '<table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="18%">Option Value</th>
                                    <th width="15%">Quantity</th>
                                    <th width="12%">Subtract Stock</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Points</th>
                                    <th class="text-center" width="5%">
                                        <button type="button" class="add-load btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="container-loads">';
                            foreach ($modelsOptionvalue[$i] as $ix => $modelLoads){
                                echo '<tr class="load-item"><td class="vcenter">';                                
                                if (!$modelLoads->isNewRecord) {
                                    echo Html::activeHiddenInput($modelLoads, "[{$i}][{$ix}]product_option_value_id");
                                }
                                $optcolour = common\models\VIPOptionValueDescription::find()->where(['option_id' => $optionID])->all();
                                $optcolourData = ArrayHelper::map($optcolour, 'option_value_id', 'name');

                                echo $form->field($modelLoads, "[{$i}][{$ix}]option_value_id")->label(false)->dropDownList($optcolourData, ['prompt' => 'Select...']);
                                echo '</td>';
                                echo '<td class="vcenter">'.$form->field($modelLoads, "[{$i}][{$ix}]quantity")->label(false)->textInput(['maxlength' => true]).'</td>
                                        <td class="vcenter">
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]subtract")->label(false)->dropDownList(['1' => 'Yes', '0' => 'No']).'
                                        </td>

                                        <td class="vcenter">
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]price_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-']).'
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]price")->label(false)->textInput(['maxlength' => true]).'</td>
                                        <td class="vcenter">
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]points_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-']).'
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]points")->label(false)->textInput(['maxlength' => true]).'</td>

                                        <td class="text-center vcenter" style="width: 90px; verti">
                                            <button type="button" class="del-load btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                                            '.$form->field($modelLoads, "[{$i}][{$ix}]option_id")->hiddenInput(['value'=> $optionID])->label(false).'
                                        </td></tr>';
                            }
                        echo '</tbody>
                        </table>';
                        DynamicFormWidget::end();
                        echo '</div>';
                    }
                    ?>

                                                
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div role="tabpanel" class="tab-pane fade" id="tab_images" aria-labelledby="profile-tab">
            <div class="form-group field-upload_files">
                <div class="col-md-2 col-sm-3 col-xs-12 text-right"><label class="control-label" for="upload_files[]"> Photos </label></div>
                <div class="col-md-10 col-sm-6 col-xs-12">
                    <?=
                    FileInput::widget([
                        'name' => 'upload_ajax[]',
                        'options' => ['multiple' => true, 'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                        'pluginOptions' => [
                            'showPreview' => true,
                            'overwriteInitial' => false,
                            'initialPreviewShowDelete' => true,
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => $initialPreview,
                            //'initialPreview'=> $initialPreview[$model->ref],
                            //'initialPreviewConfig'=>$model->initialPreview($model->docs,'docs','config'),
                            'initialPreviewConfig' => $initialPreviewConfig,
                            'uploadUrl' => Url::to(['/catalog/products/uploadajax']),
                            'uploadExtraData' => [
                                'ref' => $model->ref,
                            ],
                            'maxFileCount' => 10
                        ]
                    ]);
                    ?>
                </div>    
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_cover" aria-labelledby="profile-tab">
        <?php
            echo $form->field($model, 'main_image1')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                    'initialPreview' => [
                        //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                        $model->main_image ? Html::img('/upload/product_cover/'.$model->main_image, ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : null, // checks the models to display the preview
                    ],
                ]
            ]);
            ?>
        </div>
        
    </div>
</div>
   

  







 
          

    <?php ActiveForm::end(); ?>

</div>
