<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPSubCategories */

$this->title = 'Update Sub Categor: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sub Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->vip_sub_categories_id]];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>  
    <div class="vipsub-categories-update">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
 
