<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);


return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['site/login'],
            //'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'assetManager' => [
            'bundles' => [
                'wbraganca\dynamicform\DynamicFormAsset' => [
                    'sourcePath' => '@app/web/themes/adminLTE/custom/js',
                    'js' => [
                        'yii2-dynamic-form.js'
                    ],
                ],
            ],
        ],
        /*'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],*/
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/adminLTE'],
                'baseUrl' => '@app/themes/adminLTE',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'vip' => [
            'class' => 'app\components\VIP',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'as beforeRequest' => [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Admin',
        ],
        'settings' => [
            'class' => 'app\modules\settings\Settings',
        ],
        'bonus' => [
            'class' => 'app\modules\bonus\Bonus',
        ],
        'catalog' => [
            'class' => 'app\modules\catalog\Catalog',
        ],
        'clients' => [
            'class' => 'app\modules\clients\Clients',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Profile',
        ],
        'sales' => [
            'class' => 'app\modules\sales\Sales',
        ],
        'receipt' => [
            'class' => 'app\modules\receipt\Receipt',
        ],
        'reports' => [
            'class' => 'app\modules\reports\Reports',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ],
        'rights' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // it can be '@path/to/your/layout'.
	    'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id', // id field of model User
		    'usernameField' => 'username', // username field of model User
                ],
            ],
        ],
    ],
    
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'rights/*',
            //'some-controller/some-action',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
    
    'controllerMap' => [
        //$domain = Yii::$app->request->hostInfo;
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            //'autoload' => true,
            'root' => [
                
                //"driver" => "LocalFileSystem",
                'baseUrl'=> $_SERVER['REQUEST_SCHEME'] . '://'.$_SERVER['SERVER_NAME'],
                //'baseUrl'=>'@web', //org
                'basePath'=>'@webroot',
                //'baseUrl'=>'@webroot',
                //'basePath'=>'http://localhost/ss2/frontend/web/', //full path to backend webroot
                'path' => '/upload/files/',
                'name' => 'My Documents'
            ]
        ],
    ],
    'params' => $params,        
];
