<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

class LatestOrdersWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = 16;
        
        $query = VIPOrder::find();
        $query->andWhere(['=','clientID', $clientID]); 


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10, 
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_status_id' => SORT_ASC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('latestorderswidget',array('orderlisits'=>$dataProvider));
        
    }
}