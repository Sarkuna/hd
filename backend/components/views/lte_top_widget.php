<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\components\ProfileNameWidget;

$profile_pic = \common\models\ProfilePic::find()->where([
    'userID' => $userID = \Yii::$app->user->id,
])->count();

if($profile_pic == 1) {
    $profile_pic = \common\models\ProfilePic::find()->where([
        'userID' => $userID = \Yii::$app->user->id,
    ])->one();
    $img = '<img src="/upload/profiles/'.$profile_pic->file_name.'" class="user-image" alt="User Image">';
    $img2 = '<img src="/upload/profiles/'.$profile_pic->file_name.'" class="user-image" alt="User Image">';
}else {
    $img = '<i class="fa fa-user-circle" aria-hidden="true"></i>';
    $img2 = '<i class="fa fa-user-circle fa-5x" aria-hidden="true"></i>';
}

?>

<header class="main-header">
    <!-- Logo -->
    <a href="<?= Yii::$app->homeUrl ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><?= $clientname ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?= $clientname ?></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <i class="fas fa-bars"></i>
            <span class="sr-only">Toggle navigation</span>
        </a>
        <span class="logo1 hide"><?= $clientname ?></span>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?= $img ?>
                        
                        <span class="hidden-xs"><?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?= $img2 ?>

                            <p>
                                <?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body hide">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/site/change-password" class="btn btn-default btn-flat">Change Password</a>
                            </div>
                            <div class="pull-right">
                                <a href="/site/logout" data-method="post" class="btn btn-default btn-flat">Log Out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <?php
                if ($session['currentusertype'] == 'A') {
                echo '<li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-random"></i></a>
                </li>';
                }
                ?>
            </ul>
        </div>

    </nav>
</header>
