<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$orderlisits = $orderlisits->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($orderlisits);

?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Registration</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date Added</th>
                        <th>Membership #</th>
                        <th>Name</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                   
                    <?php
                    if ($count > 0) {
                        foreach ($orderlisits as $order) {
                            $ahrf = '<a href="sales/orders/view?id='.$order->vip_customer_id.'">'.$order->clients_ref_no.'</a>';
                            echo '<tr>'; 
                            echo '<td>' . date('d-m-Y', strtotime($order->created_datetime)). '</td>';
                            echo '<td>' . $order->clients_ref_no. '</td>';
                            echo '<td>' . $order->full_name. '</td>';
                            echo '<td>' . $order->user->getStatustext(). '</td>';
                            //echo '<td>' . $ahrf. '</td>';
                            //echo '<td>' . $order->customer->full_name . '</td>';
                            //echo '<td class="text-right"><span class="label label-'.$order->orderStatus->labelbg.'">'.$order->orderStatus->name.'</span></td>';
                            //echo '<td width="100">' . date('d-m-Y', strtotime($order->created_datetime)) . '</td>';
                            echo '</tr>';
                        }
                    }else {
                        echo "<tr><td colspan='5'><div class='empty'>No results found.</div></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/sales/customers" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div>
    <!-- /.box-footer -->
</div>
          <!-- /.box -->