<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

use app\components\ProfileNameWidget;
use common\models\AssignModules;

$session = Yii::$app->session;
$user = \common\models\User::find()
        ->where(['id' => \Yii::$app->user->id])
        ->one();
$session['currentusertype'] = $user->user_type;
$clientID = 16;

if(!empty($clientID)){
    $clientname = 'Name Show Here';
}else{
    $clientname = 'VIP Admin';
}
//$customer_notverified = 0;
//$customer_pending = 0;
$customer_notverified = \common\models\User::find()->where([
    'status' => 'P',
    'email_verification' => "N",
])->count();

$customer_pending = \common\models\User::find()->where([
    'status' => 'P',
    'email_verification' => "Y",
])->count();

$profile_pic = \common\models\ProfilePic::find()->where([
    'userID' => $userID = \Yii::$app->user->id,
])->count();

if($profile_pic == 1) {
    $profile_pic = \common\models\ProfilePic::find()->where([
        'userID' => $userID = \Yii::$app->user->id,
    ])->one();
    //$img = '/upload/profiles/'.$profile_pic->file_name;
    $img = '<img src="/upload/profiles/'.$profile_pic->file_name.'" class="img-circle" alt="User Image">';
}else {
    $img = '<i class="fa fa-user-circle fa-3x text-primary" aria-hidden="true"></i>';
}


$mod1 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '2',])->count();
$mod2 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '1',])->count();
$mod3 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '3',])->count();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?= $img ?>
        </div>
        <div class="pull-left info">
          <p><?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php
            $myurl = Yii::$app->request->url;
            $menuItems[] = '';
            $productlist = '';
            $receipts_summary = '';
            $home = ['label' => '<i class="fa fa-home"></i> <span>Dashboard</span></i>', 'url' => ['/site']];

            //if(!empty($clientID)){
                
                $models = \common\models\TypeName::find()
                        //->where(['clientID' => $clientID])
                        ->asArray()
                        ->all();

                $items = [];
                foreach ($models as $m) {
                    $items[] = [
                        'label' => '<i class="fa fa-star fa-fw"></i> <span>'.$m['name'].'</span>', 
                        'url' => ['/sales/points', 'type' => $m['tier_id']],
                        'active' => $myurl == '/sales/points?type='.$m['tier_id'],
                        //'visible' => !Yii::$app -> user -> isGuest
                    ];
                }
                
                $items = array_merge($items,[['label' => '<i class="fa fa-file-upload fa-fw"></i> <span>Import Point</span>', 'url' => ['/sales/point-upload'], 'active' => $myurl == '/sales/point-upload']],[['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Fail Points</span>', 'url' => ['/sales/point-upload-summary'], 'active' => $myurl == '/sales/point-upload-summary']],[['label' => '<i class="fa fa-trash fa-fw"></i> <span>Trash</span>', 'url' => ['/sales/point-upload-summary/trash'], 'active' => $myurl == '/sales/point-upload-summary/trash']]);
                
                //['label' => '<i class="fa fa-star fa-fw"></i> <span>Point Summary</span>', 'url' => ['/sales/points'], 'active' => $myurl == '/sales/points'],
            //}

    if ($session['currentusertype'] == 'B') {
        $menuItems = [
            $home,
            ['label' => '<i class="fa fa-users fa-fw"></i> <span>View My Users</span></i>', 'url' => ['/clients/manage-my-ids'], 'active' => $myurl == '/clients/manage-my-ids'],
            //['label' => '<i class="fa fa-briefcase fa-fw"></i> <span>Manage Projects</span></i>', 'url' => ['/clients/projects'], 'active' => $myurl == '/clients/projects'],
            //['label' => '<i class="fa fa-users fa-fw"></i> <span>View My Customers</span></i>', 'url' => ['/clients/manage-my-customers'], 'active' => $myurl == '/clients/manage-my-customers'],
            ['label' => '<i class="fa fa-briefcase fa-fw"></i> <span>Manage Projects</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-file fa-fw"></i> <span>New</span>', 'url' => ['/clients/projects/create'], 'active' => $myurl == '/clients/projects/create'],
                    ['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Pending</span><span class="pull-right-container"><span class="label label-warning pull-right">' . $project_pending . '</span></span>', 'url' => ['/clients/projects/index'], 'active' => $myurl == '/clients/projects/index'],
                    ['label' => '<i class="fa fa-check fa-fw"></i> <span>Approved</span>', 'url' => ['/clients/projects/approve'], 'active' => $myurl == '/clients/projects/approve'],
                    ['label' => '<i class="fa fa-ban fa-fw"></i> <span>Declined</span>', 'url' => ['/clients/projects/decline'], 'active' => $myurl == '/clients/projects/decline'],
                ],
            ],
        ];
    } else {
        if ($mod1 > 0) {
            $receiptsList = ['label' => '<i class="fa fa-receipt fa-fw"></i> <span>Receipts List</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-search fa-fw"></i> <span>Search</span>', 'url' => ['/receipt/receipt/search'], 'active' => $myurl == '/receipt/receipt/search'],
                    ['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Pending</span><span class="pull-right-container"><span class="label label-warning pull-right">' . $receipts_list_pending . '</span></span>', 'url' => ['/receipt/receipt'], 'active' => $myurl == '/receipt/receipt'],
                    ['label' => '<i class="fa fa-file fa-fw"></i> <span>Drafts</span><span class="pull-right-container"><span class="label label-default pull-right">' . $receipts_list_drafts . '</span></span>', 'url' => ['/receipt/receipt/drafts'], 'active' => $myurl == '/receipt/receipt/drafts'],
                    ['label' => '<i class="fa fa-hourglass-half fa-fw"></i> <span>Processing</span><span class="pull-right-container"><span class="label label-info pull-right">' . $receipts_list_processing . '</span></span>', 'url' => ['/receipt/receipt/processing'], 'active' => $myurl == '/receipt/receipt/processing'],
                    ['label' => '<i class="fa fa-check fa-fw"></i> <span>Approve</span>', 'url' => ['/receipt/receipt/approves'], 'active' => $myurl == '/receipt/receipt/approves'],
                    ['label' => '<i class="fa fa-ban fa-fw"></i> <span>Decline</span>', 'url' => ['/receipt/receipt/decline'], 'active' => $myurl == '/receipt/receipt/decline'],
                ],
            ];
            $productlist = ['label' => '<i class="fas fa-tasks fa-fw"></i> <span>Product Lists</span>', 'url' => ['/clients/product-list'], 'active' => $myurl == '/clients/product-list'];
            $receipts_summary = ['label' => '<i class="far fa-circle text-aqua"></i> <span>Receipts Summary</span></i>', 'url' => ['/reports/report/receipts-summary'], 'active' => $myurl == '/reports/report/receipts-summary'];
        } else {
            $receiptsList = '';
        }

        if ($mod2 > 0) {
            $projects = ['label' => '<i class="fa fa-briefcase fa-fw"></i> <span>Manage Projects</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Pending</span><span class="pull-right-container"><span class="label label-warning pull-right">' . $project_pending . '</span></span>', 'url' => ['/receipt/projects'], 'active' => $myurl == '/receipt/projects'],
                    ['label' => '<i class="fa fa-check fa-fw"></i> <span>Approved</span>', 'url' => ['/receipt/projects/approve'], 'active' => $myurl == '/receipt/projects/approve'],
                    ['label' => '<i class="fa fa-ban fa-fw"></i> <span>Declined</span>', 'url' => ['/receipt/projects/decline'], 'active' => $myurl == '/receipt/projects/decline'],
                ],
            ];
        } else {
            $projects = '';
        }

        if ($mod3 > 0) {
            $manage_dealers = ['label' => '<i class="fas fa-user-tie fa-fw"></i></i> <span>Manage Dealers</span>', 'url' => ['/clients/dealers/index'], 'active' => $myurl == '/clients/dealers/index'];
        } else {
            $manage_dealers = '';
        }
        $menuItems = [
            $home,
            ['label' => '<i class="fa fa-users fa-fw"></i> <span>Customers</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-search fa-fw"></i> <span>Search</span>', 'url' => ['/sales/customers/search'], 'active' => $myurl == '/sales/customers/search'],
                    //['label' => '<i class="fa fa-plus fa-fw"></i> <span>New</span>', 'url' => ['/sales/customers/create'], 'active' => $myurl == '/sales/customers/create'],
                    ['label' => '<i class="fa fa-question fa-fw"></i> <span>Not verified</span><span class="pull-right-container"><span class="label label-danger pull-right">' . $customer_notverified . '</span></span>', 'url' => ['/sales/customers/email-not-verified'], 'active' => $myurl == '/sales/customers/email-not-verified'],
                    ['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Pending</span><span class="pull-right-container"><span class="label label-warning pull-right">' . $customer_pending . '</span></span>', 'url' => ['/sales/customers'], 'active' => $myurl == '/sales/customers'],
                    ['label' => '<i class="fa fa-check fa-fw"></i> <span>Approve</span>', 'url' => ['/sales/customers/approve'], 'active' => $myurl == '/sales/customers/approve'],
                    ['label' => '<i class="fa fa-ban fa-fw"></i> <span>Not Active</span>', 'url' => ['/sales/customers/deactive'], 'active' => $myurl == '/sales/customers/deactive'],
                    ['label' => '<i class="fa fa-trash-alt fa-fw"></i> <span>Trash</span>', 'url' => ['/sales/customers/trash'], 'active' => $myurl == '/sales/customers/trash'],
                    ['label' => '<i class="fa fa-file-upload fa-fw"></i> <span>Import Database</span>', 'url' => ['/sales/import-customer-excel/'], 'active' => $myurl == '/sales/import-customer-excel/'],
                ],
            ],
            ['label' => '<i class="fa fa-star fa-fw"></i> <span>Points</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => $items,
            /* 'items' => [
              ['label' => '<i class="fa fa-star fa-fw"></i> <span>Upload Summary</span>', 'url' => ['/sales/point-upload'], 'active' => $myurl == '/sales/point-upload'],
              ], */
            ],
            //['label' => '<i class="fa fa-shopping-cart fa-fw"></i> <span>Orders</span></i>', 'url' => ['/sales/orders'], 'active' => $myurl == '/sales/orders'],
            ['label' => '<i class="fa fa-shopping-cart fa-fw"></i> <span>Orders</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-exclamation-circle fa-fw"></i> <span>Accepted</span><span class="pull-right-container"><span class="label label-warning pull-right">' . $orders_pending . '</span></span>', 'url' => ['/sales/orders'], 'active' => $myurl == '/sales/orders'],
                    ['label' => '<i class="fa fa-diagnoses fa-fw"></i> <span>Processing</span><span class="pull-right-container"><span class="label label-primary pull-right">' . $orders_processing . '</span></span>', 'url' => ['/sales/orders/processing'], 'active' => $myurl == '/sales/orders/processing'],
                    ['label' => '<i class="fa fa-truck"></i> <span>Shipped</span><span class="pull-right-container"><span class="label label-success pull-right">' . $orders_shipped . '</span></span>', 'url' => ['/sales/orders/shipped'], 'active' => $myurl == '/sales/orders/shipped'],
                    ['label' => '<i class="fa fa-handshake fa-fw"></i> <span>Complete</span>', 'url' => ['/sales/orders/complete'], 'active' => $myurl == '/sales/orders/complete'],
                    ['label' => '<i class="fa fa-hand-holding-usd fa-fw"></i> <span>Refunded</span>', 'url' => ['/sales/orders/refunded'], 'active' => $myurl == '/sales/orders/refunded'],
                    ['label' => '<i class="fa fa-ban fa-fw"></i> <span>Cancelled</span>', 'url' => ['/sales/orders/cancelled'], 'active' => $myurl == '/sales/orders/cancelled'],
                ],
            ],
            ['label' => '<i class="fas fa-project-diagram fa-fw"></i> <span>Assign Products</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fas fa-folder-open fa-fw"></i> <span>Categories</span>', 'url' => ['/clients/assign/categories'], 'active' => $myurl == '/clients/assign/categories'],
                    ['label' => '<i class="fas fa-folder-open fa-fw"></i> <span>Sub Categories</span>', 'url' => ['/clients/assign/sub-categories'], 'active' => $myurl == '/clients/assign/sub-categories'],
                    ['label' => '<i class="fas fa-box-open fa-fw"></i> <span>Products</span>', 'url' => ['/clients/assign/index'], 'active' => $myurl == '/clients/assign/index'],
                ],
            ],
            $receiptsList,
            $projects,
            //['label' => '<i class="fa fa-gift fa-fw"></i> <span>Bonus</span></i>', 'url' => ['/bonus/bonus-intake'], 'active' => $myurl == '/bonus/bonus-intake'],
            ['label' => '<i class="fas fa-clipboard-list fa-fw"></i> <span>Reports</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="far fa-circle text-aqua"></i> <span>Customer Account</span></i>', 'url' => ['/reports/report/customer-account-summary'], 'active' => $myurl == '/reports/report/customer-account-summary'],
                    ['label' => '<i class="far fa-circle text-aqua"></i> <span>Points Summary</span></i>', 'url' => ['/reports/report/points-summary'], 'active' => $myurl == '/reports/report/points-summary'],
                    ['label' => '<i class="far fa-circle text-aqua"></i> <span>Orders Summary</span></i>', 'url' => ['/reports/report/orders-summary'], 'active' => $myurl == '/reports/report/orders-summary'],
                    //['label' => '<i class="far fa-circle text-aqua"></i> <span>Receipt List Summary</span></i>', 'url' => ['/reports/report/receipts'], 'active' => $myurl == '/reports/report/receipts'],
                    $receipts_summary,
                    ['label' => '<i class="far fa-circle text-aqua"></i> <span>Detailed Point Summary</span></i>', 'url' => ['/reports/report/detailed-point-summary'], 'active' => $myurl == '/reports/report/detailed-point-summary'],
                //['label' => '<i class="far fa-circle text-aqua"></i> <span>SMS Summary</span></i>', 'url' => ['/reports/report/sms-summary'], 'active' => $myurl == '/reports/report/sms-summary'],
                ],
            ],
            ['label' => '<i class="fas fa-brush fa-fw"></i> <span>Content Management</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => '<i class="fa fa-copy fa-fw"></i> <span>Pages</span></i>', 'url' => ['/admin/pages'], 'active' => $myurl == '/admin/pages'],
                    ['label' => '<i class="fa fa-bars fa-fw"></i> <span>Menus</span>', 'url' => ['/admin/menus'], 'active' => $myurl == '/admin/menus'],
                ],
            ],
            ['label' => '<i class="fas fa-cogs fa-fw"></i> <span>Settings</span>',
                'url' => ['#'],
                'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    $productlist,
                    //['label' => '<i class="fas fa-tasks fa-fw"></i> <span>Product Lists</span>', 'url' => ['/clients/product-list'], 'active' => $myurl == '/clients/product-list'],
                    //['label' => '<i class="fa fa-copy fa-fw"></i> <span>Pages</span></i>', 'url' => ['/admin/pages'], 'active' => $myurl == '/admin/pages'],
                    //['label' => '<i class="fa fa-bullhorn fa-fw"></i> <span>Sub Categories</span>', 'url' => ['/admin/announcement'], 'active' => $myurl == '/admin/announcement'],
                    ['label' => '<i class="fa fa-user-secret fa-fw"></i> <span>Admin User</span>', 'url' => ['/clients/users'], 'active' => $myurl == '/clients/users'],
                    //['label' => '<i class="fa fa-images fa-fw"></i> <span>Slider Images</span>', 'url' => ['/admin/pages/banners'], 'active' => $myurl == '/admin/pages/banners'],
                    ['label' => '<i class="fa fa-images fa-fw"></i> <span>Logo</span>', 'url' => ['/settings/logo'], 'active' => $myurl == '/settings/logo'],
                    $manage_dealers,
                    ['label' => '<i class="fas fa-envelope fa-fw"></i> <span>Email Template</span>', 'url' => ['/admin/email-template'], 'active' => $myurl == '/admin/email-template'],
                    //['label' => '<i class="fas fa-envelope fa-fw"></i> <span>SMS Template</span>', 'url' => ['/admin/sms-template'], 'active' => $myurl == '/admin/sms-template'],
                ],
            ],
        ];
    }
?>
      
      
      <?php
            echo Menu::widget([
                'options' => ['class' => 'sidebar-menu treeview'],
                'items' => $menuItems,
                'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
                'encodeLabels' => false, //allows you to use html in labels
                'activateParents' => true,]);
        ?>
    </section>
    <!-- /.sidebar -->
  </aside>