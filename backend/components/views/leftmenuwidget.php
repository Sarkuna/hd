<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\components\ProfileNameWidget;
$clientID = $session['currentclientID'];

if(!empty($clientID)){
    $clientname = 'Name Show Here';
}else{
    $clientname = 'VIP Admin';
}
?>
<div class="left_col scroll-view">

    <div class="navbar nav_title" style="border: 0;">
        <a href="<?= Yii::$app->homeUrl ?>" class="site_title"> <span><?= $clientname ?></span></a>
    </div>
    <div class="clearfix"></div>

    <!-- menu prile quick info -->
    <div class="profile">
        <div class="profile_pic">
            <img src="/upload/profiles/img.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2><?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?></h2>
        </div>
    </div>
    <!-- /menu prile quick info -->

    <br />
    <div class="clear"></div>
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

        <div class="menu_section">

            <ul class="nav side-menu">
                
                <li><a href="<?= Yii::$app->homeUrl ?>"><i class="fa fa-home"></i> Home </a></li>
                <?php if($session['currentusertype'] == 'A'){ ?>
                <li><a href="/admin/clients"><i class="fa fa-briefcase"></i> Client Management </a></li>                
                <li><a><i class="fa fa-tags fa-fw"></i> Catalog <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                        <li><a href="/catalog/products">Products</a></li>
                        <li><a href="/catalog/categories">Categories</a></li>
                        <li><a href="/catalog/sub-categories">Sub Categories</a></li>
                        <li><a href="/catalog/manufacturers">Manufacturers</a></li>
                        <li><a href="/catalog/option">Options</a></li>
                    </ul>
                </li>
                <?php } ?>
                <?php if(!empty($session['currentclientID'])){ ?>
                <li><a><i class="fa fa-gear"></i> System <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                        <li><a href="/clients/users/">User Management</a></li>
                        <li><a href="/clients/assign/categories">Assign Categories</a></li>
                        <li><a href="/clients/assign/sub-categories">Assign Sub Categories</a></li>
                        <li><a href="/clients/assign/products">Assign Products</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-shopping-cart fa-fw"></i> Sales <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                        <li><a href="/sales/customers">Customers</a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->

</div>