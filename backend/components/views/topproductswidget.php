<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$productlisits = $productlisits->getModels();
//echo '<pre>'.print_r($productlisits).die;
$count = count($productlisits);

?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Top 5 Products Redeemed</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <ul class="products-list product-list-in-box">
            <?php
            foreach ($productlisits as $product){
                echo '<li class="item">
                <div class="product-img">
                    <img src="/upload/product_cover/thumbnail/'.$product->product->main_image.'" alt="Product Image">                    
                </div>
                <div class="product-info">
                    <a href="/clients/assign/product-view?id='.$product->vip_assign_products_id.'" class="product-title">'.$product->product->product_name.'
                        <span class="label label-warning pull-right">'.$product->product->points_value.'pts</span></a>
                </div>
            </li>';
            }
            ?>
            
            <!-- /.item -->
        </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <a href="/clients/assign/products" class="uppercase">View All Products</a>
    </div>
    <!-- /.box-footer -->
</div>
