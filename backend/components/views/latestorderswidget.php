<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$orderlisits = $orderlisits->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($orderlisits);

?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Orders</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date Added</th>
                        <th>Order#</th>
                        <th>Name</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    <?php
                    if ($count > 0) {
                        foreach ($orderlisits as $order) {
                            $ahrf = '<a href="sales/orders/view?id='.$order->order_id.'">'.$order->invoice_prefix.'</a>';
                            echo '<tr>'; 
                            echo '<td width="100">' . date('d-m-Y', strtotime($order->created_datetime)) . '</td>';
                            echo '<td>' . $ahrf. '</td>';
                            echo '<td>' . $order->customer->full_name . '</td>';
                            echo '<td class="text-right"><span class="label label-'.$order->orderStatus->labelbg.'">'.$order->orderStatus->name.'</span></td>';
                            
                            echo '</tr>';
                        }
                    }else {
                        echo "<tr><td colspan='5'><div class='empty'>No results found.</div></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/sales/orders" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
    </div>
    <!-- /.box-footer -->
</div>
          <!-- /.box -->