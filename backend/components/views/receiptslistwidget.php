<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$receiptlists = $receiptlists->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($receiptlists);

?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Receipts List</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date Added</th>
                        <th>Order#</th>
                        <th>Name</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    <?php
                    if ($count > 0) {
                        foreach ($receiptlists as $receiptlist) {
                            $ahrf = '<a href="/receipt/receipt/view?id='.$receiptlist->vip_receipt_id.'">'.$receiptlist->order_num.'</a>';
                            echo '<tr>'; 
                            //echo '<td>' . $order->storename->company. '</td>';
                            echo '<td>' . date('d-m-Y', strtotime($receiptlist->created_datetime)) . '</td>';
                            echo '<td>' . $ahrf. '</td>';
                            echo '<td>' . $receiptlist->customer->full_name . '</td>';
                            echo '<td class="text-right">'.$receiptlist->getStatustext().'</td>';
                            
                            echo '</tr>';
                        }
                    }else {
                        echo "<tr><td colspan='5'><div class='empty'>No results found.</div></td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <?php
        if ($count > 0) {
            echo '<a href="/receipt/receipt/processing" class="btn btn-sm btn-default btn-flat pull-right">View All</a>';
        }
        ?>
    </div>
    <!-- /.box-footer -->
</div>
          <!-- /.box -->