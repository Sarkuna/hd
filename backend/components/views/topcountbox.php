<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use app\modules\painter\models\PainterProfile;
    use common\models\PointOrder;
    use common\models\Redemption;
    
    $payout = \common\models\Redemption::find()->where(['redemption_status' => '19'])->sum('req_amount');
    $payoutrequest = \common\models\Redemption::find()->where(['redemption_status' => '1'])->sum('req_amount');
    
    $pending = PainterProfile::find()->where(['profile_status' => 'P'])->count();
    $review = PainterProfile::find()->where(['profile_status' => 'R'])->count();
    $approve = PainterProfile::find()->where(['profile_status' => 'A'])->count();
    $decline = PainterProfile::find()->where(['profile_status' => 'D'])->count();
    
    //$allorder =
    $opending = PointOrder::find()->where(['order_status' => '1'])->count();
    $ocancel = PointOrder::find()->where(['order_status' => '7'])->count();
    $oapprove = PointOrder::find()->where(['order_status' => '17'])->count();
    
    //Points Redemption
    $pr_approve = Redemption::find()->where(['redemption_status' => '17'])->count();
    $pr_cancel = Redemption::find()->where(['redemption_status' => '7'])->count();
    $pr_pending = Redemption::find()->where(['redemption_status' => '1'])->count();
    //Pay Out
    $po_total = Redemption::find()->where(['redemption_status_ray' => '19'])->count();
    $po_rm = Redemption::find()->where(['redemption_status_ray' => '19'])->sum('req_amount');
    $po_point = Redemption::find()->where(['redemption_status_ray' => '19'])->sum('req_points');
?>


<div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $approve ?></h3>
                    <h4>Registered Painter</h4>
                    <span class="progress-description">Pending <span class="pull-right"><?= $pending ?></span></span>
                    <span class="progress-description">Review <span class="pull-right"><?= $review ?></span></span>
                    <span class="progress-description">Decline <span class="pull-right"><?= $decline ?></span></span>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="/painter/painterprofile/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $oapprove ?></h3>
                    <h4>Transactions to Date</h4>
                    <span class="progress-description">Pending <span class="pull-right"><?= $opending ?></span></span>
                    <span class="progress-description">Cancel <span class="pull-right"><?= $ocancel ?></span></span>
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-rub"></i>
                </div>
                <a href="/management/pointorder" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $pr_approve ?></h3>
                    <h4>Redemptions to Date</h4>
                    <span class="progress-description">Pending <span class="pull-right"><?= $pr_pending ?></span></span>
                    <!--<span class="progress-description">Cancel <span class="pull-right"><?= $pr_cancel ?></span></span>-->
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-mail-forward"></i>
                </div>
                <a href="/management/redemption" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-light-blue">
                <div class="inner">
                    <h3><?= $po_total ?></h3>
                    <h4>Total Pay Out To Date</h4>
                    <span class="progress-description">Total RM Pay out <span class="pull-right">RM<?= $po_rm ?></span></span>
                    <span class="progress-description">Total Points Pay out <span class="pull-right"><?= $po_point ?></span></span>
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
                <a href="/management/redemption" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
    </div>
    