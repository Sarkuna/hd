<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use common\models\VIPOrder;
use common\models\VIPReceipt;
use common\models\Projects;
//use yii\helpers\Html;

class LTE_LeftMenuWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
       
        $receipts_list_pending = $receipts_list_processing = 0;

        
        $receipts_list_pending = VIPReceipt::find()->where(['status' => 'P'])->count();
        $receipts_list_processing = VIPReceipt::find()->where(['status' => 'N'])->count();
        $receipts_list_drafts = VIPReceipt::find()->where(['status' => 'R'])->count();
        
        $orders_pending = VIPOrder::find()->where(['order_status_id' => 10])->count();
        $orders_processing = VIPOrder::find()->where(['order_status_id' => 20])->count();
        $orders_shipped = VIPOrder::find()->where(['order_status_id' => 30])->count();
        $project_pending = Projects::find()->where(['admin_status' => 'P', 'dealerID' => Yii::$app->user->id])->count();
        
        return $this->render('lte_leftmenuwidget',
        [
            'receipts_list_pending' => $receipts_list_pending,
            'receipts_list_processing' => $receipts_list_processing,
            'receipts_list_drafts' => $receipts_list_drafts,
            'orders_pending' => $orders_pending,
            'orders_processing' => $orders_processing,
            'orders_shipped' => $orders_shipped,
            'project_pending' => $project_pending,  
        ]);
        
    }
}