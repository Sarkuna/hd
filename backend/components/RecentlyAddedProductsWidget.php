<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use common\models\VIPProduct;
use common\models\VIPAssignProducts;

class RecentlyAddedProductsWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = 16;
        $query = VIPAssignProducts::find();
        $query->joinWith(['product']);
        //$query = \common\models\VIPAssignProducts::find();
        $query->andWhere(['=','clientID', $clientID]);
        $query->andWhere(['=','status', 'E']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    //'order_status_id' => SORT_ASC
                    'vip_assign_products_id' => SORT_DESC,
                    //'vip_assign_products_id' => SORT_ASC
                ]
            ],
            'pagination' => [
                    'pageSize' => 5,
               ],
        ]);
        
        return $this->render('recentlyaddedproductswidget',array('productlisits'=>$dataProvider));
        
    }
}