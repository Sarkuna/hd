<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class LTE_Top_Widget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = 16;

        if(!empty($clientID)){
            $company = \common\models\Client::findOne($clientID);
            $companyname = $company->company_short_name;
        }else{
            $companyname = 'VIP Admin';
        }
        
        return $this->render('lte_top_widget',['clientname' => $companyname]);
        
    }
}