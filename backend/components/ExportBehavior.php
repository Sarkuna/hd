<?php
namespace app\components;

use yii\base\Behavior;
use yii\helpers\Url;
//use app\components\ExportBehavior;

class ExportBehavior extends Behavior
{
    public $prop1;
    private $_prop2;
 
    public function getProp2()
    {
        return $this->_prop2;
    }
 
    public function setProp2($value)
    {
        $this->_prop2 = $value;
    }
 
    public function export2Excel($excel_content, $excel_file
        , $excel_props = array('creator' => 'WWSP Tool'
        , 'title' => 'WWSP_Tracking EXPORT EXCEL'
        , 'subject' => 'WWSP_Tracking EXPORT EXCEL'
        , 'desc' => 'WWSP_Tracking EXPORT EXCEL'
        , 'keywords' => 'WWSP Tool Generated Excel'
        , 'category' => 'WWSP_Tracking EXPORT EXCEL'))
    {
        if (!is_array($excel_content)) {
            return FALSE;
        }
        if (empty($excel_file)) {
            return FALSE;
        }
        $excelName = ExportExcelUtil::save2Excel($excel_content, $excel_file, $excel_props);
        if ($excelName) {
            return $this->owner->redirect([Url::to('download'), "file_name" => 'temp/' . basename($excelName)
                , "file_type" => 'excel'
                , 'deleteAfterDownload' => true]);
        }
    }
 
}

