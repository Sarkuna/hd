<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomer;

class IndirectWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = 16;
        
        $query = VIPCustomer::find();
        $query->joinWith(['user']);
        
        $query->andWhere(['=','clientID', $clientID]); 
        //$query->andWhere(['=','kis_category_id', '4']);
        $query->andWhere(['=','user.status', 'P']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10, 
            ],
            'sort' => [
                'defaultOrder' => [
                    'vip_customer_id' => SORT_ASC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('indirectwidget',array('orderlisits'=>$dataProvider));
        
    }
}