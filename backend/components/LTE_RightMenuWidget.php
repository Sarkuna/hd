<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class LTE_RightMenuWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        
        $searchModel = new \common\models\ClientSearch();
        $searchModel->client_status = 'A';
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('lte_rightmenuwidget',['dataProvider'=>$dataProvider]);
        
    }
}