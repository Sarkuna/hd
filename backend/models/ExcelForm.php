<?php
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ExcelForm extends Model
{
    public $excel;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['excel', 'required'],
            //['excel', 'file'],
            [['excel'], 'file', 'extensions' => 'xls, xlsx'],
        ];
    }

    
}
