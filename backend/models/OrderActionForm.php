<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class OrderActionForm extends Model
{
    public $order_status_id;
    public $comment;
    public $notify;
    public $bb_invoice_no;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            [['order_status_id', 'comment'], 'required'],
            ['order_status_id', 'integer'],
            ['comment', 'string', 'max' => 500],
            [['bb_invoice_no','notify'], 'safe'],

            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
        
    public function attributeLabels()
    {
        return [
            'order_status_id' => 'Status',
            'comment' => 'Comment',
            'notify' => 'Email notify to customer',
            'bb_invoice_no' => 'Invoice Number',
        ];
    }
}