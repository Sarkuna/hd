<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirmpassword;
    public $usergroup;
    public $status;
    public $fname;
    public $lname;
    public $client_id;
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],*/
            
            ['fname', 'required'],
            ['fname', 'string', 'min' => 1, 'max' => 50],
            //['fname', 'match', 'pattern' => '/^[a-zA-Z]+$/','message'=>'Your first name can only contain alphabetic characters.'],
            
            ['lname', 'required'],
            ['lname', 'string', 'min' => 1, 'max' => 50],
            //['lname', 'match', 'pattern' => '/^[a-zA-Z]+$/','message'=>'Your last name can only contain alphabetic characters.'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'uniqueEmail'],
            //[['email'], 'unique', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['email' => 'email']],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email has already been taken.','targetWhere' => ['=','client_id', 6]],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email has already been taken.','filter' => ['=','client_id' ,'6']],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email has already been taken.', 'targetAttribute' => ['=','client_id' ,'6']],
            //[['email'], 'unique', 'targetAttribute' => ['client_id']],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password', 'string', 'max' => 12],

            ['confirmpassword', 'required'],
            ['confirmpassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
            ['status', 'required'],
            //['password', 'string', 'min' => 4],
            //['password', 'string', 'max' => 20],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
    
    public function attributeLabels()
    {
        return [
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'password' => 'Password',
            'confirmpassword' => 'Confirm',
            'status' => 'Status',
        ];
    }
    
    public function uniqueEmail($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        if(
            $user = User::find()->where(['email'=>$this->email, 'client_id' => $client_id, 'user_type' => 'C'])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Email "'.$this->email.'" has already been taken.');
    }
}
