<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class LogoForm extends Model
{
    public $file_image;
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            ['file_image', 'image', 'extensions' => 'png, jpg',
                'minWidth' => 100, 'maxWidth' => 500,
                'minHeight' => 100, 'maxHeight' => 500,
            ],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
        
    public function attributeLabels()
    {
        return [
            'fname' => 'Logo',
        ];
    }
}
