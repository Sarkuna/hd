<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    const STATUS_DEACTIVE = 'D';
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        /*$user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);*/
        
        $session = Yii::$app->session;
        $currentclientID = $session['currentclientID'];
        $email = $this->email;
        $returnValue = false;
        if(!empty($currentclientID)){
            $user = User::findOne(['client_id' => $currentclientID,'email' => $email, 'status' => self::STATUS_ACTIVE]);            
        }else{
            $user = User::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
        }

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        if ($user) {
            $userid = $user->id;
            $email = $user->email;
            $profile = \common\models\UserProfile::findOne([
                    'userID' => $userid,
                ]);
            $name = $profile->first_name.' '.$profile->last_name;
            //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
            $resetLink = Yii::$app->request->hostInfo.'/site/reset-password?token='.$user->password_reset_token;
            Yii::$app->vip->passwordResetEmail($email,$name,$resetLink);
            return true;
        }else{
           return false; 
        }

        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
}
