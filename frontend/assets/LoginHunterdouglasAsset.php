<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginHunterdouglasAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Open+Sans|Roboto',
        'themes/hunterdouglas/css/foundation.css',
        'themes/hunterdouglas/css/login.css',
    ];   
    public $js = [
        'themes/hunterdouglas/js/jquery.js',
        'themes/hunterdouglas/js/foundation.min.js',
        'themes/hunterdouglas/js/foundation.abide.js',
        'themes/hunterdouglas/js/foundation.offcanvas.js',
        'themes/hunterdouglas/js/foundation.orbit.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
