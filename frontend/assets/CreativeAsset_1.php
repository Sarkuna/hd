<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CreativeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700,200',
        'themes/creative/assets/css/bootstrap.min.css',
        'themes/creative/assets/css/now-ui-kit.css?v=1.1.0',
        'themes/creative/assets/css/custom.css'
    ];   
    public $js = [
        'themes/creative/assets/js/core/jquery.3.2.1.min.js',
        'themes/creative/assets/js/core/popper.min.js',
        'themes/creative/assets/js/core/bootstrap.min.js',
        //Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
        //'themes/creative/assets/js/plugins/bootstrap-switch.js',
        //Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        //'themes/creative/assets/js/plugins/nouislider.min.js',
        //Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
        //'themes/creative/assets/js/plugins/bootstrap-datepicker.js',
        //Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
        'themes/creative/assets/js/now-ui-kit.js?v=1.1.0',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
