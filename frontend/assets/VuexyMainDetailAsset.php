<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VuexyMainDetailAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600',
        // BEGIN: Vendor CSS-->
        'themes/vuexy/app-assets/vendors/css/vendors.min.css',
        'themes/vuexy/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css',
        'themes/vuexy/app-assets/vendors/css/extensions/toastr.css',
        // END: Vendor CSS-->

        // BEGIN: Theme CSS-->
        'themes/vuexy/app-assets/css/bootstrap.css',
        'themes/vuexy/app-assets/css/bootstrap-extended.css',
        'themes/vuexy/app-assets/css/colors.css',
        'themes/vuexy/app-assets/css/components.css',
        'themes/vuexy/app-assets/css/themes/dark-layout.css',
        'themes/vuexy/app-assets/css/themes/semi-dark-layout.css',

        // BEGIN: Page CSS-->
        'themes/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/vuexy/app-assets/css/core/colors/palette-gradient.css',
        'themes/vuexy/app-assets/css/pages/app-ecommerce-shop.css',
        'themes/vuexy/app-assets/css/plugins/forms/wizard.css',
        'themes/vuexy/app-assets/css/plugins/extensions/toastr.css',
        // END: Page CSS-->
        
        // BEGIN: Gallery CSS
        'themes/vuexy/app-assets/vendors/owl-carousel/owl.carousel.css',
        'themes/vuexy/app-assets/vendors/owl-carousel/owl.theme.css',
        // END: Gallery CSS
        
        // BEGIN: Custom CSS-->
        'themes/vuexy/assets/css/style.css',
        // END: Custom CSS-->
    ];
    public $js = [
        // BEGIN: Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/vendors.min.js',
        // BEGIN Vendor JS-->

        // BEGIN: Page Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js',
        'themes/vuexy/app-assets/vendors/js/extensions/jquery.steps.min.js',
        'themes/vuexy/app-assets/vendors/js/forms/validation/jquery.validate.min.js',
        'themes/vuexy/app-assets/vendors/js/extensions/toastr.min.js',
        // END: Page Vendor JS-->

        // BEGIN: Theme JS-->
        'themes/vuexy/app-assets/js/core/app-menu.js',
        'themes/vuexy/app-assets/js/core/app.js',
        'themes/vuexy/app-assets/js/scripts/components.js',
        // END: Theme JS-->
        
        // BEGIN: Gallery JS
        'themes/vuexy/app-assets/vendors/owl-carousel/owl.carousel.min.js',
        'themes/vuexy/assets/js/gallery.js', 
        // END: Gallery JS
        
        // BEGIN: CART JS IMPORTENT-->
        'themes/vuexy/assets/js/cart.js', 
        // END: CART JS-->
        
        // BEGIN: Page JS-->
        'themes/vuexy/app-assets/js/scripts/pages/app-ecommerce-shop.js',
        // END: Page JS-->
        
    ];
    /*public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];*/
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
