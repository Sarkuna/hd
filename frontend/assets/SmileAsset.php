<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SmileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        //<!-- Google Webfont -->
        '//fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900',
        '//fonts.googleapis.com/css?family=Lato:400,100,300,300italic,700,900',
        '//fonts.googleapis.com/css?family=Montserrat:400,700',

        //<!-- CSS -->
        'themes/smile/css/font-awesome/css/font-awesome.css',
        'themes/smile/css/bootstrap.min.css',
        'themes/smile/js/vendors/isotope/isotope.css',
        'themes/smile/js/vendors/slick/slick.css',
        'themes/smile/js/vendors/rs-plugin/css/settings.css',
        //'themes/smile/js/vendors/select/jquery.selectBoxIt.css',
        'themes/smile/css/subscribe-better.css',
        '//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css',
        
        'themes/smile/plugin/owl-carousel/owl.carousel.css',
        'themes/smile/plugin/owl-carousel/owl.theme.css',
        'themes/smile/plugin/prettyphoto/css/prettyPhoto.css',
        'themes/smile/css/style.css',
        //'themes/smile/css/custom_smile.css'
        'themes/smile/css/vip.css',
        'themes/smile/js/vendors/vmenu/styles.css',
        'themes/smile/css/shopping-cart.css',
        //'//adminlte.io/themes/AdminLTE/dist/css/AdminLTE.min.css'
    ];
    public $js = [
        //'themes/smile/js/jquery.js',
        '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        'themes/smile/js/bootstrap.min.js',
        'themes/smile/plugin/owl-carousel/owl.carousel.min.js',
        'themes/smile/plugin/prettyphoto/js/jquery.prettyPhoto.js',
        'themes/smile/js/bs-navbar.js',
        'themes/smile/js/vendors/isotope/isotope.pkgd.js',
        'themes/smile/js/vendors/slick/slick.min.js',
        'themes/smile/js/vendors/tweets/tweecool.min.js',
        'themes/smile/js/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'themes/smile/js/vendors/rs-plugin/js/jquery.themepunch.tools.min.js',
        'themes/smile/js/jquery.sticky.js',
        'themes/smile/js/jquery.subscribe-better.js',
        '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        'themes/smile/js/vendors/select/jquery.selectBoxIt.js',        
        'themes/smile/js/vendors/vmenu/script.js',
        'themes/smile/js/main.js',
        'themes/smile/js/cart.js'
        
    ];
    /*public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];*/
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
