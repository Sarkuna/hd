<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VuexyMainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600',
        // BEGIN: Vendor CSS-->
        'themes/vuexy/app-assets/vendors/css/vendors.min.css',
        'themes/vuexy/app-assets/vendors/css/extensions/nouislider.min.css',
        'themes/vuexy/app-assets/vendors/css/ui/prism.min.css',
        'themes/vuexy/app-assets/vendors/css/forms/select/select2.min.css',
        'https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/vendors/css/extensions/swiper.min.css',
        // END: Vendor CSS-->

        // BEGIN: Theme CSS-->
        'themes/vuexy/app-assets/css/bootstrap.css',
        'themes/vuexy/app-assets/css/bootstrap-extended.css',
        'themes/vuexy/app-assets/css/colors.css',
        'themes/vuexy/app-assets/css/components.css',
        'themes/vuexy/app-assets/css/themes/dark-layout.css',
        'themes/vuexy/app-assets/css/themes/semi-dark-layout.css',

        // BEGIN: Page CSS-->
        'themes/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/vuexy/app-assets/css/core/colors/palette-gradient.css',
        'themes/vuexy/app-assets/css/plugins/extensions/noui-slider.min.css',
        'themes/vuexy/app-assets/css/pages/app-ecommerce-shop.css',
        'themes/vuexy/app-assets/css/pages/app-user.css',
        'themes/vuexy/app-assets/css/plugins/forms/validation/form-validation.css',
        // END: Page CSS-->

        // BEGIN: Custom CSS-->
        'themes/vuexy/assets/css/style.css',
        // END: Custom CSS-->
    ];
    public $js = [
        // BEGIN: Vendor JS-->
        //'themes/vuexy/app-assets/vendors/js/vendors.min.js',
        ['themes/vuexy/app-assets/vendors/js/vendors.min.js', 'position' => \yii\web\View::POS_HEAD],
        // BEGIN Vendor JS-->

        // BEGIN: Page Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/ui/prism.min.js',
        'themes/vuexy/app-assets/vendors/js/extensions/wNumb.js',
        'themes/vuexy/app-assets/vendors/js/extensions/nouislider.min.js',
        'themes/vuexy/app-assets/vendors/js/forms/select/select2.full.min.js',
        // END: Page Vendor JS-->

        // BEGIN: Theme JS-->
        'themes/vuexy/app-assets/js/core/app-menu.js',
        'themes/vuexy/app-assets/js/core/app.js',
        'themes/vuexy/app-assets/js/scripts/components.js',
        // END: Theme JS-->

        // BEGIN: Page JS-->
        'themes/vuexy/app-assets/js/scripts/pages/app-ecommerce-shop.js',
        'themes/vuexy/app-assets/js/scripts/forms/validation/form-validation.js',
        'themes/vuexy/assets/js/cart.js'
        // END: Page JS-->
        
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
