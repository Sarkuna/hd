<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VuexyDashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600',
        //'//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        // BEGIN: Vendor CSS-->
        'themes/vuexy/app-assets/vendors/css/vendors.min.css',
        'themes/vuexy/app-assets/vendors/css/charts/apexcharts.css',
        'themes/vuexy/app-assets/vendors/css/extensions/tether-theme-arrows.css',
        'themes/vuexy/app-assets/vendors/css/extensions/tether.min.css',
        'themes/vuexy/app-assets/vendors/css/extensions/shepherd-theme-default.css',
        // END: Vendor CSS-->

        // BEGIN: Theme CSS-->
        'themes/vuexy/app-assets/css/bootstrap.css',
        'themes/vuexy/app-assets/css/bootstrap-extended.css',
        'themes/vuexy/app-assets/css/colors.css',
        'themes/vuexy/app-assets/css/components.css',
        'themes/vuexy/app-assets/css/themes/dark-layout.css',
        'themes/vuexy/app-assets/css/themes/semi-dark-layout.css',

        // BEGIN: Page CSS-->
        'themes/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/vuexy/app-assets/css/core/colors/palette-gradient.css',
        'themes/vuexy/app-assets/css/pages/dashboard-analytics.css',
        'themes/vuexy/app-assets/css/pages/card-analytics.css',
        //'themes/vuexy/app-assets/css/plugins/tour/tour.css',
        'themes/vuexy/app-assets/css/pages/app-ecommerce-shop.css',
        // END: Page CSS-->

        // BEGIN: Custom CSS-->
        'themes/vuexy/assets/css/style.css',
        // END: Custom CSS-->
    ];
    public $js = [
        //BEGIN: Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/vendors.min.js',
        //BEGIN Vendor JS-->

        //BEGIN: Page Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/charts/apexcharts.min.js',
        'themes/vuexy/app-assets/vendors/js/extensions/tether.min.js',
        //'themes/vuexy/app-assets/vendors/js/extensions/shepherd.min.js',
        //END: Page Vendor JS-->

        //BEGIN: Theme JS-->
        'themes/vuexy/app-assets/js/core/app-menu.js',
        'themes/vuexy/app-assets/js/core/app.js',
        'themes/vuexy/app-assets/js/scripts/components.js',
        //END: Theme JS-->

        //BEGIN: Page JS-->
        'themes/vuexy/app-assets/js/scripts/pages/dashboard-analytics.js',
        //END: Page JS-->
        
    ];
    /*public $jsOptions = [
    	'position' => \yii\web\View::POS_HEAD
    ];*/
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
