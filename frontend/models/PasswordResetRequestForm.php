<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $clientID;
    public $retrieve;
    public $mobile;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['retrieve', 'required'],
            /*['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],*/
            ['email', 'trim'],
            ['email', 'required'],
            /*['email', 'required', 'when' => function($model) {
                    return $model->email != '';
                }, 'whenClient' => "function (attribute, value) {
                    return $('#retrieve').val() == 'E';
                }"
            ],*/
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'uniqueEmail'],            
                        
            /*['mobile', 'required', 'when' => function($model) {
                    return $model->mobile != '';
                }, 'whenClient' => "function (attribute, value) {
                    return $('#retrieve').val() == 'M';
                }"
            ],*/

            //['mobile', 'string', 'max' => 15],            
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            //['email', 'uniqueEmail'],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        /* @var $user User */
        $user = User::findOne([
            //'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
            'client_id' => $client_id
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        if ($user) {
            $userid = $user->id;
            $email = $user->email;
            $profile = \common\models\VIPCustomer::findOne([
                    'userID' => $userid,
                ]);
            $name = $profile->full_name;
            //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
            $resetLink = Yii::$app->request->hostInfo.'/site/reset-password?token='.$user->password_reset_token;
            Yii::$app->VIPglobal->passwordResetEmail($email,$name,$resetLink);
            return true;
        }else{
           return false; 
        }
    }
    
    public function attributeLabels()
    {
        return [
            'retrieve' => 'Retrieve With',
            'email' => 'Email',
            'mobile' => 'Mobile'
        ];
    }
    
    public function uniqueEmail($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        $user = User::findOne([
            'email'=>$this->email,
            'client_id' => $client_id,
        ]);
        
        if(count($user) > 0) {
            if($user->status != 'A') {
                $this->addError($attribute, 'Please contact our support team');
            }
        }else {
            $this->addError($attribute, 'There is no email "'.$this->email.'"  with us.');
        }
    }
    
    public function uniqueMobile($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        
        if(
            $user = VIPCustomer::find()->where(['mobile_no'=>$this->mobile_no, 'clientID' => $client_id])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Mobile "'.$this->mobile_no.'" has already been taken.');
    }
}
