<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
//use kartik\password\StrengthValidator;


class ChangePasswordForm extends Model
{

    public $password;
    public $confirm_password;
    public $oldPassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','confirm_password', 'oldPassword'], 'required'],
            [['password','confirm_password'], 'required'],
            ['password', 'string', 'min' => 6, 'max' => 12],
            //['password', 'string', 'length' => [6, 12]],
            //[['password'], StrengthValidator::className(), 'preset'=>'normal',],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => ' New Password',
            'repassword' => 'Re-enter New Password',
            'oldPassword' => 'Current Password'
        ];
    }

    
}
