<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;
use yii\web\UploadedFile;
use YoHang88\LetterAvatar\LetterAvatar;

use common\models\User;
use common\models\VIPCustomer;
use common\models\AuthAssignment;
use common\models\CompanyInformation;



/**
 * Signup form
 */
class SignupFormKIS extends Model
{
    //public $username;
    //public $email;
    //public $password;
    public $phone;
    public $salutation;
    public $full_name;
    public $company_name;
    //public $nric;
    public $mobile_no;
    public $email;
    //public $address_1;
    //public $city;
    //public $postcode;
    //public $state;
    //public $yearly_purchase_volume;
    //public $distributors;
    public $type;
    public $password;
    public $re_type_password;
    public $rememberMe;
    public $agree;
    public $created_by;
    //public $type;


    public function behaviors() {
        return [
            BlameableBehavior::className(),
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salutation', 'full_name', 'company_name','mobile_no','agree','type'], 'required'],
            ['mobile_no', 'trim'],
            ['mobile_no', 'required'],
            //['mobile_no', 'unique', 'targetClass' => '\common\models\VIPCustomer', 'message' => 'This Mobile Number has already been taken.'],
            ['mobile_no', 'uniqueMobile'],
            ['mobile_no', 'string', 'min' => 2, 'max' => 255],
            [['mobile_no'], PhoneInputValidator::className()],
            //[['mobile_no'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'mobile_no']],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'uniqueEmail'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['re_type_password', 'required'],
            ['re_type_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if ($this->validate()) {
            $mob = preg_replace('/(?<=\d)\s+(?=\d)/', '', $this->mobile_no);
            
            $userAgent = \xj\ua\UserAgent::model();
            $platform = $userAgent->platform;
            $browser = $userAgent->browser;
            $version = $userAgent->version;
            
            
            $user = new User();
            $user->client_id = $clientID;
            $user->username = str_replace('+6', '', trim($mob));
            $user->email = trim($this->email);
            $user->email_verification = 'N';
            $user->status = 'P';
            $user->approved = 'Y';
            $user->newsletter = 'D';
            $user->user_type = 'D';
            $user->type = $this->type;
            $user->reg_by = 1;
            $random_str = md5(uniqid(rand()));
            $activationKey = substr($random_str, 0, 8);
            $user->activation_key = $activationKey;
            $user->ip = $_SERVER['REMOTE_ADDR'];
            $user->user_agent = $platform.'-'.$browser.'-'.$version;


            $user->setPassword($this->password);
            $user->generateAuthKey();
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $user->save(false)) {
                    if ($flag) {
                        $model = new VIPCustomer();
                        $model->userID = $user->id;
                        $model->clientID = $clientID;
                        $model->clients_ref_no = date('dmyHis');
                        $model->dealer_ref_no = $clientID . '-' . date('dmyHis');
                        $model->salutation_id = $this->salutation;
                        $model->full_name = $this->full_name;
                        $model->mobile_no = $this->mobile_no;
                        $model->created_by = $user->id;

                        if (($flag = $model->save(false)) === false) {
                            $transaction->rollBack();
                            //break;
                        }
                        $authassignment = new AuthAssignment();
                        $authassignment->item_name = 'Dealer';
                        $authassignment->user_id = $user->id;
                        $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                        //$authassignment->save(false);
                        if (($flag = $authassignment->save(false)) === false) {
                            $transaction->rollBack();
                            //break;
                        }
                        
                        $companyInformation = new CompanyInformation();
                        $companyInformation->user_id = $user->id;
                        $companyInformation->company_name = $this->company_name;
                        //$companyInformation->save(false);
                        if (($flag = $companyInformation->save(false)) === false) {
                            $transaction->rollBack();
                            //break;
                        }
                        
                        $name = $this->full_name;
                        $parts = explode(' ', $name);
                        if(count($parts) == 1){
                            $firstname = $parts[0].' '.$parts[0];
                        }else {
                            $firstname = $parts[0].' '.$parts[1];
                        }
                        
                        $avatar = new LetterAvatar($firstname);
                        $path = Yii::getAlias('@backend').'/web/upload/profiles/';           
                        $filename = Yii::$app->security->generateRandomString().".png";
                        $avatar->saveAs($path . $filename);

                        $photo = new \common\models\ProfilePic();
                        $photo->userID = $user->id;
                        $photo->file_name = $filename;
                        //$photo->save(false);
                        if (($flag = $photo->save(false)) === false) {
                            $transaction->rollBack();
                            //break;
                        }
                        //AuthAssignment
                    }
                }

                if ($flag) {
                    $transaction->commit();
                    return $user;
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
        return null;
    }
    
    public function uniqueEmail($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        
        if(
            $user = User::find()->where(['email'=>$this->email, 'client_id' => $client_id, 'user_type' => 'D'])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Email "'.$this->email.'" has already been taken.');
    }
    
    public function uniqueMobile($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        
        if(
            $user = VIPCustomer::find()->where(['mobile_no'=>$this->mobile_no, 'clientID' => $client_id])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Mobile "'.$this->mobile_no.'" has already been taken.');
    }
}
