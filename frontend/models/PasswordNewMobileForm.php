<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

use common\models\User;
use common\models\SMSOTP;




/**
 * Signup form
 */
class PasswordNewMobileForm extends Model
{

    public $otp;
    public $new_password;
    public $retype_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            ['otp', 'trim'],
            ['otp', 'required'],
            //['mobile_no', 'unique', 'targetClass' => '\common\models\VIPCustomer', 'message' => 'This Mobile Number has already been taken.'],
            ['otp', 'Checkotp'],
            ['otp', 'string', 'min' => 6, 'max' => 6],

            ['new_password', 'required'],
            ['new_password', 'string', 'min' => 6],
            ['retype_password', 'required'],
            ['retype_password', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Passwords don't match" ],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'otp' => 'OTP',
            'new_password' => 'New Password',
            'retype_password' => 'Retype Password',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */

    public function checkotp($attribute, $params)
    {
        $otp = SMSOTP::find()->where(['otp'=>$this->otp])->count();
        if($otp == 0) {
           $this->addError($attribute, 'OTP "'.$this->otp.'" does not exist.'); 
        }
          
    }
}
