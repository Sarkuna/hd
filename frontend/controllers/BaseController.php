<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class BaseController extends Controller {

    public function beforeAction($action) {

        $session = Yii::$app->session;

        if (Yii::$app->user->isGuest) {
            
            $this->getView()->theme = Yii::createObject([
                        'class' => '\yii\base\Theme',
                        'pathMap' => ['@app/views' => '@app/themes/vuexy'],
                        'baseUrl' => '@web/themes/vuexy',
            ]);
            //$this->layout = "/logincreative";
            
           
        }

        return parent::beforeAction($action);
    }

}
