<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

use common\models\Pages;

/**
 * Site controller
 */
class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $pages = VIPCmsPages::find()
                ->where(['page_id' => $id,'clientID' => $clientID])
                ->one();
        
        if(count($pages) > 0) {
            return $this->render('index',['pages' => $pages]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');    
        }
    }
    
    public function actionSlug($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $pages = Pages::find()
                ->where(['slug' => $id,'clientID' => $clientID])
                ->one();
        

        
        if(count($pages) > 0) {
            return $this->render('index',['pages' => $pages]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');    
        }
    }
    

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    
}