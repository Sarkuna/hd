<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use YoHang88\LetterAvatar\LetterAvatar;

use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
//use frontend\models\SignupFormKIS;
use frontend\models\SignupForm;
use frontend\models\SignupFormUpdate;
//use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    public function beforeAction($action)
    {
        if ($action->id == 'error') {
            $this->layout = 'main_error';
        }

        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'app-login', 'error', 'request-password-reset','reset-password','signup','signup-kis','terms', 'request-password-reset-mobile', 'new-password', 'emailconfirmation','resetPassword', 'signuptest', 'store-login', 'maintenance'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'signup-update', 'view-avatar', 'maintenance'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                //$this->context->layout = 'main_product_detail',
                //'layout' => '@app/themes/vuexy/layouts/main_product_detail',
                //$this->layout = '@app/themes/vuexy/layouts/main_product_detail';
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
   
    
    public function actionIndex()
    {
        $this->layout = 'main_dashboard';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if (!Yii::$app->user->isGuest) {
            $numRows = \common\models\VIPCustomer::find()
                            ->where(['userID' => Yii::$app->user->id])
                            ->andWhere(['IS','full_name',Null])
                            ->count();
            if($numRows > 0) {
                return $this->redirect(['signup-update']);
            }
            
            return $this->render('index');
        } else {
            Yii::$app->user->logout();
        }
        //return $this->redirect(['home']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'authmain';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = \common\models\User::findOne(Yii::$app->user->id);
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $user->date_last_login = date('Y-m-d H:i:s');
            $user->save();
            Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionAppLogin($token)
    {
        $model = new LoginForm();
        $model->token = $token;
        $model->login2();
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

    }
    
    /**
     * Login from Admin access
     */
    public function actionStoreLogin($token)
    {
        $model = new LoginForm();
        $model->token = $token;
        $model->store();
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    
    public function actionViewAvatar() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $userID = \Yii::$app->user->id;
        
        //$id = Yii::$app->request->get('id');
        $size = Yii::$app->request->get('size');
        $model = \common\models\VIPCustomer::find()
            ->where(['userID' => $userID])
            ->one();

        if(!empty($model->full_name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $model->full_name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignuptest()
    {

echo "GD: ", extension_loaded('gd') ? 'OK' : 'MISSING', '<br>';
echo "XML: ", extension_loaded('xml') ? 'OK' : 'MISSING', '<br>';
echo "zip: ", extension_loaded('zip') ? 'OK' : 'MISSING', '<br>';
die;
        $this->layout = 'registercreative';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    public function actionSignupUpdate()
    {
        $this->layout = 'authmain';
        $session = Yii::$app->session;
        
        $numRows = \common\models\VIPCustomer::find()
                            ->where(['userID' => Yii::$app->user->id])
                            ->andWhere(['IS','full_name',Null])
                            ->count();
            if($numRows == 0) {
                return $this->redirect(['index']);
            }
        
        $client = \common\models\Client::find()
            ->where(['clientID' => $session['currentclientID'],])->one();
        
        $model = new SignupFormUpdate();
        if ($model->load(Yii::$app->request->post())) {
            $numauth = \common\models\AuthAssignment::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->count();
            if($numauth == 0) {
                $authassignment = new \common\models\AuthAssignment();
                $authassignment->item_name = 'Dealer';
                $authassignment->user_id = Yii::$app->user->id;
                $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                $authassignment->save();
            }
            
            if ($user = $model->signup()) {
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Thank you.', 'text' => 'You have been complete your registration.']);        
                    return $this->goHome();
            }
        }
        return $this->render('signup_pop_form', [
            'model' => $model,
        ]);
    }
    
    public function actionSignup()
    {
        $this->layout = 'authmain';
        $session = Yii::$app->session;
        
        $client = \common\models\Client::find()
            ->where(['clientID' => $session['currentclientID'],])->one();
        
        $model = new SignupForm();
        //$model->assign_dealer_module = $client->assign_dealer_module;
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                    $data = \yii\helpers\Json::encode(array(
                        'verification_email_key' => $user->activation_key,
                    ));
                    $subject = "";
                    $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.email.verify'], $data,$subject);
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'We need to confirm your email address.', 'text' => 'Please check your email and click the confirmation link to complete your registration. The email can take a few minutes to arrive.']);        

                    return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    public function actionMaintenance()
    {
        $this->layout = 'main_error';
        return $this->render('maintenance');
    }
    
    public function actionEmailconfirmation($key)
    {
        //$this->layout = 'registercreative';
        if (Yii::$app->user->isGuest) {
            $user = \common\models\User::find()->where(['activation_key' => $key])->one();
            //echo '<pre>'.print_r($user).die;
            if (count($user) > 0) {
                if($user->email_verification == 'N') {
                    $user->email_verification = 'Y';
                    $user->removeActivationKeyToken();
                    $user->save();
                
                    $title = '<h3 class="text-success">CONGRATULATION!!!</h3>';
                    $msg = 'You have successfully verified your email address. Your account is pending for Admin\'s approval.';
                    //\Yii::$app->getSession()->setFlash('success', ['title' => 'CONGRATULATION!!!', 'text' => 'You have successfully verified your email address. Your account is pending for Admin\'s approval.']);
                }else {
                    $text = ' Your account is pending for Admin\'s approval.';
                    if($user->status == 'A'){
                        $text = ' Please proceed to login now.';
                    }
                    $title = '<h3 class="text-danger">Email already verified!</h3>';
                    $msg = 'You have already confirmed your email address.'.$text;
                }
            } else {
                $title = '<h3 class="text-danger">Email Verified</h3>';
                $msg = 'You have already confirmed your email address. Your account is pending for Admin\'s approval.';
                //\Yii::$app->getSession()->setFlash('warning', ['title' => 'Email already verified!', 'text' => 'You have already confirmed your email address. Please proceed to sign in now.']);
            }
        }
        
        return $this->render('sign_up_msg', [
            'title' => $title,
            'msg' => $msg,
        ]);
        //\Yii::$app->getSession()->setFlash('warning', ['title' => 'Email already verified!', 'text' => 'You have already confirmed your email address. Please proceed to sign in now.']);
        //return $this->goHome();
    }
    
    public function actionTerms()
    {
        $this->layout = 'registercreative';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = \common\models\Client::findOne($clientID);

        return $this->render('tnc', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'authmain';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //if($model->retrieve == 'E') {
                if ($model->sendEmail()) {
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'Check your email for further instructions.']);
                    return $this->goHome();
                } else {
                    \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Sorry, we are unable to reset password for the provided email address.']);
                }
            //}
        }else {
            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionRequestPasswordResetMobile()
    {
        $this->layout = 'registercreative';
        $model = new \frontend\models\PasswordResetRequestMobileForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $mobile_no = $model->mobile_no;
            $flag = \common\models\VIPCustomer::find()
            ->where(['mobile_no' => trim($mobile_no)])
            ->one();
        //if()

            if (count($flag) == 1) {
                    $OTP = mt_rand(100000, 999999);
                    $destination = trim($mobile_no);
                    $message = 'Dear Lafarge member, OTP for change of password is ' . $OTP . '. Do not share the OTP with anyone,';
                    $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
                    $message = urlencode($message);

                    $username = urlencode("lafage");
                    $password = urlencode("lafage2018");
                    $sender_id = urlencode("66300");
                    $type = urlencode("1");

                    $fp = "https://www.isms.com.my/isms_send_all.php?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&sendid=$sender_id&agreedterm=YES";
                    $handle = @fopen($fp, "r");
                    if ($handle) {
                        while (!feof($handle)) {
                            $buffer = fgets($handle, 10000);
                            $st = $buffer;
                        }
                    }
                    if ($st == '2000 = SUCCESS' || $st == '') {
                        $gsms = new \common\models\SMSOTP();
                        $gsms->userID = $flag->userID;
                        $gsms->mobile = trim($mobile_no);
                        $gsms->otp = $OTP;
                        $gsms->save(false);
                        //return ['message' => 'Yes'];
                        \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'we send you an OTP – a 6 digit confirmation code to your mobile']);
                        return $this->redirect(['/site/new-password']);
                    } else {
                        \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => $st]);
                        return $this->redirect(['/site/new-password']);
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Sorry, we are unable to send you an OTP – a 6 digit confirmation code to your mobile.']);
                }
        }

        return $this->render('mobile', [
            'model' => $model,
        ]);
    }
    
    public function actionNewPassword(){
        $this->layout = 'registercreative';
        $model = new \frontend\models\PasswordNewMobileForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $otp = $model->otp;
            $new_password = $model->new_password;
            $retype_password = $model->retype_password;
            
            $flag = \common\models\SMSOTP::find()
                    ->where(['otp' => trim($otp)])
                    ->one();

            if (count($flag) == 1) {
                if ($new_password == $retype_password) {
                    $user = \common\models\User::find()
                            ->where(['id' => $flag->userID])
                            ->one();

                    $user->setPassword($retype_password);
                    $user->generateAuthKey();
                    //$user->save(false);
                    if ($user->save(false)) {
                        //echo 'test';die;
                        //$this->findModel($id)->delete();
                        //$flag = \common\models\SMSOTP::findOne($flag->id);
                        //$flag->delete();
                        \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'Your password has been successfully updated']);
                        return $this->redirect(['/site/login']);
                    } else {
                        \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Password not update']);
                        //return $this->redirect(['index1']);
                    }
                    print_r($user->getErrors());
                    die;
                } else {
                    \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Password not match']);
                    return $this->redirect(['/site/new-password']);
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'OTP Code Invalid']);
                return $this->redirect(['/site/new-password']);
            }
        }

        return $this->render('mobile_new', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'auth_reset_password';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            //Yii::$app->session->setFlash('success', 'New password saved.');
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'New password saved.']);

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
