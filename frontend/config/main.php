<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    /*'on beforeRequest' => function () {
        echo $_SERVER['SERVER_NAME'];
        //die();
    },*/
    'components' => [
        'request' => [
            //'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['/login'],
            //'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/vuexy'],
                //'baseUrl' => '@app/themes/vuexy',
                'baseUrl' => '@web/../themes/vuexy',
            ],
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
            //'layout' => 'main_product_detail',
        ],
        
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6Lfqw4wUAAAAACQ9XCLVq0WdjFx0nPVKez2-WOeZ',
            'secret' => '6Lfqw4wUAAAAANftemG5AhsOwdBP76mBy7s6GIN2',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'suffix' => '.html',
            'rules' => [
                //'<slug>'=>'pages/slug',
                //'pages/<id:\d+>'=>'slug',
                //'view/<id:\d+>' => 'pages/slug',
                'index' => '/site/index',
                'login' => '/site/login',
                'register' => '/site/signup',
                'forgot-password' => 'site/request-password-reset',
                'my-profile'=>'/accounts/my-account/index',
                'my-profile/edit-profile'=>'/accounts/my-account/update-profile',
                'company'=>'/accounts/my-account/company',
                'company/edit-company'=>'/accounts/my-account/update-company',
                'my-reward-points'=>'accounts/my-account/reward-points/',
                'order-history'=>'/accounts/my-account/order',
                'addressbook'=>'/accounts/my-account/address',
                'addressbook/add-address'=>'/accounts/my-account/new-address',
                'bank-info'=>'/accounts/my-account/banks',
                'bank-info/add-bank'=>'/accounts/my-account/add-banks',
                'receipts-history'=>'/accounts/receipt/index',
                'receipts-history/submit-invoices'=>'/accounts/receipt/submit-invoices',
                'rewards' => '/products/product/index',
                'my-customer' => '/accounts/my-account/indirect',
                'my-transactions' => '/accounts/my-account/account2',
                
                'my-profile/change-password'=>'/accounts/my-account/change-password',
                
                '<id:[a-zA-Z0-9-]+>/'=>'pages/slug',
                'addressbook/edit-address/<id:\d+>'=>'accounts/my-account/update-address',
                'bank-info/edit-bank/<id:\d+>'=>'accounts/my-account/update-banks',
                'order-history/view/<id:\d+>'=>'/accounts/my-account/view',
                ///accounts/my-account/view?id=60
                //'myrewardpoint/<page:\d+>/'=>'myrewardpoint',
                
                
                //'reset-password' => 'site/reset-password',
                
                //'<module:\w+>/<controller:\w+>/<id:\d+>/'=>'<module>/<controller>/view',
                //'update-address/<id:[a-zA-Z0-9-]+>/'=>'update/id',
                //'<id:[a-zA-Z0-9-]+>/' => 'accounts/update-address/view',

                //'<view:[a-zA-Z0-9-]+>/'=>'pages/slug',
                //'pages/<id:[0-9a-zA-Z\-]+>/?' => 'pages/slug',
                //'pages/<slug>' => 'pages/slug',
                //'<controller:(products|product|product-detail)>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                //'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                //'<controller:\w+>/<action:\w+>/<id:\d+>/<qty:\d+>' => '<controller>/<action>',
            ],
        ],
        
        'vip' => [
            'class' => 'app\components\VIP',
        ],
        
        /*'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],*/
        'as beforeRequest' => [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'urlManagerBackend' => [
                'class' => 'yii\web\urlManager',
                'baseUrl' => 'backend/web/',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
        ],
    ],
    'modules' => [
        'products' => [
            'class' => 'app\modules\products\Products',
        ],
        'shopping' => [
            'class' => 'app\modules\shopping\Shopping',
        ],
        'accounts' => [
            'class' => 'app\modules\account\Account',
        ],
        /*'clients' => [
            'class' => 'app\modules\clients\Clients',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Profile',
        ],*/
        'rights' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // it can be '@path/to/your/layout'.
	    'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id', // id field of model User
		    'usernameField' => 'username', // username field of model User
                ],
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            //'rights/*',
            //'some-controller/some-action',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
    'on beforeRequest' => function () {
        $session = Yii::$app->session;
        //$domain = $_SERVER['SERVER_NAME'];
        $domain = Yii::$app->request->hostInfo;
        $numRows = \common\models\Client::find()->where([
            'cart_domain' => $domain,
        ])->count();
        if ($numRows > 0){
            $myclient = \common\models\Client::find()->where([
                'cart_domain' => $domain,
            ])->one();
            
            if ($myclient->site_offline == 'Y') {
                Yii::$app->catchAll = [
                      '/site/maintenance', 
                      //'message' => Yii::$app->params['siteSettings']->maintenance_message
                ];
            }
            
            $clientname = common\models\ClientAddress::find()->where([
                'clientID' => $myclient->clientID,
            ])->one();
            $session['currentclientID'] = $myclient->clientID;
            $session['currentclientName'] = $myclient->company;
            //$session['currentLogo'] = $clientname->company_logo;
            $session['adminURL'] = $myclient->admin_domain;
            $session['membership'] = $myclient->membership;
            $session['background'] = $myclient->background_img;
            //$session['upload_receipt_module'] = $myclient->upload_receipt_module;
            //$session['mobileapp'] = $myclient->mobile_app;
            Yii::setAlias('@back', $myclient->admin_domain);
        }else{
            $session['currentclientName'] = 'VIP<b>Admin</b>';
        }
    },
    'params' => $params,
];
