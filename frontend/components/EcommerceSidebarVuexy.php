<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class EcommerceSidebarVuexy extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];


        $sql = "SELECT vsc.vip_sub_categories_id, vsc.name FROM vip_assign_products AS vap INNER JOIN vip_product AS vp ON vap.productID=vp.product_id INNER JOIN vip_sub_categories AS vsc ON vp.sub_category_id=vsc.vip_sub_categories_id WHERE (vap.clientID = '$clientID') AND (vap.assign_status = 'A') GROUP BY vsc.vip_sub_categories_id ORDER BY `vsc`.`name` ASC";
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $categories = $command->queryAll();      
        return $this->render('ecommercesidebarvuexy', ['categories' => $categories]);
        
    }
}