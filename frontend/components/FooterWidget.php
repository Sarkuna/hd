<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class FooterWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $footerpages = \common\models\FooterWidget::find()
                ->where(['clientID' => $clientID])
                ->orderBy([
                    'position'=>SORT_ASC,
                ])
                ->limit(3)
                ->all();
        return $this->render('footerwidget',['footerpages' => $footerpages]);
        
    }
}