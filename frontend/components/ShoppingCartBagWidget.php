<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class ShoppingCartBagWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        return $this->render('shopping_cart_bag');
        
    }
}