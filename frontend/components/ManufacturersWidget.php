<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class ManufacturersWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $query = \common\models\VIPManufacturer::find();
               // ->where(['status' => 'E','clientID' => $clientID])->orderBy('created_datetime DESC');
        $query->select([
                    '*',
                ])
                //->where(['status' => 'A','condo_id' => $session['currentCondoId']])
                ->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    //'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        //return $this->render('products_bl_widget',array('bestsellerdataProvider'=>$bestsellerdataProvider,'lpdataProvider'=>$dataProvider2));      
        return $this->render('manufacturerswidget', ['manufacturers' => $dataProvider]);
        
    }
}