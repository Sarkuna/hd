<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class LatestProductsWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $query = \common\models\VIPProduct::find()
                ->joinWith('assignProduct')
                ->where(['status' => 'E','clientID' => $clientID])->orderBy('created_datetime DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    //'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        //return $this->render('products_bl_widget',array('bestsellerdataProvider'=>$bestsellerdataProvider,'lpdataProvider'=>$dataProvider2));      
        return $this->render('latestproductswidget', ['products' => $dataProvider]);
        
    }
}