<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\AssignModules;
$session = Yii::$app->session;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$clientID = $session['currentclientID'];
$mod1 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '1',])->count();
$mod2 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '2',])->count();
?>
<div class="top_bar">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="tb_center topmenubar pull-left">
                        <ul>
                            <li><i class="fa fa-home"></i><a href="<?= Yii::$app->homeUrl ?>">Home</a></li>
                            <?php
                            foreach($pages as $page){
                                $title = Html::encode($page->page_title);
                                echo '<li><a href="/pages/index?id='.$page->page_id.'">'.$title.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="tb_right pull-right">
                        <ul>
                            <li>
                                <div class="tbr-info">
                                    <span>Account <i class="fa fa-caret-down"></i></span>
                                    <div class="tbr-inner">
                                        <a href="<?php echo Url::to(['/accounts/my-account']); ?>">My Account</a>
                                        <a href="<?php echo Url::to(['/shopping/cart/checkout']); ?>">Checkout</a>
                                        <?php
                                        if(Yii::$app->user->identity->type == 5){ 
                                            if($mod1 > 0){
                                                echo '<a href="'.Url::to(['/accounts/receipt']).'">My Receipts</a>';
                                            }
                                            
                                            if($mod2 > 0){
                                                echo '<a href="'.Url::to(['/accounts/projects/']).'">My Projects</a>';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if(Yii::$app->user->identity->type == 1){                                 
                            ?>
                            <li>
                                <div class="tbr-info">
                                    <span>Account2 <i class="fa fa-caret-down"></i></span>
                                    <div class="tbr-inner">
                                        <a href="<?php echo Url::to(['/accounts/my-account/indirect']); ?>">My Customer</a>
                                        <a href="<?php echo Url::to(['/accounts/my-account/account2']); ?>">My Transactions</a>
                                        <?php
                                        /*if($session['upload_receipt_module'] == '1'){
                                            echo '<a href='.Url::to(['/accounts/my-account/upload-receipt']).'>Upload Receipt</a>';
                                            echo '<a href='.Url::to(['/accounts/receipt']).'>Receipts List</a>';
                                        }*/
                                        ?>

                                    </div>
                                </div>
                            </li>
                            <?php
                            }
                            ?>
                            
                            <li><i class="fa fa-sign-out"></i> <a href="<?php echo Url::to(['/site/logout']); ?>" data-method="post">Sign out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>