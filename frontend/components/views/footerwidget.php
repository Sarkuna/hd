<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;
?>
<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 widget-footer">
                <h5>My Account</h5>
                <ul class="list-unstyled">
                    <li><a href="/accounts/my-account">My Account</a></li>
                    <li><a href="/accounts/my-account/order">Order History</a></li>
                    <li><a href="/accounts/my-account/reward-points">Your Reward Points</a></li>
                </ul>
            </div>
            <?php
            foreach($footerpages as $footerpage){
                echo '<div class="col-md-3 col-sm-3 widget-footer">'.$footerpage->footer_description.'</div>';
            }
            ?>
        </div>
    </div>
</footer>