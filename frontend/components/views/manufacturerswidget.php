<?php
$session = Yii::$app->session;
use yii\helpers\Html;
use yii\helpers\Url;
//$role = Yii::$app->session->get('currentRole');
//use app\components\ProfileNameWidget;
?>
<div class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="clients-carousel">
                    <?php
                
                $manufacturers = $manufacturers->getModels();
                foreach($manufacturers as $model) {
                    $ur2 = Yii::$app->urlManager->createUrl(['/products/product/manufactures', 'manufacturer' => $model->manufacturer_id]);
                    echo '<div><a href="'.$ur2.'"><img src="'.Yii::getAlias('@back').'/upload/brands/'.$model->image.'" class="img-responsive" alt=""/></a></div>';
                    
                }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
