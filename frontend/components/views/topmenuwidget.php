<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');

?>
<div class="top_bar">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="tb_left pull-left">
                            
                            <?php
                            $time = date("H");
                            /* Set the $timezone variable to become the current timezone */
                            $timezone = date("e");
                            /* If the time is less than 1200 hours, show good morning */
                            if ($time < "12") {
                                $gre = "Good morning";
                            } else
                            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
                            if ($time >= "12" && $time < "17") {
                                $gre = "Good afternoon";
                            } else
                            /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
                            if ($time >= "17" && $time < "19") {
                                $gre = "Good evening";
                            } else
                            /* Finally, show good night if the time is greater than or equal to 1900 hours */
                            if ($time >= "19") {
                                $gre = "Good night";
                            }
                            echo '<p>'.$gre.' Shihan !</p>';
                            ?>
                        </div>
                        <div class="tb_center pull-left">
                            <ul>
                                <li><i class="fa fa-phone"></i> Hotline: <a href="#">(+800) 2307 2509 8988</a></li>
                                <li><i class="fa fa-envelope-o"></i> <a href="#">online support@smile.com</a></li>
                            </ul>
                        </div>
                        <div class="tb_right pull-right">
                            <ul>
                                <li>
                                    <div class="tbr-info">
                                        <span>Account <i class="fa fa-caret-down"></i></span>
                                        <div class="tbr-inner">
                                            <a href="my-account.html">My Account</a>
                                            <a href="#">Checkout</a>
                                            <a href="#">Login</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>