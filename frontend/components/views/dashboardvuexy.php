<?php
    /* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "Good morning";
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "Good afternoon";
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "24") {
        $greetings = "Good evening";
    }

?>

<section id="dashboard-analytics">
    <div class="row">
        
        
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card bg-analytics">
                <div class="card-content">
                    <div class="card-body text-center text-white">
                        <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/elements/decore-left.png" class="img-left" alt="
                             card-img-left">
                        <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/elements/decore-right.png" class="img-right" alt="
                             card-img-right">
                        <div class="avatar avatar-xl bg-primary shadow mt-0">
                            <div class="avatar-content">
                                <?php
                            //$picPro = '<img alt="User profile picture" src="/site/view-avatar" class="round" height="40" width="40">';
                            $picPro = '<img class="img-fluid" src="/site/view-avatar" alt="img placeholder">';
                            //echo '<img alt="avatar" src="/site/view-avatar" class="round" height="40" width="40">';
                            echo $picPro;
                        ?>
                            </div>
                        </div>
                        <div class="text-center">

                            <h1 class="mb-2 text-white"><?= $greetings ?> <?= Yii::$app->VIPglobal->getProfileInfo()['full_name'] ?>,</h1>
                            
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <span class="float-right"><?= Yii::$app->VIPglobal->getProfileInfo()['code'] ?></span>
                            Code
                        </li>
                        <li class="list-group-item">
                            <span class="float-right"><?= Yii::$app->VIPglobal->getProfileInfo()['company_name'] ?></span>
                            Company Name
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="mb-0">My Points</h4>                    
                </div>
                <div class="card-content">
                    <div class="card-body px-0 pb-0" style="padding: 0px;">
                        <div id="goal-overview-chart" class="mt-75"></div>
                        <div class="row text-center mx-0">
                            <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Available Balance</p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                     echo number_format(Yii::$app->VIPglobal->myAvailablePoint());
                                    ?>
                                </p>
                            </div>
                            <div class="col-6 border-top d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Expiring on 31-12-<?php echo date("y"); ?></p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                        $exppoint = Yii::$app->VIPglobal->PointsExpireUser();
                                        echo $exppoint;
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!--<div class="card hide hidden">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="mb-0">Account 2</h4>                    
                </div>
                <div class="card-content">
                    <div class="card-body px-0 pb-0" style="padding: 0px;">
                        <div id="goal-overview-chart" class="mt-75"></div>
                        <div class="row text-center mx-0">
                            <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Available Balance</p>
                                <p class="font-large-1 text-bold-700">786,617</p>
                            </div>
                            <div class="col-6 border-top d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Expiring on 31-12-19</p>
                                <p class="font-large-1 text-bold-700">13,561</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

    </div>
    <div class="row">
        <?php if(Yii::$app->user->identity->type == 5){ ?>
        <!--<div class="col-xl-4 col-md-4 col-sm-6 col-lg-4">            
            <div class="card text-center">
                <div class="card-content">
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">
                            <a href="/receipts-history/submit-invoices">
                            <div class="avatar-content">
                               <i class="fa fa-upload text-primary fa-5x"></i>
                            </div>
                            </a>
                        </div>
                        <h2 class="text-bold-700">Upload Your Invoice</h2>
                        <p class="mb-0 line-ellipsis">Hold your device steady to avoid a blurry image</p>
                        
                    </div>
                </div>
            </div>
        </div>-->
        <?php } ?>
        <div class="col-xl-4 col-md-4 col-sm-6">
            <div class="card text-center">
                <div class="card-content">
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">
                            <a href="/rewards">
                                <div class="avatar-content">
                                    <i class="fa fa-gift text-primary fa-5x"></i>
                                </div>
                            </a>
                        </div>
                        <h2 class="text-bold-700">Redeem Your Points</h2>
                        <p class="mb-0 line-ellipsis">Choose from our wide variety of merchandise</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-4 col-md-4 col-sm-6">
            <div class="card text-center">
                <div class="card-content" style="min-height: 218px;">
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">
                            <a href="/my-reward-points">
                            <div class="avatar-content">
                                <i class="feather icon-award text-primary fa-5x"></i>
                            </div>
                            </a>
                        </div>
                        <h2 class="text-bold-700">Reward Points</h2>
                        <p class="mb-0 line-ellipsis">View your complete point history</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</section>