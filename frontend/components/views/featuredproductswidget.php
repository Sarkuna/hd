<?php
$session = Yii::$app->session;
use yii\helpers\Html;
use yii\helpers\Url;
//$role = Yii::$app->session->get('currentRole');
//use app\components\ProfileNameWidget;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="heading-sub heading-sub2 text-center">
                <h5><span>Featured Products</span></h5>                
            </div>
            <div class="product-carousel3">
                <?php
                
                $products = $products->getModels();
                foreach($products as $model) {
                    $pname = Html::a($model->product_name, ['/products/product/product-detail', 'id' => $model->product_id]);
                    echo '<div class="pc-wrap">
                    <div class="product-item">
                        <div class="item-thumb">
                            <img src="'.Yii::getAlias('@back').'/upload/product_cover/thumbnail/'.$model->main_image.'" class="img-responsive" alt=""/>
                            <div class="product-overlay">
                                '.Html::a('', ['/products/product/product-detail', 'id' => $model->product_id], ['class' => 'addcart fa fa-shopping-cart']).'
                            </div>
                        </div>
                        <div class="product-info">
                            <h4 class="product-title">'.$pname.'</h4>
                            <span class="product-price">'.$model->points_value.' <em>- pts</em></span>
                        </div>
                    </div>
                </div>';
                }
                ?>
                
            </div>
        </div>
    </div>
</div>