<?php

$carts = Yii::$app->session['cart'];
$itemcount = count($carts);
echo '<span class="btn btn-info"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;My Bag - ' . $itemcount . ' item(s)</span>';
if ($itemcount > 0) {
    $subtotal = 0;
    $total = 0;
    echo '<div class="cart-info">
                            <small>You have <em class="highlight">' . $itemcount . ' item(s)</em> in your shopping bag</small>';
    foreach ($carts as $key => $carlist) {
        $totalpoint = $carlist['points_value'] * $carlist['product_qty'];
        echo '<div class="ci-item">
                                    <img src="' . Yii::getAlias('@back') . '/upload/product_cover/thumbnail/' . $carlist['main_image'] . '" width="80" alt=""/>
                                    <div class="ci-item-info">
                                        <h5><a href="/products/product/product-detail?id=' . $key . '">' . $carlist['product_name'] . '</a></h5>
                                        <p>' . $carlist['product_qty'] . 'X' . $carlist['points_value'] . '</p>    
                                        <p>' . $totalpoint . ' pts</p>
                                        ' . $carlist['opttionlisititem'] . '
                                        <div class="ci-edit">
                                            <a class="deletebagitem edit fa fa-trash" href="javascript::void(0)" onclick="deleteItem(' . $key . ');"></a>
                                        </div>
                                    </div>
                                </div>';
        $total += $totalpoint;
    }
    echo '<div class="ci-total">Subtotal: ' . $total . ' pts</div>
                            <div class="cart-btn">
                                <a href="/shopping/cart/">View Bag</a>
                            </div>
                        </div>';
} else {
    echo '<div class="cart-info" style="height:auto;">Your shopping cart is empty!</div>';
}
?>