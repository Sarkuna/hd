<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\ShoppingCartBagWidget;
//use common\models\AssignModules;
//$session = Yii::$app->session;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    .cart-dropdown .cart-dropdown-item-img {
    max-width: 100%;
    max-height: 100%;
    width: auto;
    transition: .35s;
}
.p-4 {
    padding: 1rem!important;
}
</style>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>

                    <!--<?= ShoppingCartBagWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>-->
                    

                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name text-bold-600"><?= Yii::$app->VIPglobal->getProfileInfo()['full_name'] ?></span>
                                <span class="user-status">Available</span>
                            </div>
                            <span>
                                <?php
                                $picPro = '<img alt="User profile picture" src="/site/view-avatar" class="round" height="40" width="40">';
                                //$picPro = '<img alt="User profile picture" src="view-avatar?id='.$model->vip_customer_id.'" class="profile-user-img img-responsive img-circle">';
                                //echo '<img alt="avatar" src="/site/view-avatar" class="round" height="40" width="40">';
                                echo $picPro;
                                ?>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="/my-profile/edit-profile"><i class="feather icon-user"></i> Edit Profile</a>
                            <a class="dropdown-item" href="/my-profile/change-password"><i class="feather icon-lock"></i> Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo Url::to(['/site/logout']); ?>" data-method="post"><i class="feather icon-power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>