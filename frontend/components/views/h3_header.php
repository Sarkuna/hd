<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use app\components\ShoppingCartBagWidget;
use app\components\ProfileNameWidget;
//$role = Yii::$app->session->get('currentRole');
$imgurl = $this->theme->basePath;
$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$clientname = common\models\ClientAddress::find()->where([
    'clientID' => $session['currentclientID'],
])->one();

$session['currentclientID'] = $myclient->clientID;
$session['currentclientName'] = $clientname->company;
$session['currentLogo'] = $clientname->company_logo;
$session['adminURL'] = $myclient->admin_domain;
$session['membership'] = $myclient->membership;
$session['background'] = $myclient->background_img;
$session['upload_receipt_module'] = $myclient->upload_receipt_module;

if(!empty($session['currentLogo'])){
    $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
}else{
    $img = $imgurl.'images/basic/logo-lite.png';
}

?>

<header>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="sthumbnail_container">
                    <div class="sthumbnail">
                        <a href="<?= Yii::$app->homeUrl ?>"><img src="<?= $img ?>" class="img-responsive" alt=""/></a>
                    </div>
                </div>
                
            </div>
            <div class="col-md-7 col-sm-6 col-xs-12 no-padding">
                <h4 class="welcometext">Welcome <?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?> </h4>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?= $totalavailablepoint ?></h3>
                            <p>Total Available Point</p>                            
                        </div>
                        <a href="/accounts/my-account/reward-points" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?= $pointsexpiring ?></h3>
                            <p>Points Expiring <?= date('d-m-y', strtotime($newDate)) ?></p>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <!-- ./col -->
                <div class="col-lg-4 col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $totalredeemedpoint ?></h3>

                            <p>Total Redeemed Point</p>
                        </div>  
                        <a href="/accounts/my-account/order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!--Welcome KANSAI DEALER<br>You have 3,697 points<br>Expiring on 31 March 2018: 0points-->
            </div>

            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="top-search3 pull-right">
                    <form>
                        <input type="text" placeholder="Search entire store here.">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div class="clearfix space30"></div>                            
                <div class="topcart pull-right">
                    <?= ShoppingCartBagWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                    <!-- End mini cart -->
                </div>
            </div>
        </div>
    </div>

    <div class="dark-nav">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Logo -->
                    </div>
                    <!-- Navmenu -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">                           

                            <?php
                            //$dfUrl = Yii::$app->params['dfUrl'];
                            $dfUrl = '';
                            $items = common\models\VIPAssignCategories::find()
                                    ->joinWith(['categories'])
                                    ->where(['clientID' => $session['currentclientID']])
                                    ->orderBy([
                                        'position' => SORT_ASC,
                                            //'username' => SORT_DESC,
                                    ])
                                    ->all();
                            //->all();

                            $result = [];
                            foreach ($items as $item) {
                                $parent = $item['categories_id'];
                                $cname = $item->categories->name;
                                echo '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">' . $cname . '</a><ul class="dropdown-menu submenu" role="menu">';
                                $id = $item['categories_id'];
                                $ur = Yii::$app->urlManager->createUrl(['' . $dfUrl . 'site/product', 'id' => $id]);
                                //echo '<li><a href="'.$ur.'">'.$item['cat_name'].'</a></li>';
                                $items2 = common\models\VIPAssignSubCategories::find()
                                        ->joinWith(['subcategories'])
                                        ->where(['categories_id' => $parent, 'clientID' => $session['currentclientID']])
                                        ->orderBy([
                                            'position' => SORT_ASC,
                                                //'position' => SORT_DESC,
                                        ])
                                        //->orderBy('subname')
                                        ->all();
                                foreach ($items2 as $item2) {
                                    //echo $item2['subname'];

                                    $id = $item2['categories_id'];
                                    $sid = $item2['sub_categories_id'];
                                    $subcname = $item2['subcategories']['name'];
                                    //$subcname = $item2->subcategories->name;
                                    //$subcname = 'sss';
                                    //$ur2 = Url::to(["product", 'id' => $id, 'sid' => $sid]);
                                    $ur2 = Yii::$app->urlManager->createUrl(['' . $dfUrl . '/products/product', 'id' => $id, 'sid' => $sid]);
                                    echo '<li class="nav"><a href="' . $ur2 . '">' . $subcname . '</a></li>';
                                }
                                echo '</ul></li>';
                            }
                            ?>
                            <li><a href="#">Special</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>


</header>