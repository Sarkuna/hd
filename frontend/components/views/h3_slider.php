<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- SLIDER -->
<div class="slider-wrap">
    <div class="tp-banner-container">
        <div class="tp-banner slider-5">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="images/slides/banner1.png"  data-saveperformance="on"  data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="images/slides/banner1.png"  alt="slidebg1" data-lazyload="images/slides/banner1.png" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="images/slides/banner2.png"  alt="slidebg1" data-lazyload="images/slides/banner2.png" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="images/slides/banner3.png"  alt="slidebg1" data-lazyload="images/slides/banner3.png" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>