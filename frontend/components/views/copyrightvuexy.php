<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0">
        <span class="float-md-left d-block d-md-inline-block mt-25">Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script>
            <a class="text-bold-800 grey darken-2" href="http://businessboosters.com.my" target="_blank">Rewards Solution Sdn Bhd,</a>
            All rights Reserved
        </span>
    </p>
</footer>