<?php
use yii\helpers\Url;
use yii\helpers\Html;
//use app\components\ShoppingCartBagWidget;
//use common\models\AssignModules;
//$session = Yii::$app->session;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="sidebar-detached sidebar-left">
        <div class="sidebar">
            <!-- Ecommerce Sidebar Starts -->
            <div class="sidebar-shop" id="ecommerce-sidebar-toggler">

                <div class="row">
                    <div class="col-sm-12">
                        <h6 class="filter-heading d-none d-lg-block">Filters</h6>
                    </div>
                </div>
                <span class="sidebar-close-icon d-block d-md-none">
                    <i class="feather icon-x"></i>
                </span>
                <div class="card">
                    <div class="card-body">

                        <!-- /Price Slider -->
                        <!--<div class="price-slider">
                            <div class="price-slider-title mt-1">
                                <h6 class="filter-title mb-0">Points Slider</h6>
                            </div>
                            <div class="price-slider">
                                <div class="price_slider_amount mb-2">
                                </div>
                                <div class="form-group">
                                    <div class="slider-sm my-1 range-slider" id="price-slider"></div>
                                </div>
                            </div>
                        </div><hr>-->
                        <!-- /Price Range -->
                        <div id="clear-filters">
                            <a href="/rewards" class="btn btn-block btn-primary">CLEAR ALL FILTERS</a>
                                </div>
                        <hr>
                        <!-- Categories Starts -->
                        <div id="product-categories">
                            <div class="product-category-title">
                                <h6 class="filter-title mb-1">Categories</h6>
                            </div>
                            <?php
                            if(!empty($categories)) {
                               echo '<ul class="list-unstyled categories-list">';
                               foreach($categories as $key=>$value) {
                                   echo '<li>
                                        <span class="vs-radio-con vs-radio-primary py-25">
                                            <input type="radio" name="category-filter" value="'.$value['vip_sub_categories_id'].'" data-href="/rewards?link='.$value['vip_sub_categories_id'].'">
                                            <span class="vs-radio">
                                                <span class="vs-radio--border"></span>
                                                <span class="vs-radio--circle"></span>
                                            </span>
                                            <span class="ml-50">'.$value['name'].'</span>
                                        </span>
                                    </li>';
                               }
                               echo '</ul>';
                            }
                            ?>
                            
                        </div>
                        <!-- Categories Ends -->
                        
                        <!-- Brands -->
                        <!--<hr>
                        <div class="brands">
                            <div class="brand-title mt-1 pb-1">
                                <h6 class="filter-title mb-0">Brands</h6>
                            </div>
                            <div class="brand-list" id="brands">
                                <ul class="list-unstyled">
                                    <li class="d-flex justify-content-between align-items-center py-25">
                                        <span class="vs-checkbox-con vs-checkbox-primary">
                                            <input type="checkbox" value="false">
                                            <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                </span>
                                            </span>
                                            <span class="">Insignia™</span>
                                        </span>
                                        <span>746</span>
                                    </li>
                                </ul>

                            </div>
                        </div>-->
                        <!-- /Brand -->

                    </div>
                </div>
            </div>
            <!-- Ecommerce Sidebar Ends -->

        </div>
    </div>