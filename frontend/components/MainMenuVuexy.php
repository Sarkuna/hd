<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class MainMenuVuexy extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = 16;
        $pages = \common\models\Menus::find()
                ->where(['clientID' => $clientID])
                ->andWhere(['like', 'display_location', 'header-menu'])
                ->one();

        //return $this->render('products_bl_widget',array('bestsellerdataProvider'=>$bestsellerdataProvider,'lpdataProvider'=>$dataProvider2));      
        return $this->render('mainmenuvuexy',
            [
                'pages' => $pages,
            ]
        );
        
    }
}