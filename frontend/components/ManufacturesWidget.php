<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use common\models\VIPAssignProducts;
//use yii\helpers\Html;

class ManufacturesWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        //$parent = $item['categories_id'];
        $parent = Yii::$app->getRequest()->getQueryParam('id');
        $sub_category_id = Yii::$app->getRequest()->getQueryParam('sid');
        //$cname = $item->categories->name;
        $manufactureslisits = VIPAssignProducts::find()
                ->joinWith(['product'])
                ->groupBy('manufacturer_id')
                ->where(['category_id' => $parent, 'sub_category_id' => $sub_category_id,'clientID' => $session['currentclientID']])
                ->orderBy([
                    'manufacturer_id' => SORT_ASC,
                ])
                //->groupBy('manufacturer_id')
                //->asArray()
                ->all();
        return $this->render('manufactures',['manufactureslisits' => $manufactureslisits, 'parent' => $parent, 'sub_category_id' => $sub_category_id]);
        
    }
}