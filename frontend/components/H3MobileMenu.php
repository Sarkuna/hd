<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class H3MobileMenu extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }

        
        $balance = $totaladd - $totalminus - $expired;
        
        
        $newDate = date('Y-m-d', strtotime("+6 months"));
        $pointsexpiring = 0;
        
        //$clientID = '11';
        $pages = \common\models\VIPCmsPages::find()
                ->where(['clientID' => $clientID, 'type' => 'T','status' => 'E'])
                ->orderBy([
                    'position'=>SORT_ASC,
                ])
                ->limit(10)
                ->all();

        return $this->render('h3_mobile_menu', [
            'pages' => $pages,
            'totalavailablepoint' => $balance,
            'totalredeemedpoint' => $totalminus,
            'pointsexpiring' => $pointsexpiring,
            'newDate' => $newDate,
        ]);        
    }
}