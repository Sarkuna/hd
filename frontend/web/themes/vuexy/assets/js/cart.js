function addCart(id){
    myqty = $('#myqty').val();
    optionvalue = $('#aboption').val();
    qty = 1;
    if(myqty > 0){
        qty = myqty;
    }
    selectoptionvalue = optionvalue;
    
    $.get('/shopping/cart/add-cart/', {'id': id, 'qty':qty, 'selectoptionvalue' : selectoptionvalue}, function(data){    
        location.reload();
    });
}

function deleteItem(id){
    $.get('/shopping/cart/remove-cart', {'id': id}, function(data){
        //$('#remove_' + id).remove();
        location.reload();
        //$(".cart-table").html(data);
    });
}

function itemQty(id){
    quantity = $("#quantity_"+id).val();
    $("#quantity_"+id).val(quantity);
    updateQty(id,quantity);
}

function updateQty(id,qty){
    $.get('/shopping/cart/update-cart-qty/', {'id': id, 'qty':qty}, function(data){ 
        location.reload();
        //window.location.href = '/shopping/cart';
        //$(".container").html(data);
        //$(".shop-single").append(data);
    });
}

$(function () {
    var fields = $('#myselect :input').change(calculate);

    function calculate() {
        var textToAppend = "";
        fields.each(function () {
            textToAppend += (textToAppend == "") ? "" : ",";
            textToAppend += $(this).val(); 
        })
        //$('#aboption').value(textToAppend);
        $('#aboption').attr('value', textToAppend);
    }
})

function addresdefault(id){
    //alert(id);
    $.get('/shopping/cart/address-default/', {'id': id}, function(data){ 
        if(data == 1){
            location.reload();
        }else {
            window.location.href = '/shopping/cart';
        }
    });
}