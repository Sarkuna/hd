<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login HDDC';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<div class="col-md-4 content-left">
                <div class="card card-login card-plain" style="top: 20%;position: fixed;left: 5%;">
                    <?php $form = ActiveForm::begin(['id' => 'form']); ?>
                        <div class="header header-primary text-center">
                            <div class="logo-container">
                                <?php  
                                //echo '<pre>'.print_r($session).die();
                                if (!empty($session['currentLogo'])) {
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                                } else {
                                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                                }
                                ?>
                            </div>
                        </div>
                        
                        <div class="content"> 

                            <?= $form->field($model, 'email', [
                                'template' => "<div class='input-group form-group-no-border input-lg'><span class='input-group-addon'><i class='now-ui-icons users_circle-08'></i></span>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => 'Email/Username'));
                            ?>
                            
                            <?= $form->field($model, 'password', [
                                'template' => "<div class='input-group form-group-no-border input-lg'><span class='input-group-addon'><i class='now-ui-icons ui-1_lock-circle-open'></i></span>{input}</div>\n{hint}\n{error}"
                                ])->passwordInput(array('placeholder' => 'Password'));
                            ?>
                            
                            
                        </div>
                        
                        <div class="footer text-center">
                            <?= Html::submitButton('Log in', ['class' => 'btn btn-primary btn-round btn-lg btn-block', 'name' => 'login-button']) ?>
                        </div>
                        
                        <div class="pull-left">
                            <h6>
                                <?php
                                if($session['membership'] == 'Y'){
                                    echo Html::a('Create Account', ['site/signup'], ['class' => 'link']); 
                                    /*if($session['currentclientID'] == '10'){
                                       echo Html::a('Create Account', ['site/signup-kis'], ['class' => 'link']);
                                    }else {
                                       echo Html::a('Create Account', ['site/signup'], ['class' => 'link']); 
                                    }*/
                                    
                                }
                                ?>
                                
                            </h6>
                        </div>
                        
                        <div class="pull-right">
                            <h6>
                                <?php
                                echo Html::a('Need Help?', ['site/request-password-reset'], ['class' => 'link']);
                                ?>
                            </h6>
                        </div>
                    <?php ActiveForm::end(); ?> 
                </div>
            </div>
