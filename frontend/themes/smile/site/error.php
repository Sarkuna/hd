<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$session = Yii::$app->session;
?>
<?php if (Yii::$app->user->isGuest) { 
    //Url::to(['/site/login']);
    echo '<div class="container">
        <div class="col-md-4 content-left">
            <div class="card card-login card-plain" style="top: 20%;position: fixed;left: 5%;">
                <div class="header header-primary text-center">
                    <div class="logo-container">';
                        if (!empty($session['currentLogo'])) {
                            echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                        } else {
                            echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                        }
                    echo '</div>                        
                </div>
                
                <div class="container" style="color: #000000;padding: 20px;">'.nl2br(Html::encode($message)).'</div>
                    <p><a href="'.Url::to(['/site/login']).'" class="btn btn-lg btn-theme-inverse btn-block">Go to login page</a></p>
            </div>
        </div>
    </div>';
}else {
    echo '<div class="container">
    <div class="main">
        <div class="row show-grid">
            <div class="col-main col-lg-9 col-md-9 col-sm-9 col-push-9 col-xs-12">
                <div class="std"><div class="page-title"><h1>'.Html::encode($this->title).'</h1></div>
                    <div class="alert alert-danger">
                        '.nl2br(Html::encode($message)).'
                    </div>

                    <dl>
                        <dt>What can you do?</dt>
                        <dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd>
                        <dd>
                            <ul class="disc">
                                <li><a href="#" onclick="history.go(-1); return false;">Go back</a> to the previous page.</li>
                                <li>Use the search bar at the top of the page to search for your products.</li>
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>';
}
?>



