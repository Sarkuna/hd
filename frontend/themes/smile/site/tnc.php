<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$session = Yii::$app->session;


$this->title = Html::encode($model->page_title);
$this->params['breadcrumbs'][] = $this->title;

?>
<style>

.card-body p {
    color: black !important;
    text-align: left !important;
}

ol {
    color: black;
    text-align: left;
}
h1, h2, h3, h4, h5, h6 {
    margin-top: 10px;
    margin-bottom: 10px;
    line-height: 1.25em;
    color: black;
}
h1, .h1 {
    font-size: 1.5em;
}
</style>

<div class="row">
    
                <div class="col-md-4 ml-auto mr-auto">

                </div>
                <div class="col-md-8 mr-auto">

                    <div class="card card-signup">
                        <div class="card-body">
                            <div class="logo-container ml-auto mr-auto" style="max-width:200px;">
                                <?php
                                //echo '<pre>'.print_r($session).die();
                                if (!empty($session['currentLogo'])) {
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                                } else {
                                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                                }
                                ?>
                            </div>
                            <h4 class="card-title text-center">Register</h4>
                            <?= $model->page_description; ?>

                        </div>
                    </div>
                </div>
            </div>