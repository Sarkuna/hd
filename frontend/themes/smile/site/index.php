<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\components\FeaturedProductsWidget;
use app\components\LatestProductsWidget;
use app\components\ManufacturersWidget;
use app\components\BestsellingProductsWidget;

$this->title = 'Shopping Cart';
$imgurl = $this->theme->basePath;

//die();
?>

            <div class="space10"></div>

            <!-- FEATURED PRODUCTS -->
            <?= FeaturedProductsWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <div class="space20"></div>

            <!-- LATEST PRODUCTS -->
            <?= LatestProductsWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

            <div class="space20"></div>
            <!-- BESTSELLING PRODUCTS -->
            <?= BestsellingProductsWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

<!-- PRODUCTS -->


<div class="clearfix space20"></div>

            <!-- CLIENTS -->
            <?= ManufacturersWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

            <div class="clearfix"></div>
