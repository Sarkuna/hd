<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<style>
    .login-page .card-login.card-plain .form-control:focus{
            color: #000000;
    background-color: rgb(241, 241, 241);
    }
    .login-page .card-login.card-plain .form-control{
            color: #000000;
    background-color: rgb(241, 241, 241);
    }
    .field-passwordresetrequestform-mobile {display: none;}
</style>
<div class="col-md-4">
                <div class="card card-login card-plain" style="top: 20%;position: fixed;left: 5%;">
                    <?php $form = ActiveForm::begin(['id' => 'form']); ?>
                        <div class="header header-primary text-center">
                            <div class="logo-container">
                                <?php  
                                //echo '<pre>'.print_r($session).die();
                                if (!empty($session['currentLogo'])) {
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                                } else {
                                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                                }
                                ?>
                            </div>
                        </div>
                        
                        <div class="content"> 
                            <?=
                                $form->field($model, 'retrieve', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->dropDownList(
                                    ['E' => 'Email'], ['prompt' => 'Retrieve With', 'id' => 'retrieve']
                                );
                                ?>
                            <?= $form->field($model, 'email', [
                                'template' => "<div class='input-group form-group-no-border input-lg'><span class='input-group-addon'><i class='now-ui-icons users_circle-08'></i></span>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => 'Email'));
                            ?>
                            <?= $form->field($model, 'mobile', [
                                'template' => "<div class='input-group form-group-no-border input-lg'><span class='input-group-addon'><i class='now-ui-icons users_circle-08'></i></span>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => 'Mobile'));
                            ?>

                        </div>
                        
                        <div class="footer text-center">
                            <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-round btn-lg btn-block', 'name' => 'login-button']) ?>
                        </div>
                        
    
                        
                        <div class="pull-right">
                            <h6>
                                <?= Html::a('Back', ['index'], ['class' => 'link']) ?>
                            </h6>
                        </div>
                    <?php ActiveForm::end(); ?> 
                </div>
            </div>

<?php
$script = <<< JS
    $(document).ready(function (e) {
    $('#retrieve').change(function () {
        if ($(this).val() == 'E') {
            $('.field-passwordresetrequestform-email').show();
            $('.field-passwordresetrequestform-mobile').hide();
        } else {
            $('.field-passwordresetrequestform-email').hide();
            $('.field-passwordresetrequestform-mobile').show();
        }
    });
});
JS;
$this->registerJs($script);
?>