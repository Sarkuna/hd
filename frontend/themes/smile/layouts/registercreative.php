<?php
use frontend\assets\RegCreativeAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

RegCreativeAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .page-header > .content {margin-top: 2%;}
    </style>
</head>
<body class="signup-page">
    <?php $this->beginBody() ?>

<div class="page-header section-image">
        <?php
            if (!empty($session['background'])) {
                $bgimg = Yii::getAlias('@back') . '/upload/client_bg/'.$session['background'];
            } else {
                $bgimg = '/images/hddclogin.jpg';
            }
        ?>
        <div class="page-header-image" style="background-image:url(<?= $bgimg ?>)"></div>

        <div class="content">
            <div class="container">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">

                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="//businessboosters.com.my" target="_blank">Business Boosters</a>. All Rights Reserved.
                </div>
            </div>
        </footer>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
