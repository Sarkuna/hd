<?php

/* @var $this \yii\web\View */
/* @var $content string */
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\SmileAsset;
use common\widgets\Alert;

use app\components\TopMenuWidget;
use app\components\TopMainMenuWidget;
use app\components\SliderWidget;
use app\components\FooterWidget;

use app\components\H3TopBarWidget;
use app\components\H3HeaderWidget;
use app\components\H3SliderWidget;
use app\components\H3MobileMenu;
use app\components\ProfileNameWidget;
use app\components\ShoppingCartBagWidget;

SmileAsset::register($this);
$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;

if($isHome == true){
    $home= 'header4';
}else{
    $home= '';
}

$imgurl = $this->theme->basePath;

$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$clientname = common\models\ClientAddress::find()->where([
    'clientID' => $session['currentclientID'],
])->one();

$session['currentclientID'] = $myclient->clientID;
$session['currentclientName'] = $clientname->company;
$session['currentLogo'] = $clientname->company_logo;
$session['adminURL'] = $myclient->admin_domain;
$session['membership'] = $myclient->membership;
$session['background'] = $myclient->background_img;
$session['upload_receipt_module'] = $myclient->upload_receipt_module;

if(!empty($session['currentLogo'])){
    $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
}else{
    $img = $imgurl.'images/basic/logo-lite.png';
}

if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    <?php $this->head() ?>
</head>
<body id="home3" class="home3">
    
<?php $this->beginBody() ?>
    
        <div class="body">
            
            <div class="row visible-xs">
                <div class="col-xs-12" style="background: #000000;">
                    <p style="color: #ffffff;padding: 5px 5px 0px 5px;">Welcome <?= ProfileNameWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?></p>
                </div>
                <div class="nav-trigger fa fa-bars"></div>
                <div class="vlogo col-xs-10 col-xs-offset-2 text-center">
                    <a href="<?= Yii::$app->homeUrl ?>"><img src="<?= $img ?>" class="img-responsive" alt=""/></a>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-6" style="padding-left: 1px;">
                        <div class="topcart mobilem">
                            <?= ShoppingCartBagWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                        </div>
                    </div>
                    <div class="tb_right col-xs-6" style="padding-right: 0px;">
                        <ul>
                            <li><i class="fa fa-user"></i> <a href="<?php echo Url::to(['/accounts/my-account']); ?>" >My Account</a></li>
                            <li><i class="fa fa-sign-out"></i> <a href="<?php echo Url::to(['/site/logout']); ?>" data-method="post">Sign out</a></li>
                        </ul>
                    </div>
                </div>



                
            </div>    
            
            <!-- MOBILE MENU -->
        <?= H3MobileMenu::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            
            <!-- TOPBAR -->
        <div class="hidden-xs">
            <?= H3TopBarWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

            <!-- HEADER -->
            <?= H3HeaderWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
        <!-- SLIDER -->
        <?php if($isHome == true){ ?>
        <?= H3SliderWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>    
        <?php } ?>
        
        <?php if($isHome == false){ ?>
        <div class="bcrumbs">
            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class' => 'breadcrumb123'],
                ]) ?>
            </div>
        </div>
        <?php } ?>
        <?= Alert::widget() ?>
        <?= $content ?>
  


            <?= FooterWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <br>
                            <p class="text-center">Copyright (C) 2015 <a href="#">Rewards Solution.</a> All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>	

        </div>


        <div id="backtotop"><i class="fa fa-chevron-up"></i></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
