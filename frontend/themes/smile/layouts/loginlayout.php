<?php
use frontend\assets\LoginHunterdouglasAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginHunterdouglasAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page">
    <?php $this->beginBody() ?>
    
    <div class="fontused">
        <div class="medium-6 columns">
            <div class="titletext">
                <div style="padding:20px;">
                    <h1 style="color:white;font-family:'Roboto';">Inspire & Create</h1>
                    <h4 style="color:white;font-family:'Roboto';">Exclusive designer loyalty program.</h4>
                </div>
            </div>
        </div>

        <div class="medium-4 columns">
            <div class="tran">
                <div class="large-12 columns">
                    <p style="text-align:center;margin-top: 70px;">
                        <?php    
                        if (!empty($session['currentLogo'])) {
                            echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '" width="200">';
                        } else {
                            echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                        }
                        ?>
                    </p>
                </div>

                <!-- Form -->
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
              
        </div>

    </div>	
    <?php $this->endBody() ?>
    
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</body>
</html>
<?php $this->endPage() ?>
