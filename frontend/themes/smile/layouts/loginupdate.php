<?php
use frontend\assets\LoginUpdate;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginUpdate::register($this);
$session = Yii::$app->session;
$baseURL = Yii::$app->request->url;
$session = Yii::$app->session;
//DashbordAdminLTEAsset::register($this);
//AppAsset::register($this);
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style>
    form div.required label.control-label:after {
    content: " * ";
    color: red;
}

  .login-page, .register-page {
    background: #f8f8f8;
}
.login-box, .register-box {
    width: 700px;
    background: #ffffff;
}
.selected-dial-code {
        color: #000000;
    }

.selected-flag {
    display: none;
}
.box{border: none !important;}
</style>
<body class="hold-transition register-page" style="margin-top: 2%;">
    <?php $this->beginBody() ?>
    <div class="register-logo" style="margin-bottom: 0px;">
          <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '" class="img-responsive avatar" style="margin-left: auto;margin-right: auto; max-width:200px;">';                
            }else{
                echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="100" style="margin-left: auto;margin-right: auto;">';
            }
            ?>
        </div>
    <div class="register-box" style="margin: 5% auto;">
        
        
        <div class="row">
            <?= Alert::widget() ?>
                <?= $content ?>
        </div>
    </div>
    
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>