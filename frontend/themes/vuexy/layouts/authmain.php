<?php
use frontend\assets\VuexyLoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

VuexyLoginAsset::register($this);
$session = Yii::$app->session;
if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>

<?php
        if (!empty($session['background'])) {
            $bgimg = Yii::getAlias('@back') . '/upload/client_bg/' . $session['background'];
        } else {
            $bgimg = '/images/hddclogin.jpg';
        }
        ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="vertical-layout vertical-menu-modern 1-column navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <?php $this->beginBody() ?>
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-11 d-flex justify-content-center">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <?= Alert::widget() ?>
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
