<?php

/* @var $this \yii\web\View */
/* @var $content string */
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\VuexyErrorAsset;
use common\widgets\Alert;



VuexyErrorAsset::register($this);

$imgurl = $this->theme->basePath;

$myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();

$clientname = common\models\ClientAddress::find()->where([
    'clientID' => $session['currentclientID'],
])->one();

$session['currentclientID'] = $myclient->clientID;
$session['currentclientName'] = $myclient->company;
//$session['currentLogo'] = $clientname->company_logo;
$session['adminURL'] = $myclient->admin_domain;
//$session['membership'] = $myclient->membership;
$session['background'] = $myclient->background_img;
//$session['upload_receipt_module'] = $myclient->upload_receipt_module;

if(!empty($session['currentLogo'])){
    $img = $session['adminURL']."/upload/client_logos/".$session['currentLogo'];            
}else{
    $img = $imgurl.'images/basic/logo-lite.png';
}

if(!empty($session['currentclientID'])){
   $client = \common\models\Client::findOne($session['currentclientID']);
   $favicon = $client->favicon;
   $programme_title = $client->programme_title;
}else {
   $favicon = 'faviconvip.ico';
   $programme_title = 'VIP';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@back')?>/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>

<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<?php $this->beginBody() ?>
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- error 404 -->
                <section class="row flexbox-container">
                    <div class="col-xl-7 col-md-8 col-12 d-flex justify-content-center">
                        <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
                            <div class="card-content">
                                <div class="card-body text-center">
                                    <?= $content ?>
                                    
                                    <a class="btn btn-primary btn-lg mt-2" href="<?= Yii::$app->homeUrl; ?>">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- error 404 end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

        
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
