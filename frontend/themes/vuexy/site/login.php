<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<style>
    .form-label-group.has-icon-left > label {left: 0px;}
    .help-block-error{
        color: #EA5455;
    }
</style>
<div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
    <p class="text-center"><img src="<?= Yii::$app->VIPglobal->getSiteInfo()['logo'] ?>" class="mt-100" width="300"></p>
    <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/HD_background.jpg" alt="branding logo" >
</div>

<div class="col-lg-6 col-12 p-0">
    <div class="card rounded-0 mb-0 px-2">
        <p class="text-center d-lg-none"><img src="<?= Yii::$app->VIPglobal->getSiteInfo()['logo'] ?>" class="mt-100" width="200"></p>
        <div class="card-header pb-1">
            <div class="card-title">
                <h4 class="mb-0">Login</h4>
            </div>
        </div>
        <p class="px-2">Welcome back, please login to your account.</p>
        <div class="card-content">
            <div class="card-body pt-1">
                <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['novalidate' => '']]); ?>
                    <?=
                    $form->field($model, 'email', [
                        'template' => "<fieldset class='form-label-group form-group position-relative has-icon-left'>{input}<div class='form-control-position'><i class='feather icon-user'></i></div><label for='user-email'>Email</label>{error}</fieldset>"
                    ])->textInput(array('placeholder' => 'Email/Username'));
                    ?>
                
                    <?=
                    $form->field($model, 'password', [
                        'template' => "<fieldset class='form-label-group form-group position-relative has-icon-left'>{input}<div class='form-control-position'><i class='feather icon-lock'></i></div><label for='user-password'>Password</label>{error}</fieldset>"
                    ])->passwordInput(array('placeholder' => 'Password'))->label(false);
                    ?>
                    
                    <div class="form-group d-flex justify-content-between align-items-center">
                        <div class="text-left">
                            <?= $form->field($model, 'rememberMe', [
                                'template' => "<fieldset class='checkbox'><div class='vs-checkbox-con vs-checkbox-primary'>{input}<span class='vs-checkbox'><span class='vs-checkbox--check'><i class='vs-icon feather icon-check'></i></span></span><span class=''>Remember me</span>{error}</div></fieldset>"
                            ])->checkbox(['class' => 'input-checkbox100'],false)
                            ?>
                            
                        </div>
                        <div class="text-right"><a href="/forgot-password" class="card-link">Forgot Password?</a></div>
                    </div>
                    <?php
                    if(Yii::$app->VIPglobal->getSiteInfo()['membership']== 'Y') { ?> 
                    <a href="/register" class="btn btn-outline-primary float-left btn-inline">Register</a>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary float-right btn-inline">Login</button>
                <?php ActiveForm::end(); ?> 
            </div>
        </div>
        <div class="login-footer">
        <?php
        if(Yii::$app->VIPglobal->getSiteInfo()['mobile_app']== 'Y') { ?>        
            <div class="divider">
                <div class="divider-text">Mobile App</div>
            </div>
            <div class="footer-btn d-inline text-center">
                <p class="text-center">
                    <a href="https://itunes.apple.com/my/app/kansai-paint-dealer-rewards-point/id1015991312?mt=8" target="_blank" class="btn btn-primary"><span class="fa fa-apple"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.kansai.madcat&hl=en" target="_blank" class="btn btn-primary"><span class="fa fa-android"></a>
                </p>
            </div>        
        <?php }else { echo '<div class="divider">';}?>
        </div>    
    </div>
</div>

