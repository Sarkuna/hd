<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$img = $img = $this->theme->baseUrl.'/app-assets/images/pages/maintenance-2.png';
//$this->title = $name;
echo '<img src="'.$img.'" alt="branding logo" class="img-fluid align-self-center" alt="branding logo">
      <h1 class="font-large-2 my-1">Under Maintenance!</h1>
      <p class="px-2">We apologize for inconvenience. Please come back later.</p>';
?>
