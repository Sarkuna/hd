<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use common\models\Salutation;
use common\models\EndUserLevel;
use common\models\VIPZone;


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;


?>
<style>
  .form-control{border: 1px solid #E3E3E3 !important;}
  .has-error .form-control{border: 1px solid #ff0000 !important;}
  .form-group.has-error p {
    color: red;
    /* position: absolute; */
    font-size: 12px;
    margin-top: -10px;
}
input::-webkit-input-placeholder {
color: #4c4b4b !important;
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #4c4b4b !important;  
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #4c4b4b !important;  
}
 
input:-ms-input-placeholder {  
color: #4c4b4b !important;  
}
</style>

<div class="row">
    <div class="card card-signup col-lg-6">
        <div class="card-body">
            <div class="logo-container ml-auto mr-auto" style="max-width:200px;">
                <?php
                //echo '<pre>'.print_r($session).die();
                if (!empty($session['currentLogo'])) {
                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                } else {
                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                }
                ?>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'form']); ?>
            <?= $form->errorSummary($model); ?>
            <div class="row">
                <div class="col-lg-6">   
                    <?=
                            $form->field($model, 'otp', [
                                'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                            ])->textInput(['placeholder' => 'OTP'])
                            ?>

                    <?=
                    $form->field($model, 'new_password', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->passwordInput(array('placeholder' => 'New Password'));
                    ?>
                    <?=
                    $form->field($model, 'retype_password', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->passwordInput(array('placeholder' => 'Retype Password'));
                    ?>



                </div>


            </div>


            <div class="card-footer text-center">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-round btn-lg', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?> 
        </div>

    </div>
</div>