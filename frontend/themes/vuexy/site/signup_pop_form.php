<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use common\models\Salutation;
use common\models\EndUserLevel;
use common\models\VIPZone;
use common\models\VIPCustomer;


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

$salutations = Salutation::find()->all();
$salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');

?>

<div class="col-lg-12 col-12 p-0">
    <div class="card rounded-0 mb-0 p-2">
        <div class="card-header pt-50 pb-1">
            <div class="card-title">
                <h4 class="mb-0">Account</h4>
            </div>
        </div>
        <p class="px-2">Finish setting up your account.</p>
        <div class="card-content">
            <div class="card-body pt-0">
                <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['novalidate' => '']]); ?>
                <?=
                    $form->field($model, 'salutation', [
                           // 'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->widget(Select2::classname(), [
                        'data' => $salutationsData,
                        //'language' => 'de',
                        'options' => [
                            'placeholder' => '---Salutation---',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                    <?=
                    $form->field($model, 'full_name', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textInput(array('placeholder' => 'Full Name'))->label();
                    ?>


                    <?=
                    $form->field($model, 'mobile_no', [
                        //'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->widget(PhoneInput::className(), [
                        'jsOptions' => [
                            //'initialCountry' => ['MY'],
                            //'allowExtensions' => true,
                            //'autoHideDialCode' => false,
                            'nationalMode' => false,
                            //'autoHideDialCode' => true,
                            'onlyCountries' => ['my'],
                            'separateDialCode' => false,
                            'allowDropdown' => false,                            
                        ],
                        'options' => ['placeholder' => '1234567890', 'class' => 'form-control'],
                    ]);
                    ?>
                    <?=
                    $form->field($model, 'company_address', [
                        //'template' => "<div class='col-md-4 col-sm-4 col-xs-12 text-right'>{label}</div>\n<div class='col-md-8 col-sm-8 col-xs-12'>{input}\n{hint}\n{error}</div>"
                    ])->textarea(array('rows'=>4,'cols'=>5));
                    ?>
                    <a href="<?php echo Url::to(['/site/logout']); ?>" data-method="post" class="btn btn-outline-primary float-left btn-inline mb-50">Logout</a>
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary float-right btn-inline mb-50', 'name' => 'login-button']) ?>    
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>