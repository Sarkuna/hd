<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use common\models\Salutation;
use common\models\EndUserLevel;
use common\models\VIPZone;
use common\models\VIPCustomer;


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

$salutations = Salutation::find()->all();
$salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');


$yearly_purchases = EndUserLevel::find()
            ->where([
                'clientID' => $session['currentclientID'],
        ])->all();
$yearly_purchase = ArrayHelper::map($yearly_purchases, 'end_user_level_id', 'name');

$VIPZones = VIPZone::find()
            ->where([
                'country_id' => '129',
        ])->all();
$VIPZone = ArrayHelper::map($VIPZones, 'zone_id', 'name');

/*$distributor = VIPCustomer::find()
            ->where([
                'clientID' => $session['currentclientID'],
                'kis_category_id' => '3',
                
        ])->all();*/

$actyps = common\models\TypeName::find()
            ->where([
                'clientID' => $session['currentclientID'],
                'reg_form' => '1',                
        ])->all();

$actypslist = ArrayHelper::map($actyps, 'tier_id', 'name');

$distributor = VIPCustomer::find()->joinWith(['user'])->where(['client_id' => $session['currentclientID'], 'status' => 'A', 'user_type' => 'D', 'cart_user' => 0, 'kis_category_id' => 3])->all();
//echo '<pre>'.print_r($distributor).die;
$distributorlist = ArrayHelper::map($distributor, 'vip_customer_id', 'full_name');
?>
<style>
  .form-control{border: 1px solid #E3E3E3 !important;}
  .has-error .form-control{border: 1px solid #ff0000 !important;}
  .form-group.has-error p {
    color: red;
    /* position: absolute; */
    font-size: 12px;
    margin-top: -10px;
}
input::-webkit-input-placeholder {
color: #4c4b4b !important;
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #4c4b4b !important;  
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #4c4b4b !important;  
}
 
input:-ms-input-placeholder {  
color: #4c4b4b !important;  
}
</style>


<div class="row">
    
                <div class="col-md-4 ml-auto mr-auto">

                </div>
                <div class="col-md-8 mr-auto">

                    <div class="card card-signup">
                        <div class="card-body">
                            <div class="logo-container ml-auto mr-auto" style="max-width:200px;">
                                <?php
                                //echo '<pre>'.print_r($session).die();
                                if (!empty($session['currentLogo'])) {
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                                } else {
                                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                                }
                                ?>
                            </div>
                            <h4 class="card-title text-center">Register</h4>

                            <?php $form = ActiveForm::begin(['id' => 'form']); ?>
                            <?= $form->errorSummary($model); ?>
                            <div class="row">
                            <div class="col-lg-6">   
                            <?=
                            $form->field($model, 'salutation', [
                                'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                            ])->dropDownList(
                                $salutationsData, ['prompt' => 'Salutation', 'id' => 'salutation']
                            );
                            ?>
                            <?=
                                $form->field($model, 'full_name', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->textInput(array('placeholder' => 'Full Name'))->label();
                                ?>

                            <?=
                            $form->field($model, 'mobile_no', [
                                'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                            ])->widget(PhoneInput::className(), [
                                    'jsOptions' => [
                                        'allowExtensions' => true,
                                        'preferredCountries' => ['MY'],
                                        'onlyCountries' => ['MY'],
                                        'nationalMode' => false,
                                        
                                    ],
                                    'options' => ['placeholder' => '+60XXXXXXXXX', 'class' => 'form-control'],
                                ]);
                            ?>
                                
                            
                            
                            <?=
                                $form->field($model, 'password', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->passwordInput(['placeholder' => 'Password'])
                                ?>

                                    
                                
                              
                                
                            </div>
                            <div class="col-lg-6">
                                <?=
                                $form->field($model, 'company_name', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->textInput(['placeholder' => 'Company Name'])
                                ?> 
                                <?=
                                $form->field($model, 'type', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->dropDownList(
                                    $actypslist
                                );
                                ?>
                                
<?=
                            $form->field($model, 'email', [
                                'template' => "<div class='input-group '>{input}</div>\n{hint}\n{error}"
                            ])->textInput(['placeholder' => 'Email'])
                            ?>
                                
                                
<?=
                                $form->field($model, 're_type_password', [
                                    'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                                ])->passwordInput(['placeholder' => 'Re-Password'])
                                ?>
                                
                            </div>
                            
                            </div>
                                

                                <!-- If you want to add a checkbox to this form, uncomment this code -->
                                <?=
                                $form->field($model, 'agree', [
                                    'template' => "<div class=\"form-check\"><label class=\"form-check-label\">{input}<span class=\"form-check-sign\"></span>I agree to the terms and <a href=\"/site/terms\" target=\"_blank\">conditions</a>.</label></div>\n<div class=\"col-lg-12\">{error}</div>",
                                ])->checkbox(['required' => true], false )
                                ?>


                                <div class="card-footer text-center">
                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-round btn-lg', 'name' => 'login-button']) ?>
                                </div>
                            <?php ActiveForm::end(); ?> 
                        </div>
                        <div class="text-center help" style="color: #000000;font-weight:  bold;">Already have a account?<a class="btn white outline sm" href="/site/login">Log in</a></div>
                    </div>
                </div>
            </div>