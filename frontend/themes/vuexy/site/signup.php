<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInput;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use common\models\Salutation;
use common\models\EndUserLevel;
use common\models\VIPZone;
use common\models\VIPCustomer;


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

$salutations = Salutation::find()->all();
$salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');

$typename = \common\models\TypeName::find()->all();
$typenames = ArrayHelper::map($typename,'tier_id','name');

$delercount = common\models\User::find()
    ->where([
        'user_type' => 'B',
    ])->count();

if($delercount > 0) {
    $delers = common\models\UserProfile::find()->joinWith(['user'])->where(['status' => 'A', 'user_type' => 'B'])->all();
    $delerslist = ArrayHelper::map($delers, 'userID', 'company');
    $model->assign_dealer_module = 1;
}else {
    $model->assign_dealer_module = 0;
}

?>
<style>
  .form-control{border: 1px solid #E3E3E3 !important;}
  .has-error .form-control{border: 1px solid #ff0000 !important;}
  .form-group.has-error p {
    color: red;
    /* position: absolute; */
    font-size: 12px;
    margin-top: 0px;
}
input::-webkit-input-placeholder {
color: #4c4b4b !important;
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #4c4b4b !important;  
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #4c4b4b !important;  
}
 
input:-ms-input-placeholder {  
color: #4c4b4b !important;  
}

.intl-tel-input .country-list .country.highlight {
    color: #000 !important;
}
.selected-dial-code {
        color: #000000;
    }
</style>
<!--
<div class="col-lg-6 d-lg-block d-none text-center align-self-center pl-0 pr-3 py-0">
    <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/register.jpg" alt="branding logo">
</div>-->

<div class="col-lg-12 col-12 p-0">
    <div class="card rounded-0 mb-0 p-2">
        <div class="card-header pt-50 pb-1">
            <div class="card-title">
                <h4 class="mb-0">Create Account</h4>
            </div>
        </div>
        <p class="px-2">Fill the below form to create a new account.</p>
        <div class="card-content">
            <div class="card-body pt-0">
                <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['novalidate' => '']]); ?>
                    <?=
                        $form->field($model, 'salutation', [
                                //'template' => "<div class='form-label-group'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->widget(Select2::classname(), [
                            'data' => $salutationsData,
                            //'language' => 'de',
                            'options' => [
                                'placeholder' => '---Salutation---',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                    ?>
                    <?=
                    $form->field($model, 'full_name', [
                        //'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(array('placeholder' => 'Full Name'))->label(false);
                    ?>
                    <?=
                    $form->field($model, 'company_name', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['placeholder' => 'Company Name'])
                    ?>
                    <?=
                    $form->field($model, 'email', [
                        'template' => "<div class='input-group '>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['placeholder' => 'Email'])
                    ?>
                    <?=
                    $form->field($model, 'mobile_no', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                //'allowExtensions' => false,
                                'onlyCountries' => ['my'],
                                //'separateDialCode' => true,
                                //'allowDropdown' => false,

                            ],
                            'options' => ['placeholder' => '+60XXXXXXXXX', 'class' => 'form-control'],
                        ]);
                    ?>
                    <?=
                    $form->field($model, 'password', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->passwordInput(['placeholder' => 'Password'])
                ?>
                <?=
                    $form->field($model, 're_type_password', [
                        'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                    ])->passwordInput(['placeholder' => 'Re-Password'])
                ?>
                <?php if($model->assign_dealer_module == 1) { ?>

                <?=
                    $form->field($model, 'assigndealer', [
                            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->widget(Select2::classname(), [
                        'data' => $delerslist,
                        //'language' => 'de',
                        'options' => [
                            'placeholder' => 'Find your Gallery Dealer',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                ?>
            <?php } ?>
                <?=
                $form->field($model, 'assign_dealer_module', [
                    //'template' => "<div class='input-group'>{input}</div>\n{hint}\n{error}"
                ])->hiddenInput()->label(false);
                ?>
                <?=
                        $form->field($model, 'type', [
                                //'template' => "<div class='form-label-group'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                        ])->widget(Select2::classname(), [
                            'data' => $typenames,
                            //'language' => 'de',
                            'options' => [
                                'placeholder' => 'Select Type of User',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                    ?>
                
                    <div class="form-group row">
                        <div class="col-12">
                            <?= $form->field($model, 'agree', [
                                'template' => "<fieldset class='checkbox'><div class='vs-checkbox-con vs-checkbox-primary'>{input}<span class='vs-checkbox'><span class='vs-checkbox--check'><i class='vs-icon feather icon-check'></i></span></span><span class=''>I accept the terms & conditions.</span></div>{error}</fieldset>"
                            ])->checkbox(['class' => 'input-checkbox100'],false)
                            ?>
                            
                        </div>
                    </div>
                    <a href="/login" class="btn btn-outline-primary float-left btn-inline mb-50">Login</a>
                    <button type="submit" class="btn btn-primary float-right btn-inline mb-50">Register</a>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>


