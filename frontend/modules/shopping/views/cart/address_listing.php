<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
$session = Yii::$app->session;
$addressid = $session['addressid'];
?>

<div class="table-responsive">

    <button href="<?php echo Url::to('/shopping/cart/add-address');?>" type="button" class="btn btn-primary openModal" id="signin">Add New Address</button>
    <br><br>
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>Postcode</th>
                <th>Region / State</th>
                <th>Country</th>                        
            </tr>               

            <?php
            if (count($address) > 0) {
                $n = 1;
                foreach ($address as $myaddress) {
                    if($addressid == $myaddress->vip_customer_address_id){
                        $chk = 'checked';
                    }else {
                        $chk = '';
                    }
                    echo '<tr>
                        <td><input type="radio" name="address_id" id="message_pri" name="message_pri" value="'.$myaddress->vip_customer_address_id.'" '.$chk.' /> </td>
                        <td>' . $myaddress->firstname . ' ' . $myaddress->lastname . '</td>
                        <td>' . $myaddress->address_1 . ' ' . $myaddress->address_2 . '</td>
                        <td>' . $myaddress->city . '</td>
                        <td>' . $myaddress->postcode . '</td>
                        <td>' . $myaddress->states->state_name . '</td>
                        <td>' . $myaddress->country->name . '</td>
                    </tr>';
                    $n++;
                }
            } else {
                echo '<tr><td colspan="8" class="text-center">No record!</td></tr>';
            }
            ?>
        </tbody>
    </table> 
    <?= Html::submitButton(Yii::t('app', 'Cancel'),['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
    <?= Html::submitButton(Yii::t('app', 'Save'),['class' => 'btn btn-success', 'id' => 'myadd']) ?>
</div>

<?php
$script = <<< JS
    $(document).on('click',"#myadd",function(e) {
        var seladdressid = $("input[name='address_id']:checked").val();
        saveAddress(seladdressid);
    });  
JS;
$this->registerJs($script);
?>

<script>
function saveAddress(seladdressid) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/shopping/cart/myaddress"]) ?>',
	  //data: { uid : uid},
          data: 'id='+seladdressid,
	  success: function(data)
            {
                location.reload();
            }
	});
}
</script>