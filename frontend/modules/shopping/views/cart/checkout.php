<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
$carlists = $lisitcart;
$total = 0;
$subtotal = 0;
$cartlistscount = count($carlists);
$session = Yii::$app->session;
use common\models\VipCountry;
use common\models\VIPZone;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => 129])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];


?>
<?php
    $shipping_point = 0;
    $shipping_point_total = 0;
    $shipping_Fee = 0;
    $qtys = 0;
    $stateid = $myaddress->states->zone_id;
    foreach($carlists as $key => $carlist){                            

        $shipingcost = \common\models\VIPProductDeliveryZone::find()
            ->where(['product_id' => $key, 'clientID' => $session['currentclientID'], 'zone_id' => $stateid])
            ->count();
        if($shipingcost > 0) {
            $shipingcost = \common\models\VIPProductDeliveryZone::find()
            ->where(['product_id' => $key, 'clientID' => $session['currentclientID'], 'zone_id' => $stateid])
            ->one();

            $shipping_point = $shipingcost->price / $pointpersent;
            $shipping_point_total = $shipping_point * $carlist['product_qty'];
            $shipping_Fee += $shipping_point_total;

            //$shipping_point = $shipingcost->points / $pointpersent;
            //$shipping_point_total = $shipping_point * $carlist['product_qty'];

        }
        $points_value = $carlist['points_value'] + $shipping_point;
        $total_points = $carlist['points_value']* $carlist['product_qty'];

        $subtotal += $carlist['points_value'];
        $total += $total_points;
        $qtys += $carlist['product_qty'];
        $finalprice = $total + $shipping_Fee;
    }
?>
<style>
    .help-block {
    color: red;
}
</style>
<?php $form = ActiveForm::begin(); ?>
<section id="checkout-payment" class="list-view product-checkout">
    
    <div class="amount-payable checkout-options">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Order Summary</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="detail">
                        <div class="details-title">
                            Point of <?= $qtys ?> items without delivery charges
                        </div>
                        <div class="detail-amt">
                            <strong><?= Yii::$app->formatter->asInteger($total) ?> pts</strong>
                        </div>
                    </div>
                    <div class="detail">
                        <div class="details-title">
                            Delivery Charges
                        </div>
                        <div class="detail-amt">
                            <?= Yii::$app->formatter->asInteger($shipping_Fee) ?> pts
                        </div>
                    </div>
                    <hr>
                    <div class="detail">
                        <div class="details-title">
                            Point Payable
                        </div>
                        <div class="detail-amt total-amt discount-amt"><?= Yii::$app->formatter->asInteger($finalprice) ?> pts</div>
                    </div>
                    <hr>
                    <?php
                    if ($total <= $balancepoints) {
                        echo $form->field($agreeform, 'agree', [
                            'template' => "<fieldset><div class=\"vs-checkbox-con vs-checkbox-primary\">{input}<span class=\"vs-checkbox\"><span class=\"vs-checkbox--check\"><i class=\"vs-icon feather icon-check\"></i></span></span><span class=''> I have read and agree to the <a data-toggle='modal' href='#myModal'>Terms & Conditions</a></span></div> \n{hint}\n{error}</fieldset>",
                        ])->checkbox([], false);
                        
                        echo '<button class="btn btn-primary place-order" type="submit">CONFIRM ORDER</button>';
                    } else {                        
                        echo '<div class="alert alert-info" style="color: #ffffff;">
                        <strong>Sorry!</strong> Your point balance is insufficient to make this order. Please reduce the quantity or change your products.
                      </div>';
                        unset(Yii::$app->session['cart']);
                        echo '<meta http-equiv="refresh" content="3;url=checkout"/>';
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php ActiveForm::end(); ?>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Terms and Conditions</h4>
      </div>
      <div class="modal-body">
        <?= Yii::$app->VIPglobal->myAvailablePoint() ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>