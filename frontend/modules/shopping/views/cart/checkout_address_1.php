<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');


?>

<style>
    /*
 * checkBo lightweight jQuery plugin v0.1.2 by  @ElmahdiMahmoud
 * Licensed under the MIT license - https://github.com/elmahdim/checkbo/blob/master/LICENSE
 *
 * Custom checkbox and radio
 * Author URL: elmahdim.com
 */
.cb-checkbox .cb-inner, .cb-checkbox i {
    width:18px;
    height:18px;
    -moz-border-radius:3px;
    -webkit-border-radius:3px;
    border-radius:3px
}
.cb-checkbox.cb-sm i, .cb-checkbox.cb-sm .cb-inner {
    width:14px;
    height:14px
}
.cb-checkbox.cb-md i, .cb-checkbox.cb-md .cb-inner {
    width:24px;
    height:24px;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px
}
.cb-checkbox.cb-lg i, .cb-checkbox.cb-lg .cb-inner {
    width:30px;
    height:30px;
    -moz-border-radius:6px;
    -webkit-border-radius:6px;
    border-radius:6px
}

.cb-checkbox, .cb-radio {
    padding:3px 0;
    color:inherit;
    cursor:pointer;
    overflow:hidden;
    font-size:inherit;
    font-weight:normal;
    display:inline-block;
    line-height:18px
}
.cb-checkbox.disabled, .cb-checkbox.disabled *, .cb-radio.disabled, .cb-radio.disabled * {
    cursor:default
}
.cb-checkbox input[type="checkbox"], .cb-radio input[type="radio"] {
    display:none
}
.cb-checkbox.disabled, .cb-checkbox.disabled *, .cb-radio.disabled, .cb-radio.disabled * {
    cursor:default
}
.cb-checkbox.disabled {
    color:#ddd
}
.cb-checkbox.disabled .cb-inner {
    color:#ddd
}
.cb-checkbox.disabled:hover .cb-inner {
    border-color:#ddd
}
.cb-checkbox.disabled.checked .cb-inner {
    background-color:#ddd;
    border-color:#ddd
}

.cb-checkbox .cb-inner {
    float:left;
    overflow:hidden;
    margin:0 5px 0 0;
    position:relative;
    background:#f2f2f2;
    display:inline-block;
    border:1px solid #d6d6d6;
    -moz-transition:all 0.5s ease;
    -o-transition:all 0.5s ease;
    -webkit-transition:all 0.5s ease;
    transition:all 0.5s ease
}
.cb-checkbox i {
    top:1px;
    left:2px;
    display:block;
    position:absolute
}
.cb-checkbox i:before, .cb-checkbox i:after {
    height:0;
    width:2px;
    content:"";
    display:block;
    position:absolute;
    background-color:#fff;
    -moz-transition:all 0.2s ease;
    -o-transition:all 0.2s ease;
    -webkit-transition:all 0.2s ease;
    transition:all 0.2s ease
}
.cb-checkbox i:before {
    top:0;
    left:0;
    -moz-transform:rotate(-45deg);
    -ms-transform:rotate(-45deg);
    -webkit-transform:rotate(-45deg);
    transform:rotate(-45deg)
}
.cb-checkbox i:after {
    left:7px;
    bottom:5px;
    -moz-transition-delay:0.3s;
    -o-transition-delay:0.3s;
    -webkit-transition-delay:0.3s;
    transition-delay:0.3s;
    -moz-transform:rotate(30deg);
    -ms-transform:rotate(30deg);
    -webkit-transform:rotate(30deg);
    transform:rotate(30deg)
}

.cb-checkbox.cb-sm, .cb-radio.cb-sm {
    line-height:14px
}
.cb-checkbox.cb-md, .cb-radio.cb-md {
    line-height:24px
}
.cb-checkbox.cb-lg, .cb-radio.cb-lg {
    line-height:30px
}
.cb-checkbox.cb-sm i:before {
    top:4px;
    left:1px
}
.cb-checkbox.cb-sm i:after {
    left:5px
}
.cb-checkbox.cb-md i:before {
    top:10px;
    left:5px
}
.cb-checkbox.cb-md i:after {
    bottom:6px;
    left:11px
}
.cb-checkbox.checked .cb-inner {
    border-color:#3877DC;
    background-color:#3877DC
}
.cb-checkbox.checked.cb-sm i:before {
    top:4px;
    left:1px
}
.cb-checkbox.checked.cb-sm i:after {
    height:9px
}
.cb-checkbox.checked.cb-md i:before {
    top:10px;
    left:4px;
    height:8px
}
.cb-checkbox.checked.cb-md i:after {
    bottom:6px;
    left:11px;
    height:16px
}
.cb-checkbox.checked.cb-lg i:before {
    top:11px;
    left:6px;
    height:12px
}
.cb-checkbox.checked.cb-lg i:after {
    left:14px;
    bottom:7px;
    height:20px
}
.cb-checkbox.checked i:before {
    top:6px;
    left:2px;
    height:6px
}
.cb-checkbox.checked i:after {
    height:12px
}
.cb-radio.checked .cb-inner {
    background:#fff;
    box-shadow:0 0 3px #efefef
}
.cb-radio.checked i {
    -moz-transform:scale(1.1, 1.1);
    -ms-transform:scale(1.1, 1.1);
    -webkit-transform:scale(1.1, 1.1);
    transform:scale(1.1, 1.1);
    background-color:#3877DC
}
.cb-checkbox:hover .cb-inner, .cb-radio:hover .cb-inner {
    border-color:#3877DC
}
.is-hidden {
    display:none !important;
    visibility:hidden !important
}
</style>

<?php
$script = <<< JS
$(document).ready(function(){
  $('form').checkBo();
});
JS;
$this->registerJs($script);
?>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <a href="/accounts/my-account/address" class="btn btn-primary">Add Address</a> 
            </div>
            
            <div class="col-md-12" style="margin-top: 20px;">
                <p>Please kindly select your preferred shipping address based on the options below before placing your order.</p>
                
  
                
                <div class="list-group">
                    <form>
                    <?php
                        $i = 0;
                        foreach($address as $key=>$addresslisit){
                            $fulladdress = $addresslisit->address_1.', '.$addresslisit->address_2.', '.$addresslisit->city.', '.$addresslisit->states->state_name.', '.$addresslisit->postcode.', '.$addresslisit->country->name;
                            echo '<a href="/shopping/cart/myaddress?id='.$addresslisit->vip_customer_address_id.'" class="list-group-item">  
                                <!--Heading with class .list-group-item-heading--> 
                                <label class="cb-checkbox cb-lg">
  <input type="checkbox">
  Checkbox
</label>
                                <h3 class="list-group-item-heading">'.$addresslisit->firstname.' '.$addresslisit->lastname.'</h4>  
                                <!--Paragraph with class .list-group-item-text -->  
                                <h4 class="list-group-item-text">'.$addresslisit->company.'</h4>
                                <p class="list-group-item-text">'.$fulladdress.'</p> 
                            </a>';
                            $i++;
                        }

                    ?>
                    </form>
                </div>    
            </div>

        </div>
    </div>
</div>


<?php
$script = <<< JS
        (function (b, s, t, u) {
    b.fn.checkBo = function (c) {
        c = b.extend({}, {
            checkAllButton: null,
            checkAllTarget: null,
            checkAllTextDefault: null,
            checkAllTextToggle: null
        }, c);
        return this.each(function () {
            function f(a) {
                this.input = a
            }
            function k() {
                var a = b(this).is(":checked");
                b(this).closest("label").toggleClass("checked", a)
            }
            function l(a, b, c) {
                a.parent(g).hasClass("checked") ? a.text(c) : a.text(b)
            }
            function m(a) {
                var c = a.attr("data-show");
                a = a.attr("data-hide");
                b(c).removeClass("is-hidden");
                b(a).addClass("is-hidden")
            }
            var e = b(this),
                g = e.find(".cb-checkbox"),
                h = e.find(".cb-radio"),
                n = g.find("input:checkbox"),
                p = h.find("input:radio");
            n.wrap('<span class="cb-inner"><i></i></span>');
            p.wrap('<span class="cb-inner"><i></i></span>');
            var q = new f("input:checkbox"),
                r = new f("input:radio");
            f.prototype.checkbox = function (a) {
                var b = a.find(this.input).is(":checked");
                a.find(this.input).prop("checked", !b).trigger("change")
            };
            f.prototype.radiobtn = function (a, c) {
                var d = b('input:radio[name="' + c + '"]');
                d.prop("checked", !1);
                d.closest(d.closest(h)).removeClass("checked");
                a.addClass("checked");
                a.find(this.input).get(0).checked = a.hasClass("checked");
                a.find(this.input).change()
            };
            n.on("change", k);
            p.on("change", k);
            g.on("click", function (a) {
                a.preventDefault();
                q.checkbox(b(this));
                a = b(this).attr("data-toggle");
                b(a).toggleClass("is-hidden");
                m(b(this))
            });
            h.on("click", function (a) {
                a.preventDefault();
                r.radiobtn(b(this), b(this).find("input:radio").attr("name"));
                m(b(this))
            });
            if (c.checkAllButton && c.checkAllTarget) {
                var d = b(this);
                d.find(b(c.checkAllButton)).on("click", function () {
                    d.find(c.checkAllTarget).find("input:checkbox").each(function () {
                        d.find(b(c.checkAllButton)).hasClass("checked") ? d.find(c.checkAllTarget).find("input:checkbox").prop("checked", !0).change() : d.find(c.checkAllTarget).find("input:checkbox").prop("checked", !1).change()
                    });
                    l(d.find(b(c.checkAllButton)).find(".toggle-text"), c.checkAllTextDefault, c.checkAllTextToggle)
                });
                d.find(c.checkAllTarget).find(g).on("click", function () {
                    d.find(c.checkAllButton).find("input:checkbox").prop("checked", !1).change().removeClass("checked");
                    l(d.find(b(c.checkAllButton)).find(".toggle-text"), c.checkAllTextDefault, c.checkAllTextToggle)
                })
            }
            e.find('[checked="checked"]').closest("label").addClass("checked");
            e.find("input").is("input:disabled") && e.find("input:disabled").closest("label").off().addClass("disabled")
        })
    }
})(jQuery, window, document);
JS;
$this->registerJs($script);
?>