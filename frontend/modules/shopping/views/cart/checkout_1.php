<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Cart', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
$carlists = $lisitcart;
$total = 0;
$subtotal = 0;
$cartlistscount = count($carlists);
$session = Yii::$app->session;
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => 129])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');





?>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            
            <div class="col-md-3 col-sm-4">
                <div class="contact-info space50">
                    <h5 class="heading space40"><span>Shipping Address</span></h5>
                    <div class="media-list">
                        <div class="media">
                            <i class="pull-left fa fa-user"></i>
                            <div class="media-body">
                                <strong>Name:</strong><br>
                                <?= $myaddress->firstname ?>
                            </div>
                        </div>
                        
                        <div class="media">
                            <i class="pull-left fa fa-bank"></i>
                            <div class="media-body">
                                <strong>Company Name:</strong><br>
                                <?= $myaddress->company ?>
                            </div>
                        </div>
                        
                        <div class="media">
                            <i class="pull-left fa fa-home"></i>
                            <div class="media-body">
                                <strong>Address:</strong><br>
                                <?= $myaddress->address_1 ?> <?= $myaddress->address_2 ?><br>
                                <?= $myaddress->city ?> <?= $myaddress->postcode ?><br>
                                <?= $myaddress->states->state_name ?> <?= $myaddress->country->name ?>
                            </div>
                        </div>
                        
                        


                    </div>

                </div>
            </div>

            <div class="col-md-9 col-sm-8">
                <!-- HTML -->
                <?php $form = ActiveForm::begin(); ?>
                <div id="account-id">
                    <h5 class="heading space40"><span>Your Order Details</span></h5>
                    <div class="account-form">
                        <div class="cart-infovip">
                        <?php                    

                        echo '<table class="table table-bordered" id="cart-table"><tr>
                            <th>Product Image</th>
                            <th class="text-left">Product Name</th>
                            <th class="text-center">Product Code</th>
                            <th width="10%" class="text-center">Quantity</th>
                            <th class="text-center">Unit Point</th>
                            <th class="text-center">Total Points</th>
                        </tr>';
                        foreach($carlists as $key => $carlist){
                            $shipping_point = 0;
                            $shipping_point_total = 0;
                            
                            $shipingcost = \common\models\VIPProductDeliveryZone::find()
                                ->where(['product_id' => $key,'zone_id' => $myaddress->zone_id])
                                ->count();
                            if($shipingcost > 0) {
                                $shipingcost = \common\models\VIPProductDeliveryZone::find()
                                ->where(['product_id' => $key,'zone_id' => $myaddress->zone_id])
                                ->one();
                                
                                $shipping_point = $shipingcost->points;
                                $shipping_point_total = $shipingcost->points * $carlist['product_qty'];
                                
                            }
                            $points_value = $carlist['points_value'] + $shipping_point;
                            $total_points = $carlist['points_value']* $carlist['product_qty'] + $shipping_point_total;
                            echo '<tr id="remove_'.$key.'">
                                <td><img src="'.Yii::getAlias('@back').'/upload/product_cover/thumbnail/'.$carlist['main_image'].'" class="img-responsive" alt="" style="width: 75px;"/></td>
                                <td style="text-align: left;">
                                    <h4><a href="/products/product/product-detail?id='.$key.'">'.$carlist['product_name'].'</a></h4>
                                    Delivery Charge:'.$shipping_point_total.'pts  
                                    '.$carlist['opttionlisititem'].'
                                </td>
                                <td class="text-center">'.$carlist['product_code'].'</td>
                                <td class="text-center">'.$carlist['product_qty'].'</td>    
                                <td class="text-right">'.$points_value.' pts</td>
                                <td class="text-right">'.$total_points.' pts</td>
                            </tr>';
                            $subtotal += $carlist['points_value'];
                            $total += $total_points;
                        }
                        echo '<tr><td colspan="5"></td>
                            <td class="a-right text-right">'.$total.' pts</td></tr>';
                        echo '</table>';
                        
                        
                    
                    ?>
                            
                       
                           <?php
                           if($total <= $balancepoints) {
                               echo '<div class="row">';
                               echo $form->field($agreeform, 'comment', [
                                    'template' => "<div class='col-md-12 col-sm-6'>{label}{input}\n{hint}\n{error}</div>"
                                ])->textarea(['rows' => '6']);
                               echo $form->field($agreeform, 'agree', [
                                    'template' => "<div class='col-md-12 col-sm-6'>{input} I have read and agree to the <a data-toggle='modal' href='#myModal'>Terms & Conditions</a> \n{hint}\n{error}</div>",
                                ])->checkbox([],false);
                                echo '</div>';
                               echo '<div class="details-box">';
                               echo '<button class="btn-black" type="submit">Confirm Order</button>';
                               echo '</div>';
                           }else {
                               echo '<div class="alert alert-info" style="color: #ffffff;">
                                    <strong>Sorry!</strong> Your point balance is insufficient to make this order.
                                  </div>';
                           }
                           ?>
                            
                        
                        </div>
                    </div>
                    
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= Html::encode($tc->page_title) ?></h4>
      </div>
      <div class="modal-body">
        <?= $tc->page_description ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>