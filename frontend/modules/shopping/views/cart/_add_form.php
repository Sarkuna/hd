<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['status' => '0'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

if(!($model->isNewRecord)) {
    $cid = $model->country_id;
}else {
    $cid = '129';
}
$regionlists = \common\models\VIPStates::find()
        ->where(['country_id' => $cid])
        ->orderBy([
    'state_name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'states_id', 'state_name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');

?>

<div class="jobeducation-form">
    <?php $form = ActiveForm::begin([
//    'beforeSubmit' => 'window.forms.candidate',
    //'enableClientValidation' => true,
    //'enableAjaxValidation' => true,
    'id' => 'someform',
    //'action'=>'/shopping/cart/add-address', 
]); ?>
    <div class="row">
     <ul class="form-list">
        <?=
        $form->field($model, 'firstname', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        <?=
        $form->field($model, 'lastname', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        <?=
        $form->field($model, 'company', [
            'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'address_1', [
            'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'address_2', [
            'template' => "<li class='col-md-12 col-sm-12'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'postcode', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'city', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->textInput(array('placeholder' => ''));
        ?>
        
        <?=
        $form->field($model, 'country_id', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->dropDownList($country, [
            'prompt' => 'Select...',
            'onchange' => '$.get( "' . Url::toRoute('/accounts/my-account/region') . '", {id: $(this).val() } )
                .done(function( data ) {
                   $("#vipcustomeraddress-state").html(data);
                }
            );'
          ]);
        ?>
        
        <?=
        $form->field($model, 'state', [
            'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
        ])->dropDownList($regionlist, ['prompt' => 'Select...']);
        ?>       
    </ul>
    </div>
    <div class="buttons-set">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
