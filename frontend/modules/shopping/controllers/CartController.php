<?php

//namespace app\modules\support\controllers;
namespace app\modules\shopping\controllers;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;



use common\libs\Cart;
use yii\web\Session;

use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\VIPOrderProduct;
use common\models\VIPCustomerReward;
use common\models\VIPOrderOption;
use common\models\VIPProductOptionValue;
use common\models\VIPCustomerAddress;


/**
 * Default controller for the `support` module
 */
class CartController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/vuexy/layouts/main_checkout';
        $session = Yii::$app->session;
        $cart = Yii::$app->session['cart'];
        $address = \common\models\VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->count();
        if($address > 0) {
            $addressdefaultcount = \common\models\VIPCustomerAddress::find()->where(['default_ID' => 1,'userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->count();
            if($addressdefaultcount == 0) {
                return $this->redirect(['/addressbook']);
            }else {
                $addressdefault = \common\models\VIPCustomerAddress::find()->where(['default_ID' => 1,'userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->one();
                return $this->render('index',['lisitcart' => $cart, 'addressdefault' => $addressdefault]);
            }            
        }else {
            return $this->redirect(['/addressbook/add-address']);
        }
    }
    
    /*public function actionAddressListing()
    {
        $session = Yii::$app->session;
        $address = \common\models\VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])->all();
        return $this->renderAjax('address_listing', [
            'address' => $address,
        ]);
    }*/
    
    
    /*public function actionAddAddress()
    {
        $session = Yii::$app->session;
        $model = new \common\models\VIPCustomerAddress();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
                //return $this->redirect(['view', 'id' => $model->vip_customer_address_id]);
                $model->userID = Yii::$app->user->id;
                $model->clientID = $session['currentclientID'];

                $states = \common\models\VIPStates::find()->where(['states_id' => $model->state])->one();
                $model->zone_id = $states->zone_id;
                if($model->save()){
                    $session['addressid'] = $model->vip_customer_address_id;
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified shipping address!']);
                    return $this->redirect(['/shopping/cart']);
                }else {
                    print_r($model->getErrors());
                }
        }  else {
            return $this->renderAjax('_add_form', [
                'model' => $model,
            ]);
        }
    }*/
    
    /*public function actionMyaddress($id)
    {
        $session = Yii::$app->session;
        $address = \common\models\VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID'], 'vip_customer_address_id' => $id])->count();

        if($address > 0) {
            $session = Yii::$app->session;
            $session['addressid'] = $id;
            //return $this->redirect(['/shopping/cart/checkout']);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
    }*/
    
    public function actionCheckout()
    {
        $clientsetup = Yii::$app->VIPglobal->clientSetup();
        $pointpersent = $clientsetup['point_value'];
        $session = Yii::$app->session;
        $selectoptionvalue = '';
        $cart = Yii::$app->session['cart'];
        if(empty($cart)) {
            return $this->redirect(['index']);
        }

        $model = new VIPOrder();
        $agreeform = new \common\models\AgreeForm();
        
        //$balancepoints = $this->TotalAvailablePoint();
        $balancepoints = Yii::$app->VIPglobal->myAvailablePoint();

        $myaddress = \common\models\VIPCustomerAddress::find()
            ->where(['userID' => Yii::$app->user->id, 'default_ID' => 1])
            ->count();
            
        if($myaddress > 0) {
            $myaddress = \common\models\VIPCustomerAddress::find()
            ->where(['userID' => Yii::$app->user->id, 'default_ID' => 1])
            ->one();
            $stateid = $myaddress->states->zone_id;

            $model->shipping_firstname = $myaddress->firstname;
            $model->shipping_lastname = $myaddress->lastname;
            $model->shipping_company = $myaddress->company;
            $model->shipping_address_1 = $myaddress->address_1;
            $model->shipping_address_2 = $myaddress->address_2;
            $model->shipping_city = $myaddress->city;
            $model->shipping_postcode = $myaddress->postcode;
            $model->shipping_country_id = $myaddress->country->name;
            $model->shipping_zone = $myaddress->states->state_name;
            $model->shipping_zone_id = $myaddress->states->zone->name;
        }else {
            return $this->redirect(['/addressbook']);
        }
        
        $total = 0;
        $model->order_status_id = 10;
        if ($agreeform->load(Yii::$app->request->post()) && $agreeform->validate()) {
            if(empty($cart)) {
                return $this->redirect(['index']);
            }
            $model->load(Yii::$app->request->post());
            $prefix = \common\models\ClientOption::find()->where(['clientID' => $session['currentclientID']])->one();
            $globalprefix = VIPOrder::find()->select('invoice_no')->orderBy(['invoice_no' => SORT_DESC])->one();
            $data=VIPOrder::find()->select('invoice_no')->where(['clientID' => $session['currentclientID']])->orderBy(['invoice_no' => SORT_DESC])->one();
            if(!empty($data)){
                $invoice_no = str_pad(++$data->invoice_no,5,'0',STR_PAD_LEFT);
            }else {
                $invoice_no = '00001';
            }

            $userAgent = \xj\ua\UserAgent::model();
            $platform = $userAgent->platform;
            $browser = $userAgent->browser;
            $version = $userAgent->version;
            
            $model->invoice_no = $invoice_no;
            //$model->invoice_prefix = $prefix->invoice_prefix.'-'.$invoice_no;
            $model->global_order_ID = Yii::$app->VIPglobal->clientPrefix().$invoice_no.date('dmy');
            $model->clientID = $session['currentclientID'];
            $model->customer_id = Yii::$app->user->id;
            $model->order_status_id = 10;
            $model->comment = '';
            
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->user_agent = $platform.'-'.$browser.'-'.$version;
            $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            $shipping_Fee = 0;
                            foreach ($cart as $key => $carlist) {

                                $shipping_point = 0;
                                $shipping_price = 0;
                                $shipping_point_total = 0;
                                $shipping_price_total = 0;

                                $shipingcost = \common\models\VIPProductDeliveryZone::find()
                                    ->where(['product_id' => $key,'zone_id' => $stateid])
                                    ->count();
                                if($shipingcost > 0) {
                                    $shipingcost = \common\models\VIPProductDeliveryZone::find()
                                    ->where(['product_id' => $key,'zone_id' => $stateid])
                                    ->one();

                                    //$shipping_point = $shipingcost->points / $pointpersent;
                                    //$shipping_point_total = $shipping_point * $carlist['product_qty'];
                                    
                                    $shipping_price = $shipingcost->price;
                                    $shipping_price_total = $shipping_price * $carlist['product_qty'];
                                    
                                    $shipping_point = $shipingcost->price / $pointpersent;
                                    $shipping_point_total = $shipping_point * $carlist['product_qty'];
                                    $shipping_Fee += $shipping_point_total;
                                }
                                $total_points = $carlist['points_value']* $carlist['product_qty'];
                                
                                $addproduct = new VIPOrderProduct();
                                $addproduct->order_id = $model->order_id;
                                $addproduct->product_id = $key;
                                $addproduct->name = $carlist['product_name'];
                                $addproduct->model = $carlist['product_code'];
                                $addproduct->quantity = $carlist['product_qty'];
                                $addproduct->point = $carlist['points_value'];
                                $addproduct->point_total = $total_points;
                                $addproduct->price = $carlist['price'];
                                $addproduct->total = $carlist['price']* $carlist['product_qty'];
                                $addproduct->shipping_price = $shipping_price;
                                $addproduct->shipping_price_total = $shipping_price_total;
                                $addproduct->shipping_point = $shipping_point;
                                $addproduct->shipping_point_total = $shipping_Fee;
                                $addproduct->product_status = 10;
                                
                                if (($flag = $addproduct->save(false)) === false) {
                                    $transaction->rollBack();
                                    break;
                                }
                                
                                //$product_total = $carlist['points_value']* $carlist['product_qty'];
                                
                                $total += $total_points + $shipping_point_total;
                            }
                            
                                $customerReward = new VIPCustomerReward();
                                $customerReward->clientID = $session['currentclientID'];
                                $customerReward->customer_id = Yii::$app->user->id;        
                                $customerReward->order_id = $model->order_id;
                                $customerReward->description = $model->invoice_prefix;
                                $customerReward->points = '-'.$total;
                                $customerReward->bb_type = 'V';
                                $customerReward->date_added = date('Y-m-d');
                                if (($flag = $customerReward->save(false)) === false) {        
                                    $transaction->rollBack();
                                }
                                
                                $history = new \common\models\VIPOrderHistory();
                                $history->order_id = $model->order_id;
                                $history->order_status_id = 10;
                                $history->comment = 'New Order# '.$model->invoice_prefix;
                                $history->date_added = date('Y-m-d');
                                $history->type = 1;
                                if (($flag = $history->save(false)) === false) {        
                                    $transaction->rollBack();
                                }
                                $last_point_balance = $balancepoints - $total;
                                //$last_point_balance = $balancepoints - $total;
                                $lastorder = VIPOrder::find()->where(['order_id' => $model->order_id])->one();
                                $lastorder->last_point_balance = $last_point_balance;
                                $lastorder->save(false);                               
                                
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        unset(Yii::$app->session['cart']);
                        unset(Yii::$app->session['addressid']);
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Thank you', 'text' => 'Thank you for order.']);
                        $data = '';
                        $code = 'order.status_'.$model->order_status_id;
                        $returnedValue = Yii::$app->VIPglobal->sendEmailNewOrder($model->order_id, Yii::$app->params[$code], $data);
                        return $this->redirect(['/accounts/my-account/view', 'id' => $model->order_id]);
                    }else {
                        \Yii::$app->getSession()->setFlash('error',['title' => 'Sorry', 'text' => 'Thank you for order.']);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            //$model->accept_language
            //$model->save()
        }else {
            return $this->render('checkout',['lisitcart' => $cart,'model' => $model, 'agreeform' => $agreeform, 'myaddress' => $myaddress, 'balancepoints' => $balancepoints]);
        }
    }
    
    
    function actionAddCart($id,$qty,$selectoptionvalue=null){
        $cart = new Cart();
        $cart->addCart($id,$qty,$selectoptionvalue);
    }
    
    function actionUpdateCartQty($id,$qty){
        $cart = new Cart();
        $cart->UpdateCartQty($id,$qty);
        //$this->renderPartial('index',['lisitcart' => $cart]);
    }
    
    function actionRemoveCart($id){
        $cart = new Cart();
        $cart->RemoveCart($id);
        //return $this->renderPartial('index',['lisitcart' => $cart]);
    }
    
    function TotalAvailablePoint(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         
        $balance = $totaladd - $totalminus - $expired;
        
        return $balance;
    }
    
    /*function actionGetaddress($id){
        $session = Yii::$app->session;
        $oldaddressdisplay = \common\models\VIPCustomerAddress::find()
        ->where(['vip_customer_address_id' => $id,'userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])
        ->orderBy([
            'vip_customer_address_id' => SORT_ASC,
        ])->one();
        
        if (count($oldaddressdisplay) > 0) {
            $response['shipping_firstname'] = $oldaddressdisplay->firstname;
            $response['shipping_lastname'] = $oldaddressdisplay->lastname;
            $response['shipping_company'] = $oldaddressdisplay->company;
            $response['shipping_address_1'] = $oldaddressdisplay->address_1;
            $response['shipping_address_2'] = $oldaddressdisplay->address_2;
            $response['shipping_city'] = $oldaddressdisplay->city;
            $response['shipping_postcode'] = $oldaddressdisplay->postcode;
            $response['shipping_country_id'] = $oldaddressdisplay->country_id;
            $response['shipping_country_id_text'] = $oldaddressdisplay->country->name;
            $response['shipping_zone'] = $oldaddressdisplay->state;
            $response['shipping_zone_text'] = $oldaddressdisplay->states->name;
            $response['shipping_zone_id'] = $oldaddressdisplay->zone_id;
            $response['shipping_zone_id_text'] = $oldaddressdisplay->delivery->name;
        }else{
           $response['shipping_firstname'] = "";
            $response['shipping_lastname'] = "";
            $response['shipping_company'] = "";
            $response['shipping_address_1'] = "";
            $response['shipping_address_2'] = "";
            $response['shipping_city'] = "";
            $response['shipping_postcode'] = "";
            $response['shipping_country_id'] = "";
            $response['shipping_country_id_text'] = "";
            $response['shipping_zone'] = "";
            $response['shipping_zone_text'] = "";
            $response['shipping_zone_id'] = "";
            $response['shipping_zone_id_text'] = ""; 
        }
        echo json_encode($response);
    }*/
    
    function actionRegion($id){
        $states = \common\models\VIPStates::find()->where(['country_id' => $id])->all();
        if ($states) {
            //echo $sle;
            $id = 1;
            //echo '<select>';
            foreach ($states as $state) {
                //echo '<li class="selectboxit-option">'.$state->state_name.'</li>';
                echo "<option value='" . $state->states_id . "'>" . $state->state_name . "</option>";
                /*echo '<li data-id="'.$id.'" data-val="'.$state->states_id.'" data-disabled="false" class="selectboxit-option" role="option">
                        <a class="selectboxit-option-anchor">
                                <span class="selectboxit-option-icon-container">
                                        <i class="selectboxit-option-icon  selectboxit-container"></i>
                                </span>'.$state->state_name.'
                        </a>
                </li>';*/
                $id++;
            }
            //echo '</select>';
        } else {
            echo "<option>-</option>";
        }
    }
    
    function actionAddressDefault($id){
        VIPCustomerAddress::updateAll(['default_ID' => 0], ['userID' => Yii::$app->user->id]);
        $address = \common\models\VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'vip_customer_address_id' => $id])->one();
        $address->default_ID = 1;
        $address->save(false);
        $cart = Yii::$app->session['cart'];
        if(empty($cart)) {
            $response = 1;
        }else {
            $response = 0;
        }
        
        echo json_encode($response);

    }

}
