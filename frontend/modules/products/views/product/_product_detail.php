<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$this->title = $model->product_code;
$this->params['breadcrumbs'][] = ['label' => 'Rewards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .description img{width:100%}
</style>

<section class="app-ecommerce-details">
    <div class="card">
        <div class="card-body">

            <div class="row mb-5 mt-2">
                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                    <div class="owl-carousel prod-slider sync1">
                            <?php
                            //$uploadFiles   = \common\models\Uploads::find()->where(['ref'=>$model->ref])->all();
                            $bigimages = $model->images;
                            foreach ($bigimages as $bigimage) {
                                //echo '<div class="item"> <img src="'.Yii::getAlias('@back').'/upload/photolibrarys/'.$model->ref.'/thumbnail/'.$bigimage->real_filename.'" alt=""> </div>';
                                echo '<div class="item"> 
                                            <img src="' . Yii::getAlias('@back') . '/upload/photolibrarys/' . $model->ref . '/thumbnail/' . $bigimage->real_filename . '" alt="">
                                            <a href="' . Yii::getAlias('@back') . '/upload/photolibrarys/' . $model->ref . '/' . $bigimage->real_filename . '" title="Product" class="caption-link"></a>
                                        </div>';
                            }
                            ?>

                        </div>

                </div>
                <div class="col-12 col-md-6">
                    <h5><?= $model->product_name ?></h5>
                    <p class="text-muted">by <?= $model->manufacturer_id === null ? 'N\A' : $model->manufacturer->name ?></p>
                    <div class="ecommerce-details-price d-flex flex-wrap">

                        <p class="text-primary font-medium-3 mr-1 mb-0"><?= Yii::$app->VIPglobal->myPoint($model->product_id) ?> <em>- pts</em></p>
                    </div>
                    <hr>
                    <p>Product Code - <span class="text-success"><?= $model->product_code ?></span></p>
                    <p>Available - <span class="text-success"><?= $model->stockStatus->name ?></span></p>
                    <p class="font-weight-bold mb-25"> <i class="feather icon-truck mr-50 font-medium-2"></i>Shipping
                    </p>
                    <hr>
                    <div class="form-group">
                        
                        <?php
                            $clientsetup = Yii::$app->VIPglobal->clientSetup();
                            $pointpersent = $clientsetup['point_value'];
                            $deleerycost = $profile = \common\models\VIPProductDeliveryZone::find()->where(['product_id' => $model->product_id, 'clientID' => $clientID])->count();
                            if($deleerycost > 0) {
                                $deleerycostlists = $profile = \common\models\VIPProductDeliveryZone::find()
                                        ->where(['product_id' => $model->product_id, 'clientID' => $clientID])
                                        ->andWhere(['>','price',0])       
                                        ->all();
                                echo '<ul class="list-group list-group-flush">';
                                //echo '<tr><td>Zone</td><td>Points</td></tr>';
                                foreach($deleerycostlists as $deleerycostlist) {
                                    $zone = \common\models\VIPZone::find()->where(['zone_id' => $deleerycostlist->zone_id])->one();
                                    if(!empty($deleerycostlist->price)) {
                                        $newpoint = $deleerycostlist->price / $pointpersent;
                                    }else {
                                        $newpoint = 0;
                                    }
                                    echo '<li class="list-group-item">
                                        <span class="badge badge-pill bg-info float-right">'.Yii::$app->formatter->asInteger($newpoint).' pts</span>
                                        '.$zone->name.'</li>';
                                    //echo '<tr><td>'.$zone->name.'</td><td>'.Yii::$app->formatter->asInteger($newpoint).'pts</td></tr>';
                                }
                                echo '</ul>';
                            }
                        ?>
                    </div>
                    <hr>
                    

                    <div>
                        <?php 
                                        if(Yii::$app->VIPglobal->myAvailablePoint() > 0) {
                                        ?>
                                        <div class="item-quantity">
                                            <p class="quantity-title">Quantity</p>
                                            <div class="input-group quantity-counter-wrapper">
                                                <input type="text" class="quantity-counter" value="1" id="myqty">
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row">
                                        <input type="hidden" value="0" id="aboption">
                                        <button class="addtobag btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light" onclick="addCart(<?= $model->product_id ?>,1);"><i class="feather icon-shopping-cart mr-25"></i>ADD TO CART</button>
                                        </div>
                                        <?php } else {
                                            echo '<div class="alert alert-info" style="color: #ffffff;">
                        <strong>Sorry!</strong> Your point balance is insufficient to make this order.
                      </div>';
                                        } ?>
                                        <?php
                                        if(!empty($model->remarks))
                                        {
                                            echo '<div class="space20"></div>
                                            <div class="sep"></div>
                                            <h3>Remarks</h3>';
                                            echo '<p>'.$model->remarks.'</p>';
                                        }
                                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Product Details</h4>
        </div>
        <div class="card-content">
            <div class="card-body description">
                <div class="col-lg-12">
                    <?= $model->product_description ?>
                </div>                
            </div>
        </div>
    </div>
</section>
