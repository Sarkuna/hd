<?php

use yii\helpers\Html;
use yii\helpers\Url;
//$productdescription = strip_tags($model->product_description);
$point = Yii::$app->VIPglobal->myPoint($model->product->product_id);
?>
<div class="card ecommerce-card">
    <div class="card-content">
        <div class="item-img text-center">
            <img class="img-fluid" src="<?= Yii::getAlias('@back') ?>/upload/product_cover/thumbnail/<?= $model->product->main_image ?>" alt="img-placeholder">
        </div>
        <div class="card-body">
            <div class="item-wrapper">
                <div>
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>
            <div class="item-name">
                <span>
                <?= Html::a($model->product->product_name, ['/products/product/product-detail', 'id' => $model->product->product_id,])?>
                </span>    
            </div>
            <div>

            </div>
        </div>
        <div class="item-options text-center">
            <div class="item-wrapper">
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>

            <div class="cart cartlist" onclick="addCart(<?= $model->product->product_id ?>,1);">
                <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span> <a href="#" class="view-in-cart d-none">View In Cart</a>
            </div>
        </div>
    </div>
</div>
                        

                        
                        