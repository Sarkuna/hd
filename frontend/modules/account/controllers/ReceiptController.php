<?php

namespace app\modules\account\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use common\models\VIPReceipt;
use common\models\VIPReceiptSearch;
use common\models\BBPoints;
use common\models\VIPReceiptList;
use common\models\ResellerList;
use common\models\ReceiptHistory;
use common\models\VIPTempReceipt;
use common\models\VIPCustomerReward;
use common\models\ClientOption;

/**
 * ReceiptController implements the CRUD actions for VIPReceipt model.
 */
class ReceiptController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPReceipt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $searchModel = new VIPReceiptSearch();
        $searchModel->clientID = $session['currentclientID'];
        $searchModel->userID = Yii::$app->user->id;
        $dataProvider = $searchModel->searchuser(Yii::$app->request->queryParams);
        //$dataProvider->pagination->pageSize = 10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSubmitInvoices()
    {
        $this->layout = '@app/themes/vuexy/layouts/main_file_upload';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = new \common\models\UploadInvoice(); 
        $filegroup = Yii::$app->security->generateRandomString();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $path = Yii::getAlias('@backend/web') .'/upload/upload_invoice/';
            $paththumb = Yii::getAlias('@backend/web') .'/upload/upload_invoice/thumb/';
            $imagefiles = UploadedFile::getInstances($model, 'image');

            $VIPReceipt = new \common\models\VIPReceipt();
            
            $data = \common\models\VIPReceipt::find()->select('running_no')
                    ->where(['clientID' => $clientID])
                    ->orderBy(['running_no' => SORT_DESC])->one();
            if(!empty($data)){
                $invoice_no = str_pad(++$data->running_no,6,'0',STR_PAD_LEFT);
                $VIPReceipt->running_no = $invoice_no;
            }else {
                $VIPReceipt->running_no = '000001';
            }
            
            if(!empty($model->date_of_receipt)) {
                $date_of_receipt = date('Y-m-d', strtotime($model->date_of_receipt));
            }else {
                $date_of_receipt = '';
            }
            
            $VIPReceipt->reseller_name = 0;
            $VIPReceipt->invoice_no = $model->invoice_no;
            $VIPReceipt->clientID = $clientID;
            $VIPReceipt->userID = Yii::$app->user->id;
            $VIPReceipt->date_of_receipt = $date_of_receipt;
            $VIPReceipt->order_num = $this->clientOption().'-'.$VIPReceipt->running_no;

            if($VIPReceipt->save()) {
                $lastinsid = $VIPReceipt->vip_receipt_id;
                
                $receipthistory = new ReceiptHistory();
                $receipthistory->vip_receipt_id = $lastinsid;
                $receipthistory->vip_receipt_status = 'P';
                $receipthistory->notify = '0';
                $receipthistory->comment = 'new receipt created';
                $receipthistory->date_added = date('Y-m-d H:i:s');
                $receipthistory->save(false);
                
                if (!is_null($imagefiles)) {
                    foreach ($imagefiles as $file) {
                        $productimage = new \common\models\VIPUploadInvoice();
                        $new_name = $clientID.'_'.Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(). '.' . $file->extension;

                        $productimage->userID = Yii::$app->user->id;
                        $productimage->clientID = $clientID;
                        $productimage->file_group = $filegroup;
                        $productimage->upload_file_name = $file->baseName . '.' . $file->extension;
                        $productimage->upload_file_new_name = $new_name;
                        $productimage->add_date = date('Y-m-d H:i:s');
                        $productimage->vip_receipt_id = $lastinsid;

                        if ($productimage->save(false)) {
                            $file->saveAs($path . $new_name);
                            //$imagine = Image::getImagine();
                            //$image = $imagine->open($path . $new_name);
                            //$image->resize(new Box(200, 300))->save($paththumb . $new_name, ['quality' => 100]);
                        }else {
                            echo '<pre>';
                            print_r($productimage->getErrors());
                            die;
                        }
                    }                
                }               
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'The file[s] has been successfully uploaded!']);
                return $this->redirect(['/receipts-history']);
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Upload Error. File could not be uploaded.']);
                return $this->redirect(['/submit-invoices']);
                /*echo '<pre>';
                print_r($VIPReceipt->getErrors());
                die;*/
            }
            echo '<pre>';
                print_r($model->getErrors());
                die;
        }else {
            return $this->render('_form_upload', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Displays a single VIPReceipt model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$this->layout = '/main_model';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionDstate($id){
        $dealerlists = ResellerList::find()
                ->where(['reseller_category_id' => $id])
                ->all();
        if ($dealerlists) {
            //echo $sle;
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->reseller_list_id . "'>" . $dealerlist->reseller_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }



    /**
     * Finds the VIPReceipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPReceipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPReceipt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findTempReceipt($id)
    {
        if (($model = \common\models\VIPTempReceipt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function clientOption()
    {
        $clientID = Yii::$app->user->identity->client_id;

        $rd = ClientOption::find()->where(['clientID' => $clientID])->one();
        return $rd->receipts_prefix;

    }
}
