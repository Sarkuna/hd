<?php

namespace app\modules\account\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Eventviva\ImageResize;

use common\models\Projects;
use common\models\ProjectsSearch;
use common\models\Uploads;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $session = Yii::$app->session;

        if (parent::beforeAction($action)) {
            
            if (in_array($action->id, array('view'))) {
                if (!empty($_GET['id'])) {
                    $numRows = Projects::find()
                            ->where([
                                'userID' => Yii::$app->user->id,
                                'projects_id' => $_GET['id']
                            ])
                            ->count();
                    if ($numRows == 0)
                        throw new ForbiddenHttpException('You are trying to view or modify restricted record.', 403);
                }
            }
            return true;
        } else {
            return false;
        }

    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new ProjectsSearch();
        $searchModel->userID = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($model->ref);
        if ($model->load(Yii::$app->request->post())) {
            $this->Uploads(false);
            $model->save();
            return $this->redirect(['view', 'id' => $model->projects_id]);
        } else {
            if(empty($model->ref)){
                $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
            }
            return $this->render('update', [
                'model' => $model,
                'initialPreview'=>$initialPreview,
                'initialPreviewConfig'=>$initialPreviewConfig,
            ]);
        }
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    /*|*********************************************************************************|
  |================================ Upload Ajax ====================================|
  |*********************************************************************************|*/
    public function actionUploadajax(){
           $this->Uploads(true);
     }
    private function CreateDir($folderName){
        if($folderName != NULL){
            $basePath = Projects::getUploadPath();
            if(FileHelper::createDirectory($basePath.$folderName,0777)){
                FileHelper::createDirectory($basePath.$folderName.'/thumbnail',0777);
            }
        }
        return;
    }
    private function removeUploadDir($dir){
        FileHelper::removeDirectory(Projects::getUploadPath().$dir);
    }
    private function Uploads($isAjax=false) {
             if (Yii::$app->request->isPost) {
                $images = UploadedFile::getInstancesByName('upload_ajax');
                if ($images) {
                    
                    if($isAjax===true){
                        $ref =Yii::$app->request->post('ref');
                    }else{
                        $PhotoLibrary = Yii::$app->request->post('Projects');
                        $ref = $PhotoLibrary['ref'];
                    }
                    $this->CreateDir($ref);
                    foreach ($images as $file){
                        
                        $fileName       = $file->baseName . '.' . $file->extension;
                        $realFileName   = md5($file->baseName.time()) . '.' . $file->extension;
                        $savePath       = Projects::getUploadPath().$ref.'/'. $realFileName;
                        $savePath2       = 'upload/projects_gallery/'.$ref.'/'. $realFileName;
    
                        if($file->saveAs($savePath)){
                            if($this->isImage(Yii::getAlias('@back').'/'.$savePath2)){
                                //var_dump(createThumbnail($ref,$realFileName));
                                $this->createThumbnail($ref,$realFileName);
                            } 
                            $model                  = new Uploads;
                            $model->ref             = $ref;
                            $model->file_name       = $fileName;
                            $model->real_filename   = $realFileName;
                            $model->save();
                            if($isAjax===true){
                                echo json_encode(['success' => 'true']);
                            }
                            
                        }else{
                            if($isAjax===true){
                                echo json_encode(['success'=>'false','eror'=>$file->error]);
                            }
                        }
                        
                        
                        
                    }
                }
            }
    }
    private function getInitialPreview($ref) {
            $datas = Uploads::find()->where(['ref'=>$ref])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/accounts/projects/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    private function getTemplatePreview(Uploads $model){     
            $filePath = Projects::getUploadUrl().$model->ref.'/thumbnail/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);

            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    private function createThumbnail($folderName,$fileName,$width=270){
      $uploadPath   = Projects::getUploadPath().'/'.$folderName.'/';
      $file         = $uploadPath.$fileName;
      $image        = Yii::$app->image->load($file);
      
      $image->resize($width);
      $image->save($uploadPath.'thumbnail/'.$fileName);
      return;
    }
    
    public function actionDeletefileAjax(){
        $this->enableCsrfValidation = false;
        $model = Uploads::findOne(Yii::$app->request->post('key'));
        if($model!==NULL){
            $filename  = Projects::getUploadPath().$model->ref.'/'.$model->real_filename;
            $thumbnail = Projects::getUploadPath().$model->ref.'/thumbnail/'.$model->real_filename;
            if($model->delete()){
                @unlink($filename);
                @unlink($thumbnail);
                echo json_encode(['success'=>true]);
            }else{
                print_r($model->getErrors());
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
}
