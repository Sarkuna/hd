<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;
use app\components\AccountMenuWidget;
$this->title = 'My Transactions';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$redeemed_point = str_replace('-', '', $redeemed_point);

?>

<section id="statistics-card">
    <div class="row">
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0"><?= $totalpoint ? $totalpoint : '0' ?></h2>
                        <p>Available Point</p>
                    </div>
                    <div class="avatar bg-rgba-primary p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-award text-primary font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0"><?= $redeemed_point ? $redeemed_point : '0' ?></h2>
                        <p>Redeemed Point</p>
                    </div>
                    <div class="avatar bg-rgba-danger p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-award text-danger font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0"><?= $totalpoint - $redeemed_point ?></h2>
                        <p>Balance Point</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-award text-success font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0"><?= $totalpoint - $redeemed_point ?></h2>
                        <p>Expiring on 31-12-20</p>
                    </div>
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-award text-warning font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
</section>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All Orders</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <?=
                        GridView::widget([
                            'layout' => "{items}\n{summary}\n<nav aria-label='Page navigation example'>{pager}</nav>",
                            'tableOptions' => ['class' => 'table'],
                            'options' => ['class' => ''],
                            'pager' => [
                                'hideOnSinglePage' => true,
                                //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                                'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                                'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                'maxButtonCount' => 10,
                                'options' => [
                                    //'tag' => 'div',
                                    'class' => 'pagination justify-content-center mt-2',
                                //'id' => 'pager-container',
                                ],
                                'pageCssClass' => ['class' => 'page-item'],
                                'linkOptions' => ['class' => 'page-link'],
                                'activePageCssClass' => 'active',
                                'prevPageCssClass' => 'page-item prev-item',
                                'nextPageCssClass' => 'page-item next-item',
                            //'internalPageCssClass' => 'btn btn-info btn-sm',
                            //'disabledPageCssClass' => 'mydisable'
                            ],
                            'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'account2_id',
            //'clientID',
            //'customer_id',
            [
                'attribute' => 'date_added',
                'label'=>'Added Date',                                                    
                //'format' => ['raw', 'Y-m-d H:i:s'],
                //'format' => ['date', 'medium'],
                'format' => 'html',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => '180','class' => 'text-center'],
                'value' => function ($model) {
                    if($model->date_added == '1970-01-01'){
                        return 'N/A';
                    }else {
                        return Yii::$app->formatter->asDatetime($model->date_added, "php:d-m-Y");
                    }
                },
                //'options' => ['width' => '200']
            ],
            /*[
                'attribute' => 'indirect_id',
                'label'=>'My Customer',                                                    
                //'format' => ['raw', 'Y-m-d H:i:s'],
                //'format' => ['date', 'medium'],
                'format' => 'html',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => '180','class' => 'text-center'],
                'value' => function ($model) {
                    if($model->indirect_id == '0'){
                        return '';
                    }else {
                        return $model->indirect_id;
                    }
                },
            ],*/            
            //'indirect_id',
            //'description:ntext',
            [
                'attribute' => 'description',
                'label'=>'Description',                                                    
                //'format' => ['raw', 'Y-m-d H:i:s'],
                //'format' => ['date', 'medium'],
                'format' => 'html',
                'contentOptions' => ['class' => 'text-left'],
                'headerOptions' => ['width' => '40%','class' => 'text-center'],
                'value' => function ($model) {
                    return Html::encode($model->description);
                },
            ],            
            'points_in',
            //'points_out',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
                                ]);
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->