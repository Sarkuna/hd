<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;
use app\components\AccountMenuWidget;
$this->title = 'My Reward Points';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$amount = 0;
    if (!empty($datarewardProvider->getModels())) {
        foreach ($datarewardProvider->getModels() as $key => $val) {
            $amount += $val->points;
        }
    }

?>


<!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Orders</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <?= GridView::widget([
                                            'layout' => "{summary}\n{items}\n<nav aria-label='Page navigation example'>{pager}</nav>",
                                            'tableOptions' => ['class' => 'table'],
                                            'options'=>['class'=>''],
                                            'pager' => [
                        'hideOnSinglePage'=>true,
                        //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                        'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                        'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        'maxButtonCount' => 10,
                        'options' => [
                            //'tag' => 'div',
                            'class' => 'pagination justify-content-center mt-2',
                            //'id' => 'pager-container',
                        ],
                        'pageCssClass' => ['class' => 'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                        'prevPageCssClass' => 'page-item prev-item',
                        'nextPageCssClass' => 'page-item next-item',
                        //'internalPageCssClass' => 'btn btn-info btn-sm',
                        //'disabledPageCssClass' => 'mydisable'
                    ],
        'dataProvider' => $datarewardProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'customer_reward_id',
            //'clientID',
            //'customer_id',
            //'order_id',
            //'date_added',
            [
                'attribute' => 'date_added',
                'label' => 'Date Added',
                'format' => 'html',
                'value' => function ($model) {
                    $datea = date('d/m/Y', strtotime($model->date_added));
                    return $datea;
                },
                'options' => ['width' => '100']
            ],
            [
                'attribute' => 'description',
                'label' => 'Description',
                'format' => 'html',
                'value' => function ($model) {
                    if($model->bb_type == 'V') {
                        //return $model->description;
                        return Html::a($model->description, ['/accounts/my-account/view', 'id' => $model->order_id]);
                    }else {
                        return $model->description;
                    }
                },
                //'options' => ['width' => '100']
            ],            
            //'description:ntext',
            /*[
                'attribute' => 'points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'label' => 'Points',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->points;
                },
                'options' => ['width' => '100']
            ],*/
            [
                'attribute' => 'points',
                'label' => 'Points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'format' => 'html',
                'options' => ['width' => '100'],
                'value' => function ($model, $key, $index, $widget) {
                    return $model->points;
                },
                'footer' => $amount,
            ],
                        
            // 'date_added',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

        ],
    ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->


                