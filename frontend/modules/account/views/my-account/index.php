<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\AccountMenuWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
$cid = Yii::$app->request->get('id');
    $sid = Yii::$app->request->get('sid');
?>
<section class="page-users-view">
  <div class="row">

    <!-- information start -->
    <div class="col-md-12 col-12 ">
      <div class="card">
        <div class="card-header">
          <div class="card-title mb-2">Information</div>
        </div>
        <div class="card-body">
            <div class="row">            
                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                    <table>
                        <tbody><tr>
                                <td class="font-weight-bold">Full Name </td>
                                <td><?= $model->full_name ? $model->salutations->name . '. ' . $model->full_name : Yii::t("app", "N/A") ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Email</td>
                                <td><?= $model->user->email ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Mobile</td>
                                <td><?= $model->mobile_no ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Date Of Birth</td>
                                <td>
                                    <?php
                                    //jS F Y
                                    //echo 'uuiou'.$model->date_of_Birth;
                                    if (!empty($model->date_of_Birth)) {
                                        echo date('d-m-Y', strtotime($model->date_of_Birth));
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Gender</td>
                                <td>
                                    <?php
                                    if ($model->gender == 'M') {
                                        echo 'Male';
                                    } else if ($model->gender == 'F') {
                                        echo 'Female';
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">NRIC/Passport No.</td>
                                <td><?= $model->nric_passport_no ? $model->nric_passport_no : Yii::t("app", "N/A") ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Race</td>
                                <td><?= $model->Race ? $model->races->name : Yii::t("app", "N/A") ?></td>
                            </tr>

                        </tbody></table>
                </div>
                <div class="col-12 col-md-12 col-lg-5">
                    <table class="ml-0 ml-sm-0 ml-lg-0">
                        <tbody>                            
                            <tr>
                                <td class="font-weight-bold">Address</td>
                                <td><?= ($model->address_1) ? $model->address_1 : Yii::t("app", "N/A") ?> <?= ($model->address_1) ? $model->address_1 : Yii::t("app", "") ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">City</td>
                                <td><?= $model->city ? $model->city : 'N/A' ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Postcode</td>
                                <td><?= $model->postcode ? $model->postcode : 'N/A' ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Nationality</td>
                                <td><?= $model->nationality ? $model->nationality->name : Yii::t("app", "N/A") ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Country</td>
                                <td><?= $model->country_id ? $model->country->name : Yii::t("app", "N/A") ?></td>
                            </tr>

                        </tbody></table>
                </div>
                <div class="col-12">
                    <a href="/my-profile/edit-profile" class="btn btn-primary mr-1 waves-effect waves-light"><i class="feather icon-edit-1"></i> Edit</a>
                    <a href="/my-profile/change-password" class="btn btn-info mr-1 waves-effect waves-light"><i class="feather icon-lock"></i> Change Password</a>
                </div>
                
            </div>          
        </div>
      </div>
    </div>
    <!-- information start -->

  </div>
</section>