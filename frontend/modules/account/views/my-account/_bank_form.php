<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

use common\models\Banks;

$banks = Banks::find()
        //->where(['option_id' => '2'])
        ->orderBy([
    'bank_name' => SORT_ASC,
])->all();
$bankslist = ArrayHelper::map($banks, 'id', 'bank_name');

?>

<?php $form = ActiveForm::begin(['method' => 'post', 'id' => 'bank-form', 'options' => [
                'class' => 'form form-horizontal',
                'novalidate' => ''
             ]]); ?>
    <div class="form-body">
        <div class="row">
            <?php
                echo $form->field($model, 'bank_name_id', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $bankslist, ['prompt' => '--', 'id' => 'Select...']
                );
            ?>
            <?=
                $form->field($model, 'account_name', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            <?=
                $form->field($model, 'account_number', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            <?=
                $form->field($model, 'nric_passport', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            <div class="col-md-8 offset-md-4">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                <?= Html::a('Back ', ['banks'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>
            </div>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>