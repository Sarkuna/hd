<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\VIPOrderStatus;
$this->title = 'Order History';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Orders</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <?= GridView::widget([
                                            'layout' => "{items}\n{summary}\n<nav aria-label='Page navigation example'>{pager}</nav>",
                                            'tableOptions' => ['class' => 'table'],
                                            'options'=>['class'=>''],
                                            'pager' => [
                        'hideOnSinglePage'=>true,
                        //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                        'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                        'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        'maxButtonCount' => 10,
                        'options' => [
                            //'tag' => 'div',
                            'class' => 'pagination justify-content-center mt-2',
                            //'id' => 'pager-container',
                        ],
                        'pageCssClass' => ['class' => 'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                        'prevPageCssClass' => 'page-item prev-item',
                        'nextPageCssClass' => 'page-item next-item',
                        //'internalPageCssClass' => 'btn btn-info btn-sm',
                        //'disabledPageCssClass' => 'mydisable'
                    ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'order_id',
            //'invoice_no',
            [
                'attribute' => 'created_datetime',
                'label' => 'Order Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return Html::a($model->invoice_prefix, '/order-history/view/'.$model->order_id);
                },
            ],
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalPoints();
                },
            ],
            [
                'attribute' => 'order_status_id',
                'label' => 'Status',
                'format' => 'html',
                'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    $actionlabel = '<i class="fa fa-circle font-small-3 text-'.$model->orderStatus->labelbg.' mr-50"></i>'.$model->orderStatus->name.'';

                    return $actionlabel;
                },
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                'filter' => ArrayHelper::map(VIPOrderStatus ::find()->all(), 'order_status_id', 'name'),        
                //'filter' => ["P" => "Pending", "C" => "Comply ", "N" => "Unfit for review"],        
            ],
            
            [
                'attribute' => 'updated_datetime',
                'label' => 'Updated Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],             
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<i class="feather icon-eye"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'sss']));
                    }
                ],
            ],             
        ],
    ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->