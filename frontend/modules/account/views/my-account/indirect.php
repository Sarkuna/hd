<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;
use app\components\AccountMenuWidget;
$this->title = 'My Customer';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= $this->title ?></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <?=
                        GridView::widget([
                            'layout' => "{items}\n{summary}\n<nav aria-label='Page navigation example'>{pager}</nav>",
                            'tableOptions' => ['class' => 'table'],
                            'options' => ['class' => ''],
                            'pager' => [
                                'hideOnSinglePage' => true,
                                //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                                'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                                'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                                'maxButtonCount' => 10,
                                'options' => [
                                    //'tag' => 'div',
                                    'class' => 'pagination justify-content-center mt-2',
                                //'id' => 'pager-container',
                                ],
                                'pageCssClass' => ['class' => 'page-item'],
                                'linkOptions' => ['class' => 'page-link'],
                                'activePageCssClass' => 'active',
                                'prevPageCssClass' => 'page-item prev-item',
                                'nextPageCssClass' => 'page-item next-item',
                            //'internalPageCssClass' => 'btn btn-info btn-sm',
                            //'disabledPageCssClass' => 'mydisable'
                            ],
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                //'vip_customer_id',
                                //'userID',
                                //'clientID',
                                //'salutation_id',
                                //'clients_ref_no',
                                [
                                    'attribute' => 'clients_ref_no',
                                    'label' => 'Code',
                                    'format' => 'html',
                                    'headerOptions' => ['width' => '100'],
                                    'value' => function ($model) {
                                return $model->clients_ref_no;
                            },
                                ],
                                // 'dealer_ref_no',
                                'full_name',
                                [
                                    'attribute' => 'email',
                                    'label' => 'Email',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->user->email;
                                    },
                                ],
                                [
                                    'attribute' => 'companyInfo',
                                    'label' => 'Company Name',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->companyInfo->company_name;
                                    },
                                ],
                                [
                                    'attribute' => 'profile_selection_id',
                                    'label' => 'Type',
                                    'format' => 'raw',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->profileSelection->name;
                                    },
                                    'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                                    'filter' => ["3" => "KPAP", "4" => "MPM"],
                                ],
                                [
                                    'attribute' => 'status',
                                    'label' => 'Status',
                                    'format' => 'raw',
                                    //'headerOptions' => ['width' => '180'],
                                    'value' => function ($model) {
                                        return $model->user->getStatustext();
                                    },
                                    'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                                    'filter' => ["P" => "Pending", "A" => "Active", "D" => "Deactive", "X" => "Deleted"],
                                ],
                                [
                                    'attribute' => 'created_datetime',
                                    'label' => 'Date Added',
                                    'format' => 'html',
                                    'headerOptions' => ['width' => '140'],
                                    'value' => function ($model) {
                                return $model->created_datetime;
                            },
                                ],
                            /* [
                              'attribute' => 'action',
                              'label' => 'Action',
                              'format' => 'raw',
                              'headerOptions' => ['width' => '150'],
                              'value' => function ($model) {
                              return $model->getActions();
                              },
                              ], */
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->




                