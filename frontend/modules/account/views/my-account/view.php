<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Order History', 'url' => ['/order-history']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-left pt-1">
                <div class="media pt-1">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                
                <?php echo '<div class="badge badge-'.$model->orderStatus->labelbg.' badge-lg mr-1 mb-1">' . $model->orderStatus->name . '</div>'; ?>
                
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Recipient Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 col-sm-12 text-left">
                <div class="invoice-details mt-2">
                    <h6>ORDER NO.</h6>
                    <p><?= $model->invoice_prefix ?></p>
                    <h6 class="mt-2">ORDER DATE</h6>
                    <p><?= date('d/m/Y', strtotime($model->created_datetime)) ?></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <h5>Delivery Address</h5>
                <div class="company-info my-2">
                    <p><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></p>
                    <p><?= $model->shipping_address_1 ?> <?= $model->shipping_address_1 ?></p>
                    <p><?= $model->shipping_city ?> <?= $model->shipping_postcode ?></p>
                    <p><?= $model->shipping_zone ?> - <?= $model->shipping_zone_id ?></p>
                    <p><?= $model->shipping_country_id ?></p>
                </div>
            </div>
        </div>
        <!--/ Invoice Recipient Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-1 invoice-items-table">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>PRODUCT NAME</th>
                                <th>MODEL</th>
                                <th>QUANTITY</th>
                                <th>UNIT POINTS</th>
                                <th>AMOUNT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $total = '';
                                    foreach ($model->orderProducts as $subproduct) {
                                        //echo $subproduct->name.'<br>';
                                        $optlisit = '';
                                        $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                        if ($OrderOption > 0) {
                                            $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                            foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                                    $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                                }
                                            }
                                            $productpoint = $subproduct->point + $subproduct->shipping_point;
                                            $totalproductpoint = $subproduct->point_total + $subproduct->shipping_point_total;
                                            echo '<tr>
                                              <td>' . $subproduct->name . '
                                                  ' . $optlisit . '
                                              </td>
                                              <td>' . $subproduct->model . '</td>
                                              <td>' . $subproduct->quantity . '</td>
                                              <td>' .$productpoint. 'pts</td>
                                              <td>' . Yii::$app->formatter->asInteger($totalproductpoint) . 'pts</td>
                                            </tr>';
                                            $total += $totalproductpoint;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="invoice-total-details" class="invoice-total-table">
            <div class="row">
                <div class="col-3 offset-9">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                                <tr>
                                    <th>TOTAL</th>
                                    <td><?= Yii::$app->formatter->asInteger($total) ?>pts</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>