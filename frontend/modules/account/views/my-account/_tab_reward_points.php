<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomer */
$amount = 0;
    if (!empty($datarewardProvider->getModels())) {
        foreach ($datarewardProvider->getModels() as $key => $val) {
            $amount += $val->points;
        }
    }
$amount = $datarewardProvider->getTotalCount('points');
?>

<?= GridView::widget([
        'dataProvider' => $datarewardProvider,
        //'filterModel' => $searchrewardModel,
        //'summary'=>'',
	'showFooter'=>true,
        'footerRowOptions'=>['style'=>'font-weight:bold;text-align: right;'],
	'showHeader' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'customer_reward_id',
            //'clientID',
            //'customer_id',
            //'order_id',
            //'date_added',
            [
                'attribute' => 'date_added',
                'label' => 'Date Added',
                'format' => 'html',
                'value' => function ($model) {
                    $datea = date('d/m/Y', strtotime($model->date_added));
                    return $datea;
                },
                'options' => ['width' => '100']
            ],
            'description:ntext',
            /*[
                'attribute' => 'points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'label' => 'Points',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->points;
                },
                'options' => ['width' => '100']
            ],*/
            [
                'attribute' => 'points',
                'label' => 'Points',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'format' => 'html',
                'options' => ['width' => '100'],
                'value' => function ($model, $key, $index, $widget) {
                    return $model->points;
                },
                'footer' => $amount,
            ],
                        
            // 'date_added',
            // 'created_datetime',
            // 'updated_datetime',
            // 'created_by',
            // 'updated_by',

        ],
    ]); ?>