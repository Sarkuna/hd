<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

//$this->title = 'Update Vipcustomer Address: ' . $model->vip_customer_address_id;
$this->title = 'Company';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-users-view">
  <div class="row">

    <!-- information start -->
    <div class="col-md-12 col-12 ">
      <div class="card">
        <div class="card-header">
          <div class="card-title mb-2">Information</div>
        </div>
        <div class="card-body">
          <table>
            <tbody><tr>
              <td class="font-weight-bold">Company Name </td>
              <td><?= $model->company_name ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Contact Person</td>
              <td><?= $model->contact_person ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Contact No</td>
              <td><?= $model->contact_no ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Address</td>
              <td><?= $model->mailing_address ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Telephone</td>
              <td><?= $model->tel ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Fax</td>
              <td><?= $model->fax ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Registration Type</td>
              <td>
                <?php
                if(!empty($model->company_reg_type)){
                    echo $model->registration->name;
                }else {
                    echo 'N/A';
                }
                ?>
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">Registration Number</td>
              <td><?= $model->company_registration_number ?></td>
            </tr>

          </tbody></table>
            <a href="/company/edit-company" class="btn btn-primary mr-1 waves-effect waves-light"><i class="feather icon-edit-1"></i> Edit</a>
        </div>
      </div>
    </div>
    <!-- information start -->

  </div>
</section>