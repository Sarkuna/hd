<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
//use kartik\select2\Select2;

use common\models\Salutation;
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = VIPZone::find()
        ->where(['country_id' => '129'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'zone_id', 'name');



$raceslists = common\models\Races::find()->orderBy([
    'sort_order' => SORT_ASC,
])->all();
$races = ArrayHelper::map($raceslists, 'races_id', 'name');
$salutations = Salutation::find()->all();
    $salutationsData = ArrayHelper::map($salutations, 'salutation_id', 'name');
?>


    <?php $form = ActiveForm::begin(['method' => 'post', 'id' => 'profile-form', 'options' => [
                'class' => 'form form-horizontal',
                'novalidate' => ''
             ]]); ?>
    <div class="form-body">
        <div class="row">
            <?php
                echo $form->field($model, 'salutation_id', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $salutationsData, ['prompt' => '--', 'id' => 'salutation']
                );
            ?>
            <?=
                $form->field($model, 'full_name', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            <?=
                $form->field($model, 'email', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            <?=
                $form->field($model, 'date_of_Birth', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '99-99-9999',
                        'options' => [
                            'placeholder' => 'dd-mm-yyyy',
                            'class' => 'form-control',
                        ]
                ]);
            ?>
            <?php
                echo $form->field($model, 'gender', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ])->radioList(['M' => 'Male', 'F' => 'Female'], ['itemOptions' => ['class' => 'flat', 'id' => 'client_SMS']]);
            ?>
            <?=
                $form->field($model, 'nric_passport_no', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            
            
            <?=
                $form->field($model, 'mobile_no', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            
            <?=
                $form->field($model, 'telephone_no', [
                    'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                    'options' => [
                        'class' => 'col-12 col-md-6',
                        //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                    ],
                ])->textInput(array('placeholder' => ''));
            ?>
            
            <?php
                echo $form->field($model, 'Race', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $races, ['prompt' => '--', 'id' => 'salutation']
                );
            ?>
            
            <?php
                echo $form->field($model, 'Region', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $regionlist, ['prompt' => '--', 'id' => 'salutation']
                );
            ?>
            
            <?php
                echo $form->field($model, 'nationality_id', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $country, ['prompt' => '--', 'id' => 'salutation']
                );
            ?>
            
            <?php
                echo $form->field($model, 'country_id', [
                            'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                            'options' => [
                                'class' => 'col-12 col-md-6',
                            ],
                    ]) ->dropDownList(
                        $country, ['prompt' => '--', 'id' => 'salutation']
                );
            ?>

                
            
            
            <div class="col-md-8 offset-md-4">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                <?= Html::a('Back ', ['index'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>
            </div>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>


