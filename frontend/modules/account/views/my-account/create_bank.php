<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

$this->title = 'Add Bank';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Banks', 'url' => ['banks']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?=
                            $this->render('_bank_form', [
                                'model' => $model,
                            ])
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>