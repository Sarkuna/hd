<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\FileInput;


?>


<div class="account-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">

            <?php
            echo $form->field($model, 'image[]')->widget(FileInput::classname(), [
                'options' => ['multiple' => true, 'accept' => 'image/*'],
                'pluginOptions' => [
                    'maxFileCount' => 5,
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp','jpeg'],
                    'initialPreview' => [
                    //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                    //$ClientAddress->company_logo ? Html::img('/upload/client_logos/'.$ClientAddress->company_logo, ['class'=>'thumbnail', 'alt'=>'Logo', 'title'=>'Logo']) : null, // checks the models to display the preview
                    ],
                ]
            ]);
            ?>


        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>