<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCustomerAddress */

//$this->title = 'Update Vipcustomer Address: ' . $model->vip_customer_address_id;
$this->title = 'Edit Profile';
$this->params['breadcrumbs'][] = ['label' => 'My Profile', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?=
                            $this->render('_profile_form', [
                                'model' => $model,
                            ])
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$this->registerJs('
       $("#vipcustomer-email").keyup(function (e){
            var email = $("#vipcustomer-email").val();
            jQuery.ajax({
                    type: "POST",
                    url: "checkemail",
                    data: "email="+ email,
                    cache: false,
                    success: function(response){
                        if(response > 0){
                           alert("This email has already been taken.")
                           $("#vipcustomer-email").val(email);
                        }
                    }
                });
        }); 
', \yii\web\View::POS_READY);
?>