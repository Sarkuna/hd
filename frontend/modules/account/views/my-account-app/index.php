<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\AccountMenuWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Account';
$this->params['breadcrumbs'][] = $this->title;
$cid = Yii::$app->request->get('id');
    $sid = Yii::$app->request->get('sid');
?>
<div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- HTML -->
                            <div id="accordion">
                                <h4 class="accordion-toggle"><span>01</span>Account</h4>
                                <div class="accordion-content default">                                    
                                    <div class="details-box">
                                        <ul>
                                            <li>                                                
                                                <a href="/accounts/my-account/update-profile"> <i class="fa fa-edit"></i> Edit your personal info</a>
                                            </li>                                            
                                            <li>                                              
                                                <a href="/accounts/my-account/address"> <i class="fa fa-edit"></i> Manage Shipping Address</a>
                                            </li>
                                            <li>                                              
                                                <a href="/accounts/my-account/banks"> <i class="fa fa-edit"></i> Manage Bank Info</a>
                                            </li>
                                            <li>                                               
                                                <a href="/accounts/my-account/change-password"> <i class="fa fa-edit"></i> Change your password</a>
                                            </li>
                                        </ul>
                                    </div>                                    
                                </div>
                                <div class="clearfix"></div>
                                <h4 class="accordion-toggle"><span>02</span>Reward Points</h4>
                                <div class="accordion-content"> 
                                    <div class="details-box">
                                        <ul>
                                            <li>                                               
                                        <a href="/accounts/my-account/reward-points"> <i class="fa fa-star"></i> My Reward Points</a>
                                    </li>                                         
                                        </ul>
                                    </div>
                                    
                                </div>
                                <h4 class="accordion-toggle"><span>03</span>My Orders</h4>
                                <div class="accordion-content">
                                    <div class="details-box">
                                        <ul>
                                            <li>
                                                <a href="/accounts/my-account/order"> <i class="fa fa-truck"></i> View your order history</a>
                                            </li>                                           
                                        </ul>
                                    </div>
                                </div>                                
                            </div>
                        </div>

                        <!--- Account Menu Left Side -->
                    </div>
                </div>
            </div>

