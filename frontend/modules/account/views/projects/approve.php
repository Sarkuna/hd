<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects - Approved';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary projects-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        
    </div><!-- /.box-header -->

    <div class="box-body">
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'userID',
                'label' => 'ID Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->customer->full_name;
                },
            ],
            [
                'attribute' => 'projects_date',
                'label' => 'Date',
                //'format' => 'html',
                'format' => ['date', 'php:d/m/Y'],
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return $model->projects_date;
                },
            ],
            [
                'attribute' => 'projects_ref',
                'label' => 'Project No.',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->projects_ref;
                },
            ],
            /*[
                'attribute' => 'userID',
                'label' => 'ID Code',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->userID;
                },
            ],*/
            
            [
                'attribute' => 'project_name',
                'label' => 'Project Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->project_name;
                },
            ],
            [
                'attribute' => 'project_status',
                'label' => 'Status',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return $model->getStatustext();
                },
            ],
            [
                'attribute' => 'project_nominated_points',
                'label' => 'Nominated Points',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->project_nominated_points;
                },
            ],
            [
                'attribute' => 'project_approved_points',
                'label' => 'Approved Points',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->project_approved_points;
                },
            ],            
            

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{view} {delete}
                'buttons' => [ 
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                    },
                ],
            ], 
        ],
    ]); ?>
        


    </div>
</div>
