<?php
use yii;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = 'Upload Images for ' . $model->project_name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->project_name, 'url' => ['view', 'id' => $model->projects_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <!-- HTML -->
                <div id="account-id">
                    <h4 class="account-title"><span class="fa fa-chevron-right"></span><?= Html::encode($this->title) ?></h4>                                                                 

                </div>
                <?= $this->render('_form', [
                    'model' => $model,
                    'initialPreview'=>$initialPreview,
                    'initialPreviewConfig'=>$initialPreviewConfig,
                ]) ?>
            </div>

            
        </div>
    </div>
</div>