<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Expression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = 'Receipt Invoice #'. $model->invoice_no;
$this->params['breadcrumbs'][] = ['label' => 'Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<style>
    .invoice{margin:0px;}
</style>

<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="account-title pull-left"><span class="fa fa-chevron-right"></span><?= $this->title ?></h4>
                <h4 class="account-title pull-right"><?= Html::a('Back ', ['index'], ['class' => 'btn btn-default btn-sm']) ?></h4>
                <div class="clear"></div>
            </div>
            <div class="col-md-12">
                <div id="account-id">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-left" colspan="2">Order Details</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left" style="width: 50%;">
                                    <b>Invoice#:</b> <?= $model->invoice_no ?><br>
                                    <b>Date Of Receipt:</b> <?= date('d-m-Y', strtotime($model->date_of_receipt)) ?><br>
                                    <b>Status:</b> <?= $model->getStatustext() ?><br>
                                    <b>Date Added:</b> <?php echo date('d-m-Y', strtotime($model->created_datetime)) ?></td>
                                <td class="text-left">
                                    <?php
                                    echo '<br>Date Of Receipt : '.date('d-m-Y', strtotime($model->date_of_receipt));
                                    echo '<br>Distributor : '.$model->company->company_name;
                                    //echo 'Full Name : ' . $model->customer->full_name;
                                    //echo '<br>Membership ID# : '.$model->profile->card_id;
                                    //echo '<br>NRIC/PP : '.$model->profile->ic_no;
                                    echo '<br>Mobile # : ' . $model->customer->mobile_no;
                                    //echo '<br>Email ID: '.$model->user->email;
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead style="border: solid 1px #dddddd;">
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2" style="vertical-align: middle;" class="text-center">Product Description</th>
                        <th colspan="3" class="text-center">Base Value</th>
                        <th rowspan="2">Qty</th>
                        <th colspan="3" class="text-center">Awarded Values</th>
                        <th colspan="2" class="text-center">Product Purchase Price</th>
                  </tr>
                  <tr>
                        <td>Pack Size</td>
                        <td>Points</td>
                        <td>RM</td>
                        <td>Pack Size</td>
                        <td>Point</td>
                        <td>RM</td>
                        <td>Unit Price</td>
                        <td>Total Price</td>
                  </tr>

                </thead>
                            <tbody>
                                <?php
                                $redemptionitems = common\models\VIPReceiptList::find()->where(['vip_receipt_id' => $model->vip_receipt_id])->all();
                                $n = 1;
                                $totalpoints = 0;
                                foreach ($redemptionitems as $redemptionitem) {
                                    $product_list_id = $redemptionitem->product_list_id;
                                    //$product_list = \common\models\BBPoints::find()->where(['bb_points_id' => $product_list_id])->one();
                                    $product_list_id = $redemptionitem->product_list_id;
                                    $product_list = \common\models\ProductList::find()->where(['product_list_id' => $product_list_id])->one();
                        
                                    $totalpoint = $redemptionitem->product_item_qty * $redemptionitem->product_per_value;
                                    echo '<tr>
                            <td>' . $n . '</td>
                            <td class="text-left" width="40%">'.$product_list->description.'</td> 
                            <td class="text-right">'.$product_list->pack_size.'</td>
                            <td class="text-right">'.$product_list->total_points_awarded.'</td> 
                            <td class="text-right">'.$redemptionitem->points_awarded_rm_value.'</td>     
                            <td class="text-right">'.$redemptionitem->product_item_qty.'</td> 
                            <td class="text-right">'.$product_list->pack_size * $redemptionitem->product_item_qty.'</td>    
                            <td class="text-right">'.$redemptionitem->qty_points_awarded.'</td>  
                            <td class="text-center">'.$redemptionitem->qty_points_awarded_rm_value.'</td>                                
                            <td class="text-center">'.$redemptionitem->price_total.'</td> 
                            <td class="text-center">'.$redemptionitem->price_per_product.'</td>    
                          </tr>';
                                    $totalpoints += $totalpoint;
                                    $n++;
                                }
                                ?>
                            </tbody>
                        </table>
                        <h4><b>Total</b> : <?= $totalpoints ?>pts</h4>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.col -->
</div>