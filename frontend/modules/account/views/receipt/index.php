<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts History';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Receipts</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body m-b--10">
                                    <!--<?= Html::a('<span><i class="feather icon-plus"></i> Upload Your Receipt</span>', ['/receipts-history/submit-invoices'], ['class' => 'btn btn-outline-primary']) ?>-->
                                    <div class="table-responsive">
                                        
                                        <?= GridView::widget([
                                            'layout' => "{items}\n{summary}<nav aria-label='Page navigation example'>{pager}</nav>",
                                            'tableOptions' => ['class' => 'table'],
                                            'options'=>['class'=>''],
                                            'pager' => [
                        'hideOnSinglePage'=>true,
                        //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                        'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                        'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        'maxButtonCount' => 10,
                        'options' => [
                            //'tag' => 'div',
                            'class' => 'pagination justify-content-center mt-2',
                            //'id' => 'pager-container',
                        ],
                        'pageCssClass' => ['class' => 'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                        'prevPageCssClass' => 'page-item prev-item',
                        'nextPageCssClass' => 'page-item next-item',
                        //'internalPageCssClass' => 'btn btn-info btn-sm',
                        //'disabledPageCssClass' => 'mydisable'
                    ],
'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
        'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'invoice_no',
                                'order_num',
                                [
                                    'attribute' => 'total_items',
                                    //'label' => 'Total Transactions',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-right'],
                                    'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                                ],
                                [
                                    'attribute' => 'order_total_point',
                                    'label' => 'Total Point',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'value' => function ($model) {
                                return $model->getTotalPoints();
                            },
                                ],
                                //'date_of_receipt',
                                [
                                    'attribute' => 'date_of_receipt1',
                                    'label' => 'Date Of Receipt',
                                    //'format' => ['raw', 'Y-m-d H:i:s'],
                                    //'format' => ['date', 'medium'],
                                    'format' => 'html',
                                    'contentOptions' => ['class' => 'text-center'],
                                    //'headerOptions' => ['width' => '180', 'class' => 'text-center'],
                                    'value' => function ($model) {
                                if ($model->date_of_receipt == '1970-01-01') {
                                    return 'N/A';
                                } else {
                                    return Yii::$app->formatter->asDatetime($model->date_of_receipt, "php:d-m-Y");
                                }
                            },
                                //'options' => ['width' => '200']
                                ],
                                [
                                    'attribute' => 'status',
                                    'label' => 'Status',
                                    'format' => 'html',
                                    //'headerOptions' => ['width' => '120'],
                                    'value' => function ($model) {
                                        return $model->getStatustextuser();
                                    },
                                    'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                                    'filter' => ["P" => "Pending", "N" => "Processing", "A" => "Approved", "D" => "Decline"],
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}', //{view} {delete}
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            if ($model->status != 'P') {
                                                return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                            }
                                        },
                                            ],
                                        ],
                                    ],
    ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->