<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Upload Receipt';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Receipt History', 'url' => ['/receipts-history']];
//$this->params['breadcrumbs'][] = ['label' => $model->vip_customer_address_id, 'url' => ['view', 'id' => $model->vip_customer_address_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .has-error .help-block {
    color: red;
}
</style>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<section>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <label class="card-title"><?= $this->title ?></label>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <?=
                                    $form->field($model, 'invoice_no', [
                                        //'template' => "<li class='col-md-6 col-sm-6'>{label}{input}\n{hint}\n{error}</li>"
                                    ])->textInput(array('placeholder' => ''));
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-12">
                            <?= $form->field($model, 'date_of_receipt')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '99-99-9999',
                                'options' => [
                                    'placeholder' => 'dd-mm-yyyy',
                                    'class' => 'form-control',
                                ]
                            ]) ?>
                            </div>
                        </div>
                            
                        <?php
                            echo $form->field($model, 'image[]', [
                                        'template' => "<fieldset class='form-group'><label for='basicInputFile'>{label} <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label><div class='custom-file'>{input}<label class='custom-file-label' for='inputGroupFile01'>Choose file</label>{hint}</div>{error}</fieldset>",
                                        'options' => [
                                            //'class' => 'col-12 col-md-6',
                                        ],
                                ])->fileInput(['multiple'=>true, 'id' => 'inputGroupFile01']
                            );
                        ?>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                        <?= Html::a('Back ', ['/receipts-history'], ['class' => 'btn btn-outline-warning mr-1 mb-1 waves-effect waves-light']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>

