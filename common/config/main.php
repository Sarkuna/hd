<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
         'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => ',',
            //'decimalSeparator' => '.',
            'dateFormat' => 'php:d-M-Y',
            //'datetimeFormat' => 'php:d-M-Y H:i:s',
            //'timeFormat' => 'php:H:i:s',
            //'timeZone' => 'Asia/Kuala_Lumpur',
        ],
        
        'image' => [
                'class' => 'yii\image\ImageDriver',
                'driver' => 'GD',  //GD or Imagick
        ],
        
        'VIPglobal' => [
            'class' => 'common\components\VIPGlobal',
        ],
        
    ],
];
