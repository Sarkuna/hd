<?php
return [
    'adminEmail' => 'admin@businessboosters.com.my',
    'supportEmail' => 'support@businessboosters.com.my',
    'user.passwordResetTokenExpire' => 3600,
    
    'sms.username' => 'businessboosters',
    'sms.password' => '1a2b3c4d',
    
    'email.template.code.registration' => 'SPA01',
    'reset.password' => 'RP05',
    'password.reset.request' => 'PRR06',    
    'email.template.code.email.verify' => 'EV1',
    'admin.register' => 'admin.register',
    
    'order.status_10' => 'order.status_10',
    'order.status_20' => 'order.status_20',
    'order.status_30' => 'order.status_30',
    'order.status_40' => 'order.status_40',
    'order.status_50' => 'order.status_50',
    'order.status_60' => 'order.status_60',
    
    'approval' => 'customer.register.approval',
    'register' => 'customer.register',
    'dealer.register' => 'dealer.register',
];
