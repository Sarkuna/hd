<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

use common\models\VIPCategories;

/**
 * This is the model class for table "vip_sub_categories".
 *
 * @property integer $vip_sub_categories_id
 * @property integer $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $position
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property VipCategories $parent
 */
class VIPSubCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_sub_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'slug'], 'required'],
            [['parent_id', 'position'], 'integer'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 45],
            [['is_active'], 'string', 'max' => 1],
            [['image', 'meta_title'], 'string', 'max' => 80],
            [['meta_keywords'], 'string', 'max' => 150],
            [['meta_description'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => VIPCategories::className(), 'targetAttribute' => ['parent_id' => 'vip_categories_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_sub_categories_id' => 'Vip Sub Categories ID',
            'parent_id' => 'Category',
            'name' => 'Name',
            'slug' => 'Slug',
            'description' => 'Description',
            'image' => 'Image',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'position' => 'Sort Order',
            'is_active' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(VIPCategories::className(), ['vip_categories_id' => 'parent_id']);
    }
}
