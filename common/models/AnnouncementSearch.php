<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPAnnouncement;

/**
 * AnnouncementSearch represents the model behind the search form about `common\models\VIPAnnouncement`.
 */
class AnnouncementSearch extends VIPAnnouncement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['announcement_id', 'clientID'], 'integer'],
            [['message', 'type', 'lang', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPAnnouncement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'announcement_id' => $this->announcement_id,
            'clientID' => $this->clientID,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
