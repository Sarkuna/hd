<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPOrderProduct;

/**
 * VIPOrderProductSearch represents the model behind the search form about `common\models\VIPOrderProduct`.
 */
class VIPOrderProductSearch extends VIPOrderProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_product_id', 'order_id', 'product_id', 'quantity', 'point', 'point_total', 'product_status'], 'integer'],
            [['name', 'model'], 'safe'],
            [['price', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPOrderProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_product_id' => $this->order_product_id,
            'order_id' => $this->order_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'point' => $this->point,
            'point_total' => $this->point_total,
            'price' => $this->price,
            'total' => $this->total,
            'product_status' => $this->product_status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'model', $this->model]);

        return $dataProvider;
    }
}
