<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\VIPOrderProduct;
/**
 * This is the model class for table "vip_order".
 *
 * @property integer $order_id
 * @property integer $invoice_no
 * @property string $invoice_prefix
 * @property integer $clientID
 * @property integer $customer_id
 * @property integer $customer_group_id
 * @property string $shipping_firstname
 * @property string $shipping_lastname
 * @property string $shipping_company
 * @property string $shipping_address_1
 * @property string $shipping_address_2
 * @property string $shipping_city
 * @property string $shipping_postcode
 * @property integer $shipping_country_id
 * @property string $shipping_zone
 * @property integer $shipping_zone_id
 * @property string $comment
 * @property integer $order_status_id
 * @property integer $language_id
 * @property string $ip
 * @property string $user_agent
 * @property string $accept_language
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property VipOrderProduct $vipOrderProduct
 */
class VIPOrder extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    
    public $comment_history;
    public $address_type;
    public static function tableName()
    {
        return 'vip_order';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => 'mdm\autonumber\Behavior',
                'attribute' => 'invoice_prefix', // required
                'group' => $this->clientID, // optional
                'value' => Yii::$app->VIPglobal->clientPrefix().'-?' , // format auto number. '?' will be replaced with generated number
                'digit' => 6 // optional, default to null. 
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'customer_id', 'customer_group_id', 'order_status_id', 'language_id', 'created_by', 'updated_by', 'last_point_balance'], 'integer'],
            [['shipping_firstname', 'shipping_lastname', 'order_status_id', 'shipping_address_1', 'shipping_city', 'shipping_postcode', 'shipping_country_id','shipping_zone_id'], 'required'],
            [['comment','comment_history'], 'string'],
            [['bb_invoice_no','created_datetime', 'updated_datetime'], 'safe'],
            [['invoice_no'], 'string', 'max' => 5],
            [['invoice_prefix'], 'string', 'max' => 26],
            [['global_order_ID'], 'string', 'max' => 15],
            [['shipping_firstname', 'shipping_lastname'], 'string', 'max' => 32],
            [['shipping_company', 'ip'], 'string', 'max' => 40],
            [['shipping_address_1', 'shipping_address_2', 'shipping_city'], 'string', 'max' => 128],
            [['shipping_postcode'], 'string', 'max' => 10],
            [['user_agent', 'accept_language'], 'string', 'max' => 255],
            [['shipping_country_id'], 'string', 'max' => 250],
            [['shipping_zone'], 'string', 'max' => 128],
            [['shipping_zone_id'], 'string', 'max' => 200],
            
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'invoice_no' => 'Invoice No',
            'invoice_prefix' => 'Invoice Prefix',
            'clientID' => 'Client ID',
            'customer_id' => 'Customer ID',
            'customer_group_id' => 'Customer Group ID',
            'shipping_firstname' => 'First Name',
            'shipping_lastname' => 'Last Name',
            'shipping_company' => 'Company Name',
            'shipping_address_1' => 'Address 1',
            'shipping_address_2' => 'Address 2',
            'shipping_city' => 'Shipping City',
            'shipping_postcode' => 'Postcode',
            'shipping_country_id' => 'Country',
            'shipping_zone' => 'Region / State',
            'shipping_zone_id' => 'Zone',
            'comment' => 'Comment',
            'comment_history' => 'Comment',
            'order_status_id' => 'Order Status',
            'bb_invoice_no' => 'Invoice Number',
            'last_point_balance' => 'Last Point Balance',
            'language_id' => 'Language',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'accept_language' => 'Accept Language',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVipOrderProduct()
    {
        return $this->hasOne(VipOrderProduct::className(), ['order_id' => 'order_id']);
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'customer_id']);
    }
    
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'customer_id']);
    }
    
    public function getCustomeremail() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'customer_id']);
    }
    
    public function getOrderStatus() {
        return $this->hasOne(\common\models\VIPOrderStatus::className(), ['order_status_id' => 'order_status_id']);
    }
    
    public function getStorename(){
        return $this->hasOne(\common\models\ClientAddress::className(), ['clientID' => 'clientID']);
    }
    
    public function getStoreurl(){
        return $this->hasOne(\common\models\Client::className(), ['clientID' => 'clientID']);
    }
    
    public function getCustomerAddress(){
        return $this->hasOne(\common\models\VIPCustomerAddress::className(), ['userID' => 'customer_id']);
    }
    
    public function getOrderProducts()
    {
        return $this->hasMany(VIPOrderProduct::className(), ['order_id' => 'order_id']);
    }
    
    public function getTotalPoints()
    {
        return $this->hasMany(VIPOrderProduct::className(), ['order_id' => 'order_id'])->sum('point_total');
    }
    
    public function getShippingPointTotal()
    {
        return $this->hasMany(VIPOrderProduct::className(), ['order_id' => 'order_id'])->sum('shipping_point_total');
    }
    
    public function getTotalTransactions(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return VIPOrderProduct::find()->where(['order_id' => $this->order_id])->count();
    }
    
    public function getCompany() {
        return $this->hasOne(CompanyInformation::className(), ['user_id' => 'customer_id']);
    }
    
    public function getStates() {
        return $this->hasOne(\common\models\VIPStates::className(), ['states_id' => 'shipping_zone']);
    }
    
    public function getDeliveryZone() {
        return $this->hasOne(\common\models\VIPZone::className(), ['zone_id' => 'shipping_zone_id']);
    }
}