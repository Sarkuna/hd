<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "BB_Points".
 *
 * @property integer $bb_points_id
 * @property integer $clientID
 * @property string $product_code
 * @property string $product_description
 * @property string $product_kg
 * @property integer $category_bb_id
 * @property integer $product_point
 * @property string $status
 */
class BBPoints extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BB_Points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'product_code', 'product_description', 'product_kg', 'category_bb_id', 'product_point'], 'required'],
            [['clientID', 'category_bb_id', 'product_point'], 'integer'],
            [['product_code'], 'string', 'max' => 100],
            [['product_description'], 'string', 'max' => 255],
            [['product_kg'], 'string', 'max' => 10],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bb_points_id' => 'Bb Points ID',
            'clientID' => 'Client ID',
            'product_code' => 'Product Code',
            'product_description' => 'Product Description',
            'product_kg' => 'Product Kg',
            'category_bb_id' => 'Category Bb ID',
            'product_point' => 'Product Point',
            'status' => 'Status',
        ];
    }
}
