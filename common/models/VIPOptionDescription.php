<?php

namespace common\models;
use common\models\VIPOptionValueDescription;

use Yii;

/**
 * This is the model class for table "vip_option_description".
 *
 * @property integer $option_id
 * @property integer $language_id
 * @property string $name
 *
 * @property VipOptionValueDescription[] $vipOptionValueDescriptions
 */
class VIPOptionDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $testaaa;
    public static function tableName()
    {
        return 'vip_option_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['language_id'], 'integer'],
            [['name'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVipOptionValueDescriptions()
    {
        return $this->hasMany(VIPOptionValueDescription::className(), ['option_id' => 'option_id']);
    }
    
    public function getVipOptionValueDescriptions1()
    {
        return $this->hasOne(VIPOptionValueDescription::className(), ['option_id' => 'option_id']);
    }
}
