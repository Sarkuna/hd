<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "assign_product_excel_upload".
 *
 * @property integer $id
 * @property string $product_code
 * @property integer $clientID
 * @property string $msrp
 * @property string $partner_price
 * @property string $delivery
 * @property string $featured_status
 * @property string $status
 */
class AssignProductExcelUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assign_product_excel_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_code'], 'required'],
            [['clientID'], 'integer'],
            [['msrp', 'partner_price'], 'number'],
            [['delivery'], 'string'],
            [['product_code'], 'string', 'max' => 100],
            [['featured_status', 'status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_code' => 'Product Code',
            'clientID' => 'Client ID',
            'msrp' => 'Msrp',
            'partner_price' => 'Partner Price',
            'delivery' => 'Delivery',
            'featured_status' => 'Featured Status',
            'status' => 'Status',
        ];
    }
}
