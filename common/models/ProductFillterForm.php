<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ProductFillterForm extends Model
{
    public $categories;
    public $subcategories;
    public $manufacturer;
    public $price;
    public $quantity;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categories', 'subcategories'], 'required'],
        ];
    }

    
}
