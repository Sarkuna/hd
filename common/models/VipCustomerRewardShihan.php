<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_customer_reward_shihan".
 *
 * @property integer $customer_reward_id
 * @property integer $customer_id
 * @property integer $order_id
 * @property string $description
 * @property integer $points
 * @property string $date_added
 */
class VipCustomerRewardShihan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_reward_shihan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'order_id', 'points'], 'integer'],
            [['description', 'date_added'], 'required'],
            [['description'], 'string'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_reward_id' => 'Customer Reward ID',
            'customer_id' => 'Customer ID',
            'order_id' => 'Order ID',
            'description' => 'Description',
            'points' => 'Points',
            'date_added' => 'Date Added',
        ];
    }
}
