<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPAssignProducts;

/**
 * VIPAssignProductsSearch represents the model behind the search form about `common\models\VIPAssignProducts`.
 */
class VIPAssignProductsSearch extends VIPAssignProducts
{
    /**
     * @inheritdoc
     */
    public $product_name;
    public $product_description;
    public $product_code;
    
    public function rules()
    {
        return [
            [['vip_assign_products_id', 'clientID', 'productID'], 'integer'],
            [['date_assign', 'product_name', 'product_description', 'product_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPAssignProducts::find();
        $query->joinWith(['product']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_assign_products_id' => $this->vip_assign_products_id,
            'clientID' => $this->clientID,
            'productID' => $this->productID,
            'date_assign' => $this->date_assign,
        ]);
        
        $query->andFilterWhere(['like', 'vip_product.product_name', $this->product_name])
            ->andFilterWhere(['like', 'vip_product.product_description', $this->product_description])
            ->andFilterWhere(['like', 'vip_product.product_code', $this->product_code]);

        return $dataProvider;
    }
}
