<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "vip_customer_account2".
 *
 * @property integer $account2_id
 * @property integer $clientID
 * @property integer $customer_id
 * @property integer $indirect_id
 * @property string $description
 * @property integer $points_in
 * @property string $date_added
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomerAccount2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_account2';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'description', 'date_added'], 'required'],
            [['clientID', 'customer_id', 'indirect_id', 'points_in', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['date_added', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account2_id' => 'Account2 ID',
            'clientID' => 'Client ID',
            'customer_id' => 'Customer ID',
            'indirect_id' => 'Indirect ID',
            'description' => 'Description',
            'points_in' => 'Points',
            'date_added' => 'Date Added',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'customer_id']);
    }
    
    public function getCompanyInfo() {
        return $this->hasOne(\common\models\CompanyInformation::className(), ['user_id' => 'customer_id']);
    }
    
    public function getTotalAdd() {
        $total = 0;
        $totaladd = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->customer_id, 'bb_type' => 'A'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points_in');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         //return $totaladd + $this->getPointExpiry();
         return $totaladd;
    }
    
    public function getTotalMinus() {        
        $totalminus = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->customer_id, 'bb_type' => 'V'])
        ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         return $totalminus;
    }
    
    public function getTotalExpiry() {        
        $totalminus = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->customer_id, 'bb_type' => 'E'])
        ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         return $totalminus;
    }
    
    //No Use
    public function getTotalAdd123() {
        $total = 0;
        $totaladd = VIPCustomerAccount2::find()
        ->where(['>', 'points_in', 0])
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->customer_id])
        ->sum('points_in');

        if(empty($totaladd)){
            $totaladd = 0; 
         }
        return $totaladd;
    }
    public function getTotalMinus123() {
        $totalminus = 0;

        $totalminus = VIPCustomerAccount2::find()
        ->where(['<', 'points_in', 0])
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->customer_id])
        ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
        return $totalminus;
    }
    
    
}
