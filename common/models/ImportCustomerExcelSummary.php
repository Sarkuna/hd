<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "import_customer_excel_summary".
 *
 * @property integer $import_customer_upload_summary_id
 * @property integer $clientID
 * @property string $account_type
 * @property string $description
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ImportCustomerExcelSummary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_customer_excel_summary';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type'], 'required'],
            [['clientID', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['account_type'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'import_customer_upload_summary_id' => 'Import Customer Upload Summary ID',
            'clientID' => 'Client ID',
            'account_type' => 'Account Type',
            'description' => 'Description',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getNoofAccountsSuccess()
    {
       return $this->hasMany(\common\models\ImportCustomerExcel::className(), ['import_customer_upload_summary_id' => 'import_customer_upload_summary_id'])->
           andWhere(['status' => 'S'])->count();
    }
    
    public function getNoofAccountsUpdate()
    {
       return $this->hasMany(\common\models\ImportCustomerExcel::className(), ['import_customer_upload_summary_id' => 'import_customer_upload_summary_id'])->
           andWhere(['status' => 'U'])->count();
    }
    
    public function getNoofAccounts()
    {
       return $this->hasMany(\common\models\ImportCustomerExcel::className(), ['import_customer_upload_summary_id' => 'import_customer_upload_summary_id'])->count();
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getUsercreate() {
        return $this->hasOne(\common\models\UserProfile::className(), ['userID' => 'created_by']);
    }
    
    public function getAccountType() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        return $this->hasOne(\common\models\TypeName::className(), ['tier_id' => 'account_type']);
    }

}
