<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category_bb".
 *
 * @property integer $category_bb_id
 * @property string $name
 * @property integer $clientID
 * @property string $status
 */
class CategoryBB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_bb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['clientID'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_bb_id' => 'Category Bb ID',
            'name' => 'Name',
            'clientID' => 'Client ID',
            'status' => 'Status',
        ];
    }
}
