<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\EndUserLevel;
use common\models\Salutation;

/**
 * This is the model class for table "vip_customer".
 *
 * @property integer $vip_customer_id
 * @property integer $userID
 * @property integer $clientID
 * @property integer $salutation_id
 * @property string $clients_ref_no
 * @property string $dealer_ref_no
 * @property string $full_name
 * @property string $gender
 * @property string $telephone_no
 * @property string $mobile_no
 * @property string $date_of_Birth
 * @property string $nric_passport_no
 * @property string $Race
 * @property string $Region
 * @property integer $country_id
 * @property integer $address_id
 * @property integer $nationality_id
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $phone;
    public $email;
    public $approved;
    public $newsletter;
    public $status;
    public $type;
            
    public static function tableName()
    {
        return 'vip_customer';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','salutation_id', 'clients_ref_no', 'full_name', 'mobile_no'], 'required'],
            [['userID', 'clientID', 'salutation_id', 'country_id', 'address_id', 'nationality_id', 'created_by', 'updated_by','state', 'type'], 'integer'],
            [['approved','newsletter','status','date_of_Birth', 'created_datetime', 'updated_datetime','end_user_level_id','company_name','profile_selection_id','kis_category_id','dealer_CGrp','dealer_Rg', 'address_1', 'address_2', 'city', 'state', 'postcode'], 'safe'],
            [['clients_ref_no', 'dealer_ref_no', 'telephone_no', 'mobile_no', 'nric_passport_no', 'Race', 'Region'], 'string', 'max' => 20],
            [['full_name','CIDB_registration_number'], 'string', 'max' => 200],
            [['CIDB_Grade'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 1],
            
            ['clients_ref_no', 'trim'],
            ['clients_ref_no', 'required'],
            ['clients_ref_no', 'string', 'max' => 20],
            //['clients_ref_no', 'uniqueClientsrefno'],
            [['clients_ref_no'], 'uniqueClientsrefno',
                //'targetClass' => 'common\models\User',
                'when' => function ($model, $attribute){
                    //return $model->isAttributeChanged('clients_ref_no');
                    return $model->{$attribute} !== $model->getOldAttribute($attribute);
                }
            ],
            
            [['mobile_no'], PhoneInputValidator::className(), 'region' => ['MY','my'], 'message' => 'The format of the phone is invalid or system not supported your country.'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'uniqueEmail',
                //'targetClass' => 'common\models\User',
                'on'=>'update',
                'when' => function ($model, $attribute){
                    //echo $model->{$attribute}.'-'.$model->user->getOldAttribute($attribute);
                    return $model->{$attribute} !== $model->user->getOldAttribute($attribute);
                }
            ],
            ['email', 'uniqueEmail',
                //'targetClass' => 'common\models\User',
                'on'=>'create',
            ],        
            
            /*['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_customer_id' => 'Vip Customer ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'salutation_id' => 'Salutation',
            'clients_ref_no' => 'Clients Ref No',
            'dealer_ref_no' => 'Dealer Ref No',
            'full_name' => 'Full Name',
            'email' => 'E-Mail',
            'gender' => 'Gender',
            'telephone_no' => 'Telephone No',
            'mobile_no' => 'Mobile Number',
            'date_of_Birth' => 'Date Of Birth',
            'nric_passport_no' => 'NRIC/Passport No.',
            'Race' => 'Race',
            'Region' => 'Region',
            'country_id' => 'Country',
            'address_id' => 'Address',
            'nationality_id' => 'Nationality',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'newsletter' => 'Newsletter',
            'CIDB_registration_number' => 'CIDB Registration',
            'CIDB_Grade' => 'CIDB Grade',
            'end_user_level_id' => 'Yearly Purchase Volume',
            'profile_selection_id' => 'Profile',
            'status' => 'Status',
            'approved' => 'Approved',
            'type' => 'Account Type'
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'userID']);
    }
    
    public function getCountry()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'country_id']);
    }
    public function getZones()
    {
        return $this->hasOne(VIPZone::className(), ['zone_id' => 'Region']);
    }
    
    public function getNationality()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'nationality_id']);
    }
    public function getYearly()
    {
        return $this->hasOne(EndUserLevel::className(), ['end_user_level_id' => 'end_user_level_id']);
    }
    public function getActions() {
        $session = Yii::$app->session;
        $myclient = \common\models\Client::find()->where([
                'clientID' => $session['currentclientID'],
            ])->one();
        
        $returnValue = "";
        $url = '/sales/customers/sendpassword?id='.$this->userID;
        $url2 = '/sales/customers/active?id='.$this->userID;

        //$returnValue = $returnValue . '<a href="' . Url::to(['/sales/customers/update', 'id' => $this->vip_customer_id]) . '" title="Edit" class="btn btn-success btn-sm" ><i class="fa fa-pencil"></i></a>';
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/view', 'id' => $this->vip_customer_id]) . '" title="View" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> ';
        if($this->user->email_verification != 'Y') {
            $url = '/sales/customers/resend-email-verify?id='.$this->user->id;
            $returnValue = $returnValue . Html::a('<span class="glyphicon glyphicon-link"></span>', $url, [
                'title' => Yii::t('app', 'Resend Verification Email'),
                'class' => "btn btn-info btn-sm",
                'data' => [
                    'confirm' => 'Are you sure you want resend the email?',
                ],
            ]); 
            
            $url = '/sales/customers/email-verify?id='.$this->user->id;
            $returnValue = $returnValue .  Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                'title' => Yii::t('app', 'Verification Email & Approve'),
                'class' => "btn btn-warning btn-sm sp",
                'data' => [
                    'confirm' => 'Are you sure to skip the email verification?',
                ],
            ]);
        }
        return $returnValue;
    }
    
    public function getActionsapprove() {
        $session = Yii::$app->session;
        $returnValue = "";
        $url = '/sales/customers/sendpassword?id='.$this->userID;        
        $url2 = '/sales/customers/active?id='.$this->userID;
        $url3 = '/sales/customers/send-password-manual?id='.$this->userID;
        $url4 = '/sales/customers/trash-account?id='.$this->userID;
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/view', 'id' => $this->vip_customer_id]) . '" title="View" class="btn btn-info btn-sm"><i class="fa fa-address-card"></i></a> ';

               
        $returnValue = $returnValue . Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url3, [
            'title' => Yii::t('app', 'Resend password'),
            'class' => "btn btn-warning btn-sm",
        ]);
        
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/log', 'id' => $this->userID]) . '" title="View Log" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a> ';
        
        if($session['currentusertype'] == 'A') {
          $returnValue = $returnValue . ' <a target="_blank" href="' . Url::to(['/sales/customers/loginto-store', 'id' => $this->userID]) . '" title="Login into Store" class="btn btn-info btn-sm"><i class="fa fa-lock"></i></a> ';  
        }
        
        $returnValue = $returnValue . Html::a('<span class="glyphicon glyphicon-trash"></span>', $url4, [
            'title' => Yii::t('app', 'Trash'),
            'class' => "btn btn-danger btn-sm",
            'data' => [
                'confirm' => 'Are you sure you want trash this account?',
            ],
        ]);
        
        return $returnValue;
    }
    
    public function getAllaction() {
        $returnValue = "";
        $url = '/sales/customers/sendpassword?id='.$this->userID;        
        $url2 = '/sales/customers/active?id='.$this->userID;
        $url3 = '/sales/customers/send-password-manual?id='.$this->userID;
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/view', 'id' => $this->vip_customer_id]) . '" title="View" class="btn btn-info btn-sm"><i class="fa fa-address-card"></i></a> ';

        if($this->user->email_verification != 'Y') {
            $url = '/sales/customers/resend-email-verify?id='.$this->user->id;
            $returnValue = $returnValue . Html::a('<span class="glyphicon glyphicon-link"></span>', $url, [
                'title' => Yii::t('app', 'Resend Verification Email'),
                'class' => "btn btn-info btn-sm",
                'data' => [
                    'confirm' => 'Are you sure you want resend the email?',
                ],
            ]); 
            
            $url = '/sales/customers/email-verify?id='.$this->user->id;
            $returnValue = $returnValue .  Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                'title' => Yii::t('app', 'Verification Email & Approve'),
                'class' => "btn btn-warning btn-sm sp",
                'data' => [
                    'confirm' => 'Are you sure to skip the email verification?',
                ],
            ]);
        }else {      
            $returnValue = $returnValue . Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url3, [
                'title' => Yii::t('app', 'Resend password'),
                'class' => "btn btn-warning btn-sm",
            ]);
        }
        $returnValue = $returnValue . ' <a href="' . Url::to(['/sales/customers/log', 'id' => $this->userID]) . '" title="View Log" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a> ';
        
        return $returnValue;
    }
        
    public function getPointExpiry() {
        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totaladd = str_replace('-', '', $totaladd);
        if(empty($totaladd)){
            $totaladd = 0; 
         }
        return $totaladd;
         
        /*$totaladd = \common\models\VIPCustomerPointExpiry::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        if(empty($totaladd)){
            $totaladd = 0; 
         }
        return $totaladd;*/
        
    }
    
    public function getGoingtoExpiry() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = $y1B + $y2V;
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
         
        return $y2B;

    }
    
    public function getY1() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = 0;
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
         
        return $y1E;

    }
    
    public function getY2() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = $y1B + $y2V;
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
        
        if($y1E < 0 ){
            $y1E = 0;
        }
         
        return $y1E;

    }
    
    public function getY3() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y1E = $y1B + $y2V;
        if($y1E < 0){
            $y1E = 0;
        }
        $y2B = $y1B + $y2A + $y2V - $y1E;
        
        
        
        $y3B = $y2B + $y3V;
        
        
        if(empty($y1B)){
            $y1B = 0; 
        }
        
        if(empty($y2B)){
            $y2B = 0; 
        }
        
        if($y3B < 0 ){
            $y3B = 0;
        }
         
        return $y3B;

    }
    
    public function getY4() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2B = $y2A + $y1B + $y2V;
        
        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3B = $y3A + $y2B + $y3V;
        
        $tot = $y3B - $this->getY2() - $this->getY3();
        
        $adj = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'description' => 'Point Value Adjustment (0.36 to 0.5) - 2017'])       
        ->sum('points');
        
        $tot = $y3B - $this->getY2() - $this->getY3() + $adj;
        
        $y4A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-12-31 23:59:59'])        
        ->sum('points');
        
        $y4V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-03-31 23:59:59'])        
        ->sum('points');
        
        $tot4 = $tot + $y4V;
        
        return $tot4;

    }
    
    public function getY5() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2B = $y2A + $y1B + $y2V;
        
        $y3A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y3B = $y3A + $y2B + $y3V;
        
        $tot = $y3B - $this->getY2() - $this->getY3();
        
        $adj = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'description' => 'Point Value Adjustment (0.36 to 0.5) - 2017'])       
        ->sum('points');
        
        //$tot = $y3B - $this->getY2() - $this->getY3() + $adj;
        $tot = $y3B - $this->getY2() - $this->getY3();
        
        $y4A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-12-31 23:59:59'])        
        ->sum('points');
        
        $y4V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-03-31 23:59:59'])        
        ->sum('points');
        
        $tot4 = $tot + $y4V;
        
        //$tot5 = $y4A + $tot + $y4V - $tot4;
        $tot5 = $y4A + $tot + $y4V - $tot4;
        //$tot $y4V
        
        
        $goingto = $tot + $y4V;
        
        if($goingto < 0) {
            $goingto = 0;
        }
        return '-'.$goingto;

    }
    
    public function getGoingtoExpiry4() {
        $total = 0;
        $totaladd = 0;
        $totaladdr = 0;
        
        $totaladda = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $totaladdr = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
       
        

        $totaladd1 = $totaladda + $totaladdr;
        
        $totaladd = $totaladd1;
        
        if(empty($totaladd)){
            $totaladd = 0; 
        }
         
        return $totaladd;

    }
    
    public function getGoingtoExpiry5() {
        $expiryp = 0;
        
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E']) 
        ->andWhere(['like', 'description', '2018'])
        ->sum('points');

        return $expiryp;

    }
    
    public function getRedemption() {
        
        $totaladdr = \common\models\VIPOrder::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->andWhere(['>=', 'created_datetime', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'created_datetime', '2019-03-31 23:59:59'])        
        ->count();
        
        $totaladdr2 = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->andWhere(['LIKE', 'description', 'Order ID:'])        
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-03-31 23:59:59'])        
        ->count();
        
        return $totaladdr + $totaladdr2;
    }
    
    public function getEy1() {
        
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E']) 
        ->andWhere(['like', 'description', '2016'])     
        ->sum('points');
        $expiryp = str_replace('-', '', $expiryp);
        return $expiryp;
    }
    
    public function getTotalorder() {
        
        $orders = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V']) 
        ->andWhere(['>', 'order_id', 0])     
        ->count();

        return $orders;
    }
    
    public function getEy2() {
        
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E']) 
        ->andWhere(['like', 'description', '2017'])     
        ->sum('points');
        $expiryp = str_replace('-', '', $expiryp);
        return $expiryp;
    }
    
    public function getEy3() {
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E'])       
        ->andWhere(['like', 'description', '2018'])        
        ->sum('points');
        $expiryp = str_replace('-', '', $expiryp);
        return $expiryp;
    }
    
    public function getEy4() {
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E'])
        ->andWhere(['like', 'description', '2019'])
        ->sum('points');
        
        $expiryp = str_replace('-', '', $expiryp);
        
        return $expiryp;
    }
    
    public function getTotalAdd() {
        $total = 0;
        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         //return $totaladd + $this->getPointExpiry();
         return $totaladd;
    }
    
    public function getTotalMinus() {        
        //$totalminus = 0;
        $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         return $totalminus;
    }
    
    public function getExpired() {
        $expiryp = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E'])
        //->andWhere(['like', 'description', '2019'])
        ->sum('points');
        
        $expiryp = str_replace('-', '', $expiryp);
        
        return $expiryp;
    }
    
    public function getAdjustment() {
        $totaladjustment = 0;
        $adjustment = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'B'])
        //->andWhere(['like', 'description', '2019'])
        ->sum('points');
        
        $totaladjustment = str_replace('-', '', $adjustment);
        
        return $totaladjustment;
    }
    
    public function getTotalExpiry() {
        $expyear=date("Y");
        $expyearminus=$expyear-1;
        $startdate = ($expyearminus-1).'-04-01 00:00:00';
        $enddate = $expyear.'-03-31 23:59:59';

        
        $totalexpiry = \common\models\VIPCustomerReward::find()
        //->where(['>', 'points', 0])
        //->andWhere(['not like', 'description', 'Order ID'])        
        ->andWhere(['between', 'date_added', $startdate, $enddate])        
        ->andWhere(['clientID'=>$this->clientID, 'customer_id'=>$this->userID])
        ->sum('points');
        
        if(empty($totalexpiry)){
            $totalexpiry = 0; 
         }
         
         $totalexpiry = $this->getPointExpiry();
        return $totalexpiry;
    }
    
    public function getBalance() {
        $balance = $this->getTotalAdd() - $this->getTotalMinus() - $this->getPointExpiry();
        $totalbalance = $balance - $this->getAdjustment();
        return $totalbalance;
    }
    
    public function getProfileSelection() {
        return $this->hasOne(\common\models\ProfileSelection::className(), ['profile_selection_id' => 'profile_selection_id']);
    }
    
    public function getDistributor() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['vip_customer_id' => 'distributors_ref']);
    }
    
    public function getCompanyInfo() {
        return $this->hasOne(\common\models\CompanyInformation::className(), ['user_id' => 'userID']);
    }
    
    public function getSalutations() {
        return $this->hasOne(Salutation::className(), ['salutation_id' => 'salutation_id']);
    }
    
    public function getCompany() {
        return $this->hasOne(CompanyInformation::className(), ['user_id' => 'userID']);
    }
    
    public function getDealerInformation() {
        return $this->hasOne(UserProfile::className(), ['userID' => 'assign_dealer_id']);
    }
    
    public function getRaces() {
        return $this->hasOne(Races::className(), ['races_id' => 'Race']);
    }
    
    public function uniqueClientsrefno($attribute, $params) {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        if(
            $clients_ref_no = VIPCustomer::find()->where(['clients_ref_no'=>$this->clients_ref_no, 'clientID' => $client_id])->exists()
        )
          $this->addError($attribute, 'Clients Ref No "'.$this->clients_ref_no.'" has already been taken.');
    }
    
    public function uniqueEmail($attribute, $params) {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        if(
            $user = User::find()->where(['email'=>$this->email, 'client_id' => $client_id, 'user_type' => 'D'])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Email "'.$this->email.'" has already been taken.');
    }
    //Delete code once testing finish
    public function getY1bf() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
         
        return $y1B;

    }
    
    public function getY2bf() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
        
        $y1A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2015-08-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2016-03-31 23:59:59'])        
        ->sum('points');
        
        $y1B = $y1A + $y1V;
        
        
        
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2016-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2017-03-31 23:59:59'])        
        ->sum('points');
        

        $y2B = $y1B + $y2A + $y2V - $this->getEy2();

         
        return $y2B;

    }
    
    public function getY3bf() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
 
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2017-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-03-31 23:59:59'])        
        ->sum('points');
        

        $y2B = $y2A + $y2V + $this->getY2bf()- $this->getEy3();

         
        return $y2B;

    }
    
    public function getY4bf() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
 
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2018-12-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2018-04-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-04-04 23:59:59'])        
        ->sum('points');
        
        //$y2A + $y2V + $this->getY3bf()- $this->getEy3();
        $y2B = $y2A + $y2V + $this->getY3bf()- $this->getEy4();

         
        return $y2B;

    }
    
    public function getY5bf() {
        $y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
 
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2019-01-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        
        $y2B = $y2A + $y2V + $this->getY4bf();

         
        return $y2B;

    }
    
    public function getExpiryY4() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $userid = $this->userID;
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $balnce = \common\models\BalanceCarryforward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
        ->count();
        
        if($balnce > 0) {
            $balnce = \common\models\BalanceCarryforward::find()
            ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
            ->orderBy([
                'balance_bf_year' => SORT_DESC,
            ])
            ->one();
            $lastbalnce = $balnce->balance_points;
        }else {
            $lastbalnce = 0;
        }
        
        
        $y2B = $lastbalnce + $y2V;
        
        if($y2B > 0) {
            $y2B = $y2B;
        }else {
           $y2B = 0; 
        }

         
        return $y2B;
        
        /*$y2B = 0;
        $y2V = 0;
        $totaladdr = 0;
        $adj = 0;
 
        $y2A = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        ->andWhere(['>=', 'date_added', '2019-01-01 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        
        $y2B = $this->getY4bf() + $y2V;
        
        if($y2B > 0) {
            $y2B = $y2B;
        }else {
           $y2B = 0; 
        }

         
        return $y2B;*/

    }
    
    public function getTotalAddAcc2() {
        $total = 0;
        $totaladd = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'A'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points_in');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         //return $totaladd + $this->getPointExpiry();
         return $totaladd;
    }
    
    public function getTotalMinusAcc2() {        
        $totalminus = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'V'])
        ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         return $totalminus;
    }
    
    public function getTotalExpiryAcc2() {        
        $totalminus = VIPCustomerAccount2::find()
        ->where(['clientID'=>$this->clientID, 'customer_id'=>$this->userID, 'bb_type' => 'E'])
        ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         return $totalminus;
    }
    
    public function getBalanceAcc2() {
        return $this->getTotalAddAcc2() - $this->getTotalMinusAcc2() - $this->getTotalExpiryAcc2();
    }
}