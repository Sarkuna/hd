<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_zone".
 *
 * @property integer $zone_id
 * @property integer $region_id
 * @property string $name
 * @property string $code
 * @property integer $status
 */
class VIPZone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'name', 'code'], 'required'],
            [['region_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['code'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zone_id' => 'Zone ID',
            'region_id' => 'Region ID',
            'name' => 'Name',
            'code' => 'Code',
            'status' => 'Status',
        ];
    }
}
