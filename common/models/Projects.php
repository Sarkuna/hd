<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\Uploads;

/**
 * This is the model class for table "projects".
 *
 * @property integer $projects_id
 * @property integer $userID
 * @property integer $clientID
 * @property integer $dealerID
 * @property string $projects_ref
 * @property string $projects_date
 * @property string $project_name
 * @property string $project_status
 * @property string $project_client_name
 * @property string $project_client_email
 * @property string $project_client_tel
 * @property string $project_details
 * @property integer $project_nominated_points
 * @property integer $project_approved_points
 * @property string $ref
 * @property string $admin_status
 */
class Projects extends \yii\db\ActiveRecord
{
    const UPLOAD_FOLDER= 'web/upload/projects_gallery';
    const UPLOAD_FOLDER_VIEW= 'upload/projects_gallery';
    //const UPLOAD_FOLDER= 'upload/photolibrarys';
    
    public $type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }
    
    public function behaviors() {
        return [
            [
                'class' => 'mdm\autonumber\Behavior',
                'attribute' => 'projects_ref', // required
                'group' => 'Projects', // optional
                'value' => 'P?' , // format auto number. '?' will be replaced with generated number
                'digit' => 4 // optional, default to null. 
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'project_name', 'project_status', 'project_nominated_points', 'admin_status'], 'required'],
            [['userID', 'clientID', 'dealerID', 'project_nominated_points', 'project_approved_points'], 'integer'],
            [['projects_date'], 'safe'],
            [['project_details'], 'string'],
            [['projects_ref'], 'string', 'max' => 10],
            [['project_name', 'ref'], 'string', 'max' => 200],
            [['project_status', 'admin_status'], 'string', 'max' => 1],
            [['project_client_name'], 'string', 'max' => 100],
            [['project_client_email'], 'string', 'max' => 255],
            [['project_client_tel'], 'string', 'max' => 20],
            [['remark'], 'string', 'max' => 600],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'projects_id' => 'Projects ID',
            'userID' => 'User Name',
            'clientID' => 'Client ID',
            'dealerID' => 'Dealer ID',
            'projects_ref' => 'Projects Ref',
            'projects_date' => 'Projects Date',
            'project_name' => 'Project Name',
            'project_status' => 'Project Status',
            'project_client_name' => 'Project Client Name',
            'project_client_email' => 'Project Client Email',
            'project_client_tel' => 'Project Client Tel',
            'project_details' => 'Project Details',
            'project_nominated_points' => 'Project Nominated Points',
            'project_approved_points' => 'Project Approved Points',
            'ref' => 'Ref',
            'admin_status' => 'Status',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'userID']);
    }
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'userID']);
    }
    
    public function getDealerInformation() {
        return $this->hasOne(UserProfile::className(), ['userID' => 'dealerID']);
    }
    
    public function getStatustext() {
        //P=Potential, I=In Progress, C=Close Sales, U=Unsuccessful
        $returnValue = "";
        $project_status = $this->project_status;
        if ($project_status == "C") {
            $returnValue = '<span class="label label-warning">Close Sales</span>';
        } else if ($project_status == "I") {
            $returnValue = '<span class="label label-success">In Progress</span>';
        } else if ($project_status == "U") {
            $returnValue = '<span class="label label-danger">Unsuccessful</span>';
        }
        return $returnValue;
        
    }
    
    public function getStatus() {
        //P=Potential, I=In Progress, C=Close Sales, U=Unsuccessful
        $returnValue = "";
        $admin_status = $this->admin_status;
        if ($admin_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($admin_status == "A") {
            $returnValue = '<span class="label label-success">Approved</span>';
        } else if ($admin_status == "D") {
            $returnValue = '<span class="label label-danger">Declined</span>';
        }
        return $returnValue;
        
    }
    
    public function getImages()
    {
        return $this->hasMany(Uploads::className(), ['ref' => 'ref']);
    }

        public static function getUploadPath(){
        return Yii::getAlias('@backend').'/'.self::UPLOAD_FOLDER.'/';
    }
    public static function getUploadUrl(){
        return Yii::getAlias('@back').'/'.self::UPLOAD_FOLDER_VIEW.'/';
    }
    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'label'=>'Home',
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name, 'class'=>'col-lg-4 col-xs-6'],
                //'options'=> ['class'=>'list-group-item'],
            ];
        }
        return $preview;
    }
}
