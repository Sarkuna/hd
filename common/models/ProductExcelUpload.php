<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_excel_upload".
 *
 * @property integer $id
 * @property string $product_name
 * @property string $links
 * @property string $product_code
 * @property string $sp_code
 * @property string $variant
 * @property string $msrp
 * @property string $partner_price
 * @property string $delivery
 * @property string $manufacturer
 * @property string $category
 * @property string $sub_category
 * @property string $create_date
 * @property string $status
 */
class ProductExcelUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_excel_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'links', 'delivery'], 'string'],
            [['product_code'], 'required'],
            [['msrp', 'partner_price'], 'number'],
            [['create_date'], 'safe'],
            [['product_code', 'variant', 'manufacturer', 'category', 'sub_category'], 'string', 'max' => 100],
            [['sp_code'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'links' => 'Links',
            'product_code' => 'Product Code',
            'sp_code' => 'Sp Code',
            'variant' => 'Variant',
            'msrp' => 'Msrp',
            'partner_price' => 'Partner Price',
            'delivery' => 'Delivery',
            'manufacturer' => 'Manufacturer',
            'category' => 'Category',
            'sub_category' => 'Sub Category',
            'create_date' => 'Create Date',
            'status' => 'Status',
        ];
    }
}
