<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "client".
 *
 * @property integer $clientID
 * @property string $company
 * @property string $company_short_name
 * @property string $cart_domain
 * @property string $admin_domain
 * @property string $membership
 * @property string $favicon
 * @property string $programme_title
 * @property string $client_status
 * @property string $background_img
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User[] $users
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company', 'cart_domain', 'admin_domain', 'created_datetime', 'updated_datetime'], 'required'],
            [['favicon', 'background_img'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['company'], 'string', 'max' => 150],
            [['company_short_name'], 'string', 'max' => 15],
            [['cart_domain', 'admin_domain'], 'string', 'max' => 300],
            [['membership', 'client_status'], 'string', 'max' => 1],
            [['programme_title'], 'string', 'max' => 200],
            [['cart_domain'], 'unique'],
            [['admin_domain'], 'unique'],
            [['cart_domain','admin_domain'],'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'clientID' => 'Client ID',
            'company' => 'Company',
            'company_short_name' => 'Company Short Name',
            'cart_domain' => 'Cart Domain',
            'admin_domain' => 'Admin Domain',
            'membership' => 'Membership',
            'favicon' => 'Favicon',
            'programme_title' => 'Programme Title',
            'client_status' => 'Client Status',
            'background_img' => 'Background Img',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['client_id' => 'clientID']);
    }

    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }
    
    public function getClientAddress()
    {
        return $this->hasOne(ClientAddress::className(), ['clientID' => 'clientID']);
    }
    
    public function getStatus() {
        $returnValue = "";
        $client_status = $this->client_status;
        if ($client_status == "X") {
            $returnValue = '<span class="label label-danger">Deleted</span>';
        } else if ($client_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($client_status == "A") {
            $returnValue = '<span class="label label-success">Active</span>';
        } else if ($client_status == "D") {
            $returnValue = '<span class="label label-danger">Deactive</span>';
        }
        return $returnValue;
        
    }
    
    public function getActions() {
        $returnValue = "";


        //$returnValue = $returnValue . '<a href="' . Url::to(['/admin/clients/update', 'id' => $this->clientID]) . '" title="Edit" class="btn btn-success btn-sm" ><i class="fa fa-edit"></i></a>';
        $returnValue = $returnValue . '<a href="' . Url::to(['/admin/clients/view', 'id' => $this->clientID]) . '" title="View" class="btn btn-success btn-sm" ><i class="fa fa-eye"></i></a>';
        //$returnValue = $returnValue . ' <a href="' . Url::to(['/admin/switch/', 'clientID' => $this->clientID]) . '" title="Switch" class="btn btn-info btn-sm"><i class="fa fa-exchange"></i></a>';
        $returnValue = $returnValue . ' <a href="' . Url::to(['/admin/clients/logo', 'logoID' => $this->clientAddress->client_address_ID]) . '" title="Logo" class="btn btn-info btn-sm"><i class="fa fa-file-image"></i></a>';

        return $returnValue;
    }
}
