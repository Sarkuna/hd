<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ImportCustomerExcelSummary;

/**
 * ImportCustomerExcelSummarySearch represents the model behind the search form about `common\models\ImportCustomerExcelSummary`.
 */
class ImportCustomerExcelSummarySearch extends ImportCustomerExcelSummary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_customer_upload_summary_id', 'clientID', 'created_by', 'updated_by'], 'integer'],
            [['account_type', 'description', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImportCustomerExcelSummary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'import_customer_upload_summary_id' => $this->import_customer_upload_summary_id,
            'clientID' => $this->clientID,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
