<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class Approve extends Model
{
    public $status;
    public $remark;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status'], 'string', 'max' => 1],
            [['remark'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'status' => 'Status',
            'remark' => 'Remark',
        ];
    }

    
}
