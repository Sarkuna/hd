<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomerBank;

/**
 * VIPCustomerBankSearch represents the model behind the search form about `common\models\VIPCustomerBank`.
 */
class VIPCustomerBankSearch extends VIPCustomerBank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_customer_bank_id', 'userID', 'clientID', 'bank_name_id', 'created_by', 'updated_by'], 'integer'],
            [['account_name', 'account_number', 'nric_passport', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPCustomerBank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_customer_bank_id' => $this->vip_customer_bank_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'bank_name_id' => $this->bank_name_id,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'account_name', $this->account_name])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'nric_passport', $this->nric_passport]);

        return $dataProvider;
    }
}
