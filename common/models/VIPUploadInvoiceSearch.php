<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPUploadInvoice;

/**
 * VIPUploadInvoiceSearch represents the model behind the search form about `common\models\VIPUploadInvoice`.
 */
class VIPUploadInvoiceSearch extends VIPUploadInvoice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_id', 'userID', 'clientID'], 'integer'],
            [['file_group', 'upload_file_name', 'upload_file_new_name', 'add_date', 'invoice_status','upload_via'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPUploadInvoice::find()->groupBy('file_group');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['upload_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upload_id' => $this->upload_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'add_date' => $this->add_date,
            'invoice_status' => $this->invoice_status,
        ]);

        $query->andFilterWhere(['like', 'file_group', $this->file_group])
            ->andFilterWhere(['like', 'upload_file_name', $this->upload_file_name])
            ->andFilterWhere(['like', 'upload_file_new_name', $this->upload_file_new_name])
            ->andFilterWhere(['like', 'invoice_status', $this->invoice_status])
            ->andFilterWhere(['like', 'upload_via', $this->upload_via]);

        return $dataProvider;
    }
    
    public function searchlist($params)
    {
        $query = VIPUploadInvoice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upload_id' => $this->upload_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'add_date' => $this->add_date,
            'invoice_status' => $this->invoice_status,
        ]);

        $query->andFilterWhere(['like', 'file_group', $this->file_group])
            ->andFilterWhere(['like', 'upload_file_name', $this->upload_file_name])
            ->andFilterWhere(['like', 'upload_file_new_name', $this->upload_file_new_name])
            ->andFilterWhere(['like', 'upload_via', $this->upload_via]);

        return $dataProvider;
    }
}
