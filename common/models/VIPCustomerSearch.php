<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomer;

/**
 * VIPCustomerSearch represents the model behind the search form about `common\models\VIPCustomer`.
 */
class VIPCustomerSearch extends VIPCustomer
{
    /**
     * @inheritdoc
     */
    public $email;
    public $type;
    public $company_name;
    public $email_verification;
    public $assign_dealer_id;
    public $globalSearch;
    public $cartuser;


    public function rules()
    {
        return [
            [['vip_customer_id', 'userID', 'clientID', 'salutation_id', 'country_id', 'address_id', 'nationality_id', 'created_by', 'updated_by'], 'integer'],
            [['clients_ref_no', 'dealer_ref_no', 'full_name', 'gender', 'telephone_no', 'mobile_no', 'date_of_Birth', 'nric_passport_no', 'Race', 'Region', 'created_datetime', 'updated_datetime', 'kis_category_id','distributors_ref','profile_selection_id','status','email', 'type','company_name','assign_dealer_id','globalSearch','cartuser'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPCustomer::find();
        $query->joinWith(['user','companyInfo']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        //$query->andFilterWhere(['!=', 'user.status', 'X']);
        //$query->andFilterWhere(['=', 'user.status', 'A']);

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_customer_id' => $this->vip_customer_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'salutation_id' => $this->salutation_id,
            'date_of_Birth' => $this->date_of_Birth,
            'country_id' => $this->country_id,
            'address_id' => $this->address_id,
            'nationality_id' => $this->nationality_id,
            'kis_category_id' => $this->kis_category_id,
            'distributors_ref' => $this->distributors_ref,
            'profile_selection_id' => $this->profile_selection_id,
            //'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'user.email_verification' => $this->email_verification,
            
            'assign_dealer_id' => $this->assign_dealer_id,
        ]);

        $query->andFilterWhere(['like', 'clients_ref_no', $this->clients_ref_no])
            ->andFilterWhere(['like', 'user.created_datetime', $this->created_datetime])    
            ->andFilterWhere(['like', 'dealer_ref_no', $this->dealer_ref_no])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'telephone_no', $this->telephone_no])
            ->andFilterWhere(['like', 'mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'nric_passport_no', $this->nric_passport_no])
            ->andFilterWhere(['like', 'Race', $this->Race])
            ->andFilterWhere(['like', 'distributors_ref', $this->distributors_ref])    
            ->andFilterWhere(['like', 'profile_selection_id', $this->profile_selection_id])
            //->andFilterWhere(['like', 'created_datetime', $this->created_datetime])    
            ->andFilterWhere(['like', 'Region', $this->Region]);
        
        $query->andFilterWhere(['like', 'user.status', $this->status]);
        $query->andFilterWhere(['like', 'user.email', $this->email]);
        $query->andFilterWhere(['like', 'user.type', $this->type]);
        $query->andFilterWhere(['like', 'company_information.company_name', $this->company_name]);
        $query->andFilterWhere(['=', 'user.cart_user', 0]);

        return $dataProvider;
    }
    
    public function searchall($params)
    {
        $query = VIPCustomer::find();
        $query->joinWith(['user','companyInfo']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        //$query->andFilterWhere(['!=', 'user.cart_user', '1']);
        $query->orFilterWhere(['like', 'user.cart_user', $this->cartuser]);

        $query->orFilterWhere(['like', 'clients_ref_no', $this->globalSearch])    
            ->orFilterWhere(['like', 'dealer_ref_no', $this->globalSearch])
            ->orFilterWhere(['like', 'full_name', $this->globalSearch]);
        
        $query->orFilterWhere(['like', 'user.status', $this->globalSearch]);
        $query->orFilterWhere(['like', 'user.email', $this->globalSearch]);
        $query->orFilterWhere(['like', 'user.type', $this->globalSearch]);
        $query->orFilterWhere(['like', 'company_information.company_name', $this->globalSearch]);

        return $dataProvider;
    }
    
    public function searchpoints($params)
    {
        $query = VIPCustomer::find();
        $query->joinWith(['user','companyInfo']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['!=', 'user.status', 'X']);
        $query->andFilterWhere(['=', 'user.status', 'A']);

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_customer_id' => $this->vip_customer_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'salutation_id' => $this->salutation_id,
            'date_of_Birth' => $this->date_of_Birth,
            'country_id' => $this->country_id,
            'address_id' => $this->address_id,
            'nationality_id' => $this->nationality_id,
            'kis_category_id' => $this->kis_category_id,
            'distributors_ref' => $this->distributors_ref,
            'profile_selection_id' => $this->profile_selection_id,
            //'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'user.email_verification' => $this->email_verification,
            'assign_dealer_id' => $this->assign_dealer_id,
        ]);

        $query->andFilterWhere(['like', 'clients_ref_no', $this->clients_ref_no])
            ->andFilterWhere(['like', 'user.created_datetime', $this->created_datetime])    
            ->andFilterWhere(['like', 'dealer_ref_no', $this->dealer_ref_no])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'telephone_no', $this->telephone_no])
            ->andFilterWhere(['like', 'mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'nric_passport_no', $this->nric_passport_no])
            ->andFilterWhere(['like', 'Race', $this->Race])
            ->andFilterWhere(['like', 'distributors_ref', $this->distributors_ref])    
            ->andFilterWhere(['like', 'profile_selection_id', $this->profile_selection_id])
            //->andFilterWhere(['like', 'created_datetime', $this->created_datetime])    
            ->andFilterWhere(['like', 'Region', $this->Region]);
        
        $query->andFilterWhere(['like', 'user.status', $this->status]);
        $query->andFilterWhere(['like', 'user.email', $this->email]);
        $query->andFilterWhere(['like', 'user.type', $this->type]);
        $query->andFilterWhere(['like', 'company_information.company_name', $this->company_name]);
        $query->andFilterWhere(['=', 'user.cart_user', 0]);

        return $dataProvider;
    }
    
    public function searchin($params)
    {
        $query = VIPCustomer::find();
        $query->joinWith(['user'])->orderBy([
            'status' => SORT_DESC,
            //'vip_customer_id'=>SORT_ASC
          ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['vip_customer_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_customer_id' => $this->vip_customer_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'salutation_id' => $this->salutation_id,
            'date_of_Birth' => $this->date_of_Birth,
            'country_id' => $this->country_id,
            'address_id' => $this->address_id,
            'nationality_id' => $this->nationality_id,
            'kis_category_id' => $this->kis_category_id,
            'distributors_ref' => $this->distributors_ref,
            'profile_selection_id' => $this->profile_selection_id,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'clients_ref_no', $this->clients_ref_no])
            ->andFilterWhere(['like', 'dealer_ref_no', $this->dealer_ref_no])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'telephone_no', $this->telephone_no])
            ->andFilterWhere(['like', 'mobile_no', $this->mobile_no])
            ->andFilterWhere(['like', 'nric_passport_no', $this->nric_passport_no])
            ->andFilterWhere(['like', 'Race', $this->Race])
            ->andFilterWhere(['like', 'distributors_ref', $this->distributors_ref])    
            ->andFilterWhere(['like', 'profile_selection_id', $this->profile_selection_id])
            ->andFilterWhere(['like', 'Region', $this->Region]);
        
        $query->andFilterWhere(['like', 'user.status', $this->status]);

        return $dataProvider;
    }
}
