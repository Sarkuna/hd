<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "bonus_intake_list".
 *
 * @property integer $bonus_intake_list_id
 * @property integer $bonus_intake_id
 * @property integer $clientID
 * @property string $month_start
 * @property string $month_end
 * @property integer $list_target
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class BonusIntakeList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_intake_list';
    }
    
    /**
     * @behaviors
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_intake_id', 'clientID', 'month_start', 'month_end', 'list_target', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'required'],
            [['bonus_intake_id', 'clientID', 'list_target', 'created_by', 'updated_by'], 'integer'],
            [['month_start', 'month_end', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_intake_list_id' => 'Bonus Intake List ID',
            'bonus_intake_id' => 'Bonus Intake ID',
            'clientID' => 'Client ID',
            'month_start' => 'Month Start',
            'month_end' => 'Month End',
            'list_target' => 'Target',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
