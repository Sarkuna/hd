<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_receipt_history".
 *
 * @property integer $vip_receipt_history
 * @property integer $vip_receipt_id
 * @property string $vip_receipt_status
 * @property integer $notify
 * @property string $comment
 * @property string $date_added
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ReceiptHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_receipt_history';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_receipt_id', 'vip_receipt_status', 'comment', 'date_added'], 'required'],
            [['vip_receipt_id', 'notify', 'created_by', 'updated_by'], 'integer'],
            [['comment'], 'string'],
            [['date_added', 'created_datetime', 'updated_datetime'], 'safe'],
            [['vip_receipt_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_receipt_history' => 'Vip Receipt History',
            'vip_receipt_id' => 'Vip Receipt ID',
            'vip_receipt_status' => 'Vip Receipt Status',
            'notify' => 'Notify',
            'comment' => 'Comment',
            'date_added' => 'Date Added',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getStatustext() {
        $returnValue = "";
        $client_status = $this->vip_receipt_status;
        
        if ($client_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($client_status == "N") {
            $returnValue = '<span class="label label-info">Processing</span>';
        } else if ($client_status == "A") {
            $returnValue = '<span class="label label-success">Approve</span>';
        } else if ($client_status == "D") {
            $returnValue = '<span class="label label-danger">Decline</span>';
        } else if ($client_status == "R") {
            $returnValue = '<span class="label label-default">Drafts</span>';
        } else if ($client_status == "M") {
            $returnValue = '<span class="label label-primary">Modify</span>';
        }
        return $returnValue;
        
    }
}
