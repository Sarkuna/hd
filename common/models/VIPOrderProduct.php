<?php

namespace common\models;

use Yii;
use common\models\VIPOrder;

/**
 * This is the model class for table "vip_order_product".
 *
 * @property integer $order_product_id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $name
 * @property string $model
 * @property integer $quantity
 * @property integer $point
 * @property integer $point_total
 * @property string $price
 * @property string $total
 * @property integer $product_status
 *
 * @property VipOrder $order
 */
class VIPOrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_order_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'model', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity', 'point', 'point_total', 'product_status'], 'integer'],
            [['price', 'total'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['model'], 'string', 'max' => 64],
            //[['order_id'], 'unique'],
            //[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => VIPOrder::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_product_id' => 'Order Product ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'model' => 'Model',
            'quantity' => 'Quantity',
            'point' => 'Point',
            'point_total' => 'Point Total',
            'price' => 'Price',
            'total' => 'Total',
            'product_status' => 'Product Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(VipOrder::className(), ['order_id' => 'order_id']);
    }
}
