<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class Invoice_No_Form extends Model
{
    public $invoice_no;
    public $selected_image;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['invoice_no', 'required'],
            [['selected_image'], 'safe'],
        ];
    }

    
}
