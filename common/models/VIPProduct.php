<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

use common\models\VIPStockStatus;
use common\models\VIPAssignProducts;
use common\models\VIPManufacturer;
use common\models\Uploads;
use common\models\VIPOptionDescription;
use common\models\VIPOptionValueDescription;
use common\models\VIPProductOptionValue;
use common\models\VIPCategories;
use common\models\VIPSubCategories;

/**
 * This is the model class for table "vip_product".
 *
 * @property integer $product_id
 * @property integer $manufacturer_id
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property string $product_name
 * @property string $links
 * @property string $product_description
 * @property string $meta_tag_title
 * @property string $main_image
 * @property string $product_code
 * @property string $suppliers_product_code
 * @property string $variant
 * @property string $msrp
 * @property string $msrp_inc
 * @property integer $mark
 * @property string $bb_price
 * @property string $bb_price_rounded
 * @property integer $points_value
 * @property string $disc
 * @property string $partner_price
 * @property string $ptofit
 * @property string $partner_price_inc
 * @property integer $minimum
 * @property string $featured_status
 * @property string $status
 * @property integer $stock_status_id
 * @property string $date_available
 * @property string $price
 * @property integer $quantity
 * @property string $ref
 * @property string $remarks
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property VipAssignProducts[] $vipAssignProducts
 */
class VIPProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const UPLOAD_FOLDER= 'upload/photolibrarys';
    public $main_image1;
    public static function tableName()
    {
        return 'vip_product';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'category_id', 'sub_category_id', 'mark', 'points_value', 'minimum', 'stock_status_id', 'quantity', 'created_by', 'updated_by'], 'integer'],
            [['category_id', 'sub_category_id', 'product_name', 'product_code', 'stock_status_id'], 'required'],
            [['links', 'product_description'], 'string'],
            [['msrp', 'msrp_inc', 'bb_price', 'bb_price_rounded', 'partner_price', 'ptofit', 'partner_price_inc', 'price'], 'number'],
            [['remarks','date_available', 'created_datetime', 'updated_datetime'], 'safe'],
            [['product_name', 'meta_tag_title', 'main_image', 'variant'], 'string', 'max' => 255],
            [['product_code', 'suppliers_product_code'], 'string', 'max' => 100],
            [['disc'], 'string', 'max' => 50],
            [['featured_status', 'status'], 'string', 'max' => 1],
            [['ref'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'manufacturer_id' => 'Manufacturer ID',
            'category_id' => 'Category ID',
            'sub_category_id' => 'Sub Category ID',
            'product_name' => 'Product Name',
            'links' => 'Links',
            'product_description' => 'Product Description',
            'meta_tag_title' => 'Meta Tag Title',
            'main_image' => 'Main Image',
            'product_code' => 'Product Code',
            'suppliers_product_code' => 'Suppliers Product Code',
            'variant' => 'Variant',
            'msrp' => 'MSRP',
            'msrp_inc' => 'MSRP Inc',
            'mark' => 'Mark',
            'bb_price' => 'Bb Price',
            'bb_price_rounded' => 'Bb Price Rounded',
            'points_value' => 'Points Value',
            'disc' => 'Disc',
            'partner_price' => 'Partner Price',
            'ptofit' => 'Ptofit',
            'partner_price_inc' => 'Partner Price Inc',
            'minimum' => 'Minimum',
            'featured_status' => 'Featured Status',
            'status' => 'Status',
            'stock_status_id' => 'Stock Status ID',
            'date_available' => 'Date Available',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'ref' => 'Ref',
            'remarks' => 'Remarks',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignProduct()
    {
        return $this->hasOne(VIPAssignProducts::className(), ['productID' => 'product_id']);
    }
    
    public function getStockStatus()
    {
        return $this->hasOne(VipStockStatus::className(), ['stock_status_id' => 'stock_status_id']);
    }
    
    public function getManufacturer()
    {
        return $this->hasOne(VIPManufacturer::className(), ['manufacturer_id' => 'manufacturer_id']);
    }
    
    public function getCategories()
    {
        return $this->hasOne(VIPCategories::className(), ['vip_categories_id' => 'category_id']);
    }
    
    public function getSubCategories()
    {
        return $this->hasOne(VIPSubCategories::className(), ['vip_sub_categories_id' => 'sub_category_id']);
    }
    
    /*public function getAssignProducts123()
    {
        return $this->hasOne(VIPAssignProducts::className(), ['productID' => 'product_id'])->andOnCondition(['=','productID','177']);
    }*/
    
    public function getStatus() {
        $returnValue = "";
        if ($this->status == "E") {
            $returnValue = "<span class='label label-success'>Enabled</span>";
        } else if ($this->status == "D") {
            $returnValue = "<span class='label label-danger'>Disabled</span>";
        }
        return $returnValue;
    }
    
    public function getInfoProductBy($id)
    {
        $data = VIPProduct::find()->asArray()->where('product_id=:id',['id' => $id])->one();
        return $data;
    }
    
    public function getOptionsValue()
    {
        return $this->hasMany(VIPProductOptionValue::className(), ['product_id' => 'product_id']);
    }
    
    public function getImages()
    {
        return $this->hasMany(Uploads::className(), ['ref' => 'ref']);
    }

        public static function getUploadPath(){
        return Yii::getAlias('@webroot').'/'.self::UPLOAD_FOLDER.'/';
    }
    public static function getUploadUrl(){
        return Url::base(true).'/'.self::UPLOAD_FOLDER.'/';
    }
    public function getThumbnails($ref,$event_name){
         $uploadFiles   = Uploads::find()->where(['ref'=>$ref])->all();
         $preview = [];
        foreach ($uploadFiles as $file) {
            $preview[] = [
                'label'=>'Home',
                'url'=>self::getUploadUrl(true).$ref.'/'.$file->real_filename,
                'src'=>self::getUploadUrl(true).$ref.'/thumbnail/'.$file->real_filename,
                'options' => ['title' => $event_name, 'class'=>'col-lg-4 col-xs-6'],
                //'options'=> ['class'=>'list-group-item'],
            ];
        }
        return $preview;
    }
    
    public static function getHierarchy() {
        $options = [];
         
        $parents = VIPOptionDescription::find()->all();
        
        foreach($parents as $id => $p) {
            $children = VIPOptionValueDescription::find()->where(["option_id" => $p->option_id])->all();
            $child_options = [];
            foreach($children as $child) {
                $child_options[$child->option_value_id] = $child->name;
            }
            $options[$p->name] = $child_options;
        }
        return $options;
    }
    
    public function getVipProductOptionValueDescriptions()
    {
        return $this->hasMany(VIPProductOptionValue::className(), ['product_id' => 'product_id']);
    }
    
    public function getDeliveryZones()
    {
        return $this->hasMany(VIPProductDeliveryZone::className(), ['product_id' => 'product_id']);
    }
}
