<?php

namespace common\models;

use Yii;
use yii\base\Model;


class AssignCategoriesForm extends Model
{

    //public $block;
    //public $residence_type;
    //public $message_type;
    public $categories_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categories_id'], 'required'],
            [['categories_id'], 'validateCheckBox'],
            //[['condoname','date_from','date_to','status'], 'safe']
            //[['block','residence_type','message_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //'block' => 'Block',
            //'residence_type' => 'Residence Type',
            //'message_type' => 'Message Type',
            'categories_id' => 'Categories',
        ];
    }
    
    public function validateCheckBox($attribute, $params) {
        $error=true;
        if (count($this->categories_id) > 2){
            $error=true;
        }
        if($error){
            $this->addError($attribute, 'Kindly restrict yourself for less than two options');
        }
    }

    public function listCheck(){
        $error=true;
        foreach($this->categories_id as $list_id=>$selection){
            if($selection){
                $error=false;
            }
        }
        if($error){
            $this->addError('categories_id','Select atleast one list to continue');
        }
    }

    
}
