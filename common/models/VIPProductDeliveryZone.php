<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_product_delivery_zone".
 *
 * @property integer $product_option_value_id
 * @property integer $product_id
 * @property integer $clientID
 * @property integer $zone_id
 * @property integer $quantity
 * @property string $price
 */
class VIPProductDeliveryZone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_product_delivery_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'clientID', 'zone_id', 'price'], 'required'],
            [['product_id', 'clientID', 'zone_id', 'quantity'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_option_value_id' => 'Product Option Value ID',
            'product_id' => 'Product ID',
            'clientID' => 'Client ID',
            'zone_id' => 'Zone ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
        ];
    }
    
    public function getZoneName()
    {
        return $this->hasOne(VIPZone::className(), ['zone_id' => 'zone_id']);
    }
}
