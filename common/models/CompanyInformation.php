<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "company_information".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $company_name
 * @property integer $company_reg_type
 * @property string $company_registration_number
 * @property string $mailing_address
 * @property string $tel
 * @property string $fax
 * @property string $contact_person
 * @property string $contact_no
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 */
class CompanyInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_information';
    }
    
    /**
     * @behaviors
     */
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_name'], 'required'],
            [['user_id', 'company_reg_type', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['company_name'], 'string', 'max' => 150],
            [['company_registration_number'], 'string', 'max' => 50],
            [['mailing_address', 'contact_person'], 'string', 'max' => 300],
            [['tel', 'fax', 'contact_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_name' => 'Company Name',
            'company_reg_type' => 'Registration Type',
            'company_registration_number' => 'Registration Number',
            'cidb_no' => 'CIDB No',
            'mailing_address' => 'Company Address',
            'tel' => 'Tel',
            'fax' => 'Fax',
            'contact_person' => 'Contact Person',
            'contact_no' => 'Contact No',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }
    
    public function getRegistration() {
        return $this->hasOne(\common\models\RegistrationType::className(), ['registration_type_id' => 'company_reg_type']);
    }
}
