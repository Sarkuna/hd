<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ProjectActionForm extends Model
{
    public $status;
    public $point;
    public $remark;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status'], 'string', 'max' => 1],
            ['point', 'required',
                'when' => function ($model) {
                    return $model->status == 'A';
                }, 'whenClient' => "function (attribute, value) {
                    return $('#projectactionform-status').val() == 'A';
                }"
            ],  
            [['remark'], 'string', 'max' => 600],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'status' => 'Status',
            'point' => 'Approve Points',
            'remark' => 'Remark\'s',
        ];
    }

    
}
