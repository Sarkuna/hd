<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "point_upload_summary".
 *
 * @property integer $point_upload_summary_id
 * @property integer $clientID
 * @property string $description
 * @property string $effective_month
 * @property string $expiry_date
 * @property integer $type
 * @property integer $acc_division
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class PointUploadSummary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $excel;
    public $agreevalue;
    public static function tableName()
    {
        return 'point_upload_summary';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'effective_month', 'expiry_date'], 'required'],
            ['excel', 'required', 'on' => 'create'],
            [['clientID', 'created_by', 'updated_by', 'type', 'acc_division', 'agreevalue'], 'integer'],
            [['description'], 'string'],
            [['effective_month', 'expiry_date', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point_upload_summary_id' => 'Point Upload Summary ID',
            'clientID' => 'Client ID',
            'description' => 'Description',
            'effective_month' => 'Effective Month',
            'expiry_date' => 'Expiry Date',
            'type' => 'User Type',
            'acc_division' => 'Value/%',
            'agreevalue' => 'Add value to account2?',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getNoofAccounts()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->count();
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getNoofAccounts2()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
            ->andWhere(['>', 'points2', 0])
            ->count();
    }
    
    public function getTotalPts()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->sum('points');
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getTotalPts2()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->sum('points2');
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getNoofAccountsSuccess()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->
           andWhere(['status' => 'S'])->count();
    }
    
    public function getNoofAccountsSuccess2()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
            ->andWhere(['status' => 'S'])
            ->andWhere(['>', 'points2', 0])   
            ->count();
    }
    
    public function getInvalidAccounts()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->
           andWhere(['status' => 'F'])->count();
    }
    
    public function getInvalidAccounts2()
    {
       return $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])->
           andWhere(['status' => 'F'])->andWhere(['>', 'points2', 0])->count();
    }
    
    public function getTotalPtsSuccess()
    {
        $return = $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
               ->andWhere(['status' => 'S'])
               ->sum('points');
        if(!empty($return)) {
            return $return; 
        }else {
            return 0;
        } 
    }
    
    public function getTotalPtsSuccess2()
    {
        $return = $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
               ->andWhere(['status' => 'S'])
               ->sum('points2');
        if(!empty($return)) {
            return $return; 
        }else {
            return 0;
        }       
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getTotalPtsInvalid()
    {
        $return = $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
               ->andWhere(['status' => 'F'])
               ->sum('points');
        if(!empty($return)) {
            return $return; 
        }else {
            return 0;
        }        
           //andWhere(['imageable_type' => 'Ingredient']);
    }
    
    public function getTotalPtsInvalid2()
    {
        $return = $this->hasMany(\common\models\PointUploadSummaryReport::className(), ['point_upload_summary_id' => 'point_upload_summary_id'])
               ->andWhere(['status' => 'F'])
               ->sum('points2');
        if(!empty($return)) {
            return $return; 
        }else {
            return 0;
        } 
    }
}
