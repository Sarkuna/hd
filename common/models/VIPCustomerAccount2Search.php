<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomerAccount2;

/**
 * VIPCustomerAccount2Search represents the model behind the search form about `common\models\VIPCustomerAccount2`.
 */
class VIPCustomerAccount2Search extends VIPCustomerAccount2
{
    /**
     * @inheritdoc
     */
    public $code;
    public $company_name;
            
    public function rules()
    {
        return [
            [['account2_id', 'clientID', 'customer_id', 'indirect_id', 'points_in', 'created_by', 'updated_by'], 'integer'],
            [['description', 'date_added', 'created_datetime', 'updated_datetime', 'code', 'company_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPCustomerAccount2::find()->groupBy('customer_id');
        $query->joinWith(['customer','companyInfo']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'account2_id' => $this->account2_id,
            'vip_customer_account2.clientID' => $this->clientID,
            'customer_id' => $this->customer_id,
            'indirect_id' => $this->indirect_id,
            'points_in' => $this->points_in,
            //'points_out' => $this->points_out,
            'date_added' => $this->date_added,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        
        $query->andFilterWhere(['like', 'clients_ref_no', $this->code]);
        $query->andFilterWhere(['like', 'company_information.company_name', $this->company_name]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
