<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "sms_queue".
 *
 * @property integer $id
 * @property integer $sms_template_id
 * @property integer $clientID
 * @property integer $user_id
 * @property string $mobile
 * @property string $data
 * @property string $status
 * @property string $date_to_send
 * @property string $remark
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class SMSQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_queue';
    }
    
    /**
     * @behaviors
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sms_template_id', 'clientID', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['data', 'remark'], 'string'],
            [['date_to_send', 'created_datetime', 'updated_datetime', 'sms_type'], 'safe'],
            [['created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'required'],
            [['mobile'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_template_id' => 'Sms Template ID',
            'clientID' => 'Client ID',
            'user_id' => 'User ID',
            'mobile' => 'Mobile',
            'data' => 'Data',
            'status' => 'Status',
            'date_to_send' => 'Date To Send',
            'remark' => 'Remark',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
