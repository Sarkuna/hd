<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

/**
 * VIPOrderSearch represents the model behind the search form about `common\models\VIPOrder`.
 */
class VIPOrderSearch extends VIPOrder
{
    /**
     * @inheritdoc
     */
    public $clients_ref_no;
    public $full_name;
    public $company_name;
    public $type;
    public function rules()
    {
        return [
            [['order_id', 'invoice_no', 'clientID', 'customer_id', 'customer_group_id', 'shipping_country_id', 'shipping_zone_id', 'order_status_id', 'language_id', 'created_by', 'updated_by'], 'integer'],
            [['invoice_prefix', 'shipping_firstname', 'shipping_lastname', 'shipping_company', 'shipping_address_1', 'shipping_address_2', 'shipping_city', 'shipping_postcode', 'shipping_zone', 'shipping_method', 'shipping_code', 'comment', 'ip', 'user_agent', 'accept_language', 'created_datetime', 'updated_datetime','clients_ref_no','full_name','company_name','bb_invoice_no','type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPOrder::find();
        $query->joinWith(['customer','company','user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_datetime'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'invoice_no' => $this->invoice_no,
            'vip_order.clientID' => $this->clientID,
            'customer_id' => $this->customer_id,
            'customer_group_id' => $this->customer_group_id,
            'shipping_country_id' => $this->shipping_country_id,
            'shipping_zone_id' => $this->shipping_zone_id,
            'order_status_id' => $this->order_status_id,
            'language_id' => $this->language_id,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'invoice_prefix', $this->invoice_prefix])
            ->andFilterWhere(['like', 'shipping_firstname', $this->shipping_firstname])
            ->andFilterWhere(['like', 'shipping_lastname', $this->shipping_lastname])
            ->andFilterWhere(['like', 'shipping_company', $this->shipping_company])
            ->andFilterWhere(['like', 'shipping_address_1', $this->shipping_address_1])
            ->andFilterWhere(['like', 'shipping_address_2', $this->shipping_address_2])
            ->andFilterWhere(['like', 'shipping_city', $this->shipping_city])
            ->andFilterWhere(['like', 'shipping_postcode', $this->shipping_postcode])
            ->andFilterWhere(['like', 'shipping_zone', $this->shipping_zone])
            //->andFilterWhere(['like', 'shipping_method', $this->shipping_method])
            //->andFilterWhere(['like', 'shipping_code', $this->shipping_code])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'bb_invoice_no', $this->bb_invoice_no])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'user_agent', $this->user_agent])
            ->andFilterWhere(['like', 'vip_customer.clients_ref_no', $this->clients_ref_no])
            ->andFilterWhere(['like', 'vip_customer.full_name', $this->full_name])
            ->andFilterWhere(['like', 'vip_customer.full_name', $this->full_name])
            ->andFilterWhere(['like', 'user.type', $this->type])    
            ->andFilterWhere(['like', 'accept_language', $this->accept_language]);

        return $dataProvider;
    }
}
