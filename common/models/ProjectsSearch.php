<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Projects;

/**
 * ProjectsSearch represents the model behind the search form about `common\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    /**
     * @inheritdoc
     */
    public $user_name;
    public $type;
    public function rules()
    {
        return [
            [['projects_id', 'userID', 'clientID', 'dealerID', 'project_nominated_points', 'project_approved_points'], 'integer'],
            [['projects_ref', 'projects_date', 'project_name', 'project_status', 'project_client_name', 'project_client_email', 'project_client_tel', 'project_details', 'ref','admin_status','user_name','type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();
        $query->joinWith(['customer']);
        $query->joinWith(['user']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'projects_id' => $this->projects_id,
            'projects.userID' => $this->userID,
            'clientID' => $this->clientID,
            'dealerID' => $this->dealerID,
            'projects_date' => $this->projects_date,
            'project_nominated_points' => $this->project_nominated_points,
            'project_approved_points' => $this->project_approved_points,
            'admin_status' => $this->admin_status,
        ]);

        $query->andFilterWhere(['like', 'projects_ref', $this->projects_ref])
            ->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'project_status', $this->project_status])
            ->andFilterWhere(['like', 'project_client_name', $this->project_client_name])
            ->andFilterWhere(['like', 'project_client_email', $this->project_client_email])
            ->andFilterWhere(['like', 'project_client_tel', $this->project_client_tel])
            ->andFilterWhere(['like', 'project_details', $this->project_details])
            ->andFilterWhere(['like', 'vip_customer.full_name', $this->user_name])
            ->andFilterWhere(['like', 'user.type', $this->type]) 
            ->andFilterWhere(['like', 'ref', $this->ref]);
        

        return $dataProvider;
    }
}
