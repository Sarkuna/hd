<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_announcement".
 *
 * @property integer $announcement_id
 * @property integer $clientID
 * @property string $message
 * @property string $type
 * @property string $lang
 * @property string $status
 */
class VIPAnnouncement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_announcement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'message', 'type'], 'required'],
            [['clientID'], 'integer'],
            [['message'], 'string'],
            [['type', 'status'], 'string', 'max' => 1],
            [['lang'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'announcement_id' => 'Announcement ID',
            'clientID' => 'Client ID',
            'message' => 'Message',
            'type' => 'Type',
            'lang' => 'Lang',
            'status' => 'Status',
        ];
    }
    
    public function getStatustext() {
        $returnValue = "";
        $client_status = $this->type;
        if ($client_status == "N") {
            $returnValue = '<span class="label label-warning">Not Approve User</span>';
        } else if ($client_status == "A") {
            $returnValue = '<span class="label label-success">Approve User</span>';
        }
        return $returnValue;
        
    }
}
