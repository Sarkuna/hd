<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\VIPCustomer;

/**
 * This is the model class for table "vip_customer_reward".
 *
 * @property integer $customer_reward_id
 * @property integer $clientID
 * @property integer $customer_id
 * @property integer $order_id
 * @property string $description
 * @property integer $points
 * @property string $date_added
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomerReward extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_reward';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'description', 'date_added', 'points','bb_type'], 'required'],
            [['clientID', 'customer_id', 'order_id', 'points', 'created_by', 'updated_by'], 'integer'],
            [['description','bb_type'], 'string'],
            [['date_added', 'created_datetime', 'updated_datetime','points_void'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_reward_id' => 'Customer Reward ID',
            'clientID' => 'Client ID',
            'customer_id' => 'Customer ID',
            'order_id' => 'Order ID',
            'description' => 'Description',
            'points' => 'Points',
            'bb_type' => 'Type',
            'date_added' => 'Date Added',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getCustomer()
    {
        return $this->hasOne(VIPCustomer::className(), ['userID' => 'customer_id']);
    }
}
