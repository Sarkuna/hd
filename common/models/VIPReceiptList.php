<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_receipt_list".
 *
 * @property integer $vip_receipt_list_id
 * @property integer $vip_receipt_id
 * @property integer $userID
 * @property integer $clientID
 * @property integer $product_list_id
 * @property integer $product_per_value
 * @property integer $product_item_qty
 * @property integer $user_selector
 * @property integer $reseller_selector
 * @property integer $profile_selector
 */
class VIPReceiptList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_receipt_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_receipt_id', 'userID', 'clientID', 'product_list_id', 'product_per_value', 'product_item_qty', 'user_selector', 'reseller_selector', 'profile_selector'], 'required'],
            [['vip_receipt_id', 'userID', 'clientID', 'product_list_id', 'product_per_value', 'product_item_qty', 'user_selector', 'reseller_selector', 'profile_selector'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_receipt_list_id' => 'Vip Receipt List ID',
            'vip_receipt_id' => 'Vip Receipt ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'product_list_id' => 'Product List ID',
            'product_per_value' => 'Product Per Value',
            'product_item_qty' => 'Product Item Qty',
            'user_selector' => 'User Selector',
            'reseller_selector' => 'Reseller Selector',
            'profile_selector' => 'Profile Selector',
        ];
    }
    
    public function getProduct() {
        return $this->hasOne(\common\models\BBPoints::className(), ['bb_points_id' => 'product_list_id']);
    }
}
