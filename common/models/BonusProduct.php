<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

use common\models\VIPCustomer;

/**
 * This is the model class for table "bonus_product".
 *
 * @property integer $bonus_product_id
 * @property integer $clientID
 * @property integer $userID
 * @property integer $vip_receipt_id
 * @property string $product_description
 * @property string $pack_size
 * @property integer $base_points
 * @property string $base_rm
 * @property integer $qty
 * @property string $total_pack_size
 * @property integer $total_pack_point
 * @property string $total_pack_rm
 * @property string $date_of_receipt
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class BonusProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_product';
    }
    
    /**
     * @behaviors
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'userID', 'vip_receipt_id', 'product_description', 'pack_size', 'base_points', 'base_rm', 'qty', 'total_pack_size', 'total_pack_point', 'total_pack_rm', 'date_of_receipt'], 'required'],
            [['clientID', 'userID', 'vip_receipt_id', 'base_points', 'qty', 'total_pack_point', 'created_by', 'updated_by'], 'integer'],
            [['pack_size', 'base_rm', 'total_pack_size', 'total_pack_rm'], 'number'],
            [['date_of_receipt', 'created_datetime', 'updated_datetime'], 'safe'],
            [['product_description'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_product_id' => 'Bonus Product ID',
            'clientID' => 'Client ID',
            'userID' => 'User ID',
            'vip_receipt_id' => 'Vip Receipt ID',
            'product_description' => 'Product Description',
            'pack_size' => 'Pack Size',
            'base_points' => 'Base Points',
            'base_rm' => 'Base Rm',
            'qty' => 'Qty',
            'total_pack_size' => 'Total Pack Size',
            'total_pack_point' => 'Total Pack Point',
            'total_pack_rm' => 'Total Pack Rm',
            'date_of_receipt' => 'Date Of Receipt',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getCustomer()
    {
        return $this->hasOne(VIPCustomer::className(), ['userID' => 'userID']);
    }
}
