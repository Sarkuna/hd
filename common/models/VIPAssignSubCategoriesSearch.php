<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPAssignSubCategories;

/**
 * VIPAssignSubCategoriesSearch represents the model behind the search form about `common\models\VIPAssignSubCategories`.
 */
class VIPAssignSubCategoriesSearch extends VIPAssignSubCategories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_assign_sub_categories_id', 'clientID', 'categories_id', 'sub_categories_id'], 'integer'],
            [['status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPAssignSubCategories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_assign_sub_categories_id' => $this->vip_assign_sub_categories_id,
            'clientID' => $this->clientID,
            'categories_id' => $this->categories_id,
            'sub_categories_id' => $this->sub_categories_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
