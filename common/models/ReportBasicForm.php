<?php

namespace common\models;

use Yii;
use yii\base\Model;
//use kartik\password\StrengthValidator;


class ReportBasicForm extends Model
{

    public $type;
    //public $confirm_password;
    //public $oldPassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Type',
        ];
    }

    
}
