<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_option".
 *
 * @property integer $option_ID
 * @property integer $clientID
 * @property string $invoice_prefix
 */
class ClientOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_prefix'], 'required'],
            [['clientID'], 'integer'],
            [['invoice_prefix'], 'string', 'max' => 20],
            [['receipts_prefix'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_ID' => 'Option  ID',
            'clientID' => 'Client  ID',
            'invoice_prefix' => 'Invoice Prefix',
            'receipts_prefix' => 'Receipts Prefix'
        ];
    }
}
