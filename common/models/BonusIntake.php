<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "bonus_intake".
 *
 * @property integer $bonus_intake_id
 * @property integer $clientID
 * @property integer $bonus_intake_year
 * @property integer $bonus_intake_month
 * @property integer $bonus_intake_type
 * @property integer $bonus_intake_target
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class BonusIntake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'bonus_intake';
    }
    
    /**
     * @behaviors
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'bonus_intake_year', 'bonus_intake_month', 'bonus_intake_type', 'bonus_intake_target'], 'required'],
            [['clientID', 'bonus_intake_year', 'bonus_intake_month', 'bonus_intake_type', 'bonus_intake_target', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_intake_id' => 'Bonus Intake ID',
            'clientID' => 'Client ID',
            'bonus_intake_year' => 'Year',
            'bonus_intake_month' => 'Month',
            'bonus_intake_type' => 'Interval',
            'bonus_intake_target' => 'Total Annual Target',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
