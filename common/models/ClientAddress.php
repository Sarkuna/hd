<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_address".
 *
 * @property integer $client_address_ID
 * @property integer $clientID
 * @property string $name
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $state
 * @property string $postcode
 * @property integer $country_id
 * @property string $email_address
 * @property string $telephone
 * @property string $fax_no
 * @property string $mobile_no
 */
class ClientAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'address_1', 'address_2', 'city', 'postcode', 'country_id'], 'required'],
            [['clientID', 'country_id'], 'integer'],
            [['name', 'state', 'email_address'], 'string', 'max' => 200],
            [['address_1', 'address_2', 'city'], 'string', 'max' => 128],
            [['postcode'], 'string', 'max' => 10],
            [['telephone', 'fax_no', 'mobile_no'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_address_ID' => 'Client Address  ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'address_1' => 'Address 1',
            'address_2' => 'Address 2',
            'city' => 'City',
            'state' => 'State',
            'postcode' => 'Postcode',
            'country_id' => 'Country ID',
            'email_address' => 'Email Address',
            'telephone' => 'Telephone',
            'fax_no' => 'Fax No',
            'mobile_no' => 'Mobile No'
        ];
    }
    
    public function getCountry()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'country_id']);
    }
}
