<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_manufacturer".
 *
 * @property integer $manufacturer_id
 * @property string $name
 * @property string $image
 * @property integer $sort_order
 */
class VIPManufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file_image;
    public static function tableName()
    {
        return 'vip_manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['image'], 'string', 'max' => 255],
            /*['file_image', 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg', 'gif', 'png', 'bmp',
                'minHeight' => 90, 'maxHeight' =>90,
            ],*/
            [['file_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpeg, jpg, png, gif','wrongExtension'=>'You can only upload following files: {extensions}','maxSize' => 5242880, 'tooBig' => 'Maximum file size cannot exceed 5MB', 'maxFiles' => 4, 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Manufacturer Name',
            'file_image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }
}
