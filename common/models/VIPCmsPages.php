<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_cms_pages".
 *
 * @property integer $page_id
 * @property integer $clientID
 * @property string $page_title
 * @property string $page_description
 * @property integer $position
 * @property string $type
 * @property string $status
 */
class VIPCmsPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_cms_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'page_title', 'page_description', 'type'], 'required'],
            [['clientID', 'position'], 'integer'],
            [['page_description'], 'string'],
            [['page_title'], 'string', 'max' => 300],
            [['type', 'status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'clientID' => 'Client ID',
            'page_title' => 'Page Title',
            'page_description' => 'Page Description',
            'position' => 'Position',
            'type' => 'Type',
            'status' => 'Status',
        ];
    }
}
