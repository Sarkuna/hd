<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\ClientUserAssign;

/**
 * UserProfileSearch represents the model behind the search form about `common\models\UserProfile`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public $email;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['username','user_type','email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['adminUserProfile']);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'user.client_id' => $this->client_id,
            //'email' => $this->email,
            'user.user_type' => $this->user_type,
        ]);

        //$query->andFilterWhere(['like', 'user.client_id', $this->client_id])
            //->andFilterWhere(['like', 'user.email', $this->email]);
            /*->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'address1', $this->address1])
            ->andFilterWhere(['like', 'address2', $this->address2])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postcode', $this->postcode])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'image', $this->image]);*/

        return $dataProvider;
    }
}