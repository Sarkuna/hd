<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;
use common\models\TypeName;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $status
 * @property string $approved
 * @property string $newsletter
 * @property string $user_type
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 *
 * @property Client $client
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    const STATUS_DEACTIVE = 'D';
    const STATUS_NOTVERIFIED = 'N';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',                
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            //['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_PENDING, self::STATUS_DEACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE,self::STATUS_NOTVERIFIED]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    /*public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }*/
    
    public static function findByEmailBackend($email) {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        $user = static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE, 'user_type' => ['A','B','C']]);
        
        
        if ($user) {
            $session['currentusertype'] = $user->user_type;
            $returnValue = $user;
        }
        return $returnValue;
    }
    
    public static function findByEmailFrontend($email) {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        $session = Yii::$app->session;
        $currentclientID = $session['currentclientID'];
        $returnValue = false;
        
        
        if (strpos($email, '@') !== false) {
            $user = static::findOne(['email' => $email, 'email_verification' => 'Y', 'status'=>['A','N'], 'client_id' => $currentclientID, 'user_type' => 'D']);
        } else {
            //Otherwise we search using the username
            $user = static::findOne(['username' => $email, 'status'=>['A','N'], 'client_id' => $currentclientID, 'user_type' => 'D']);
        }
        //echo '<pre>';print_r($user).die();
        if ($user) {
            $session['currentusertype'] = $user->user_type;
            $session['approved'] = $user->approved;
            $returnValue = $user;
        }
        
        return $returnValue;
    }
    
    public static function findByTokenFrontend($token) {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        //$session = Yii::$app->session;
        //$currentclientID = $session['currentclientID'];
        $returnValue = false;
        //'client_id' => '10',
        $user = static::findOne(['access_token' => $token, 'status'=>['A','N'], 'user_type' => 'D']);
        if(count($user) > 0){
            if ($user) {
                
                $session['currentusertype'] = $user->user_type;
                $session['approved'] = $user->approved;
                $session['currentclientID'] = $user->client_id;
                $returnValue = $user;
            }
        }else {
            $session = Yii::$app->session;
            $session->destroy();
        }
        return $returnValue;
    }
    
    public static function findByTokenStoreFrontend($token) {
        $returnValue = false;
        $user = static::findOne(['token' => $token, 'status'=>['A','N']]);
        if(count($user) > 0){
            if ($user) {                
                $session['currentusertype'] = $user->user_type;
                $session['approved'] = $user->approved;
                $session['currentclientID'] = $user->client_id;
                $returnValue = $user;
                $user->token = '';
                $user->save(false);
            }
        }else {
            $session = Yii::$app->session;
            $session->destroy();
        }
        return $returnValue;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            //'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    /**
     * Removes password reset token
     */
    public function removeAccessToken()
    {
        $this->access_token = null;
    }
    
    public function removeActivationKeyToken() {
        $this->activation_key = null;
    }
    
     public function login()
    {
         if ($this->validate()) {
             return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
         }
         return false;
    }
    
    public function getStatustext() {
        $returnValue = "";
        $client_status = $this->status;
        if ($client_status == "X") {
            $returnValue = '<span class="label label-danger">Deleted</span>';
        } else if ($client_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($client_status == "C") {
            $returnValue = '<span class="label label-warning">Cancel</span>';
        } else if ($client_status == "A") {
            $returnValue = '<span class="label label-success">Approved</span>';
        } else if ($client_status == "D") {
            $returnValue = '<span class="label label-danger">Deactive</span>';
        } else if ($client_status == "N") {
            $returnValue = '<span class="label label-info">Not Approved</span>';
        }
        return $returnValue;
        
    }
    
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['clientID' => 'client_id']);
    }
    
    public function getAdminUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['userID' => 'id']);
    }
    
    public function getTypeName()
    {
        $session = Yii::$app->session;
        $client_id = 16;
        return $this->hasOne(TypeName::className(), ['tier_id' => 'type'])->andWhere(['clientID' => $client_id]);
    }
    
}
