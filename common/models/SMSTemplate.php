<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_template".
 *
 * @property integer $id
 * @property string $code
 * @property integer $clientID
 * @property string $name
 * @property string $template
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class SMSTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'clientID', 'name', 'template', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'required'],
            [['clientID', 'created_by', 'updated_by'], 'integer'],
            [['template'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'template' => 'Template',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
