<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomerAddress;

/**
 * VIPCustomerAddressSearch represents the model behind the search form about `common\models\VIPCustomerAddress`.
 */
class VIPCustomerAddressSearch extends VIPCustomerAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vip_customer_address_id', 'userID', 'clientID', 'country_id', 'zone_id', 'created_by', 'updated_by'], 'integer'],
            [['firstname', 'lastname', 'company', 'address_1', 'address_2', 'city', 'postcode', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPCustomerAddress::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vip_customer_address_id' => $this->vip_customer_address_id,
            'userID' => $this->userID,
            'clientID' => $this->clientID,
            'country_id' => $this->country_id,
            'zone_id' => $this->zone_id,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'address_1', $this->address_1])
            ->andFilterWhere(['like', 'address_2', $this->address_2])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postcode', $this->postcode]);

        return $dataProvider;
    }
}
