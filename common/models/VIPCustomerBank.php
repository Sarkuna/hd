<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_customer_bank".
 *
 * @property integer $vip_customer_bank_id
 * @property integer $userID
 * @property integer $clientID
 * @property integer $bank_name_id
 * @property string $account_name
 * @property string $account_number
 * @property string $nric_passport
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class VIPCustomerBank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_customer_bank';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'clientID', 'bank_name_id', 'account_name', 'account_number', 'nric_passport'], 'required'],
            [['userID', 'clientID', 'bank_name_id', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime','bank_status'], 'safe'],
            [['account_name'], 'string', 'max' => 200],
            [['account_number'], 'string', 'max' => 50],
            [['nric_passport'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_customer_bank_id' => 'Vip Customer Bank ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'bank_name_id' => 'Bank Name',
            'account_name' => 'Account Name',
            'account_number' => 'Account Number',
            'nric_passport' => 'NRIC/Passport',
            'bank_status' => 'Verify',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getBank() {
        return $this->hasOne(\common\models\Banks::className(), ['id' => 'bank_name_id']);
    }
    

    public function getStatus() {
        $returnValue = "";
        $bank_status = $this->bank_status;
        if ($bank_status == "P") {
            $returnValue = '<span class="label label-warning">Pending</span>';
        } else if ($bank_status == "V") {
            $returnValue = '<span class="label label-success">Verified</span>';
        } else if ($bank_status == "N") {
            $returnValue = '<span class="label label-danger">Not Verify</span>';
        }
        return $returnValue;
        
    }
}
