<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIPCustomerReward;

/**
 * VIPCustomerRewardSearch represents the model behind the search form about `common\models\VIPCustomerReward`.
 */
class VIPCustomerRewardSearch extends VIPCustomerReward
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_reward_id', 'clientID', 'customer_id', 'order_id', 'points', 'created_by', 'updated_by'], 'integer'],
            [['description', 'date_added', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIPCustomerReward::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date_added'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_reward_id' => $this->customer_reward_id,
            'clientID' => $this->clientID,
            'customer_id' => $this->customer_id,
            'order_id' => $this->order_id,
            'points' => $this->points,
            'date_added' => $this->date_added,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
