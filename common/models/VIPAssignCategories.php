<?php

namespace common\models;

use Yii;
use common\models\VIPAssignCategories;
/**
 * This is the model class for table "vip_assign_categories".
 *
 * @property integer $vip_assign_categories_id
 * @property integer $clientID
 * @property integer $categories_id
 * @property string $status
 */
class VIPAssignCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_assign_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'categories_id'], 'required'],
            [['clientID', 'categories_id'], 'integer'],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vip_assign_categories_id' => 'Vip Assign Categories ID',
            'clientID' => 'Client ID',
            'categories_id' => 'Categories ID',
            'status' => 'Status',
        ];
    }
    
    public function getCategories()
    {
        //return $this->hasOne(VIPCategories::className(), ['vip_categories_id' => 'categories_id']);
        return $this->hasOne(VIPCategories::className(), ['vip_categories_id' => 'categories_id']);
    }
}
