<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_modules".
 *
 * @property integer $module_id
 * @property string $module_name
 * @property string $remark
 */
class VIPModules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_modules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_name'], 'required'],
            [['remark'], 'string'],
            [['module_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'module_name' => 'Module Name',
            'remark' => 'Remark',
        ];
    }
}
