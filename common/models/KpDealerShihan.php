<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "kp_dealer_shihan".
 *
 * @property integer $dealer_Id
 * @property string $dealer_CustomerCode
 * @property string $dealer_Name
 * @property string $dealer_Email
 * @property string $dealer_PoBox
 * @property string $dealer_Tel
 * @property string $dealer_DtCreate
 * @property string $dealer_CGrp
 * @property string $dealer_Rg
 * @property string $dealer_Category
 * @property string $dealer_Pass
 * @property string $dealer_DtUpdate
 * @property integer $dealer_Login
 */
class KpDealerShihan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kp_dealer_shihan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dealer_CustomerCode', 'dealer_Name'], 'required'],
            [['dealer_DtCreate', 'dealer_DtUpdate', 'del'], 'safe'],
            [['dealer_Login'], 'integer'],
            [['dealer_CustomerCode'], 'string', 'max' => 12],
            [['dealer_Name', 'dealer_Email'], 'string', 'max' => 50],
            [['dealer_PoBox'], 'string', 'max' => 6],
            [['dealer_Tel', 'dealer_Category'], 'string', 'max' => 20],
            [['dealer_CGrp'], 'string', 'max' => 3],
            [['dealer_Rg'], 'string', 'max' => 4],
            [['dealer_Pass'], 'string', 'max' => 15],
            [['dealer_CustomerCode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dealer_Id' => 'Dealer  ID',
            'dealer_CustomerCode' => 'Dealer  Customer Code',
            'dealer_Name' => 'Dealer  Name',
            'dealer_Email' => 'Dealer  Email',
            'dealer_PoBox' => 'Dealer  Po Box',
            'dealer_Tel' => 'Dealer  Tel',
            'dealer_DtCreate' => 'Dealer  Dt Create',
            'dealer_CGrp' => 'Dealer  Cgrp',
            'dealer_Rg' => 'Dealer  Rg',
            'dealer_Category' => 'Dealer  Category',
            'dealer_Pass' => 'Dealer  Pass',
            'dealer_DtUpdate' => 'Dealer  Dt Update',
            'dealer_Login' => 'Dealer  Login',
        ];
    }
}
