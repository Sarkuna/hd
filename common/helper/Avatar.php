<?php

namespace common\helper;

use Yii;
use YoHang88\LetterAvatar\LetterAvatar;

class Avatar {
    
    public static function generateAvatarFromLetter($letter = 'No Name', $setting = array()){
        $parrams = array(
            'shape' => 'circle',
            'mimetype' => 'image/png',
            'size' => 120,
            'quality' => 90
        );

        if(empty($letter)){
            $letter = 'No Name';
        }
        if(!empty($setting['shape'])){
            $parrams['shape'] = $setting['shape'];
        }
        if(!empty($setting['mimetype'])){
            $parrams['mimetype'] = $setting['mimetype'];
        }
        if(!empty($setting['size'])){
            $parrams['size'] = $setting['size'];
        }
        if(!empty($setting['quality'])){
            $parrams['quality'] = $setting['quality'];
        }
        //Circle Shape, Size 120px
        $avatar = new LetterAvatar($letter, $parrams['shape'], $parrams['size']);
        $imageObj = $avatar->generate()->encode($parrams['mimetype'], $parrams['quality']);
        return $imageObj->encoded;
    }
}
