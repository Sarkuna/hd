<?php

namespace common\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\base\Component;
use yii\base\Exception;
use mPDF;
use kartik\mpdf\Pdf;

class VIPGlobal extends Component {
    
    public function getProfileInfo() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $userID = \Yii::$app->user->id;

        $profile = \common\models\VIPCustomer::find()
            ->where(['userID' => $userID])
            ->one();
        $company = \common\models\CompanyInformation::find()
            ->where(['user_id' => $userID])
            ->one();
        
        //$session['distributors'] = $profile->kis_category_id;
        //$picPro = '<img alt="avatar" src="'.$this->Avatar($profile->full_name).'" class="round" height="40" width="40">';
        $users = [
            'full_name' => $profile->full_name,
            'code' => $profile->clients_ref_no,
            'company_name' => $company->company_name,
        ];

        return $users;
    }
    
    public function getSiteInfo() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        //$userID = \Yii::$app->user->id;

        $client = \common\models\Client::find()
            ->where(['clientID' => $clientID])
            ->one();

        if (!empty($client->company_logo)) {
            $logo = Yii::getAlias('@back') . '/upload/client_logos/' . $client->company_logo;
        } else {
            $logo = Yii::getAlias('@back') . '/images/logo.png';
        }
        $siteinfo = [
            'logo' => $logo,
            'membership' => $client->membership,
            'mobile_app' => $client->mobile_app,
            'site_offline' => $client->site_offline,
        ];

        return $siteinfo;
    }
    
    
    
    public function passwordResetEmail($email,$username,$resetLink) {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => 'RP05', 'clientID' => $session['currentclientID']])
                ->one();

        $emailSubjectTemp = $emailTemplate->subject;
        $emailSubject = $emailSubjectTemp;
        $emailSubject = str_replace('{name}', $username, $emailSubject);
        
        $emailBodyTemp = $emailTemplate->template;
        $emailBody = $emailBodyTemp;
        $emailBody = str_replace('{name}', $username, $emailBody);
        $emailBody = str_replace('{resetLink}', $resetLink, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['supportEmail'] => 'support'])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
    }

    public function sendEmail($userId, $emailTemplateCode, $data = null, $subject = null)
    {
        $session = Yii::$app->session;
        $session['currentclientID'] = 16;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        $client_programme_title = $client->programme_title;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        
        $emailSubject = $emailTemplate->subject;

        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $verification_email_key = isset($data2['verification_email_key']) ? $data2['verification_email_key'] : null;
        
        $emailBody = $emailTemplate->template;
        
        $emailSubject = str_replace('{name}', $name, $emailSubject);
        
        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{client_name}', $client_name, $emailBody);
        $emailBody = str_replace('{programme_title}', $client_programme_title, $emailBody);
        $emailBody = str_replace('{verification_email_key}', $verification_email_key, $emailBody);
        
        $emailBody = str_replace('/upload', $siteUrl . "/upload", $emailBody);

        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setBcc($bccemail)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
        
    }
    
    public function sendEmailAdmin($userId, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $session['currentclientID'] = 16;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['clientID' => $session['currentclientID'],'code' => $emailTemplateCode])
                ->one();
        
        $emailSubject = $emailTemplate->subject;

        $profile = \common\models\User::find()->where(['id' => $userId])->one();
        $email = $profile->email;
        
        $UserProfile = \common\models\UserProfile::find()->where(['userID' => $userId])->one();
        $admin_name = $UserProfile->first_name.' '.$UserProfile->last_name; 
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{name}', $admin_name, $emailBody);
        $emailBody = str_replace('{admin_name}', $admin_name, $emailBody);
        $emailBody = str_replace('{admin_url}', $admin_url, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        
        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setBcc($bccemail)
                ->setFrom([$client_email =>  $client_name])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
        }else {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$client_email =>  $client_name])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
        }
    }
    
    
    public function sendEmailOrder($order_id, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $comment ='';
        $order = \common\models\VIPOrder::find()->where(['order_id' => $order_id, 'clientID' => $session['currentclientID']])->one();
        $userId = $order->customer_id;
        $order_date = date('d/m/Y', strtotime($order->created_datetime));
        $order_id = $order->invoice_prefix;
        $ip = $order->ip;
        $status_name = $order->orderStatus->name;
        $comment = $order->comment;
        $telephone = $order->customer->telephone_no;
        $mobile = $order->customer->mobile_no;    
        $payment_address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.'<br>'.$order->customerAddress->country->name;
        $shipping_address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.'<br>'.$order->customerAddress->country->name;
        $tbl = '<table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
	<thead>
		<tr>
                    <td style="border:solid #dddddd 1.0pt;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Product</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Model</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Quantity</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Unit Points</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Total</span></b></p>
                    </td>
                </tr>
	</thead>
	<tbody>';
        
        $total = '';
        foreach ($order->orderProducts as $subproduct) {
            //echo $subproduct->name.'<br>';
            $optlisit = '';
            $OrderOption = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
            if ($OrderOption > 0) {
                $OrderOptionlisits = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                foreach ($OrderOptionlisits as $OrderOptionlisit) {
                        $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                    }
                }
                
                $tbl .= '<tr style="height:1.15pt">
                    <td style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->name . '</span> <span style="font-size:8.0pt">' . $optlisit . ' </span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->model . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . $subproduct->quantity . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point) . 'pts</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</span></p>
                    </td>
                </tr>';
                $total += $subproduct->point_total;
        }
        
        $tbl .= '<tr style="height:1.15pt">
            <td colspan="4" style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt">Total:</span></b><span style="font-size:9.0pt"></span></p>
            </td>
            <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($total) . 'pts</span></p>
            </td>
        </tr>
        </tbody></table>';
        
        $products = $tbl;
        
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $siteUrl2 = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;

        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        $emailSubject = str_replace('{order_id}', $order_id, $emailSubject);
        $emailSubject = str_replace('{status_name}', $status_name, $emailSubject);
        
        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        //$name = isset($data2['name']) ? $data2['name'] : null;
        //$email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $email_comment = isset($data2['email_comment']) ? $data2['email_comment'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        //$emailBody = str_replace('/files/'.$session['currentclientID'].'', $siteUrl2.'/files/'.$session['currentclientID'].'', $emailBody);
        
        //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['accounts/my-account/view', 'id' => $order_id]);
        $resetLink = '<a href="' . Url::to([$siteUrl2.'/accounts/my-account/view', 'id' => $order_id]) . '" title="View Order Browser" class = "btn btn-info btn-sm">View Order</a>';
        $emailBody = str_replace('{order_link}', $resetLink, $emailBody);
        $emailBody = str_replace('{order_id}', $order_id, $emailBody);
        $emailBody = str_replace('{order_date}', $order_date, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{telephone}', $telephone, $emailBody);
        $emailBody = str_replace('{mobile}', $mobile, $emailBody);
        $emailBody = str_replace('{ip}', $ip, $emailBody);
        $emailBody = str_replace('{status_name}', $status_name, $emailBody);
        $emailBody = str_replace('{comment}', $comment, $emailBody);
        $emailBody = str_replace('{payment_address}', $payment_address, $emailBody);
        $emailBody = str_replace('{shipping_address}', $shipping_address, $emailBody);
        $emailBody = str_replace('{products}', $products, $emailBody);
        $emailBody = str_replace('{email_comment}', $email_comment, $emailBody);
        
        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setBcc($bccemail)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
        
    }
    
    public function sendEmailNewOrder($order_id, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $comment ='';
        $order = \common\models\VIPOrder::find()->where(['order_id' => $order_id, 'clientID' => $session['currentclientID']])->one();
        $userId = $order->customer_id;
        $order_date = date('d/m/Y', strtotime($order->created_datetime));
        $order_id = $order->invoice_prefix;
        $ip = $order->ip;
        $status_name = $order->orderStatus->name;
        $comment = $order->comment;
        $telephone = $order->customer->telephone_no;
        $mobile = $order->customer->mobile_no;    
        $payment_address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_2.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.'<br>'.$order->customerAddress->country->name;
        $shipping_address = $order->shipping_firstname.' '.$order->shipping_lastname.'<br>'.$order->shipping_address_1.','.$order->shipping_address_2.'<br>'.$order->shipping_city.' - '.$order->shipping_postcode.'<br>'.$order->customerAddress->states->state_name.','.$order->customerAddress->country->name;
        $tbl = '<table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
	<thead>
		<tr>
                    <td style="border:solid #dddddd 1.0pt;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Product</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Model</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Quantity</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Unit Points</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Total</span></b></p>
                    </td>
                </tr>
	</thead>
	<tbody>';
        
        $total = '';
        foreach ($order->orderProducts as $subproduct) {
            //echo $subproduct->name.'<br>';
            $optlisit = '';
            $OrderOption = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
            if ($OrderOption > 0) {
                $OrderOptionlisits = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                foreach ($OrderOptionlisits as $OrderOptionlisit) {
                        $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                    }
                }
                
                $tbl .= '<tr style="height:1.15pt">
                    <td style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->name . '</span> <span style="font-size:8.0pt">' . $optlisit . ' </span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->model . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . $subproduct->quantity . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point + $subproduct->shipping_point) . 'pts</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point_total + + $subproduct->shipping_point_total) . 'pts</span></p>
                    </td>
                </tr>';
                $total += $subproduct->point_total + $subproduct->shipping_point_total;
        }
        
        $tbl .= '<tr style="height:1.15pt">
            <td colspan="4" style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt">Total:</span></b><span style="font-size:9.0pt"></span></p>
            </td>
            <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($total) . 'pts</span></p>
            </td>
        </tr>
        </tbody></table>';
        
        $products = $tbl;
        
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $siteUrl2 = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        $emailSubject = str_replace('{order_id}', $order_id, $emailSubject);
        
        
        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        $code = $profile->clients_ref_no;
        
        $company = \common\models\CompanyInformation::find()->where(['user_id' => $userId])->one();
        if(count($company) > 0) {
            $company_name = $company->company_name;
        }else {
            $company_name = 'N/A';
        }
        
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('/files/'.$session['currentclientID'].'', $siteUrl2.'/files/'.$session['currentclientID'].'', $emailBody);
        
        //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['accounts/my-account/view', 'id' => $order_id]);
        $resetLink = '<a href="' . Url::to([$siteUrl2.'/accounts/my-account/view', 'id' => $order_id]) . '" title="View Order Browser" class = "btn btn-info btn-sm">View Order</a>';
        $emailBody = str_replace('{order_link}', $resetLink, $emailBody);
        $emailBody = str_replace('{order_id}', $order_id, $emailBody);
        $emailBody = str_replace('{order_date}', $order_date, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{telephone}', $telephone, $emailBody);
        $emailBody = str_replace('{mobile}', $mobile, $emailBody);
        $emailBody = str_replace('{ip}', $ip, $emailBody);
        $emailBody = str_replace('{status_name}', $status_name, $emailBody);
        $emailBody = str_replace('{comment}', $comment, $emailBody);
        $emailBody = str_replace('{payment_address}', $payment_address, $emailBody);
        $emailBody = str_replace('{shipping_address}', $shipping_address, $emailBody);
        $emailBody = str_replace('{products}', $products, $emailBody);
        $emailBody = str_replace('{dealer_code}', $code, $emailBody);
        $emailBody = str_replace('{company_name}', $company_name, $emailBody);
        
        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            //->setBcc($clientemail)
            ->setCc([$client_email =>  $client_name.'-'.$order_id])
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            //->setCc(['sarkuna@businessboosters.com.my' =>  $client_name.'-'.$order_id])
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();;
        }
    }
    
    /*public function sendEmail($userId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();

        $customer = \common\models\VIPCustomer::find()
                ->where(['userID' => $userId])
                ->one();
        
        $client_name = $customer->full_name;

        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        $name = isset($data2['name']) ? $data2['name'] : null;
        $email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $verification_email_key = isset($data2['verification_email_key']) ? $data2['verification_email_key'] : null;

        if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
            $emailSubject = str_replace('{name}', $name, $emailSubject);
        }
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{verification_email_key}', $verification_email_key, $emailBody);
        $emailBody = str_replace('{client_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($emailTo)
            ->setFrom([Yii::$app->params['supportEmail'] =>  ''])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
    }*/
    
    /*function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }*/
    
    public static function myPoint($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value, msrp')
           ->where(['productID' => $productID, 'clientID' => $clientID])
           //->andWhere(['not', ['p_type' => null]])     
           ->one();
        
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
        }
        
        $msrp = $assignproduct->msrp;
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue / 100 * $msrp;
            }else if($assignproduct->p_type == 'R') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue;
            }
        }else {
            $perpice =  $pervalue / 100 * $msrp;
        }

        $totalprice = $msrp + $perpice;

        $productvalue = $totalprice / $point_value;
        
        return round($productvalue);
    }
    
    public static function myPrice($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();

        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value, msrp')
           ->where(['productID' => $productID, 'clientID' => $clientID])
           //->andWhere(['not', ['p_type' => null]])     
           ->one();
        
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
        }
        
        $msrp = $assignproduct->msrp;
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue / 100 * $msrp;
            }else if($assignproduct->p_type == 'R') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue;
            }
        }else {
            $perpice =  $pervalue / 100 * $msrp;
        }
        

        $totalprice = $msrp + $perpice;

        //$productvalue = round($totalprice) / $point_value;
        
        return $totalprice;
    }
    
    public static function markUp($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value, msrp')
           ->where(['productID' => $productID, 'clientID' => $clientID])   
           ->one();
        
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
        }
        
        $msrp = $assignproduct->msrp;
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue / 100 * $msrp;
            }else if($assignproduct->p_type == 'R') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue;
            }
        }else {
            $perpice =  $pervalue / 100 * $msrp;
        }

        $totalprice = $perpice;
        
        return $totalprice;
    }
    
    public static function markUpType($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value')
           ->where(['productID' => $productID, 'clientID' => $clientID])
           //->andWhere(['not', ['p_type' => null]])     
           ->one();
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $perpice =  $assignproduct->p_value.'%';
            }else if($assignproduct->p_type == 'R') {
                $perpice =  'RM'.$assignproduct->p_value;
            }
        }else {
            $perpice =  $client->bb_percent.'%';
        }
        return $perpice;
    }
    
    public static function clientSetup()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
            $pervalue = '0';
        }
        
        return ['point_value' => $point_value, 'pervalue' => $pervalue];
    }
    
    public static function clientPrefix()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $prefix = \common\models\ClientOption::find()->where(['clientID' => $session['currentclientID']])->one();
        if(count($prefix) > 0) {
            $myprefix = $prefix->invoice_prefix;
        }else {
            $myprefix = 'NA';
        }
        
        return $myprefix;
    }
    
    public function importProductUpload() {
        $errorMessage = "";
        $products = \common\models\ProductExcelUpload::find()
            ->where(['status' => 'P'])
            ->orderBy('id ASC')
            ->all();
        $manufacturer_id = 0;
        $category_id = 0;
        $sub_category_id = 0;
        
        if(count($products) > 0) {
            foreach($products as $product) {
                $product_code = $product->product_code;
                
                if(!empty($product->manufacturer)){
                    $manufacturer = \common\models\VIPManufacturer::find()->where(['name' => $product->manufacturer])->one();
                    if(count($manufacturer) == 0) {
                        $manufacturer = new \common\models\VIPManufacturer();
                        $manufacturer->name = $product->manufacturer;
                        $manufacturer->sort_order = 0;
                        $manufacturer->save(false);
                    }
                    $manufacturer_id = $manufacturer->manufacturer_id;
                }
                
                if(!empty($product->category)){
                    $category = \common\models\VIPCategories::find()->where(['name' => $product->category])->one();
                    if(count($category) == 0) {
                        $category = new \common\models\VIPCategories();
                        $category->name = $product->category;
                        $category->slug = $product->category;
                        $category->save(false);
                    }
                    $category_id = $category->vip_categories_id;
                }
                
                if(!empty($product->sub_category)){
                    $subcategory = \common\models\VIPSubCategories::find()->where(['parent_id' => $category_id, 'name' => $product->sub_category])->one();
                    if(count($subcategory) == 0) {
                        $subcategory = new \common\models\VIPSubCategories();
                        $subcategory->parent_id = $category_id;
                        $subcategory->name = $product->sub_category;
                        $subcategory->slug = $product->sub_category;
                        $subcategory->save(false);
                    }
                    $sub_category_id = $subcategory->vip_sub_categories_id;
                }

                $checkproduct = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->count();
                if($checkproduct == 0) {
                    $model = new \common\models\VIPProduct();
                    $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
                    $msg = 'Created';
                }else {
                    $model = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->one();
                    \common\models\VIPProductDeliveryZone::deleteAll(['product_id' => $model->product_id]);
                    $msg = 'Updated';                    
                }
                
                if(!empty($product->product_name)) {
                    $pname = strtolower($product->product_name);
                    $pname = str_replace(' ', '_', $pname);
                }else {
                    $pname = '';
                }

                $model->manufacturer_id = $manufacturer_id ;
                $model->category_id = $category_id;
                $model->sub_category_id = $sub_category_id;
                $model->product_name = $product->product_name;
                $model->meta_tag_title = $pname;
                $model->links = $product->links;
                $model->product_code = $product->product_code;
                $model->suppliers_product_code = $product->sp_code;
                $model->variant = $product->variant;
                $model->msrp = $product->msrp;
                $model->price = $product->partner_price;
                $model->stock_status_id = 7;
                $model->quantity = 1000;
                
                
                if($model->save(false)) { 
                    $deliverys = \yii\helpers\Json::decode($product->delivery);
                    if(count($deliverys) > 0) {
                        $data = array();
                        foreach($deliverys as $key=>$delivery){
                            $data[] = [$model->product_id,$key,$delivery];
                        }
                        Yii::$app->db
                            ->createCommand()
                            ->batchInsert('vip_product_delivery_zone', ['product_id','zone_id', 'price'],$data)
                            ->execute();
                    }
                    
                    $productUpload = \common\models\ProductExcelUpload::find()
                            ->where(["product_code" => $product_code])
                            ->one();
                    $productUpload->status = "S";
                    $productUpload->save();
                    $errorMessage = $errorMessage . "Product has been (" . $product_code . ") ".$msg."<BR>";
                }else {
                    $errorMessage = $errorMessage . "Product not add(" . $product_code . ")<BR>";
                }    
            }
            
        }
        return $errorMessage;
    }
    
    public function importAssignProduct($clientID) {
        $errorMessage = "";
        $products = \common\models\AssignProductExcelUpload::find()
            ->where(['status' => 'P', 'clientID' => $clientID])
            ->orderBy('id ASC')
            ->all();

        
        if(count($products) > 0) {
            foreach($products as $product) {
                $product_code = $product->product_code;
                $checkproduct = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->count();
                if($checkproduct > 0) {
                    $globalproduct = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->one();
                    $product_id = $globalproduct->product_id;
                    $checkassignproduct = \common\models\VIPAssignProducts::find()->where(['productID' => $product_id, 'clientID' => $clientID])->count();
                    if($checkassignproduct == 0) {
                        $model = new \common\models\VIPAssignProducts();
                        $model->clientID = $product->clientID;
                        $model->productID = $product_id;
                        $model->date_assign = date('Y-m-d');
                        $model->msrp = $product->msrp;
                        $model->partner_price = $product->partner_price;
                        $model->featured_status = $product->featured_status;
                        //$model->save();
                        $msg = 'New';
                    }else {
                        $model = \common\models\VIPAssignProducts::find()->where(['productID' => $product_id, 'clientID' => $clientID])->one();
                        $model->date_assign_update = date('Y-m-d');
                        $model->msrp = $product->msrp;
                        $model->partner_price = $product->partner_price;
                        $model->featured_status = $product->featured_status;
                        //$model->save();
                        \common\models\VIPProductDeliveryZone::deleteAll(['product_id' => $model->productID, 'clientID' => $model->clientID]);
                        $msg = 'Updated'; 
                    }
                    
                }else {
                    $msg = 'No Product code found!'; 
                }

                
                if($model->save(false)) { 
                    $deliverys = \yii\helpers\Json::decode($product->delivery);
                    if(count($deliverys) > 0) {
                        $data = array();
                        foreach($deliverys as $key=>$delivery){
                            $data[] = [$model->productID,$model->clientID,$key,$delivery];
                        }
                        Yii::$app->db
                            ->createCommand()
                            ->batchInsert('vip_product_delivery_zone', ['product_id','clientID','zone_id','price'],$data)
                            ->execute();
                    }
                    
                    $productUpload = \common\models\AssignProductExcelUpload::find()
                            ->where(["product_code" => $product_code])
                            ->one();
                    $productUpload->status = "S";
                    $productUpload->save();
                    $errorMessage = $errorMessage . "Product has been (" . $product_code . ") ".$msg."<BR>";
                }else {
                    $errorMessage = $errorMessage . "Product not add(" . $product_code . ")<BR>";
                }    
            }
            
        }
        return $errorMessage;
    }
    
    public function importCustomer($id) {
        //$session = Yii::$app->session;
        $clientID = 16;

        $errorMessage = "";
        $remark = '';
        $customers = \common\models\ImportCustomerExcel::find()
            ->where(['import_customer_upload_summary_id' => $id, 'status' => 'P'])
            ->orderBy('import_customer_excel_id ASC')
            ->all();

        
        if(count($customers) > 0) {
            foreach($customers as $customer) {
                $updatecustomers = \common\models\ImportCustomerExcel::find()
                ->where(['import_customer_excel_id' => $customer->import_customer_excel_id])
                ->one();
                
                $query = \common\models\VIPCustomer::find()
                ->joinWith('user')        
                ->select('vip_customer.vip_customer_id,vip_customer.clientID,vip_customer.clients_ref_no, user.email')        
                ->where(['vip_customer.clientID' => $clientID])     
                ->andWhere(['or',['vip_customer.clients_ref_no' => $customer->user_code],['user.email' => $customer->email]])       
                ->all();
                
                if(!empty($customer->full_name)) {
                    $full_name = $customer->full_name;
                }else {
                    $full_name = '';
                }

                if(!empty($customer->mobile)) {
                    $mobile_no = $customer->mobile;
                }else {
                    $mobile_no = '';
                }
                
                $random_str = md5(uniqid(rand()));
                $generatedPassword = substr($random_str, 0, 6);
   
                if(count($query) == 0) {
                    $user = new \common\models\User();
                    $user->client_id = $clientID;
                    $user->username = trim($customer->user_code);
                    $user->email = $customer->email;
                    $user->status = 'A';
                    $user->approved = 'Y';
                    $user->newsletter = 'Y';
                    if ($customer->account_type == 7){
                       $user->user_type = 'B'; 
                    }else{
                        $user->user_type = 'D';
                    }
                    $user->type = $customer->account_type;
                    $user->email_verification = 'Y';

                    $user->setPassword($generatedPassword);
                    $user->generateAuthKey();
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $user->save(false)) {
                            if ($flag) {
                                $model = new \common\models\VIPCustomer();
                                $model->userID = $user->id;
                                $model->clientID = $clientID;
                                $model->dealer_ref_no = $clientID . '-' . date('dmyHis');
                                $model->clients_ref_no = $customer->user_code;
                                $model->full_name = $full_name;
                                $model->mobile_no = $mobile_no;
                                $model->salutation_id = 1;
                                if(!empty($customer->distributor_code)){
                                    $dealers = \common\models\UserProfile::find()
                                        ->where(['dealer_code' => $customer->distributor_code])
                                        ->one();
                                    $model->assign_dealer_id = $dealers->userID;  
                                }

                                if ($flag = $model->save(false)) {
                                    $companyInformation = new \common\models\CompanyInformation();
                                    $companyInformation->user_id = $model->userID;
                                    $companyInformation->company_name = $customer->company_name;
                                    $companyInformation->mailing_address = $customer->company_address;
                                    if (($flag = $companyInformation->save(false)) === false) {
                                        $transaction->rollBack();
                                        //break;
                                    }
                                }
                                
                                $authassignment = new \common\models\AuthAssignment();
                                $authassignment->item_name = 'Dealer';
                                $authassignment->user_id = $user->id;
                                $authassignment->created_at = strtotime(date('Y-m-d H:i:s'));
                                $authassignment->save();
                                
                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            //\Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'You have modified customer!']);
                            $data = \yii\helpers\Json::encode(array(
                                        'password' => $generatedPassword,
                            ));
                            $remark = 'Profile Create';
                            $status = 'S';
                            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                            $errorMessage = $errorMessage . "New user has been (" . $customer->user_code . ") created<BR>";
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }else {
                    $editcustomers = \common\models\VIPCustomer::find()
                        ->where(['clients_ref_no' => $customer->user_code])
                        ->count();
                    if($editcustomers > 0){
                        $editcustomers = \common\models\VIPCustomer::find()
                        ->where(['clients_ref_no' => $customer->user_code])
                        ->one();

                        $editcustomers->full_name = $full_name;
                        $editcustomers->mobile_no = $mobile_no;
                        $editcustomers->save(false);
                        
                        $updateemail = \common\models\user::find()
                        ->where(['email' => $customer->email])
                        ->count();
                        
                        if($updateemail == 0) {
                            $user = \common\models\User::find()
                            ->where(['id' => $editcustomers->userID])
                            ->one();

                            $user->email = $customer->email;
                            $user->status = 'A';
                            $user->approved = 'Y';
                            $user->email_verification = 'Y';
                            $user->setPassword($generatedPassword);
                            $user->generateAuthKey();
                            $user->save(false);
                            
                            $data = \yii\helpers\Json::encode(array(
                                'password' => $generatedPassword,
                            ));
                            $returnedValue = Yii::$app->VIPglobal->sendEmail($user->id, Yii::$app->params['email.template.code.registration'], $data);
                            
                        }
                        
                        $companyInformation = \common\models\CompanyInformation::find()
                            ->where(['user_id' => $editcustomers->userID])
                            ->one();
                        $companyInformation->company_name = $customer->company_name;
                        $companyInformation->mailing_address = $customer->company_address;
                        $companyInformation->save(false);
                        $remark = 'Profile Update';
                        $status = 'U';
                    }
                }                
                $updatecustomers->status = $status;
                $updatecustomers->remark = $remark;
                $updatecustomers->save(false);
                $errorMessage = $errorMessage . "User has been (" . $customer->user_code . ") updated<BR>";
            }
            
        }
        return $errorMessage;
    }
   
    public function importPoints($id) {
        $session = Yii::$app->session;
        $clientID = 16;
        
        $errorMessage = "";
        $remark = '';
        
        $mainlist = \common\models\PointUploadSummary::find()      
            ->where(['clientID'=>$clientID])
            ->andWhere('point_upload_summary_id = :point_upload_summary_id', [':point_upload_summary_id' => $id]) 
            ->one();
        
        $points = \common\models\PointUploadSummaryReport::find()
            ->where(['point_upload_summary_id' => $id, 'status' => 'N'])
            ->orderBy('point_upload_summary_id ASC')
            ->all();
        
        if(count($points) > 0) {
            foreach($points as $userpoint) {
                $pointsupdate = \common\models\PointUploadSummaryReport::findOne($userpoint->id);
                $clients_ref_no = $userpoint->dealer_id;
                $countUsers = \common\models\VIPCustomer::find()    
                    ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no])               
                    ->count();
                if ($countUsers == 1) {
                    $userlisit = \common\models\VIPCustomer::find()     
                        ->where(['clientID'=>$clientID, 'clients_ref_no' => $clients_ref_no]) 
                        ->one();
                    
                        $smsTemplate = \common\models\SMSTemplate::find()
                            ->where(['code' => 'MPAA01'])
                            ->one();
                            
                        if(!empty($userlisit->mobile_no)) {
                            $jsonData = \yii\helpers\Json::encode(array(
                                'name' => $userlisit->full_name,
                                'point' => $userpoint->points,
                                'effective_month' => date('M/y', strtotime($mainlist->effective_month)),
                                'expired_date' => date('d/m/y', strtotime($mainlist->expiry_date)),
                            ));

                            $smsQueue = new \common\models\SMSQueue();
                            $smsQueue->sms_template_id = $smsTemplate->id;
                            $smsQueue->sms_type = 'pts_upload';
                            $smsQueue->clientID = $clientID;
                            $smsQueue->user_id = $userlisit->userID;
                            $smsQueue->mobile = $userlisit->mobile_no;
                            $smsQueue->data = $jsonData;
                            $smsQueue->status = 'N';
                            $smsQueue->save(false);
                        }

                        $pointawerd = new \common\models\VIPCustomerReward();
                        $pointawerd->clientID = $clientID;
                        $pointawerd->customer_id = $userlisit->userID;
                        $pointawerd->point_upload_summary_id = $id;
                        $pointawerd->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                        $pointawerd->description = $mainlist->description;
                        $pointawerd->points = $userpoint->points;
                        $pointawerd->bb_type = 'A';
                        $pointawerd->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                        $pointawerd->save(false);
                        
                        
                        
                        if($userpoint->points2 > 0){
                            $accountpoint2 = new \common\models\VIPCustomerAccount2();
                            $accountpoint2->clientID = $clientID;
                            $accountpoint2->customer_id = $pointawerd->customer_id;
                            $accountpoint2->indirect_id = 0;
                            $accountpoint2->bb_type = 'A';
                            $accountpoint2->expiry_date = date('Y-m-d', strtotime($mainlist->expiry_date));
                            $accountpoint2->description = $mainlist->description;
                            $accountpoint2->points_in = $userpoint->points2;
                            $accountpoint2->date_added = date('Y-m-d', strtotime($mainlist->effective_month));
                            $accountpoint2->save(false);
                            
                            $smsTemplate = \common\models\SMSTemplate::find()
                            ->where(['code' => 'MPAA02'])
                            ->one();
                            
                            if(!empty($userlisit->mobile_no)) {
                                $jsonData = \yii\helpers\Json::encode(array(
                                    'name' => $userlisit->full_name,
                                    'point' => $userpoint->points2,
                                    'effective_month' => date('M/y', strtotime($mainlist->effective_month)),
                                    'expired_date' => date('d/m/y', strtotime($mainlist->expiry_date)),
                                ));

                                $smsQueue = new \common\models\SMSQueue();
                                $smsQueue->sms_template_id = $smsTemplate->id;
                                $smsQueue->sms_type = 'pts_upload';
                                $smsQueue->clientID = $clientID;
                                $smsQueue->user_id = $userlisit->userID;
                                $smsQueue->mobile = $userlisit->mobile_no;
                                $smsQueue->data = $jsonData;
                                $smsQueue->status = 'N';
                                $smsQueue->save(false);
                            }
                        }
                        
                        $remark = 'Point Awarded';
                        $status = 'S';
                        $errorMessage = $errorMessage . "Point Awarded to (" . $clients_ref_no . ")<BR>";
                }else {
                    $remark = 'Dealer ID do not exist';
                    $status = 'F';
                    $errorMessage = $errorMessage . "Dealer ID do not exist<BR>";
                }

                $pointsupdate->status_remark = $remark;
                $pointsupdate->status = $status;
                $pointsupdate->save(false);
            }
        }
        
        return $errorMessage;
    }
    
    public function myAvailablePoint(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         
        
        $adjustment = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'B'])
        ->sum('points');
        
        $totaladjustment = str_replace('-', '', $adjustment);
        if(empty($totalminus)){
            $totaladjustment = 0; 
         }
         
        $balance = $totaladd - $totalminus - $expired;
        $totalbalance = $balance - $totaladjustment;
        
        return $totalbalance;
    }
    
    
    
    public function Terms() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = \common\models\Client::findOne($clientID);
        if(!empty($model->terms)) {
            $terms = $model->terms;
        }else {
            $terms = '';
        }
        return $terms;

    }


    public function customersAvailablePoint($userid){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         
        $balance = $totaladd - $totalminus - $expired;
        
        return $balance;
    }
    
    public function PointsExpire($userid){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $balnce = \common\models\BalanceCarryforward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
        ->count();
        
        if($balnce > 0) {
            $balnce = \common\models\BalanceCarryforward::find()
            ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
            ->orderBy([
                'balance_bf_year' => SORT_DESC,
            ])
            ->one();
            $lastbalnce = $balnce->balance_points;
        }else {
            $lastbalnce = 0;
        }
        
        
        $y2B = $lastbalnce + $y2V;
        
        if($y2B > 0) {
            $y2B = $y2B;
        }else {
           $y2B = 0; 
        }

         
        return $y2B;
    }
    
    public function PointsExpireUser(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $userid = Yii::$app->user->id;
        
        $y2V = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', '2019-04-05 00:00:00'])
        ->andWhere(['<=', 'date_added', '2019-12-31 23:59:59'])        
        ->sum('points');
        
        $balnce = \common\models\BalanceCarryforward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
        ->count();
        
        if($balnce > 0) {
            $balnce = \common\models\BalanceCarryforward::find()
            ->where(['clientID'=>$clientID, 'customer_id'=>$userid])       
            ->orderBy([
                'balance_bf_year' => SORT_DESC,
            ])
            ->one();
            $lastbalnce = $balnce->balance_points;
        }else {
            $lastbalnce = 0;
        }
        
        
        $y2B = $lastbalnce + $y2V;
        
        if($y2B > 0) {
            $y2B = $y2B;
        }else {
           $y2B = 0; 
        }

         
        return $y2B;
    }

    public function sendSMS($userId, $smsTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        $client_programme_title = $client->programme_title;
        
        if(!empty($client->sms_username)) {
            $username = $client->sms_username;
            $password = $client->sms_password;
        }else {
            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
        }
        
        $smsTemplate = \common\models\SMSTemplate::find()
                ->where(['code' => $smsTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        


        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $mobile_no = $profile->mobile_no;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $point = isset($data2['point']) ? $data2['point'] : null;
        $invoice_no = isset($data2['invoice_no']) ? $data2['invoice_no'] : null;
        $effective_month = isset($data2['effective_month']) ? $data2['effective_month'] : null;
        $expired_point = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['expired_point']) ? $data2['expired_point'] : null;
        $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
        $expired_date = isset($data2['expired_date']) ? $data2['expired_date'] : null;
        $today_date = isset($data2['today_date']) ? $data2['today_date'] : null;
        
        $smsBody = $smsTemplate->template;
        $smsBody = str_replace('{name}', $name, $smsBody);
        $smsBody = str_replace('{point}', $point, $smsBody);
        $smsBody = str_replace('{invoice}', $invoice_no, $smsBody);
        $smsBody = str_replace('{effective_month}', $effective_month, $smsBody);
        $smsBody = str_replace('{expired_point}', $expired_point, $smsBody);
        $smsBody = str_replace('{balance_point}', $balance_point, $smsBody);
        $smsBody = str_replace('{expired_date}', $expired_date, $smsBody);
        $smsBody = str_replace('{today_date}', $today_date, $smsBody);
        //$smsBody = str_replace('{client_name}', $client_name, $smsBody);        
        
        $message = $smsBody;

        $destination = $mobile_no;        
        $sender_id = urlencode("66300");    
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);
        $type = '1';
        $agreedterm = 'YES';

        $fp = "https://www.isms.com.my/isms_send_all.php";
        $fp .= "?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&agreedterm=$agreedterm&sendid=$sender_id";
        //$result = Yii::$app->VIPglobal->ismscURL($fp);
        
        $http = curl_init($fp);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;        
    }
    
    public function sendSMStest($mobile) {
        //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        $emailBody = "test";
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);

        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';

        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";

        //print_r($link);
        //die();
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        print_r($http_result);
        die();
        //return $http_result;
    }

    public static function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    public static function addtolog($status = null, $message = null, $uID = 0)
    {
        \common\models\ActionLog::add($status, $message, $uID);
    }
}
